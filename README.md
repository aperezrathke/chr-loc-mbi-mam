# Chromatin **m**any-**b**ody **i**nteractions for **mam**malian cells #

Application of [CHROMATIX](https://doi.org/10.1186/s13059-019-1904-z) for detection of **m**any-**b**ody **i**nteractions in the **mam**malian GM12878 cell line.

If using this software, please cite:

* Perez-Rathke, Alan, Qiu Sun, Boshen Wang, Valentina Boeva, Zhifeng Shao, and Jie Liang. "CHROMATIX: computing the functional landscape of many-body chromatin interactions in transcriptionally active loci from deconvolved single cells." Genome Biology 21, no. 1 (2020): 1-17. [https://doi.org/10.1186/s13059-019-1904-z](https://doi.org/10.1186/s13059-019-1904-z).

See also additional **CHROMATIX** software:

* [CHR-FOLDER](https://bitbucket.org/aperezrathke/chr-folder/) - 3-D chromatin folder using fractal Monte Carlo
* [CMX](https://bitbucket.org/aperezrathke/cmx/) - Markov chain Monte Carlo Bayesian sampler for deconvolving population Hi-C into single-cell contact states

## *Workflow* ##

We provide a summary of the workflow used for [analyzing GM12878 cells](https://doi.org/10.1186/s13059-019-1904-z). Analysis was primarily done under a Linux environment.

![workflow](./workflow.png)

### **Environment setup** ###

#### Script languages ####

Scripts were developed using [R](https://www.r-project.org/), [python 2.7x](https://www.python.org/), and [BASH](https://tldp.org/HOWTO/Bash-Prog-Intro-HOWTO.html) scripting languages.

#### R packages ####

A non-exhaustive list of R packages utilized includes:

* [apcluster](https://cran.r-project.org/web/packages/apcluster/) - For coarse-graining Hi-C specific contacts
* [caret](https://cran.r-project.org/web/packages/caret/) - For cross-validation of random forest models
* [igraph](https://cran.r-project.org/web/packages/igraph/) - For chromatin many-body clique detection
* [optparse](https://cran.r-project.org/web/packages/optparse/) - For parsing command-line arguments in R
* [randomForest](https://cran.r-project.org/web/packages/randomForest/) - For building random forest machine learning models
* [reticulate](https://cran.r-project.org/web/packages/reticulate/) - For calling Python code from within R scripts
* [ROCR](https://cran.r-project.org/web/packages/ROCR/) - For plotting ROC curves from cross-validated random forest models

Note, many R analysis scripts may require the *scriptName* package which, depending on R version, may not be available on CRAN. If this is the case, please:

1. Download an archived tar.gz of the *scriptName* package [here](https://cran.r-project.org/src/contrib/Archive/scriptName/)
1. Open an R terminal (or RStudio) and install the downloaded tar.gz using command:

```R
install.packages("<path_to_package.tar.gz>", repos = NULL, type="source")
```

See also stack overflow discussion on [installing a local R package](https://stackoverflow.com/questions/13940688/installing-package-from-a-local-tar-gz-file-on-linux).

Alternatively, you can install an R package directly from the CRAN URL. For example:

```R
install.packages("https://cran.r-project.org/src/contrib/Archive/scriptName/scriptName_1.0.0.tar.gz", repos=NULL, method="libcurl")
```

This will install the archived version of the *scriptName* package.

#### Directory structure ####

We assume a working directory `[WorkDir]` which is freely named by the user. However, for high performance computing (HPC) scripts running on the [UIC Extreme HPC cluster](https://acer.uic.edu/services/computation/hpc/extreme/), the `[WorkDir]` path is assumed to be `~/chr-working`.

Via command line, navigate to `[WorkDir]` and `git clone` the [CHR-FOLDER](https://bitbucket.org/aperezrathke/chr-folder/) and [CMX](https://bitbucket.org/aperezrathke/cmx/) projects within this directory:

* `git clone https://bitbucket.org/aperezrathke/chr-folder.git folder`
    * This will copy the source code for the 3-D chromatin folder into the `[WorkDir]/folder` subdirectory.
* `git clone https://bitbucket.org/aperezrathke/cmx.git`
    * This will copy the source code for the Bayesian Hi-C single-cell deconvolver into the `[WorkDir]/cmx` subdirectory.

Next, still from within `[WorkDir]`, clone the **MBI-MAM** project:

* `git clone https://bitbucket.org/aperezrathke/chr-loc-mbi-mam.git ./loci/mbi-mam`
    * This will copy the scripts for many-body analysis of the GM12878 cell line into the `[WorkDir]/loci/mbi-mam` subdirectory.

We will refer to the local path `[WorkDir]/loci/mbi-mam` as `[RepoDir]`.

##### Output directory #####

Finally, within `[RepoDir]`, many scripts will generate output data to the directory `[RepoDir]/gm12878/fractal2d.hic5k.tad2mb/output`. This folder is generated automatically when needed. **All script output is stored within this directory.**

### **Running background scripts** ###

We assume that users are familiar with running background scripts in linux. However, here is a brief [article](https://en.wikipedia.org/wiki/Nohup) on this process. Essentially, for a command-line:

```bash
nohup <path_to_script> [arguments...] &> <path_to_log> &
```

* `nohup` is used to allow the script to continue running even if the user logs out of the linux terminal
* `<path_to_script>` is the path to the script to be run
* `[arguments...]` are any user command-line arguments to pass to the script
* `&> <path_to_log>` redirects all console output to the log file located at `<path_to_log>`
* `&` at the end of the command is used to run the script in the background; this allows multiple scripts to be running in parallel in the background


Unless baking or running an interactive script, all scripts are assumed to be running in the background. If unsure, most scripts will have comments near the top of the file indicating intended usage. See also this [guide](https://www.tldp.org/LDP/abs/html/x9644.html) for how to manage background processes in linux.

### **Baking** ###

The concept of *baking* is explained in detail [here](https://bitbucket.org/aperezrathke/chr-folder/src/master/tutorial/A/tutorial.md). The baking process is necessary to configure and then compile the 3-D chromatin folder.

Navigate to the GM12878 [common](./gm12878/fractal2d.hic5kb.tad2mb/common) subdirectory at `[RepoDir]/gm12878/fractal2d.hic5kb.tad2mb/common`.

From within a command line terminal, run the [bake.py](/gm12878/fractal2d.hic5kb.tad2mb/common/bake.py) script. This will work for most standard linux environments such as Ubuntu or CentOs.

For examples of alternate platform bake scripts, as in module-based high-performance computing (HPC) environments, please refer to:

* [alcf.theta/bake.py](./gm12878/fractal2d.hic5kb.tad2mb/common/alcf.theta/bake.py) - Script for baking on the [Argonne Leadership Computing Facility](https://www.alcf.anl.gov/) theta cluster
* [extreme/bake.py](./gm12878/fractal2d.hic5kb.tad2mb/common/extreme/bake.py) - Script for baking with the intel compiler on the [UIC Extreme HPC cluster](https://acer.uic.edu/services/computation/hpc/extreme/)

### **Null distribution** ###

The next step is to generate a uniform random chromatin 3-D polymer ensemble. This 3-D polymer ensemble will be repeatedly bootstrapped to generate a **null distribution** over chromatin contact frequencies. Using this bootstrapped distribution, we can then call *specific* (i.e. significant or not well explained through uniform random polymer folding) chromatin contacts within the Hi-C.

1. Navigate to the GM12878 [null](./gm12878/fractal2d.hic5kb.tad2mb/null) subdirectory at `[RepoDir]/gm12878/fractal2d.hic5kb.tad2mb/null`.
1. Run the script [null_folder_run.py](./gm12878/fractal2d.hic5kb.tad2mb/null/null_folder_run.py). This will generate a large ensemble of uniform random 3-D chromatin polymers.
1. Run the script [null_blb_run_parallel.sh](./gm12878/fractal2d.hic5kb.tad2mb/null/null_blb_run_parallel.sh) to generate the bootstrap null distribution over chromatin contact frequencies. Note, this script simply spawns multiple instances of the [null_blb_run.py](./gm12878/fractal2d.hic5kb.tad2mb/null/null_blb_run.py) script to generate multiple [Bag of Little Bootstraps (BLB)](https://doi.org/10.1111/rssb.12050) replicates.
1. Run the script [null_cache_run.sh](./gm12878/fractal2d.hic5kb.tad2mb/null/null_cache_run.sh). This will convert the output from the previous steps into RData format usable for R analysis.
1. Optionally, run the script [null_analysis_run.sh](./gm12878/fractal2d.hic5kb.tad2mb/null/null_analysis_run.sh) to generate heat maps of the null ensemble.

### **Calling specific contacts** ###

We can now use the bootstrap null distribution to call specific contacts within the Hi-C data. However, first we must identify the TAD regions of interest and pull down the corresponding Hi-C data from [Rao et al](https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE63525).

All scripts will be located in the the GM12878 [common](./gm12878/fractal2d.hic5kb.tad2mb/common) subdirectory at `[RepoDir]/gm12878/fractal2d.hic5kb.tad2mb/common`.

#### Identify and format TADs ####

1. Run the script [tad_filter.py](./gm12878/fractal2d.hic5kb.tad2mb/common/tad_filter.py). Here we process summarized [SPRITE](https://doi.org/10.1016/j.cell.2018.05.024) data to determine minimally overlapping TAD regions containing super-enhancers.
1. Run the script [tad_scale.py](./gm12878/fractal2d.hic5kb.tad2mb/common/tad_scale.py). This script will generate the master table of TAD regions to be modeled in this study.
1. Run the script [tad_juicer.py](./gm12878/fractal2d.hic5kb.tad2mb/common/tad_juicer.py). This will process the previously generated master table of TADs and download the corresponding Hi-C data using [Juicer](https://github.com/aidenlab/juicer/).

#### Clean TADs ####

Now that we have downloaded the Hi-C and generated the null distribution, we can now call the specific contacts within the Hi-C. We call this process *cleaning* the data - i.e. retaining only the specific contacts for subsequent downstream analysis.

* Run the script [tad_clean_run.sh](./gm12878/fractal2d.hic5kb.tad2mb/common/tad_clean_run.sh). This will call specific contacts at each TAD listed within the master table of TAD regions. It will also generate corresponding heat maps.

### **Coarse-graining specific contacts** ###

We can now coarse-grain the specific contacts. We facilitate coarse-graining by clustering with the [affinity propagation](https://doi.org/10.1126%2Fscience.1136800) algorithm. Coarse-graining has the benefits of *i)* reducing the downstream computational cost as well as *ii)* helping to regularize the model.

* Run the script [tad_clust_run.sh](./gm12878/fractal2d.hic5kb.tad2mb/common/tad_clust_run.sh). This will cluster (i.e. coarse-grain) the specific contacts at each TAD listed within the master table of TAD regions. It will also generate corresponding heat maps of the coarse-grained specific contacts.

### **Biophysical perturbations** ###

We can now perform biophysical [perturbations](https://bitbucket.org/aperezrathke/chr-folder/src/master/tutorial/E/tutorial.md) over the coarse-grained specific contacts. Specifically, we perform *i)* single knock-in perturbations and *ii)* infeasible geometry perturbations. These generated biophysical data sets will be used to inform a sparsity-inducing [prior distribution](https://en.wikipedia.org/wiki/Prior_probability) for Bayesian deconvolution of the population Hi-C into single-cell contact states.

All perturbation scripts are located within the GM12878 [common](./gm12878/fractal2d.hic5kb.tad2mb/common) subdirectory at `[RepoDir]/gm12878/fractal2d.hic5kb.tad2mb/common`.

#### High-performance computing ####

We recommend that perturbations should be multiplexed over a distributed high-performance computing environment such as [TORQUE](https://github.com/adaptivecomputing/torque) or [SLURM](https://slurm.schedmd.com/documentation.html). For this project, perturbations were performed on the [University of Illinois at Chicago (UIC) Extreme HPC cluster](https://acer.uic.edu/services/computation/hpc/extreme/) which supports [TORQUE](https://github.com/adaptivecomputing/torque). However, we will provide links to the following scripts:

* The **multiplexer** script - The script that calls TORQUE `qsub` commands to distribute jobs over the HPC cluster. The multiplexer script is aware of the HPC environment and is therefore **not portable** to a different HPC environment.
* The **kernel** script - Each HPC job will ultimately call a kernel script which runs locally on a single compute node to generate the desired data. The kernel script is *not aware* of the HPC environment and therefore **is portable** across different HPC environments such as TORQUE or SLURM.

Therefore, if using a different HPC environment such as SLURM, this mostly entails writing a new set of *multiplexer* scripts, as the *kernel* scripts are fairly portable across computing environments.

Note, for HPC scripts running on [UIC Extreme HPC cluster](https://acer.uic.edu/services/computation/hpc/extreme/), the `[WorkDir]` path is assumed to be `~/chr-working`.

#### Single knock-in perturbations ####

We assess possible dependencies among the coarse-grained specific contacts through single knock-in perturbations. Essentially, for each coarse-grained specific contact, we generate a new 3-D chromatin ensemble such that the respective contact is knocked-in (i.e. forced to occur). Later, we will compare each perturbed ensemble to the null distribution to assess if other specific contacts are significantly affected by the respective perturbations.

1. Run the script [tad_perturb_setup.py](./gm12878/fractal2d.hic5kb.tad2mb/common/extreme/tad_perturb_setup.py). This script will aggregate coarse-grained specific contacts across all modeled TAD regions into a single master list to avoid performing redundant knock-in perturbations.
1. Run the multiplexer script [extreme/tad_perturb.queue.sh](./gm12878/fractal2d.hic5kb.tad2mb/common/extreme/tad_perturb.queue.sh). This script will multiplex the kernel script [tad_perturb.py](./gm12878/fractal2d.hic5kb.tad2mb/common/tad_perturb.py) over each compute node.

#### Infeasible geometry perturbations ####

Here, we generate random knock-in combinations among the coarse-grained specific contacts to assess if such combinations are geometrically-embeddable in 3-D Euclidean space. If a knock-in combination is geometrically-embeddable, we say that it is a **feasible** combination. In contrast, if the knock-in combination is not geometrically-embeddable, we say that it is **infeasible**. To determine feasible versus infeasible labels, we use a simple heuristic: if our chromatin folder can successfully fold the requested knock-in combination within a threshold number of attempts, then we say the combination is feasible, else it is infeasible. We will generate a corpus of all encountered infeasible combinations for use in our Bayesian prior for deconvolving single-cell contact states from population Hi-C.

* Run the multiplexer script [tad_infea.queue.sh](./gm12878/fractal2d.hic5kb.tad2mb/common/extreme/tad_infea.queue.sh). This script will multiplex the kernel script [tad_infea.py](./gm12878/fractal2d.hic5kb.tad2mb/common/tad_infea.py) over each compute node.

### **Bayesian deconvolution of single-cell contact states** ###

We are now ready to probabilistically deconvolve the population Hi-C into single-cell contact states. This is done by Markov chain Monte Carlo (MCMC) sampling of the Bayesian posterior as described in the supplementary information of [Perez-Rathke et al](https://doi.org/10.1186/s13059-019-1904-z). Our C++ MCMC sampler, [CMX](https://bitbucket.org/aperezrathke/cmx/), should already be cloned into the `[WorkDir]/cmx` subdirectory.

#### Building CMX ####

CMX build (i.e. compilation) scripts are located within the CMX [build](https://bitbucket.org/aperezrathke/cmx/src/master/script/template/build/) subdirectory at `[WorkDir]/cmx/script/template/build`. Navigate to this build scripts directory.

From within a command line terminal, run the [linux/compile.gnu.threaded.sh](https://bitbucket.org/aperezrathke/cmx/src/master/script/template/build/linux/compile.gnu.threaded.sh) script. Next, run [linux/compile.gnu.sh](https://bitbucket.org/aperezrathke/cmx/src/master/script/template/build/linux/compile.gnu.sh). These build scripts will work for most standard linux environments such as Ubuntu or CentOs.

For examples of alternate platform build scripts, as in module-based high-performance computing (HPC) environments, please refer to:

* [alcf.theta/compile.intel.threaded.sh](https://bitbucket.org/aperezrathke/cmx/src/master/script/template/build/alcf.theta/compile.intel.threaded.sh) - Script for compiling CMX with the intel compiler on the [Argonne Leadership Computing Facility](https://www.alcf.anl.gov/) theta cluster. See also [alcf.theta/compile.intel.sh](https://bitbucket.org/aperezrathke/cmx/src/master/script/template/build/alcf.theta/compile.intel.sh) for serial CMX build.
* [extreme/compile.login2.intel.threaded.sh](https://bitbucket.org/aperezrathke/cmx/src/master/script/template/build/extreme/compile.login2.intel.threaded.sh) - Script for compiling CMX with the intel compiler on the [UIC Extreme HPC cluster](https://acer.uic.edu/services/computation/hpc/extreme/). See also [extreme/compile.login2.intel.sh](https://bitbucket.org/aperezrathke/cmx/src/master/script/template/build/extreme/compile.login2.intel.sh) for serial CMX build.

Finally, run the script [copy_bin.sh](https://bitbucket.org/aperezrathke/cmx/src/master/script/template/build/copy_bin.sh) to copy the compiled CMX binaries to location expected by subsequent scripts.

#### Bayesian deconvolution ####

All deconvolution scripts are located within the GM12878 [common](./gm12878/fractal2d.hic5kb.tad2mb/common) subdirectory at `[RepoDir]/gm12878/fractal2d.hic5kb.tad2mb/common`.

1. Run the script [tad_cmx_setup.sh](./gm12878/fractal2d.hic5kb.tad2mb/common/tad_cmx_setup.sh). Primarily, this script will determine p-values based on the single knock-in perturbation ensembles in order to generate edge (i.e. dependency) restrictions among the coarse-grained specific contacts.
    * Note, the script [extreme/tad_cmx_setup.run.sh](./gm12878/fractal2d.hic5kb.tad2mb/common/extreme/tad_cmx_setup.run.sh) may be qsub'd (e.g. `qsub [RepoDir]/gm12878/fractal2d.hic5kb.tad2mb/common/extreme/tad_cmx_setup.run.sh`) to instead setup CMX under a TORQUE HPC environment. This script avoids running [tad_cmx_setup.sh](./gm12878/fractal2d.hic5kb.tad2mb/common/tad_cmx_setup.sh) on the login (i.e. head) node by running it on a compute node instead!
1. Run the multiplexer script [extreme/tad_cmx.queue.sh](./gm12878/fractal2d.hic5kb.tad2mb/common/extreme/tad_cmx.queue.sh). This script will multiplex the kernel script [tad_cmx.py](./gm12878/fractal2d.hic5kb.tad2mb/common/tad_cmx.py) over each compute node.
1. Optionally, run script [tad_cmx.residual.sh](./gm12878/fractal2d.hic5kb.tad2mb/common/extreme/tad_cmx.residual.sh). Intended for TAD loci with no identified dependencies among coarse-grained specific contacts, this script will instruct [tad_cmx.py](./gm12878/fractal2d.hic5kb.tad2mb/common/tad_cmx.py) to generate single-cell contact states according to a naive (i.e. all contacts independent) model.

### **3-D folding of single-cell contact states** ###

We can now 3-D fold the [MAP estimated](https://en.wikipedia.org/wiki/Maximum_a_posteriori_estimation) single-cell contact states, the output of our MCMC sampling, at each modeled TAD locus.

All single-cell 3-D folding scripts are located within the GM12878 [common](./gm12878/fractal2d.hic5kb.tad2mb/common) subdirectory at `[RepoDir]/gm12878/fractal2d.hic5kb.tad2mb/common`.

1. Run the multiplexer script [extreme/tad_b2p.queue.sh](./gm12878/fractal2d.hic5kb.tad2mb/common/extreme/tad_b2p.queue.sh). This script will multiplex the kernel script [tad_bit2pol.py](./gm12878/fractal2d.hic5kb.tad2mb/common/tad_bit2pol.py) over each compute node.
1. Run the multiplexer script [extreme/tad_b2p_post.queue.sh](./gm12878/fractal2d.hic5kb.tad2mb/common/extreme/tad_b2p_post.queue.sh). This script will multiplex the kernel script [tad_bit2pol_post.sh](./gm12878/fractal2d.hic5kb.tad2mb/common/tad_bit2pol_post.sh) over each compute node. This kernel script primarily defers to the utility script [tad_bit2pol_post.R](./gm12878/fractal2d.hic5kb.tad2mb/common/tad_bit2pol_post.R) to cache files into RData format and to generate heat maps of the 3-D folded single-cell contacts (e.g. **simulated Hi-C** heat maps).
1. Optionally, the script [tad_bit2pol_heat.num_se.sh](./gm12878/fractal2d.hic5kb.tad2mb/common/tad_bit2pol_heat.num_se.sh) shows how to generate heat maps such that the empirical Hi-C is on the upper triangle and the simulated Hi-C is on the lower triangle.

### **Calling specific many-body interactions** ###

We can now inspect the 3-D folded single-cell contact states to identify specific many-body (>2) interactions. The process is conceptually similar to how pairwise chromatin contacts are called specific.

A many-body clique is a set of fully-interacting chromatin regions, i.e. all pairs of chromatin regions within the set are spatially within an 80 nm Euclidean distance. A **principle loop** is the chromatin loop with longest genomic distance (in base pairs) among all pairs within a many-body clique. A *maximal* many-body clique is a many-body clique such that no other chromatin region may be added to the set and still remain fully interacting. For this project, all chromatin regions are of genomic length 5 kb. Please refer to [Perez-Rathke et al](https://doi.org/10.1186/s13059-019-1904-z) for illustrations of many-body cliques and their principle loops.

All specific many-body calling scripts are located within the GM12878 [common](./gm12878/fractal2d.hic5kb.tad2mb/common) subdirectory at `[RepoDir]/gm12878/fractal2d.hic5kb.tad2mb/common`.

1. Run the multiplexer script [extreme/tad_mbi.queue.sh](./gm12878/fractal2d.hic5kb.tad2mb/common/extreme/tad_mbi.queue.sh). This script will multiplex the kernel script [tad_mbi.sh](./gm12878/fractal2d.hic5kb.tad2mb/common/tad_mbi.sh) over each compute node. The kernel script will direct the utility script [tad_mbi.R](./gm12878/fractal2d.hic5kb.tad2mb/common/tad_mbi.R) to identify *specific principle loops* of *maximal* many-body interactions.
1. See the script [tad_mbi_3b.sh](./gm12878/fractal2d.hic5kb.tad2mb/common/extreme/tad_mbi.queue.sh) for example of how to direct the utility script [tad_mbi.R](./gm12878/fractal2d.hic5kb.tad2mb/common/tad_mbi.R) to identify *specific 3-body* chromatin interactions. Note, these 3-body interactions are *not* restricted to be maximal.

### **Predicting chromatin regions enriched in specific principle loops** ###

We now provide scripts for training a random forest machine learning model to predict 5 kb chromatin regions which are enriched in anchors of specific principle loops. Note, the principle loops are tabulated exclusively from *maximal* many-body cliques.

All machine learning prediction scripts for specific principle loop anchors are located within the GM12878 [common](./gm12878/fractal2d.hic5kb.tad2mb/common) subdirectory at `[RepoDir]/gm12878/fractal2d.hic5kb.tad2mb/common`.

1. Run the script [tad_bio_wget.sh](./gm12878/fractal2d.hic5kb.tad2mb/common/tad_bio_wget.sh). This script will download all [ENCODE](https://www.encodeproject.org/) bigWig data sets used to generate input features.
1. Run the script [tad_bio_bigWigToBedGraph.sh](./gm12878/fractal2d.hic5kb.tad2mb/common/tad_bio_bigWigToBedGraph.sh). This script will use the [bigWigToBedGraph](https://genome.ucsc.edu/goldenpath/help/bigWig.html) utility to convert each bigWig file into bedGraph format only at the TAD loci modeled in this study.
1. Run the script [tad_bio_binBedGraph.sh](./gm12878/fractal2d.hic5kb.tad2mb/common/tad_bio_binBedGraph.sh). This script will summarize the ENCODE signal data into a single value at each 5 kb bin. The summarized values will ultimately be standardized and serve as the input features into the machine learning model.
1. Run the script [tad_bio_binMbiCount.prin_loop.sh](./gm12878/fractal2d.hic5kb.tad2mb/common/tad_bio_binMbiCount.prin_loop.sh). This script will count principle loop participation at each 5 kb bin. Ultimately, these counts will serve to generate *enriched* versus *not-enriched* labels at each 5 kb bin.

Now that the have generated the raw input features and data necessary for assigning class labels, we can interactively train and cross-validate a random forest model in an R environment.

* Within an interactive R environment (e.g. console or [RStudio](https://rstudio.com/)), `source` the script [tad_bio_ml.R](./gm12878/fractal2d.hic5kb.tad2mb/common/tad_bio_ml.R).
    * To plot a [receiver operating characteristic (ROC) curve](https://en.wikipedia.org/wiki/Receiver_operating_characteristic) on held-out test folds, call the method `plot_bio_rf_roc()`
    * To plot [random forest variable importance](https://en.wikipedia.org/wiki/Random_forest#Variable_importance), call the method `plot_bio_rf_varImp()`

## **License** ##

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see [https://www.gnu.org/licenses/](https://www.gnu.org/licenses/).
