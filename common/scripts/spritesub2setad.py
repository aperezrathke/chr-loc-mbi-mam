#!/usr/bin/python
# -*- coding: utf-8 -*-

# Script parses SPRITE clusters file and retains only intra-chromosomal
# sub-clusters with at least 3 reads and overlapping an SE region. Further,
# at least 3 reads must be separated by a minimum base pair threshold to
# help mitigate genomic distance bias. It is recommended to run the script
# 'sprite.keep.se.py' before running this script.
#
# Reference:
#
#   Quinodoz, Sofia A., Noah Ollikainen, Barbara Tabak, Ali Palla, Jan Marten
#   Schmidt, Elizabeth Detmar, Mason M. Lai et al. "Higher-Order Inter-
#   chromosomal Hubs Shape 3D Genome Organization in the Nucleus." Cell (2018).
#
# SPRITE cluster data can be obtained from GEO: GSE114242
#
# For SPRITE processing protocol, refer to Guttman lab wiki:
#   https://github.com/GuttmanLab/sprite-pipeline/wiki/4.-Clustering

###########################################
# Imports
###########################################

# For parsing user supplied arguments
import argparse

# For file path testing
import os

# For testing range overlap
import numpy as np

# For parsing SE regions
import pandas as pd

# For flushing stdout
import sys

# For mapping reads to a chromosome identifier
from collections import defaultdict

# Sub-cluster record
from collections import namedtuple

# Sub-cluster
# .id_spr - SPRITE cluster identifier (non-unique!)
# .chr - chromosome identifier
# .bp_start_tad - bounding TADs start base pair
# .bp_end_tad - bounding TADs end base pair (padded)
# .bp_span_tad - bounding TADs base pair span (padded)
# .bp_start_spr - start SPRITE read
# .bp_end_spr - end SPRITE read (unpadded)
# .bp_span_spr - SPRITE base pair span (unpadded)
# .n_read_spr_all - number of SPRITE reads (intra + inter)
# .n_read_spr_intra - number of w/in chrome SPRITE reads
# .n_se - Number of associated super-enhancers
# .is_single_tad - 1 if single TAD spans SPRITE sub-cluster, 0 o/w
# .id_rec = line record identifier from originating SPRITE file

SUBCLUST_COLS = ["id_spr", "chr", "bp_start_tad", "bp_end_tad", "bp_span_tad",
                 "bp_start_spr", "bp_end_spr", "bp_span_spr", "n_read_spr_all",
                 "n_read_spr_intra", "n_se", "is_single_tad", "id_rec"]
                 
Subclust = namedtuple('Subclust', SUBCLUST_COLS)

###########################################
# Globals
###########################################

# Path to directory containing this script
# https://stackoverflow.com/questions/4934806/how-can-i-find-scripts-directory-with-python
SCRIPT_DIR = os.path.abspath(os.path.dirname(os.path.realpath(__file__)))

# Default cell line identifier
DEF_CELL_ID = "gm12878"

# Default SPRITE MAPQ threshold, options are 10 or 30
DEF_MAPQ_THRESH = 10

# Default TAD directory
DEF_TAD_IN_DIR = os.path.join(SCRIPT_DIR, "..", "arrow")

# Default TAD file name
DEF_TAD_IN_FILE = str(DEF_CELL_ID) + ".txt"

# Default path to TAD data
DEF_TAD_IN_PATH = os.path.join(DEF_TAD_IN_DIR, DEF_TAD_IN_FILE)

# Default base data directory
DEF_DATA_DIR = os.path.join(SCRIPT_DIR, "..", "output")

# Default super-enhancer directory
DEF_SE_IN_DIR = os.path.join(DEF_DATA_DIR, "se2tad.csv")

# Default super-enhancer file name
DEF_SE_IN_FILE = str(DEF_CELL_ID) + ".csv"

# Default super-enhancer file path
DEF_SE_IN_PATH = os.path.join(DEF_SE_IN_DIR, DEF_SE_IN_FILE)

# Default SPRITE input directory
DEF_SPRITE_IN_DIR = os.path.join(DEF_DATA_DIR, "sprite.keep.se")

# Default SPRITE input clusters file name
DEF_SPRITE_IN_FILE = str(DEF_CELL_ID) + ".mapq" + str(DEF_MAPQ_THRESH) + \
                       ".cluster"

# Default SPRITE input path
DEF_SPRITE_IN_PATH = os.path.join(DEF_SPRITE_IN_DIR, DEF_SPRITE_IN_FILE)

# Default number of base pairs assumed to be spanned by a SPRITE read
DEF_SPRITE_BP = 5000

# Default number of base pairs spanned by a Hi-C bin
DEF_HIC_BIN_BP = 10000

# Default threshold separation in base pairs among intra-chrome SPRITE
# clusters (to help control for genomic distance bias)
DEF_GD_THRESH_BP = DEF_HIC_BIN_BP * 4

# Default SPRITE output directory
DEF_SPRITE_OUT_DIR = os.path.join(DEF_DATA_DIR, "spritesub2setad.csv")

# Default SPRITE output clusters file name
DEF_SPRITE_OUT_FILE = '.'.join([DEF_SPRITE_IN_FILE,
                                'spr'+str(DEF_SPRITE_BP/1000),
                                'hic'+str(DEF_HIC_BIN_BP/1000),
                                'gd' + str(DEF_GD_THRESH_BP/1000), "csv"])

# Default SPRITE output clusters file path
DEF_SPRITE_OUT_PATH = os.path.join(DEF_SPRITE_OUT_DIR, DEF_SPRITE_OUT_FILE)

###########################################
# Utils
###########################################

# Recursively creates build directory such that all parent directories are
# created if necessary
# http://stackoverflow.com/questions/273192/in-python-check-if-a-directory-exists-and-create-it-if-necessary
def make_dir(d):
    '''Utility for making a directory if not existing.'''
    if not os.path.exists(d):
        print "Creating directory: " + str(d)
        os.makedirs(d)

def load_se(fin_se):
    '''Return super-enhancer regions data.frame'''
    print "Loading: " + str(fin_se)
    df = pd.read_csv(filepath_or_buffer=fin_se, header=0, index_col=False,
                     usecols=['chr', 'bp_start', 'bp_end'])
    return df

def load_tad(fin_tad, tad_pad = DEF_HIC_BIN_BP):
    '''Return TAD regions data.frame'''
    df = pd.read_csv(filepath_or_buffer=fin_tad, header=0, index_col=False,
                     delim_whitespace=True,
                     usecols=['chr1', 'x1', 'x2', 'chr2', 'y1', 'y2'])
    # Keep only intra-chromosomal TADs
    is_intra = (df.chr1 == df.chr2)
    is_intra = is_intra & (df.x1 == df.y1)
    is_intra = is_intra & (df.x2 == df.y2)
    df = df.loc[is_intra, ["chr1", "x1", "x2"]]
    # Convert chromosome identifiers to 'chr' prefix
    df.chr1 = map(lambda x : 'chr' + str(x), df.chr1)
    # Pad end by Hi-C bin resolution
    df.x2 = df.x2 + tad_pad
    return df

###########################################
# spritesub2setad
###########################################

# @param fin_tad - Input TAD regions
# @param fin_se - Input super-enhancer regions
# @param fin_sprite - Input path to SPRITE clusters
# @param fout_sprite - Output path to filtered SPRITE clusters
# @param sprite_bp - Assumed base pair span of sprite read
# @param hic_bin_bp - Hi-C bin resolution in base pairs
# @param gd_thresh_bp - At least 3 reads must be separated by at least this
#   base pair distance for sub-cluster to be retained 
# @param heart - heartbeat interval
def spritesub2setad(fin_tad = DEF_TAD_IN_PATH, fin_se = DEF_SE_IN_PATH,
                    fin_sprite = DEF_SPRITE_IN_PATH,
                    fout_sprite = DEF_SPRITE_OUT_PATH,
                    sprite_bp = DEF_SPRITE_BP,
                    hic_bin_bp = DEF_HIC_BIN_BP,
                    gd_thresh_bp = DEF_GD_THRESH_BP,
                    heart=50):
    '''Retain SPRITE clusters with >2 intra-chrome reads and overlapping SE'''
    IX_CHR = 0
    IX_BP = 1
    tups = []
    chr_reads = defaultdict(list)
    cluster_id = 0
    subcluster_keep = 0
    subcluster_unbounded = 0
    se = load_se(fin_se=fin_se)
    tad = load_tad(fin_tad=fin_tad, tad_pad=hic_bin_bp)
    with open(fin_sprite, "r") as fin:
        make_dir(os.path.dirname(fout_sprite))
        with open(fout_sprite, "w") as fout:
            # Write column header
            fout.write('id_se' + ',')
            fout.write(','.join(SUBCLUST_COLS) + '\n')
            # Process input cluster lines
            for line in fin:
                # Update cluster identifier
                cluster_id = cluster_id + 1
                # Check heartbeat
                if (cluster_id % heart) == 0:
                    print "Processing cluster (id, sub-keep, sub-unbounded): (" + \
                          str(cluster_id) + ", " + str(subcluster_keep) + \
                          ", " + str(subcluster_unbounded) + ")"
                    sys.stdout.flush()
                # Split by whitespace and strip SPRITE cluster ID
                tups = line.split()
                id_spr = tups[0]
                tups = tups[1:]
                # Get mapping from chr to reads
                chr_reads = defaultdict(list)
                for tup_str in tups:
                    tup = tup_str.split(":")
                    chr_ = tup[IX_CHR]
                    bp = tup[IX_BP]
                    chr_reads[chr_].append(bp)
                # Determine SE overlaps
                for chr_ in chr_reads:
                    # Skip chr if not enough reads
                    reads = chr_reads[chr_]
                    if len(reads) <= 2:
                        continue
                    ##########################################################
                    # Keep SEs on same chromosome
                    se_chr = se.loc[se["chr"] == chr_]
                    if len(se_chr.index) < 1:
                        # No SEs on chromosome
                        continue
                    # Check if a SPRITE read overlaps an SE region
                    se_ids = set([])
                    reads = np.array(map(int, reads))
                    bp_start_spr = np.min(reads)
                    bp_end_spr = np.max(reads)
                    for read_bp_start in reads:
                        read_bp_end = read_bp_start + sprite_bp                        
                        # https://stackoverflow.com/questions/3269434/whats-the-most-efficient-way-to-test-two-integer-ranges-for-overlap
                        has_se_overlap = (se_chr.bp_start <= read_bp_end) & \
                                         (se_chr.bp_end >= read_bp_start)
                        if np.any(has_se_overlap):
                            se_ov = se_chr.loc[has_se_overlap]
                            se_min_bp = np.min(se_ov.bp_start)
                            bp_start_spr = min(bp_start_spr, se_min_bp)
                            se_max_bp = np.max(se_ov.bp_end)
                            bp_end_spr = max(bp_end_spr, se_max_bp)
                            se_ids.update(has_se_overlap.index[has_se_overlap])
                    if len(se_ids) < 1:
                        # Skip chromosome, as no associated SE
                        continue
                    ##########################################################
                    # Determine if many-body exceeds genomic distance thresh
                    gd_okay = False
                    for i in xrange(len(reads)-2):
                        read_i = reads[i]
                        for j in xrange(i+1, len(reads)-1):
                            read_j = reads[j]
                            if abs(read_i - read_j) < gd_thresh_bp:
                                continue
                            for k in xrange(j+1, len(reads)):
                                read_k = reads[k]
                                if abs(read_i - read_k) < gd_thresh_bp:
                                    continue
                                if abs(read_j - read_k) < gd_thresh_bp:
                                    continue
                                # We found a many-body triplet!
                                gd_okay = True
                                break
                            if gd_okay:
                                break
                        if gd_okay:
                            break
                    if not gd_okay:
                        # Genomic distance threshold not satisfied, process
                        # next chromosome
                        continue
                    ##########################################################
                    # Determine bounding TADs
                    # Keed TADs on same chromosome
                    tad_chr = tad.loc[tad["chr1"] == chr_]
                    if len(tad_chr.index) < 1:
                        # No TADs found on this chromosome!
                        continue
                    is_lte_start = tad_chr.x1 <= bp_start_spr
                    is_gte_end = tad_chr.x2 >= bp_end_spr
                    is_within_tad = is_lte_start & is_gte_end
                    is_single_tad = 0
                    bp_start_tad = -1
                    bp_end_tad = -1
                    bp_span_tad = -1
                    if np.any(is_within_tad):
                        # Sub-cluster is contained within single TAD, find
                        # smallest bounding TAD
                        tad_chr_sub = tad_chr.loc[is_within_tad]
                        tad_chr_sub.reset_index(drop=True, inplace=True)
                        spans = np.array(tad_chr_sub.x2 - tad_chr_sub.x1)
                        ix_min = np.argmin(spans)
                        is_single_tad = 1
                        bp_start_tad = tad_chr_sub.at[ix_min, "x1"]
                        bp_end_tad = tad_chr_sub.at[ix_min, "x2"]
                        bp_span_tad = spans[ix_min]          
                    elif np.any(is_lte_start) and np.any(is_gte_end):
                        # Sub-cluster is not contained within single TAD, see
                        # if can bound with multiple TADs
                        ix_bp_start = np.where(is_lte_start)[0]
                        ix_bp_end = np.where(is_gte_end)[0]
                        # Multiplex (start, end)
                        ix_bp_tup = np.array([(x, y) for x in ix_bp_start \
                                                     for y in ix_bp_end])
                        ix_bp_tup_start = ix_bp_tup[:,0]
                        ix_bp_tup_end = ix_bp_tup[:,1]
                        bp_start = np.minimum(tad_chr.iloc[ix_bp_tup_start].x1,
                                              tad_chr.iloc[ix_bp_tup_end].x1)
                        bp_start = np.array(bp_start)
                        bp_end = np.maximum(tad_chr.iloc[ix_bp_tup_start].x2,
                                            tad_chr.iloc[ix_bp_tup_end].x2)
                        bp_end = np.array(bp_end)
                        spans = bp_end - bp_start
                        ix_min = np.argmin(spans)
                        is_single_tad = 0 
                        bp_start_tad = bp_start[ix_min]
                        bp_end_tad = bp_end[ix_min]
                        bp_span_tad = spans[ix_min]
                    ##########################################################
                    # Export bounded TADs
                    if bp_span_tad > 0:
                        subclust = Subclust(id_spr=id_spr, chr=chr_,
                                            bp_start_tad=bp_start_tad,
                                            bp_end_tad=bp_end_tad,
                                            bp_span_tad=bp_span_tad,
                                            bp_start_spr=bp_start_spr,
                                            bp_end_spr=bp_end_spr,
                                            bp_span_spr= \
                                                (bp_end_spr-bp_start_spr),
                                            n_read_spr_all=len(tups),
                                            n_read_spr_intra=len(reads),
                                            n_se=len(se_ids),
                                            is_single_tad=is_single_tad,
                                            id_rec=cluster_id)
                        line_suffix = ''
                        for col in SUBCLUST_COLS:
                            line_suffix = line_suffix + ',' + str(getattr(subclust, col))
                        line_suffix = line_suffix + '\n'
                        # Replicate SPRITE sub-cluster TAD record for each SE
                        for se_id in se_ids:
                            fout.write(str(se_id) + line_suffix)
                        # Update retained subcluster count
                        subcluster_keep = subcluster_keep + 1
                    else:
                        subcluster_unbounded = subcluster_unbounded + 1
                        print "Warning: no TADs bound SPRITE record " + \
                               id_spr + ":" + str(cluster_id) + ":" + chr_

###########################################
# Main
###########################################

# Main script entry point
def __main__():
    print "======================== spritesub2setad ========================"
    # Initialize command line parser
    parser = argparse.ArgumentParser()
    # Optional argument(s)
    parser.add_argument('-tad', '--ftad', default=DEF_TAD_IN_PATH,
                        help='Path to input TAD regions')
    parser.add_argument('-se', '--fse', default=DEF_SE_IN_PATH,
                        help='Path to input super-enhancer regions')
    parser.add_argument('-i', '--fin', default=DEF_SPRITE_IN_PATH,
                        help='Path to input SPRITE clusters')
    parser.add_argument('-o', '--fout', default=DEF_SPRITE_OUT_PATH,
                        help='Path to output processed SPRITE clusters')
    parser.add_argument('-bp', '--sprite_bp', default=DEF_SPRITE_BP,
                        help='Assumed length of SPRITE read')
    parser.add_argument('-hic', '--hic_bin_bp', default=DEF_HIC_BIN_BP,
                        help='Hi-C bin resolution in base pairs')
    parser.add_argument('-gd', '--gd_thresh_bp', default=DEF_GD_THRESH_BP,
                        help='Genomic distance threshold in base pairs')
    # Parse command line
    args = parser.parse_args()
    # Print command line
    print '\t[--ftad] = ' + str(args.ftad)
    print '\t[--fse] = ' + str(args.fse)
    print '\t[--fin] = ' + str(args.fin)
    print '\t[--fout] = ' + str(args.fout)
    print '\t[--sprite_bp] = ' + str(args.sprite_bp)
    print '\t[--hic_bin_bp] = ' + str(args.hic_bin_bp)
    print '\t[--gd_thresh_bp] = ' + str(args.gd_thresh_bp)

    # Feed to utility
    spritesub2setad(fin_tad = args.ftad, fin_se = args.fse,
                    fin_sprite = args.fin, fout_sprite = args.fout,
                    sprite_bp = args.sprite_bp, hic_bin_bp = args.hic_bin_bp,
                    gd_thresh_bp = args.gd_thresh_bp)
    print "Finished."

# Run main if we are the active script
if __name__ == '__main__':
    __main__()
