#!/bin/bash 

# Retrieve centromere base pair regions at each chromosome

# Based on Biostars discussion:
#
# "How Can I Get The Human Chromosome Centromere Position And Chromosome
#  Length In Grch37/Hg19"
#
# https://www.biostars.org/p/2349/

# Directory containing this script
SCRIPT_PATH="$(readlink -f $0)"
SCRIPT_DIR="$(dirname $SCRIPT_PATH)"
SCRIPT_DIR=$(cd "$SCRIPT_DIR"; pwd)

# Set output directory
OUTPUT_DIR="$SCRIPT_DIR/../output/meta"
mkdir -p "$OUTPUT_DIR"
OUTPUT_PATH="$OUTPUT_DIR/centromere.txt"

curl -s "http://hgdownload.cse.ucsc.edu/goldenPath/hg19/database/cytoBand.txt.gz" | gunzip -c | grep acen > "$OUTPUT_PATH"

echo "Finished. Please check $OUTPUT_PATH"
