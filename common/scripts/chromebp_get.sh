#!/bin/bash 

# Retrieve centromere base pair regions at each chromosome

# Based on Biostars discussion:
#
# "Easiest Way To Obtain Chromosome Length?"
#
# https://www.biostars.org/p/16396/

# Directory containing this script
SCRIPT_PATH="$(readlink -f $0)"
SCRIPT_DIR="$(dirname $SCRIPT_PATH)"
SCRIPT_DIR=$(cd "$SCRIPT_DIR"; pwd)

# Set output directory
OUTPUT_DIR="$SCRIPT_DIR/../output/meta"
mkdir -p "$OUTPUT_DIR"
OUTPUT_PATH="$OUTPUT_DIR/chromebp.txt"

curl -s "http://hgdownload.cse.ucsc.edu/goldenPath/hg19/database/chromInfo.txt.gz" | gunzip -c | awk '!/_/' > "$OUTPUT_PATH"

echo "Finished. Please check $OUTPUT_PATH"
