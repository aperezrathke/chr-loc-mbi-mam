#!/bin/bash

# Utility script to copy file(s) to remote destination.
# Change SRC and DST to configure what is being copied.

# Copies CSV gzipped geometry coordinates

# Usage:
#   nohup <path_to_script_dir>/<script_name> &> <path_to_log_dir>/<logfile> &
#

# To avoid password prompt, see following tutorials:
# http://ubuntuforums.org/showthread.php?t=238672
# http://troy.jdmz.net/rsync/index.html
#
# Essentially, on local machine, run
#
# ssh-keygen -t rsa
# <hit return three times to create default password-less key>
#
# ssh-copy-id -i ~/.ssh/id_rsa.pub <username@remote_host>
# <enter your password for username on remote_host>
#
# To test password-less login:
# ssh <username@remote_host>
#
# If no password prompt appears, then key login is working.
# @TODO - restrict connections and commands available to key.

SCRIPT_DIR="$(dirname "$(readlink -f "$0")")"
PROJ_DIR="$SCRIPT_DIR/../../.."
PROJ_DIR=$(cd "$PROJ_DIR"; pwd)
PROJ_NAME=`basename "$PROJ_DIR"`

# http://qdosmsq.dunbar-it.co.uk/blog/2013/02/rsync-to-slash-or-not-to-slash/
# Trailing slash means to copy source contents
TARGET="/"
SRC="$PROJ_DIR/$TARGET"
DST="perezrat@lianglabfile.bioe.uic.edu:~/$PROJ_NAME-mirror-slim/$TARGET"
echo "mirroring $SRC to $DST"

# NOTE: If $DST is local, then remove '-e ssh' and change '~' to '$HOME'

# https://superuser.com/questions/775140/how-can-i-rsync-a-set-of-subdirectories
# The first include rule might not be obvious, but guarantees that all
# directories (note the trailing /) are searched, the second finally adds
# everything in [csv.gz] directories and the exclude rule excludes everything
# else. -m makes sure, that empty directories are not copied.

while [ 1 ]
do
    rsync -e ssh -av -m --include='**/' --include='**/csv.gz/**' --exclude='*' --progress "$SRC" "$DST"
    if [ "$?" = "0" ] ; then
        echo "Rsync success."
        break
    else
        echo "Rsync failure. Sleeping for a bit then retrying ..."
        sleep 180
    fi
done

echo "Mirroring complete."
