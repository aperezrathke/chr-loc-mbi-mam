#!/bin/bash

# Utility script to copy file(s) to remote destination.
# Change SRC and DST to configure what is being copied.

# Currently configured to sync FROM local TO extreme, script is assumed to be
# running on non-extreme machine.

# Usage:
#   nohup <path_to_script_dir>/<script_name> &> <path_to_log_dir>/<logfile> &
#

##############################################################################
# Overview of rsync parameters (not all may be used)
##############################################################################
# -a, --archive
#   This is equivalent to -rlptgoD. It is a quick way of saying you want
#   recursion and want to preserve almost everything (with -H <preserve
#   hardlinks> being a notable omission)
#   -r: copy folders recursively
#   -l: copy symlinks
#   -p: copy permissions
#   -t: preserve modification times (avoids copying duplicate files)
#   -g: preserve group for each file
#   -o: preserve owner for each file
#   -D: for transferring weird special files
#
# --exclude=PATTERN
#   This option is a simplified form of the --filter option that defaults to an
#   exclude rule and does not allow the full rule-parsing syntax of normal filter
#   rules. See the FILTER RULES section for detailed information on this option.
#   (see man rsync for FILTER RULES)
#
# -z, --compress
#   With this option, rsync compresses the file data as it is sent to the
#   destination machine, which reduces the amount of data being transmitted,
#   something that is useful over a slow connection.
#
# -e, --rsh=COMMAND
#   specify the remote shell to use
#
# --delete
#   delete extraneous files from dest dirs <deletes files on dest that do
#   not also exist on src>
#
# --progress
#   show progress during transfer
#
#
# Example: to mirror a directory from local to remote:
#
#   rsync -az -e ssh --delete --progress SRC DEST

# To avoid password prompt, see following tutorials:
# http://ubuntuforums.org/showthread.php?t=238672
# http://troy.jdmz.net/rsync/index.html
#
# Essentially, on local machine, run
#
# ssh-keygen -t rsa
# <hit return three times to create default password-less key>
#
# ssh-copy-id -i ~/.ssh/id_rsa.pub <username@remote_host>
# <enter your password for username on remote_host>
#
# To test password-less login:
# ssh <username@remote_host>
#
# If no password prompt appears, then key login is working.
# @TODO - restrict connections and commands available to key.

SCRIPT_DIR="$(dirname "$(readlink -f "$0")")"
PROJ_DIR="$SCRIPT_DIR/../../.."
PROJ_DIR=$(cd "$PROJ_DIR"; pwd)
PROJ_NAME=`basename "$PROJ_DIR"`

# http://qdosmsq.dunbar-it.co.uk/blog/2013/02/rsync-to-slash-or-not-to-slash/
# Trailing slash means to copy source contents
TARGET="/"
SRC="$PROJ_DIR/$TARGET"
DST="perezrat@login-2.extreme.uic.edu:~/$PROJ_NAME-mirror/$TARGET"
echo "mirroring $SRC to $DST"

while [ 1 ]
do
    rsync -az -e ssh --exclude='.git/' --exclude='CMakeBuild/' --delete --progress "$SRC" "$DST"
    if [ "$?" = "0" ] ; then
        echo "Rsync success."
        break
    else
        echo "Rsync failure. Sleeping for a bit then retrying ..."
        sleep 180
    fi
done

echo "Mirroring complete."
