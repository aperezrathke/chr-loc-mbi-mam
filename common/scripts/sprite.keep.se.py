#!/usr/bin/python
# -*- coding: utf-8 -*-

# Script parses SPRITE clusters file and retains only intra-chromosomal
# sub-clusters with at least 3 reads and overlapping an SE region. It is
# recommended to run 'sprite.keep.intra3.py' before running this script.
#
# Reference:
#
#   Quinodoz, Sofia A., Noah Ollikainen, Barbara Tabak, Ali Palla, Jan Marten
#   Schmidt, Elizabeth Detmar, Mason M. Lai et al. "Higher-Order Inter-
#   chromosomal Hubs Shape 3D Genome Organization in the Nucleus." Cell (2018).
#
# SPRITE cluster data can be obtained from GEO: GSE114242
#
# For SPRITE processing protocol, refer to Guttman lab wiki:
#   https://github.com/GuttmanLab/sprite-pipeline/wiki/4.-Clustering

###########################################
# Imports
###########################################

# For parsing user supplied arguments
import argparse

# For file path testing
import os

# For testing range overlap
import numpy as np

# For parsing SE regions
import pandas as pd

# For mapping reads to a chromosome identifier
from collections import defaultdict

###########################################
# Globals
###########################################

# Path to directory containing this script
# https://stackoverflow.com/questions/4934806/how-can-i-find-scripts-directory-with-python
SCRIPT_DIR = os.path.abspath(os.path.dirname(os.path.realpath(__file__)))

# Default cell line identifier
DEF_CELL_ID = "gm12878"

# Default SPRITE MAPQ threshold, options are 10 or 30
DEF_MAPQ_THRESH = 10

# Default base data directory
DEF_DATA_DIR = os.path.join(SCRIPT_DIR, "..", "output")

# Default super-enhancer directory
DEF_SE_IN_DIR = os.path.join(DEF_DATA_DIR, "se2tad.csv")

# Default super-enhancer file name
DEF_SE_IN_FILE = str(DEF_CELL_ID) + ".csv"

# Default super-enhancer file path
DEF_SE_IN_PATH = os.path.join(DEF_SE_IN_DIR, DEF_SE_IN_FILE)

# Default SPRITE input directory
DEF_SPRITE_IN_DIR = os.path.join(DEF_DATA_DIR, "sprite.keep.intra3")

# Default SPRITE input clusters file name
DEF_SPRITE_IN_FILE = str(DEF_CELL_ID) + ".mapq" + str(DEF_MAPQ_THRESH) + ".cluster"

# Default SPRITE input path
DEF_SPRITE_IN_PATH = os.path.join(DEF_SPRITE_IN_DIR, DEF_SPRITE_IN_FILE)

# Default SPRITE output directory
DEF_SPRITE_OUT_DIR = os.path.join(DEF_DATA_DIR, "sprite.keep.se")

# Default SPRITE output clusters file name
DEF_SPRITE_OUT_FILE = DEF_SPRITE_IN_FILE

# Default SPRITE output clusters file path
DEF_SPRITE_OUT_PATH = os.path.join(DEF_SPRITE_OUT_DIR, DEF_SPRITE_OUT_FILE)

# Number of base pairs assumed to be spanned by a SPRITE read
DEF_SPRITE_BP = 5000

###########################################
# Utils
###########################################

# Recursively creates build directory such that all parent directories are
# created if necessary
# http://stackoverflow.com/questions/273192/in-python-check-if-a-directory-exists-and-create-it-if-necessary
def make_dir(d):
    '''Utility for making a directory if not existing.'''
    if not os.path.exists(d):
        print "Creating directory: " + str(d)
        os.makedirs(d)

def load_se(fin_se):
    '''Return super-enhancer regions data.frame'''
    print "Loading: " + str(fin_se)
    df = pd.read_csv(filepath_or_buffer=fin_se, header=0, index_col=False,
                     usecols=['chr', 'bp_start', 'bp_end'])
    return df

###########################################
# Keep
###########################################

# @param fin_se - Input super-enhancer regions
# @param fin_sprite - Input path to SPRITE clusters
# @param fout_sprite - Output path to filtered SPRITE clusters
# @param sprite_bp - Assumed base pair span of sprite read
# @param heart - heartbeat interval
def keep_se(fin_se = DEF_SE_IN_PATH, fin_sprite = DEF_SPRITE_IN_PATH,
            fout_sprite = DEF_SPRITE_OUT_PATH,
            sprite_bp = DEF_SPRITE_BP, heart=1000):
    '''Retain SPRITE clusters with >2 intra-chrome reads and overlapping SE'''
    IX_CHR = 0
    IX_BP = 1
    tups = []
    chr_reads = defaultdict(list)
    cluster_id = 0
    cluster_keep = 0
    se = load_se(fin_se)
    with open(fin_sprite, "r") as fsprite_pre:
        make_dir(os.path.dirname(fout_sprite))
        with open(fout_sprite, "w") as fsprite_keep:
            for lpre in fsprite_pre:
                # Update cluster identifier
                cluster_id = cluster_id + 1
                # Check heartbeat
                if (cluster_id % heart) == 0:
                    print "Processing cluster (id, keep): (" + \
                          str(cluster_id) + ", " + str(cluster_keep) + ")"
                # Split by whitespace and strip cluster ID
                tups = lpre.split()[1:]
                # Get mapping from chr to reads
                chr_reads = defaultdict(list)
                for tup_str in tups:
                    tup = tup_str.split(":")
                    chr_ = tup[IX_CHR]
                    bp = tup[IX_BP]
                    chr_reads[chr_].append(bp)
                # Determine if SE overlaps
                has_se_overlap = False
                for chr_ in chr_reads:
                    # Skip chr if not enough reads
                    reads = chr_reads[chr_]
                    if len(reads) <= 2:
                        continue
                    # Keep SEs on same chromosome
                    se_chr = se.loc[se["chr"] == chr_]
                    if len(se_chr.index) < 1:
                        # No SEs on chromosome
                        continue
                    # Check if a SPRITE read overlaps an SE region
                    for read_bp_start_str in reads:
                        read_bp_start = int(read_bp_start_str)
                        read_bp_end = read_bp_start + sprite_bp                        
                        # https://stackoverflow.com/questions/3269434/whats-the-most-efficient-way-to-test-two-integer-ranges-for-overlap
                        has_se_overlap = (se_chr.bp_start <= read_bp_end) & \
                                         (read_bp_start <= se_chr.bp_end)
                        has_se_overlap = np.any(has_se_overlap)
                        if has_se_overlap:
                            break
                    # Check if we should write cluster
                    if has_se_overlap:
                        fsprite_keep.write(lpre)
                        cluster_keep = cluster_keep + 1
                        break

###########################################
# Main
###########################################

# Main script entry point
def __main__():
    print "======================== sprite.keep.se ========================"
    # Initialize command line parser
    parser = argparse.ArgumentParser()
    # Optional argument(s)
    parser.add_argument('-se', '--fse', default=DEF_SE_IN_PATH,
                        help='Path to input super-enhancer regions')
    parser.add_argument('-i', '--fin', default=DEF_SPRITE_IN_PATH,
                        help='Path to input SPRITE clusters')
    parser.add_argument('-o', '--fout', default=DEF_SPRITE_OUT_PATH,
                        help='Path to output processed SPRITE clusters')
    parser.add_argument('-bp', '--sprite_bp', default=DEF_SPRITE_BP,
                        help='Assumed length of SPRITE read')
    # Parse command line
    args = parser.parse_args()
    # Print command line
    print '\t[--fse] = ' + str(args.fse)
    print '\t[--fin] = ' + str(args.fin)
    print '\t[--fout] = ' + str(args.fout)
    print '\t[--sprite_bp] = ' + str(args.sprite_bp)
    # Feed to utility
    keep_se(fin_se = args.fse, fin_sprite = args.fin, fout_sprite = args.fout,
            sprite_bp = args.sprite_bp)
    print "Finished."

# Run main if we are the active script
if __name__ == '__main__':
    __main__()
