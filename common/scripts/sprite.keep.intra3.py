#!/usr/bin/python
# -*- coding: utf-8 -*-

# Script parses SPRITE clusters file and retains only clusters with at least 3
# reads from same chromosome.
#
# Reference:
#
#   Quinodoz, Sofia A., Noah Ollikainen, Barbara Tabak, Ali Palla, Jan Marten
#   Schmidt, Elizabeth Detmar, Mason M. Lai et al. "Higher-Order Inter-
#   chromosomal Hubs Shape 3D Genome Organization in the Nucleus." Cell (2018).
#
# SPRITE cluster data can be obtained from GEO: GSE114242
#
# For SPRITE processing protocol, refer to Guttman lab wiki:
#   https://github.com/GuttmanLab/sprite-pipeline/wiki/4.-Clustering

###########################################
# Imports
###########################################

# For parsing user supplied arguments
import argparse

# For file path testing
import os

###########################################
# Globals
###########################################

# Path to directory containing this script
# https://stackoverflow.com/questions/4934806/how-can-i-find-scripts-directory-with-python
SCRIPT_DIR = os.path.abspath(os.path.dirname(os.path.realpath(__file__)))

# Default path to raw (unfiltered) SPRITE clusters directory
DEF_SPRITE_RAW_DIR = os.path.join(SCRIPT_DIR, "..", "output", "sprite.raw")

# Default SPRITE MAPQ threshold
DEF_MAPQ_THRESH = 10

# Default SPRITE clusters file name
DEF_SPRITE_RAW_FILE = "human.combined.mapq-ge" + str(DEF_MAPQ_THRESH) + ".clusters"

# Default SPRITE clusters file path
DEF_SPRITE_RAW_PATH = os.path.join(DEF_SPRITE_RAW_DIR, DEF_SPRITE_RAW_FILE)

# Default processed SPRITE output directory
DEF_SPRITE_OUT_DIR = os.path.join(DEF_SPRITE_RAW_DIR, "..", "sprite.keep.intra3")

# Default processed SPRITE output clusters path
DEF_SPRITE_OUT_FILE = "gm12878.mapq" + str(DEF_MAPQ_THRESH) + ".cluster"

# Default process SPRITE output path
DEF_SPRITE_OUT_PATH = os.path.join(DEF_SPRITE_OUT_DIR, DEF_SPRITE_OUT_FILE)

###########################################
# Utils
###########################################

# Recursively creates build directory such that all parent directories are
# created if necessary
# http://stackoverflow.com/questions/273192/in-python-check-if-a-directory-exists-and-create-it-if-necessary
def make_dir(d):
    '''Utility for making a directory if not existing.'''
    if not os.path.exists(d):
        print "Creating directory: " + str(d)
        os.makedirs(d)

# @param tup - string of form "chr<int>:<bp_int>"
def get_chr(tup):
    '''Return chromosome identifiers for chr:bp tuples'''
    return tup.split(":")[0]

###########################################
# Keep
###########################################

# @param fin - Input path to raw SPRITE clusters
# @param fout - Output path to filtered SPRITE clusters
# @param heart - heartbeat interval
def keep_intra3(fin = DEF_SPRITE_RAW_PATH, fout = DEF_SPRITE_OUT_PATH,
                heart=1000):
    '''Retain SPRITE clusters with at least 3 reads within same chromosome'''
    tups = []
    chrs = []
    chr_counts = {}
    count = 0
    cluster_id = 0
    with open(fin, "r") as fsprite_raw:
        make_dir(os.path.dirname(fout))
        with open(fout, "w") as fsprite_keep:
            for lraw in fsprite_raw:
                # Check heartbeat
                cluster_id = cluster_id + 1
                if (cluster_id % heart) == 0:
                    print "Processing cluster " + str(cluster_id)
                # Split by whitespace and strip cluster ID
                tups = lraw.split()[1:]
                # Get list of chr<int> identifiers
                chrs = map(get_chr, tups)
                # Count number of occurrences for each chromosome
                chr_counts = {}
                for chr in chrs:
                    count = chr_counts.get(chr, 0) + 1
                    chr_counts[chr] = count
                    if (count > 2):
                        # >= 3 reads in single chromosome, retain cluster
                        fsprite_keep.write(lraw)
                        break

###########################################
# Main
###########################################

# Main script entry point
def __main__():
    print "======================== sprite.keep.intra3 ========================"
    # Initialize command line parser
    parser = argparse.ArgumentParser()
    # Optional argument(s)
    parser.add_argument('-i', '--fin', default=DEF_SPRITE_RAW_PATH,
                        help='Path to raw (unfiltered) SPRITE clusters')
    parser.add_argument('-o', '--fout', default=DEF_SPRITE_OUT_PATH,
                        help='Path to write filtered SPRITE clusters')
    # Parse command line
    args = parser.parse_args()
    # Print command line
    print '\t[--fin] = ' + str(args.fin)
    print '\t[--fout] = ' + str(args.fout)
    # Feed to utility
    keep_intra3(fin = args.fin, fout = args.fout)
    print "Finished."

# Run main if we are the active script
if __name__ == '__main__':
    __main__()
