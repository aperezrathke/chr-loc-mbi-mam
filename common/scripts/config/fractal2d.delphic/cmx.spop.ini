# Any of these parameters can be overridden via command line using
# --<option_name> <option_value>

# Common configuration settings
arch = cmx.arch.ini

#### BEGIN ARCHETYPE OVERLOADS
# Number of iterations used for Gibbs model burn-in
controller_burn_iters = 50000
# Number of iterations to perform after burn-in
controller_post_burn_iters = 1000001
# Size of latent pseudo-population, should be similar scale to Bnet_cpt_alpha
pseudopop_size = 500
# Size of proposal sub-population
pseudopop_block_size = 5
# Defer to Polya urn pseudo-population proposal if correlation dips below this
pseudopop_bpoly_corr_thresh = 0.95
# Hyper-parameter for empirical Hi-C likelihood, if using Beta distribution,
# this corresponds to a scaled pseudo-population with pseudo-counts
# proportional to the latent samples. If using similarity (e.g Pearson
# correlation), then the log-likelihood is assumed to be proportional to
# similarity score multiplied by this scale factor.
hic_alpha = 3.0
# [Optional] Set number of models (independent Gibbs chains) to simulate.
# Default is 5
num_models = 20
#### END ARCHETYPE OVERLOADS

# Enable log-score snapshot observer, captures model log-scores at user-
# defined iteration period
obs_enable_0 = logscore_snap
obs_logscore_snap_period = 1
# Enable log-score console observer, reports log-scores to stdout at user-
# defined iteration period 
obs_enable_1 = logscore_cons
obs_logscore_cons_period = 10000
# Enable log-score trace observer, records trace plots (iteration vs log-score)
# at user-defined iteration period
obs_enable_2 = logscore_trac
obs_logscore_trac_export = 1
obs_logscore_trac_period = 10000
# Start iteration for log-score trace plot
obs_logscore_trac_start_iter = 0
# Enable log-score MAP observers
obs_enable_3 = logscore_mapc
obs_enable_4 = logscore_mapg
obs_logscore_mapc_min_corr_hic = 0.9
obs_logscore_mapc_start_iter = 100
obs_logscore_mapg_min_corr_hic = 0.9
obs_logscore_mapg_start_iter = 100

# Enable Hi-C correlation snapshot observer
obs_enable_5 = corr_hic_snap
obs_corr_hic_snap_period = 10000
# Enable Hi-C correlation MAP observers
obs_enable_6 = corr_hic_mapc
obs_enable_7 = corr_hic_mapg
# Enable Hi-C correlation console observer
obs_enable_8 = corr_hic_cons
obs_corr_hic_cons_period = 10000
# Enable Hi-C correlation trace observer
obs_enable_9 = corr_hic_trac
obs_corr_hic_trac_export = 1
obs_corr_hic_trac_period = 10000
# Start iteration for trace plot
obs_corr_hic_trac_start_iter = 0

# Configure acceptance tracking if supported
obs_enable_10 = pacc_bnet_cons
obs_pacc_bnet_cons_period = 10000
obs_enable_11 = pacc_pseu_cons
obs_pacc_pseu_cons_period = 10000

# Enable Pseudo-population Hi-C MAP observers
obs_enable_12 = pseu_hic_mapc
obs_enable_13 = pseu_hic_mapg
obs_pseu_hic_mapc_export = 1
obs_pseu_hic_mapg_export = 1

# Enable Bnet snapshot observer
obs_enable_14 = bnet_snap
obs_bnet_snap_period = 10000
# Enable Bnet ring buffer trace observer
obs_enable_15 = bnet_ring
obs_bnet_ring_period = 10000
# Enable Bnet MAP observers
obs_enable_16 = bnet_mapc
obs_enable_17 = bnet_mapg

# Enable Pseudo-population frame MAP observers
obs_enable_18 = pseu_frame_mapc
obs_enable_19 = pseu_frame_mapg
obs_pseu_frame_mapc_export = 1
obs_pseu_frame_mapg_export = 1

# Enable Pseudo-population frame observers
#obs_enable_20 = pseu_frame_cons
#obs_pseu_frame_cons_period = 10000
