#!/bin/bash

# Script launches multiple BLB jobs
# Example usage:
#
#   nohup <path_to_script> [-jobs <int>] [-procs <int>] &> <path_to_log> &
#   : nohup means to keep job running even when logged out
#   : &> means to redirect stdout and stderr to log file
#   : & means to run script in background
#
# User may specify optional command-line overrides for number of jobs,
# processes, threads, and target executable binary:
#   -jobs <int>: Specify total number of jobs to run - must be positive integer.
#   -procs <int>: Specify total number of processes to distribute jobs over -
#       must be positive integer. Should not be greater than the number of
#       CPU cores available on the machine running this script.
#   -threads <int>: Determine number of threads per processes. The value:
#       <procs> * <max(threads,1)> should not exceed total number of CPU cores
#       available on compute node. If threads < 2, then a serial
#       build is used.
#   -exe <path>: Path to binary executable, will override defaults

###################################################
# Script paths
###################################################

# Path to script directory
SCRIPT_PATH="$(readlink -f $0)"
SCRIPT_DIR="$(dirname $SCRIPT_PATH)"

# Path to python BLB script
PY_SCRIPT_PATH="$SCRIPT_DIR/null_blb_run.py"

# Output base directory
OUT_BASE_DIR="$SCRIPT_DIR/../output"
OUT_BASE_DIR=$(cd "$OUT_BASE_DIR"; pwd)

# Executable defaults
BIN_DIR="$OUT_BASE_DIR/build/bin"
EXE_DIR="$BIN_DIR/Release_commandlets"
# Executable name
EXE_NAME=u_sac
# Default path to binary executable
EXE_PATH="$EXE_DIR/$EXE_NAME"

# Target log directory
LOG_DIR="$OUT_BASE_DIR/logs/blb"
mkdir -p "$LOG_DIR"

# Number of worker threads (if <= 1, then serial build used)
NUM_THREADS=0
NUM_THREADS_ARG=""

# The number of jobs to queue
# This value can be overridden with command line: -jobs <integer>
NUM_JOBS=20

###################################################
# Timestamp utils
###################################################

# From http://stackoverflow.com/questions/17066250/create-timestamp-variable-in-bash-script
timestamp() {
  date +"%T"
}

###################################################
# JOBQUEUE UTILS
###################################################

# From https://pebblesinthesand.wordpress.com/2008/05/22/a-srcipt-for-running-processes-in-parallel-in-bash/

NUM=0
QUEUE=""
# @WARNING - On extreme, this should match the number of CPUs per node
# This value can be overridden with command line: -procs <integer>
MAX_NPROC=10

function queue {
    QUEUE="$QUEUE $1"
    NUM=$(($NUM+1))
}

function regeneratequeue {
    OLDREQUEUE=$QUEUE
    QUEUE=""
    NUM=0
    for PID in $OLDREQUEUE
    do
        if [ -d /proc/$PID  ] ; then
            QUEUE="$QUEUE $PID"
            NUM=$(($NUM+1))
        fi
    done
}

function checkqueue {
    OLDCHQUEUE=$QUEUE
    for PID in $OLDCHQUEUE
    do
        if [ ! -d /proc/$PID ] ; then
            regeneratequeue # at least one PID has finished
            break
        fi
    done
}

###################################################
# Command line overrides
###################################################

# Check if there are any parameter overrides
EXE_USER=""
while [ $# -gt 1 ]
do
    case "$1" in
    -jobs)
        NUM_JOBS=$2
        shift
        ;;
    -procs)
        MAX_NPROC=$2
        shift
        ;;
    -threads)
        NUM_THREADS=$2
        shift
        ;;
    -exe)
        EXE_USER="$2"
        shift
        ;;
    esac
    shift
done

# Validate number of jobs
if [ "$NUM_JOBS" -le "0" ]; then
    # http://stackoverflow.com/questions/4381618/exit-a-script-on-error
    # Redirect to stderror (file descriptor 2)
    echo "Error: Invalid job count: $NUM_JOBS <= 0. Exiting." 1>&2
    # Exit status 1 indicates error
    exit 1
fi

# Validate number of processes per job
if [ "$MAX_NPROC" -le "0" ]; then
    echo "Error: Invalid process count: $MAX_NPROC <= 0. Exiting." 1>&2
    exit 1
fi

# Validate number of worker threades per process
if [ "$NUM_THREADS" -lt "0" ]; then
    echo "Error: Invalid thread count: $NUM_THREADS < 0. Exiting." 1>&2
    exit 1
fi

# Check if threaded-build requested
if [ "$NUM_THREADS" -gt 1 ]; then
    EXE_DIR="$BIN_DIR/Release_commandlets_threaded"
    # Default threaded path
    EXE_PATH="$EXE_DIR/$EXE_NAME"
    NUM_THREADS_ARG="--num_threads $NUM_THREADS"
fi

# Check if user supplied executable path (empty string test)
# https://www.cyberciti.biz/faq/unix-linux-bash-script-check-if-variable-is-empty/
if [ ! -z "$EXE_USER" ]; then
    EXE_DIR=dirname "$EXE_USER"
    EXE_NAME=basename "$EXE_USER"
    EXE_PATH="$EXE_USER"
fi

# Validate executable path exists
# https://stackoverflow.com/questions/638975/how-do-i-tell-if-a-regular-file-does-not-exist-in-bash
if [ ! -f "$EXE_PATH" ]; then
    echo "Error: Invalid executable path (not found): $EXE_PATH. Exiting." 1>&2
    exit 1
fi

# Resolve final exectuable path
EXE_ARG="--exe $EXE_PATH"

###################################################
# Queue jobs
###################################################

# Timestamp script execution - append this value to each job prefix
# to avoid overwriting previously generated loops
SCRIPT_ID="$(date +%Y%m%d%H%M%S).$RANDOM"

# Report arguments
echo "$(timestamp): Distributing $NUM_JOBS jobs over ($MAX_NPROC:$NUM_THREADS) processes."

# The current job being batched
job_id=0

# Queue the jobs!
while [ $job_id -lt $NUM_JOBS ]
do
    # Modify output prefix with job identifier, else data files will
    # be overwritten from job to job
    job_prefix="$SCRIPT_ID.$job_id"
    job_prefix_arg="--job_id $job_prefix"

    # Determine path to log file for this job
    log_path="$LOG_DIR/$job_prefix.txt"

    # Build command line
    cmd="python $PY_SCRIPT_PATH"
    cmd="$cmd $EXE_ARG"
    cmd="$cmd $job_prefix_arg"
    cmd="$cmd $NUM_THREADS_ARG"
    echo "$(timestamp): $cmd"

    # Run command in background
    # http://www.cyberciti.biz/faq/redirecting-stderr-to-stdout/
    $cmd &> "$log_path" &

    # Help ensure unique seeds by sleeping for a bit between
    # job queues - technically should not be needed as u-sac
    # uses random device
    sleep 2s

    # Capture process id of last process ran
    PID=$!
    # Store process id
    queue $PID

    # Check if we need to spin until at least one of the jobs finishes
    while [ $NUM -ge $MAX_NPROC ]; do
        checkqueue
        # Sleep for *s -> seconds, *m -> minutes, *h -> hours
        sleep 2m
    done # end queue spinning

    # Increment job identifier
    job_id=$(( $job_id + 1 ))
done

###################################################
# Wait on last set of jobs
###################################################

echo "$(timestamp): Waiting on final set of jobs to finish..."
wait

echo "$(timestamp): Finished."
