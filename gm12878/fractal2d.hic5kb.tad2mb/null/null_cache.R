###################################################################
# null_cache.R
#
# Utilities for converting to binary RData format
###################################################################

# Guard variable to avoid multiple sourcing
SOURCED_null_cache = TRUE

###################################################################
# Path to this script
###################################################################

# Cached directory to script
# https://stackoverflow.com/questions/3452086/getting-path-of-an-r-script
NULL_CACHE_SCRIPT_DIR = getSrcDirectory(function(x) {
  x
})

# getSrcDirectory trick will not work via Rscript. If this is the
# case, then NULL_CACHE_SCRIPT_DIR will be length zero.
# http://stackoverflow.com/questions/1815606/rscript-determine-path-of-the-executing-script
if (length(NULL_CACHE_SCRIPT_DIR) == 0) {
  get_script_dir <- function() {
    cmdArgs = commandArgs(trailingOnly = FALSE)
    needle = "--file="
    ixmatch = grep(needle, cmdArgs)
    if (length(match) > 0) {
      # Rscript
      return(dirname(normalizePath(sub(
        needle, "", cmdArgs[ixmatch]
      ))))
    }
    return(dirname(scriptName::current_filename()))
  }
  NULL_CACHE_SCRIPT_DIR = get_script_dir()
}

# @return Path to directory containing this script
get_null_cache_script_dir <- function() {
  return(NULL_CACHE_SCRIPT_DIR)
}

###################################################################
# Source utility script(s)
###################################################################

# @return Path to local common directory
get_local_common_dir <- function() {
  return(file.path(get_null_cache_script_dir(), "..", "common"))
}

# Common configuration - loads local python module "tad_core"
source(file.path(get_local_common_dir(), "tad_core.R"))

# Import/export utilities
source(file.path(tad_core$MODULE_COMMON_DIR, "scripts", "utils.io.R"))

###################################################################
# Defaults
###################################################################

# Default: If TRUE, overwrite cached data
DEF_SHOULD_OVERWRITE = tad_core$DEF_SHOULD_OVERWRITE

# Default: If TRUE, verbose logging to stdout
DEF_VERBOSE = tad_core$DEF_VERBOSE

###################################################################
# Cache
###################################################################

# @param logw - log-transformed importance weights
# @return Effective sample size
get_ess <- function(logw) {
  w = exp(logw - max(logw))
  v = var(w)
  m = mean(w)
  ess = 1.0 / (1.0 + (v / (m * m)))
  return(ess)
}

# Determine non-outlier weights
# @param logw - Numeric vector of log weights
# @param verbose - If TRUE, reports fraction removed and ESS
# @return Logical vector of non-outlier weights
get_regular_weights <- function(logw, verbose = DEF_VERBOSE) {
  logw_max = max(logw)
  w = exp(logw - max(logw))
  q = quantile(x = w, probs = c(0.25, 0.75))
  iqr = q["75%"] - q["25%"]
  h = 1.5 * iqr
  w_min = q["25%"] - h
  w_max = q["75%"] + h
  keep = (w >= w_min) & (w <= w_max)
  if (verbose) {
    n_all = length(keep)
    n_rmv = n_all - length(which(keep))
    perc_rmv = 100.0 * n_rmv / n_all
    print(
      paste0(
        "Outlier detection removed ",
        n_rmv,
        " of ",
        n_all,
        " (",
        format(round(perc_rmv, 1), nsmall = 1),
        "%) ligc data samples."
      )
    )
    ess = get_ess(logw[keep])
    print(paste0("ESS (outliers removed): ", ess))
  } # End verbose ESS check
  #
  return(invisible(keep))
}

# @param logw - Numeric vector of log weights
# @return Normalized weights (after exp{log} transform) such that
#   sum of weights is 1
get_norm_weights <- function(logw) {
  w = exp(logw - max(logw))
  w = w / sum(w)
  return(invisible(w))
}

# Parse ligation tuple file and associated log weight
# @WARNING - CONVERTS LIGC TUPLES TO 1-BASED INDEXING!
# @param ligc_fpath - Path to ligation tuple file
# @return NULL if error, else list with members:
#   $logw - log weight of ligc file
#   $ligc - N x 2 1-based index matrix of ligation tuples
load_ligc <- function(ligc_fpath) {
  # Determine loader
  CONFUNC = file
  if (tools::file_ext(ligc_fpath) == "gz") {
    CONFUNC = gzfile
  }
  # Log weight prefix
  comment = "#"
  log_weight_prefix = paste0(comment, " LOG WEIGHT: ")
  log_weight_prefix_len = nchar(log_weight_prefix) + 1
  # Open [compressed] file handle
  con = CONFUNC(description = ligc_fpath)
  # Find line with log weight
  is_logw_line = FALSE
  repeat {
    l = readLines(con, n = 1)
    # Test line was read and is a comment
    if ((length(l) == 1) && startsWith(l, comment)) {
      # Test line is a log weight comment
      if (startsWith(l, log_weight_prefix)) {
        # Success
        is_logw_line = TRUE
        break
      }
    } else {
      # Fail
      break
    }
  }
  if (!is_logw_line) {
    # We did not find a log weight!
    print("Warning, unable to load ligc log weight for file:")
    print(paste0(ligc_fpath))
    close(con)
    return(NULL)
  }
  # Parse log weight
  log_weight = as.numeric(substr(l, log_weight_prefix_len,
                                 nchar(l)))
  # Parse ligation contacts
  # Note: This closes the file connection!
  ligc = as.matrix(read.csv(
    file = con,
    header = FALSE,
    stringsAsFactors = FALSE,
    comment.char = "#"
  ))
  # Convert to 1-based indexing
  ligc = ligc + 1
  colnames(ligc) = c("i1", "j1")
  # Make sure mode is integer
  mode(ligc) = "integer"
  # Combine log weight and contacts into single tuple
  return(list("logw" = log_weight, "ligc" = ligc))
}

# Utility converts CSV formatted ligation contacts into binary
# RData. Specifically, each file is converted into a list with two
# members:
#   $logw : log-transforwed weight as numeric scalar
#   $ligc : |NUM_CONTACTS| x 2 integer index (1-based) matrix where
#       each row is an (i,j) tuple indicating that the monomers at
#       1-based indices i and j were within the ligation threshold.
#       Note that NUM_CONTACTS is specific to each sample.
# @WARNING: Converts to 1-based indexing!
# @param in_csv_dir - Input directory containing ligation contact
#   data in [gzipped] CSV format
# @param out_rdata_dir - Output directory to store binary Rdata
# @param verbose - If TRUE, log to stdout
null_cache_ligc_csv <-
  function(in_csv_dir = tad_core$NULL_LIGC_CSV_GZ_DIR,
           out_rdata_dir = tad_core$NULL_LIGC_RDATA_DIR,
           verbose = DEF_VERBOSE) {
    # Get list of input ligc files
    fnames_in = list.files(
      path = in_csv_dir,
      all.files = FALSE,
      full.names = FALSE,
      recursive = FALSE,
      include.dirs = FALSE,
      no.. = TRUE
    )
    # Iterate over ligc files
    for (fname_in in fnames_in) {
      fpath_in = file.path(in_csv_dir, fname_in)
      if (verbose) {
        print(paste0("Reading file: ", fpath_in))
      }
      tuple = load_ligc(ligc_fpath = fpath_in)
      if (is.null(tuple)) {
        next
      }
      # Save file
      fname_out = file_path_sans_sub_ext(x = fname_in,
                                         sub_ext_no_dot = "csv")
      fname_out = smush(fname_out, "rdata")
      fpath_out = file.path(out_rdata_dir, fname_out)
      save_rdata(
        data = tuple,
        rdata_path = fpath_out,
        verbose = verbose,
        create_subdirs = TRUE
      )
    }
  }

# Combines all individual ligc RData files into single RData file.
# Specifically, creates a single list structure with members:
#   $logw - a parallel numeric vector of log-transformed weights
#   $ligc - a parallel list of |NUM_CONTACTS| x 2 integer index
#     (1-based) matrices where each row is (i,j) tuple indicating
#     that monomers at 1-based indices i and j where within the
#     ligation threshold distance. Note that NUM_CONTACTS is
#     specific to each sample.
# @WARNING: Assumes null_cache_ligc_csv() has already been called!
# @param rmvout - If TRUE, remove outlier samples with weights not
#   in inclusive range: [Q1 - 1.5 IQR, Q3 + 1.5 IQR]
# @param in_rdata_dir - Input RData directory with individual
#   ligation contact tuples
# @param out_rdata_path - Output path for merged ligation contact
#   data
# @param should_overwrite - Overwrite any existing cached data
# @param verbose - If TRUE, log to stdout
# @return list of merged ligation contact tuples
null_cache_ligc_merge <-
  function(rmvout = tad_core$NULL_REMOVE_OUTLIERS,
           in_rdata_dir = tad_core$NULL_LIGC_RDATA_DIR,
           out_rdata_path = tad_core$get_null_ligc_merge_rdata_path(rmvout =
                                                                      rmvout),
           should_overwrite = DEF_SHOULD_OVERWRITE,
           verbose = DEF_VERBOSE) {
    # Check if cached file exists
    if (is.character(out_rdata_path) &&
        file.exists(out_rdata_path) &&
        !should_overwrite) {
      if (verbose) {
        print("Loading cached data")
      }
      return(invisible(load_rdata(
        rdata_path = out_rdata_path, verbose = verbose
      )))
    }
    # Else, process individual RData tuples
    # Get list of input ligc files
    fnames_in = list.files(
      path = in_rdata_dir,
      all.files = FALSE,
      full.names = FALSE,
      recursive = FALSE,
      include.dirs = FALSE,
      no.. = TRUE
    )
    # Iterate over ligc files
    logw = rep(0, length(fnames_in))
    ligc = list()
    index = 1
    for (fname_in in fnames_in) {
      fpath_in = file.path(in_rdata_dir, fname_in)
      tuple = load_rdata(rdata_path = fpath_in, verbose = verbose)
      ligc[[index]] = tuple$ligc
      logw[index] = tuple$logw
      index = index + 1
    }
    if (verbose) {
      ess = get_ess(logw)
      print(paste0("ESS (all): ", ess))
    }
    # Remove outliers
    if (rmvout) {
      keep = get_regular_weights(logw = logw, verbose = verbose)
      logw = logw[keep]
      ligc = ligc[keep]
    }
    out = list(logw = logw, ligc = ligc)
    # Cache data
    if (is.character(out_rdata_path)) {
      save_rdata(data = out,
                 rdata_path = out_rdata_path,
                 verbose = verbose)
    }
    return(invisible(out))
  }

# Convert ligc samples into a symmetric contact frequency matrix
# @WARNING: Assumes null_cache_ligc_merge() has already been called!
# @WARNING: Assumes number of bins can be obtained from max
#   encountered (i, j) ligation contact index tuple
# @WARNING: Assumes (i, j) ligation contact tuples are either all
#   lower or all upper triangle (all samples from same triangle!)
# @WARNING: Will set 0-diag and 1-diag equal to 1.0
# @param in_rdata_path - Path to merged ligc data, assumes data is a
#   list with elements:
#     $logw - numeric vector of log weights of length K, where K is
#       total number of [possibly outlier removed] polymer samples
#     $ligc - parallel list of K integer matrices, each matrix has
#       dimensions [NUM_CONTACTS(k) x 2] where NUM_CONTACTS(k) is
#       the number of ligation contacts in the k-th sample, and each
#       row is an (i,j) tuple of 1-based monomer indices that where
#       within the ligation distance threshold
# @param out_csv_gz_path - Path to write contact frequency matrix in
#   gzipped CSV format
# @param out_rdata_path - Path to write contact frequency matrix in
#   binary RData format
# @param func_weight_trans - Transforms log weights vector to normalized
#   weight contribution
# @param should_overwrite - If TRUE, overwrite any cached data
# @param verbose - If TRUE, log to stdout
# @return Symmetric numeric frequency matrix, each cell in [0,1]
null_cache_ligc_freqmat <-
  function(in_rdata_path = tad_core$get_null_ligc_merge_rdata_path(),
           out_csv_gz_path = tad_core$get_null_ligc_freqmat_csv_gz_path(),
           out_rdata_path = tad_core$get_null_ligc_freqmat_rdata_path(),
           func_weight_trans = get_norm_weights,
           should_overwrite = DEF_SHOULD_OVERWRITE,
           verbose = DEF_VERBOSE) {
    # Check if cached file exists
    if (is.character(out_rdata_path) &&
        file.exists(out_rdata_path) &&
        !should_overwrite) {
      if (verbose) {
        print("Loading cached data")
      }
      return(invisible(load_rdata(
        rdata_path = out_rdata_path, verbose = verbose
      )))
    }
    # Else, load merged ligation contact samples
    if (verbose) {
      print("Loading merged ligation contacts")
    }
    ligc_lst = load_rdata(rdata_path = in_rdata_path, verbose = verbose)
    stopifnot(length(ligc_lst$logw) == length(ligc_lst$ligc))
    K = length(ligc_lst$ligc)
    stopifnot(K > 0)
    # Find number of bins as max observed 1-based ligation index
    max_ixs = sapply(X = ligc_lst$ligc,
                     FUN = max,
                     simplify = TRUE)
    num_bin = max(max_ixs)
    if (verbose) {
      print(paste0("Found ", num_bin, " contact bins"))
    }
    stopifnot(num_bin > 1)
    # Allocate matrix
    out = matrix(data = 0.0,
                 nrow = num_bin,
                 ncol = num_bin)
    # Normalize weights
    w = func_weight_trans(logw = ligc_lst$logw)
    # Process samples
    if (verbose) {
      print(paste0("Processing ", K, " samples..."))
    }
    for (i in 1:K) {
      # Weighted update
      out[ligc_lst$ligc[[i]]] = out[ligc_lst$ligc[[i]]] + w[i]
    }
    # Create symmetric matrix
    diag(out) = 0.0
    # Help verify all samples either a) all lower tri or b) all
    # upper tri by checking that one of the tris is zero
    stopifnot(all(out[lower.tri(out)] == 0.0) ||
                all(out[upper.tri(out)] == 0.0))
    # Reflect non-zero triangle to the other triangle
    out = out + t(out)
    # Update 0-diagonal
    diag(out) = 1.0
    # Update |1|-diagonals
    # https://stackoverflow.com/questions/30631531/how-does-one-get-the-kth-diagonal-in-r-what-about-the-opposite-diagonals
    delta = col(out) - row(out)
    out[abs(delta) == 1] = 1.0
    stopifnot(isSymmetric(out))
    # Save (compressed) plain-text
    if (is.character(out_csv_gz_path)) {
      save_csv_gz(x = out,
                  gz_path = out_csv_gz_path,
                  verbose = verbose)
    }
    # Save binary
    if (is.character(out_rdata_path)) {
      save_rdata(data = out,
                 rdata_path = out_rdata_path,
                 verbose = verbose)
    }
    return(invisible(out))
  }

# Convert genomic distance to 1-based integer index for diagonal
# @param gd - genomic distance (non-negative)
# @return 1-based diagonal index key for genomic distance
gd2ixdiag <- function(gd) {
  return(gd + 1)
}

# Convert 1-based diagonal integer index to genomic distance
# @param ix - 1-based diagonal index key
# @return Corresponding genomic distance
ixdiag2gd <- function(ix) {
  return(ix - 1)
}

# Utility converts lower-banded, CSV formatted ligation contact
# bootstrap replicates into binary RData. Specifically, converts
# each sample into a list keyed by diagonal's (genomic distance+1).
# For instance, if the sample has 5 diagonals, 3 of which are non-
# bonded, and bonded is FALSE, then the resulting list will look
# like the following:
#   [[1]] = NULL
#   [[2]] = NULL
#   [[3]] = numeric vector of length N - 2
#   [[4]] = numeric vector of length N - 3
#   [[5]] = numeric vector of length N - 4
# where 'N' is the number of monomer positions (in this case 5). See
# gd2ixdiag(gd) for converting genomic distance 'gd' to list key and
# ixdiag2df(ix) for converting list key back to genomic distance.
# @param in_csv_dir - Input directory containing lower banded
#   bootstrap replicates for ligation contact data in [gzipped] CSV
#   format
# @param out_rdata_dir = Output directory to store binary Rdata
# @param CONFUNC - Open connection callback, uses first argument
# @param bonded - If TRUE, bonded ligations are assumed present
# @param verbose - If TRUE, log to stdout
null_cache_ligc_popd_lband_csv <-
  function(in_csv_dir = tad_core$NULL_LIGC_POPD_LBAND_CSV_GZ_DIR,
           out_rdata_dir = tad_core$NULL_LIGC_POPD_LBAND_RDATA_DIR,
           CONFUNC = c(gzfile, file),
           bonded = tad_core$NULL_BONDED,
           verbose = DEF_VERBOSE) {
    CONFUNC = CONFUNC[[1]]
    # Start genomic distance diagonal
    GD_START = 2
    if (bonded) {
      GD_START = 0
    }
    # Get list of input files
    fnames_in = list.files(
      path = in_csv_dir,
      all.files = FALSE,
      full.names = FALSE,
      recursive = FALSE,
      include.dirs = FALSE,
      no.. = TRUE
    )
    # Iterate over ligc popd replicates
    for (fname_in in fnames_in) {
      fpath_in = file.path(in_csv_dir, fname_in)
      if (verbose) {
        print(paste0("Reading file: ", fpath_in))
      }
      # Open [compressed] file handle
      con = CONFUNC(description = fpath_in)
      csv_lines = readLines(con)
      close(con)
      out = vector(mode = "list", length = length(csv_lines) + GD_START)
      gd = GD_START
      # Process each lower diagonal
      for (csv_line in csv_lines) {
        # Split diagonal by ',' and convert to numeric
        out[[gd2ixdiag(gd)]] = as.numeric(unlist(strsplit(
          x = csv_line,
          split = ',',
          fixed = TRUE
        )))
        gd = gd + 1
      }
      # Save file
      fname_out = file_path_sans_sub_ext(x = fname_in,
                                         sub_ext_no_dot = "csv")
      fname_out = smush(fname_out, "rdata")
      fpath_out = file.path(out_rdata_dir, fname_out)
      save_rdata(
        data = out,
        rdata_path = fpath_out,
        verbose = verbose,
        create_subdirs = TRUE
      )
    }
  }

# Combines all individual ligc bootsrap replicated RData files into
# single RData file. Specifically, creates a single list structure
# with numeric matrices for each genomic distance diagonal, where
# each row is from a different replicate. For instance, if there
# are 10 bootstrap replicates, 5 total diagonals, bonded=FALSE (see
# null_cache_ligc_popd_lband_csv()), then the resulting list will
# look like the following:
#   [[1]] = NULL
#   [[2]] = NULL
#   [[3]] = 10 x |N - 2| numeric matrix
#   [[4]] = 10 x |N - 3| numeric matrix
#   [[5]] = 10 x |N - 4| numeric matrix
# where 'N' is the number of monomer positions (in this case 5). See
# gd2ixdiag(gd) for converting genomic distance 'gd' to list key and
# ixdiag2df(ix) for converting list key back to genomic distance.
# @WARNING: assumes null_cache_ligc_popd_lband_csv() has already
#   been called!
# @param in_rdata_dir - Input RData directory with individual
#   ligation contact bootstrap replicates
# @param out_rdata_path -Output path for merged ligation contact
#   bootstrap replicate data
# @param should_overwrite - Overwrite any existing cached data
# @param verbose - If TRUE, log to stdout
# @return list of merged ligation contact BLB replicate matrices
#   keyed by (genomic distance + 1) - see gd2ixdiag
null_cache_ligc_popd_lband_merge <-
  function(in_rdata_dir = tad_core$NULL_LIGC_POPD_LBAND_RDATA_DIR,
           out_rdata_path = tad_core$NULL_LIGC_POPD_LBAND_MERGE_RDATA_PATH,
           should_overwrite = DEF_SHOULD_OVERWRITE,
           verbose = DEF_VERBOSE) {
    # Check if cached file exists
    if (is.character(out_rdata_path) &&
        file.exists(out_rdata_path) &&
        !should_overwrite) {
      if (verbose) {
        print("Loading cached data")
      }
      return(invisible(load_rdata(
        rdata_path = out_rdata_path, verbose = verbose
      )))
    }
    # Get list of input files
    fnames_in = list.files(
      path = in_rdata_dir,
      all.files = FALSE,
      full.names = FALSE,
      recursive = FALSE,
      include.dirs = FALSE,
      no.. = TRUE
    )
    N_REPS = length(fnames_in)
    if (N_REPS < 1) {
      if (verbose) {
        print("Warning: no data found")
      }
      return(NULL)
    }
    # Process first replicate
    func_prealloc <- function(lband, N_REPS) {
      if (is.null(lband)) {
        return(NULL)
      }
      m = matrix(data = 0,
                 nrow = N_REPS,
                 ncol = length(lband))
      m[1, ] = lband
      return(m)
    }
    out = list()
    fname_in = fnames_in[1]
    fpath_in = file.path(in_rdata_dir, fname_in)
    lbands = load_rdata(rdata_path = fpath_in, verbose = verbose)
    out = lapply(X = lbands, FUN = func_prealloc, N_REPS)
    # Iterate over remaining replicates
    ix_rep = 2
    fnames_in = fnames_in[-1]
    for (fname_in in fnames_in) {
      fpath_in = file.path(in_rdata_dir, fname_in)
      lbands = load_rdata(rdata_path = fpath_in, verbose = verbose)
      for (ix_band in 1:length(lbands)) {
        lband = lbands[[ix_band]]
        if (!is.null(lband)) {
          out[[ix_band]][ix_rep,] = lband
        }
      }
      ix_rep = ix_rep + 1
    }
    # Save merged data
    if (is.character(out_rdata_path)) {
      save_rdata(data = out,
                 rdata_path = out_rdata_path,
                 verbose = verbose)
    }
    return(invisible(out))
  }
