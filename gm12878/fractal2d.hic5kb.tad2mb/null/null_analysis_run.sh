#!/bin/bash

# Path to this script's directory
SCRIPT_PATH="$(readlink -f $0)"
SCRIPT_DIR="$(dirname $SCRIPT_PATH)"
SCRIPT_DIR=$(cd "$SCRIPT_DIR"; pwd)

# Number of CPUs for parallel BLB analysis, <= 0 will use all cores!
NUM_CPU="100"

# Build command line
cmd="Rscript --vanilla $SCRIPT_DIR/null_analysis_shell.R"
cmd="$cmd --launch_null_heat_ligc_freqmat"
cmd="$cmd --launch_null_analysis_ligc_popd_lband --num_cpu $NUM_CPU"

# Run command line
echo "Running:"
echo $cmd
$cmd
echo "Finished."
