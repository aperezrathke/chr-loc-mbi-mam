#!/usr/bin/env Rscript

##################################################################
# null_analysis_shell.R
#
# Command-interface for null analysis
###################################################################

# Similar to python optparse package
library(optparse)

###################################################################
# Path to this script
###################################################################

# Cached directory to script
# https://stackoverflow.com/questions/3452086/getting-path-of-an-r-script
NULL_ANALYSIS_SHELL_SCRIPT_DIR = getSrcDirectory(function(x) {
  x
})

# getSrcDirectory trick will not work via Rscript. If this is the
# case, then NULL_ANALYSIS_SHELL_SCRIPT_DIR will be length zero.
# http://stackoverflow.com/questions/1815606/rscript-determine-path-of-the-executing-script
if (length(NULL_ANALYSIS_SHELL_SCRIPT_DIR) == 0) {
  NULL_ANALYSIS_SHELL_SCRIPT_DIR = dirname(scriptName::current_filename())
}

# @return Path to directory containing this script
get_null_analysis_shell_script_dir <- function() {
  return(NULL_ANALYSIS_SHELL_SCRIPT_DIR)
}

###################################################################
# Source utility script(s)
###################################################################

# Cache utilities
if (!exists("SOURCED_null_analysis")) {
  source(file.path(get_null_analysis_shell_script_dir(), "null_analysis.R"))
}

# Remove outliers path resolution utilities
source(file.path(
  get_null_analysis_shell_script_dir(),
  "utils.rmvout.shell.R"
))

###################################################################
# Parse command line
###################################################################

# https://www.r-bloggers.com/passing-arguments-to-an-r-script-from-command-lines/
option_list = list(
  # Utilities (in order encountered)
  make_option(
    c("--launch_null_heat_ligc_freqmat"),
    action = "store_true",
    default = FALSE,
    help = "Generates heatmaps for null ligation contact frequencies"
  ),
  make_option(
    c("--launch_null_analysis_ligc_popd_lband"),
    action = "store_true",
    default = FALSE,
    help = "Bootstrap (BLB) correlation and heatmap analysis"
  ),
  # Options (in order encountered)
  make_option(
    c("--no-rmvout"),
    action = "store_false",
    dest = "rmvout",
    default = tad_core$NULL_REMOVE_OUTLIERS,
    help = "Disable outlier removal"
  ),
  make_option(
    c("--rmvout"),
    action = "store_true",
    dest = "rmvout",
    default = tad_core$NULL_REMOVE_OUTLIERS,
    help = "Enable outlier removal"
  ),
  make_option(
    c("--ligc_freqmat_rdata_path"),
    type = "character",
    default = tad_core$get_null_ligc_freqmat_rdata_path(rmvout = TRUE),
    help = paste0(
      "Ligc frequency matrix binary RData path. Set to ",
      FROM_RMVOUT_KEY,
      " to derive default from 'rmvout' field."
    )
  ),
  make_option(
    c("--ligc_freqmat_heat_png_path"),
    type = "character",
    default = get_null_ligc_freqmat_heat_path(ext_no_dot = "png"),
    help = paste0(
      "Ligc frequency matrix heatmap PNG path. Set to ",
      FROM_RMVOUT_KEY,
      " to derive default from 'rmvout' field."
    )
  ),
  make_option(
    c("--heat_png_pix"),
    type = "double",
    default = DEF_HEAT_PNG_PIX,
    help = "PNG heatmap width and height pixels"
  ),
  make_option(
    c("--ligc_freqmat_heat_pdf_path"),
    type = "character",
    default = get_null_ligc_freqmat_heat_path(ext_no_dot = "pdf"),
    help = paste0(
      "Ligc frequency matrix heatmap PDF path. Set to ",
      FROM_RMVOUT_KEY,
      " to derive default from 'rmvout' field."
    )
  ),
  make_option(
    c("--heat_pdf_in"),
    type = "double",
    default = DEF_HEAT_PDF_IN,
    help = "PDF heatmap width and height inches"
  ),
  make_option(
    c("--no-verbose"),
    action = "store_false",
    dest = "verbose",
    default = DEF_VERBOSE,
    help = "Disable verbose logging to stdout"
  ),
  make_option(
    c("--verbose"),
    action = "store_true",
    dest = "verbose",
    default = DEF_VERBOSE,
    help = "Enable verbose logging to stdout"
  ),
  make_option(
    c("--ligc_popd_lband_rdata_dir"),
    type = "character",
    default = tad_core$NULL_LIGC_POPD_LBAND_RDATA_DIR,
    help = "Ligc bootstrap replicates lower-banded RData directory"
  ),
  make_option(
    c("--ligc_popd_analysis_corr_csv_path"),
    type = "character",
    default = get_null_ligc_popd_analysis_corr_csv_path(),
    help = "Path to store BLB replicate correlations to full ensemble frequency matrix in CSV format"
  ),
  make_option(
    c("--ligc_popd_analysis_heat_png_dir"),
    type = "character",
    default = get_null_ligc_popd_analysis_heat_dir(ext_no_dot = "png"),
    help = "Directory to store BLB replicate PNG heat maps"
  ),
  make_option(
    c("--no-heat-png-export"),
    action = "store_false",
    dest = "heat_png_enable",
    default = DEF_HEAT_PNG_ENABLE,
    help = "Disable PNG heatmap export"
  ),
  make_option(
    c("--heat-png-export"),
    action = "store_true",
    dest = "heat_png_enable",
    default = DEF_HEAT_PNG_ENABLE,
    help = "Enable PNG heatmap export"
  ),
  make_option(
    c("--ligc_popd_analysis_heat_pdf_dir"),
    type = "character",
    default = get_null_ligc_popd_analysis_heat_dir(ext_no_dot = "pdf"),
    help = "Directory to store BLB replicate PDF heat maps"
  ),
  make_option(
    c("--no-heat-pdf-export"),
    action = "store_false",
    dest = "heat_pdf_enable",
    default = DEF_HEAT_PDF_ENABLE,
    help = "Disable PDF heatmap export"
  ),
  make_option(
    c("--heat-pdf-export"),
    action = "store_true",
    dest = "heat_pdf_enable",
    default = DEF_HEAT_PDF_ENABLE,
    help = "Enable PDF heatmap export"
  ),
  make_option(
    c("--num_cpu"),
    type = "integer",
    default = DEF_NCPU,
    help = "Number of CPUs for parallel BLB replicate analysis. If <= 0, then all CPUs are used!"
  )
)

opt_parser = OptionParser(option_list = option_list)

opt = parse_args(opt_parser)

###################################################################
# Determine which utility to run
###################################################################

# Derives ligc heatmap png path from opt$rmvout if enabled
from_rmvout_ligc_freqmat_heat_png_path <- function(opt) {
  return(invisible(
    from_rmvout(
      opt_val = opt$ligc_freqmat_heat_png_path,
      func = get_null_ligc_freqmat_heat_path,
      opt,
      ext_no_dot = "png"
    )
  ))
}

# Derives ligc heatmap pdf path from opt$rmvout if enabled
from_rmvout_ligc_freqmat_heat_pdf_path <- function(opt) {
  return(invisible(
    from_rmvout(
      opt_val = opt$ligc_freqmat_heat_pdf_path,
      func = get_null_ligc_freqmat_heat_path,
      opt,
      ext_no_dot = "pdf"
    )
  ))
}

# Print command-line options
if (opt$verbose) {
  print("Options:")
  print(opt)
}

# launch_null_heat_ligc_freqmat
if (opt$launch_null_heat_ligc_freqmat) {
  print("Running launch_null_heat_null_ligc_freqmat()")
  launch_null_heat_ligc_freqmat(
    in_rdata_path = from_rmvout_ligc_freqmat_rdata_path(opt),
    out_png_path = from_rmvout_ligc_freqmat_heat_png_path(opt),
    png_pix = opt$heat_png_pix,
    out_pdf_path = from_rmvout_ligc_freqmat_heat_pdf_path(opt),
    pdf_in = opt$heat_pdf_in,
    verbose = opt$verbose
  )
}

# launch_null_analysis_ligc_popd_lband
if (opt$launch_null_analysis_ligc_popd_lband) {
  print("Running launch_null_analysis_ligc_popd_lband()")
  launch_null_analysis_ligc_popd_lband(
    in_ligc_popd_lband_rdata_dir = opt$ligc_popd_lband_rdata_dir,
    in_ligc_freqmat_rdata_path = from_rmvout_ligc_freqmat_rdata_path(opt),
    out_corr_csv_path = opt$ligc_popd_analysis_corr_csv_path,
    out_heat_png_dir = opt$ligc_popd_analysis_heat_png_dir,
    png_enable = opt$heat_png_enable,
    png_pix = opt$heat_png_pix,
    out_heat_pdf_dir = opt$ligc_popd_analysis_heat_pdf_dir,
    pdf_enable = opt$heat_pdf_enable,
    pdf_in = opt$heat_pdf_in,
    verbose = opt$verbose,
    ncpu = opt$num_cpu
  )
}
