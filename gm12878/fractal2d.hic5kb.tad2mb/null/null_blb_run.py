#!/usr/bin/python
# -*- coding: utf-8 -*-

# Compule BLB (bag-of-little boostraps) null contact distribution

###################################################
# Imports
###################################################

# For path manipulation 
import os

# For launching shell scripts
import subprocess

# For generating random unique identifiers
import uuid
import base64

# For flushing stdout
import sys
import argparse

###################################################
# Globals
###################################################

# Path to directory containing this script
# https://stackoverflow.com/questions/4934806/how-can-i-find-scripts-directory-with-python
SCRIPT_DIR = os.path.abspath(os.path.dirname(os.path.realpath(__file__)))
SIM_ID = os.path.basename(SCRIPT_DIR)

# Determine dispatch
DISPATCH_ID = "null"
DISPATCH_KEY = "-cmdlet_ligc_popd"

# Root module directory
MODULE_DIR = os.path.join(SCRIPT_DIR, "..", "..", "..")
MODULE_DIR = os.path.abspath(MODULE_DIR)

# Common utilities directory
MODULE_COMMON_DIR = os.path.join(MODULE_DIR, "common")

# Configuration type
CONFIG_ID = "fractal2d"

# Path to INI
CONF_PATH = os.path.join(MODULE_COMMON_DIR, "scripts", "config", CONFIG_ID,
                         "folder." + DISPATCH_ID + ".parent.ini")

# Overload archetype
ARCH_PATH = os.path.join(SCRIPT_DIR, "config", "folder.arch.ini")

# Output base directory
OUT_BASE_DIR = os.path.join(SCRIPT_DIR, "..", "output")
OUT_BASE_DIR = os.path.abspath(OUT_BASE_DIR)

# Target data directory
OUT_TARGET_DIR = os.path.join(OUT_BASE_DIR, SIM_ID)

# Number of outer bag of little bootstraps (BLB) replicates
LIGC_POPD_BLB_REPO = 500

# Number of inner BLB replicates
LIGC_POPD_BLB_REPI = 200

# Population size scaling exponent in [0,1] such that n^nexp (where n is total
# population size after any outlier removal) defines the number of unique data
# samples per outer BLB replicate
LIGC_POPD_BLB_NEXP = 0.6

# 1 -> exclude samples with weights (>|1.5 IQR|), 0 -> retain all samples
LIGC_POPD_EXCLUDE_OUTLIERS = 1

# Export outer BLB replicates as lower-banded CSV matrices
LIGC_POPD_EXPORT_LBAND_CSV_GZ = 1

###################################################
# Defaults
###################################################

# Exe path
DEF_EXE_PATH = os.path.join(OUT_BASE_DIR, "build", "bin",
                            "Release_commandlets", "u_sac")

# Unique job identifier prefix
DEF_JOB_ID = DISPATCH_ID + "." + \
                base64.urlsafe_b64encode(uuid.uuid4().bytes).rstrip('=')

# Default number of worker threads
DEF_NUM_THREADS = 0

###################################################
# Run
###################################################

def run_blb(exe=DEF_EXE_PATH, job_id=DEF_JOB_ID,
            num_threads=DEF_NUM_THREADS):
    # Ligc Popd (BLB) arguments
    # @TODO - allow overload
    ligc_popd_args = ["--ligc_popd_blb_repo", str(LIGC_POPD_BLB_REPO),
                      "--ligc_popd_blb_repi", str(LIGC_POPD_BLB_REPI),
                      "--ligc_popd_blb_nexp", str(LIGC_POPD_BLB_NEXP),
                      "--ligc_popd_exclude_outliers",
                        str(LIGC_POPD_EXCLUDE_OUTLIERS),
                      "--ligc_popd_export_lband_csv_gz",
                        str(LIGC_POPD_EXPORT_LBAND_CSV_GZ)]
    # Build command line
    cmd = [exe, DISPATCH_KEY, "--conf", CONF_PATH,
           "--arch", ARCH_PATH, "--output_dir", OUT_TARGET_DIR,
           "--job_id", job_id]
    cmd.extend(ligc_popd_args)
    # Add threads argument
    # Note, if threads <= 0, default is used
    if num_threads >= 1:
        cmd.extend(["--num_worker_threads", str(num_threads)])
    print "Running:"
    print " ".join(cmd)
    sys.stdout.flush()
    # Run job
    subprocess.call(cmd)
    print "Finished."

###################################################
# Main
###################################################

# Main script entry point
def __main__():
    print "======================== null_blb_run ========================"
    # Initialize command line parser
    parser = argparse.ArgumentParser()
    # Optional argument(s)
    parser.add_argument('-x', '--exe', default=DEF_EXE_PATH,
                        help='Path to BLB commandlet executable')
    parser.add_argument('-jid', '--job_id', default=DEF_JOB_ID,
                        help='Job identifier prefix')
    parser.add_argument('-threads', '--num_threads', default=DEF_NUM_THREADS,
                        help='Number of worker threads (<=0 -> default)')
    # Parse command line
    args = parser.parse_args()
    # Print command line
    print '\t[--exe] = ' + str(args.exe)
    print '\t[--job_id] = ' + str(args.job_id)
    print '\t[--num_threads] = ' + str(args.num_threads)
    # Feed to utility
    run_blb(exe=args.exe, job_id=args.job_id,
            num_threads=int(args.num_threads))

# Run main if we are the active script
if __name__ == '__main__':
    __main__()
