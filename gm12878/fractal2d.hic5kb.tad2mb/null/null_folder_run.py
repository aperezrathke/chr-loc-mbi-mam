#!/usr/bin/python
# -*- coding: utf-8 -*-

# Generates 3-D polymers using null model

###################################################
# Imports
###################################################

# For path manipulation 
import os

# For launching shell scripts
import subprocess

# For generating random unique identifiers
import uuid
import base64

# For flushing stdout
import sys

###################################################
# Globals
###################################################

# Path to directory containing this script
# https://stackoverflow.com/questions/4934806/how-can-i-find-scripts-directory-with-python
SCRIPT_DIR = os.path.abspath(os.path.dirname(os.path.realpath(__file__)))
SIM_ID = os.path.basename(SCRIPT_DIR)

# Determine dispatch
DISPATCH_ID = "null"
DISPATCH_KEY = "--" + DISPATCH_ID + "_dispatch"
DISPATCH_VAL = "loc_" + DISPATCH_ID

# Root module directory
MODULE_DIR = os.path.join(SCRIPT_DIR, "..", "..", "..")
MODULE_DIR = os.path.abspath(MODULE_DIR)

# Common utilities directory
MODULE_COMMON_DIR = os.path.join(MODULE_DIR, "common")

# Configuration type
CONFIG_ID = "fractal2d"

# Path to INI
CONF_PATH = os.path.join(MODULE_COMMON_DIR, "scripts", "config", CONFIG_ID,
                         "folder." + DISPATCH_ID + ".parent.ini")

# Overload archetype
ARCH_PATH = os.path.join(SCRIPT_DIR, "config", "folder.arch.ini")

# Output base directory
OUT_BASE_DIR = os.path.join(SCRIPT_DIR, "..", "output")
OUT_BASE_DIR = os.path.abspath(OUT_BASE_DIR)

# Target data directory
OUT_TARGET_DIR = os.path.join(OUT_BASE_DIR, SIM_ID)

# Exe path
EXE_PATH = os.path.join(OUT_BASE_DIR, "build", "bin", "Release_threaded",
                        "u_sac")

# Unique job identifier prefix
JOB_PREFIX = DISPATCH_ID + "." + \
                base64.urlsafe_b64encode(uuid.uuid4().bytes).rstrip('=')

# Export options
EXPORT = ["-export_csv_gz", "-export_pdb_gz", "-export_ligc_csv_gz",
          "-export_log_weights"]

###################################################
# Run
###################################################

# Initialize iterator variables
current = 0
target = 500000
step = 1000
job_id = 0

# Loop until finished
while current < target:
    ens_size = min(step , target - current)
    job_name = JOB_PREFIX + "." + str(job_id)
    # Build command line
    cmd = [EXE_PATH, DISPATCH_KEY, DISPATCH_VAL, "--conf", CONF_PATH,
           "--arch", ARCH_PATH, "--output_dir", OUT_TARGET_DIR,
           "--ensemble_size", str(ens_size), "--job_id", job_name]
    cmd.extend(EXPORT)
    print "Running:"
    print " ".join(cmd)
    sys.stdout.flush()
    # Run job
    subprocess.call(cmd)
    current += ens_size
    job_id += 1

print "Finished."
