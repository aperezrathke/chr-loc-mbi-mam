#!/bin/bash

# Path to this script's directory
SCRIPT_PATH="$(readlink -f $0)"
SCRIPT_DIR="$(dirname $SCRIPT_PATH)"
SCRIPT_DIR=$(cd "$SCRIPT_DIR"; pwd)

# Build command line
FROM_RMVOUT_KEY="FROM_RMVOUT"
cmd="Rscript --vanilla $SCRIPT_DIR/null_cache_shell.R"
cmd="$cmd --util_null_cache_ligc_csv"
cmd="$cmd --util_null_cache_ligc_merge --rmvout"
cmd="$cmd --ligc_rdata_path $FROM_RMVOUT_KEY"
cmd="$cmd --util_null_cache_ligc_freqmat"
cmd="$cmd --ligc_freqmat_csv_gz_path $FROM_RMVOUT_KEY"
cmd="$cmd --ligc_freqmat_rdata_path $FROM_RMVOUT_KEY"
cmd="$cmd --util_null_cache_ligc_popd_lband_csv"
cmd="$cmd --util_null_cache_ligc_popd_lband_merge"

# Run command line
echo "Running:"
echo $cmd
$cmd
echo "Finished."

# Also create ligc merge for outlier retained data
cmd="Rscript --vanilla $SCRIPT_DIR/null_cache_shell.R"
cmd="$cmd --util_null_cache_ligc_merge --no-rmvout"
cmd="$cmd --ligc_rdata_path $FROM_RMVOUT_KEY"
cmd="$cmd --util_null_cache_ligc_freqmat"
cmd="$cmd --ligc_freqmat_csv_gz_path $FROM_RMVOUT_KEY"
cmd="$cmd --ligc_freqmat_rdata_path $FROM_RMVOUT_KEY"

# Run command line
echo "Running:"
echo $cmd
$cmd
echo "Finished."
