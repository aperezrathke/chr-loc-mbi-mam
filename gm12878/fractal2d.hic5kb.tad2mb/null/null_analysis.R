###################################################################
# null_analysis.R
#
# Generates heat maps and performs correlation analysis
###################################################################

# Guard variable to avoid multiple sourcing
SOURCED_null_analysis = TRUE

###################################################################
# Path to this script
###################################################################

# Cached directory to script
# https://stackoverflow.com/questions/3452086/getting-path-of-an-r-script
NULL_ANALYSIS_SCRIPT_DIR = getSrcDirectory(function(x) {
  x
})

# getSrcDirectory trick will not work via Rscript. If this is the
# case, then NULL_ANALYSIS_SCRIPT_DIR will be length zero.
# http://stackoverflow.com/questions/1815606/rscript-determine-path-of-the-executing-script
if (length(NULL_ANALYSIS_SCRIPT_DIR) == 0) {
  NULL_ANALYSIS_SCRIPT_DIR = dirname(scriptName::current_filename())
}

# @return Path to directory containing this script
get_null_analysis_script_dir <- function() {
  return(NULL_ANALYSIS_SCRIPT_DIR)
}

###################################################################
# Source utility script(s)
###################################################################

# Cache utilities
if (!exists("SOURCED_null_cache")) {
  source(file.path(get_null_analysis_script_dir(), "null_cache.R"))
}

# Heatmap utilities
source(file.path(get_local_common_dir(), "heat_core.R"))

# Parallel utilities
source(file.path(tad_core$MODULE_COMMON_DIR, "scripts", "utils.parallel.R"))

###################################################################
# Full ensemble heat launcher
###################################################################

# Default heatmap format extension
DEF_HEAT_EXT = "png"

# Default heatmap PNG pixels
DEF_HEAT_PNG_PIX = 512

# PNG heatmaps default enable
DEF_HEAT_PNG_ENABLE = TRUE

# Default heatmap PDF inches
DEF_HEAT_PDF_IN = 7

# PDF heatmaps take up lots of disk space
DEF_HEAT_PDF_ENABLE = FALSE

# Whether null heatmaps are rank converted by default
DEF_HEAT_NULL_AS_RANK = FALSE

# Whether null heatmaps are divided by max by default
DEF_HEAT_NULL_DIV_BY_MAX = DEF_HEAT_NULL_AS_RANK

# Default heatmap vmax
DEF_HEAT_NULL_VMAX = if (DEF_HEAT_NULL_AS_RANK) {
  1.0
} else {
  0.35
}

# @param rmvout - If TRUE, outliers are assumed removed
# @param ext_no_dot - image format
# @return image file path
get_null_ligc_freqmat_heat_path <-
  function(rmvout = tad_core$NULL_REMOVE_OUTLIERS,
           ext_no_dot = DEF_HEAT_EXT) {
    based = tad_core$NULL_BASE_OUT_DIR
    prefix = smush("ligc", "freqmat", "heat")
    subd = smush(prefix, ext_no_dot)
    ro = if (rmvout) {
      "1"
    } else {
      "0"
    }
    fname = smush(prefix, "rmvout", ro, ext_no_dot)
    return(file.path(based, subd, fname))
  }

# Generates heatmaps for null ligation contact frequencies
# @param in_rdata_path - Input contact frequency matrtix
# @param out_png_path - Output PNG format heatmap path
# @param png_pix - PNG width and height pixels
# @param out_pdf_path - Output PDF format heatmap path
# @param pdf_in - PDF width and height inches
# @param verbose = If TRUE, log to stdout
# @param vmax - Max heatmap anchor value
# @param as_rank - Whether heatmap is rank converted
# @param div_by_max - whether heatmap is divided by max element
# @param ... remaining arguments to heat_ligc_freqmat(...)
launch_null_heat_ligc_freqmat <-
  function(in_rdata_path = tad_core$get_null_ligc_freqmat_rdata_path(),
           out_png_path = get_null_ligc_freqmat_heat_path(ext_no_dot =
                                                            "png"),
           png_pix = DEF_HEAT_PNG_PIX,
           out_pdf_path = get_null_ligc_freqmat_heat_path(ext_no_dot =
                                                            "pdf"),
           pdf_in = DEF_HEAT_PDF_IN,
           verbose = DEF_VERBOSE,
           vmax = DEF_HEAT_NULL_VMAX,
           as_rank = DEF_HEAT_NULL_AS_RANK,
           div_by_max = DEF_HEAT_NULL_DIV_BY_MAX,
           ...) {
    # Load contact frequencies
    freqmat = load_rdata(rdata_path = in_rdata_path, verbose = verbose)
    # PNG format heatmap
    if (is.character(out_png_path)) {
      if (verbose) {
        print(paste0("Generating ", out_png_path))
      }
      make_subdirs(fpath = out_png_path)
      png(file = out_png_path,
          width = png_pix,
          height = png_pix)
      heat_ligc(
        mat = freqmat,
        vmax = vmax,
        as_rank = as_rank,
        div_by_max = div_by_max,
        ...
      )
      dev.off()
    }
    # PDF format heatmap
    if (is.character(out_pdf_path)) {
      if (verbose) {
        print(paste0("Generating ", out_pdf_path))
      }
      make_subdirs(fpath = out_pdf_path)
      pdf(file = out_pdf_path,
          width = pdf_in,
          height = pdf_in)
      heat_ligc(
        mat = freqmat,
        vmax = vmax,
        as_rank = as_rank,
        div_by_max = div_by_max,
        ...
      )
      dev.off()
    }
    if (verbose) {
      print(paste0("Finished"))
    }
  }

###################################################################
# Bootstrap replicate analysis
###################################################################

# @return Base directory for ligc popd analysis output
get_null_ligc_popd_analysis_dir <- function() {
  return(file.path(tad_core$NULL_BASE_OUT_DIR, "ligc.popd.analysis"))
}

# @return Path to store correlations of BLB replicates to full
#   (outlier removed) ensemble
get_null_ligc_popd_analysis_corr_csv_path <- function() {
  return(file.path(get_null_ligc_popd_analysis_dir(), "ligc.popd.corrs.csv"))
}

# @param ext_no_dot - image format extension (no '.')
# @return Directory to store BLB replicate heat maps
get_null_ligc_popd_analysis_heat_dir <-
  function(ext_no_dot = DEF_HEAT_EXT) {
    return(file.path(
      get_null_ligc_popd_analysis_dir(),
      smush("heat", ext_no_dot)
    ))
  }

# Bootstrap (BLB) correlation and heatmap analysis
# @param in_ligc_popd_lband_rdata_dir - Directory containing BLB
#   replicates in lower-banded matrix RData format
# @param in_ligc_freqmat_rdata_path - Path to full (outlier removed)
#   ensemble frequency matrix
# @param out_corr_csv - Path to store BLB replicate correlations to
#   full ensemble frequency matrix in CSV format
# @param out_heat_png_dir - Directory to store PNG heat maps
# @param png_enable - If FALSE, no PNG heatmaps will be written
# @param png_pix - PNG width and height pixels
# @param out_heat_pdf_dir - Directory to store PDF heat maps
# @param pdf_enable - If FALSE, no PDF heatmaps will be written
# @param pdf_in - PDF width and height inches
# @param verbose - If TRUE, log to stdout
# @param vmax - Max heatmap anchor value
# @param as_rank - Whether heatmap is rank converted
# @param div_by_max - whether heatmap is divided by max element
# @param parallel_opt - one of "multicore" | "snow" | "no"
#   Specifies parallel processing platform
#   - multicore: uses "multicore" package only available on linux
#   - snow: uses "snow" package available on all platforms
#   - no: disable parallel processing
# @param ncpu - Number of CPUs to use for parallel computation
#   If ncpu <= 0, then all available CPUs are used!
# @param ... remaining arguments to heat_ligc_freqmat(...)
launch_null_analysis_ligc_popd_lband <-
  function(in_ligc_popd_lband_rdata_dir = tad_core$NULL_LIGC_POPD_LBAND_RDATA_DIR,
           in_ligc_freqmat_rdata_path = tad_core$get_null_ligc_freqmat_rdata_path(rmvout =
                                                                                    TRUE),
           out_corr_csv_path = get_null_ligc_popd_analysis_corr_csv_path(),
           out_heat_png_dir = get_null_ligc_popd_analysis_heat_dir(ext_no_dot =
                                                                     "png"),
           png_enable = DEF_HEAT_PNG_ENABLE,
           png_pix = DEF_HEAT_PNG_PIX,
           out_heat_pdf_dir = get_null_ligc_popd_analysis_heat_dir(ext_no_dot =
                                                                     "pdf"),
           pdf_enable = DEF_HEAT_PDF_ENABLE,
           pdf_in = DEF_HEAT_PDF_IN,
           verbose = DEF_VERBOSE,
           vmax = DEF_HEAT_NULL_VMAX,
           as_rank = DEF_HEAT_NULL_AS_RANK,
           div_by_max = DEF_HEAT_NULL_DIV_BY_MAX,
           parallel_opt = DEF_PARALLEL_OPT,
           ncpu = DEF_NCPU,
           ...) {
    # From:
    #
    #   Bianco, S. et al. Polymer physics predicts the effects of structural
    #   variants on chromatin architecture. Nat. Genet. 50, 662-667 (2018).
    #
    #   To compute 'distance-corrected correlation', we divide each diagonal
    #   by the mean diagonal value
    to_dc <- function(out, delta) {
      for (k in 1:ncol(out)) {
        ix_k = (delta == k)
        out[ix_k] = out[ix_k] / max(mean(out[ix_k]), .Machine$double.eps)
      }
      # Make symmetric
      d = diag(out)
      out[lower.tri(out)] = 0.0
      out = out + t(out)
      diag(out) = d
      return(out)
    }
    # Convert list of diagonals to symmetric matrix
    to_mat <- function(blb, delta) {
      out = matrix(data = 0.0,
                   nrow = nrow(delta),
                   ncol = ncol(delta))
      if (ncol(out) >= 3) {
        for (k in 3:ncol(out)) {
          out[delta == (k - 1)] = blb[[k]]
        }
      }
      # Make symmetric
      out[lower.tri(out)] = 0.0
      out[delta == 1] = 1.0
      out = out + t(out)
      diag(out) = 1.0
      return(out)
    }
    # Load full (outlier removed) ensemble contact frequencies
    freqmat_full_raw = load_rdata(rdata_path = in_ligc_freqmat_rdata_path,
                                  verbose = verbose)
    delta = col(freqmat_full_raw) - row(freqmat_full_raw)
    # DC correct full ensemble
    freqmat_full_dc = to_dc(out = freqmat_full_raw, delta = delta)
    # Get list of input BLB replicate files
    fnames_in = list.files(
      path = in_ligc_popd_lband_rdata_dir,
      all.files = FALSE,
      full.names = FALSE,
      recursive = FALSE,
      include.dirs = FALSE,
      no.. = TRUE
    )
    # Allocate output correlations data.frame
    corr_df = data.frame(
      fid = tools::file_path_sans_ext(fnames_in),
      pears_raw = rep(0.0, length(fnames_in)),
      pears_dc = rep(0.0, length(fnames_in)),
      spear_raw = rep(0.0, length(fnames_in)),
      spear_dc = rep(0.0, length(fnames_in)),
      stringsAsFactors = FALSE
    )
    # Correlation indices
    IX_PEARS_RAW = 1
    IX_PEARS_DC = 2
    IX_SPEAR_RAW = 3
    IX_SPEAR_DC = 4
    NUM_CORR = 4
    # Only correlate diagonals at or above k_min
    k_min = 2
    k_min_ix = delta >= k_min
    k_min_full_raw = freqmat_full_raw[k_min_ix]
    k_min_full_dc = freqmat_full_dc[k_min_ix]
    # Computes correlations and heatmaps for parameter BLB replicate
    process_blb_rep <- function(i, ...) {
      # Load BLB replicate
      fpath_in = file.path(in_ligc_popd_lband_rdata_dir, fnames_in[i])
      blb_rep = load_rdata(rdata_path = fpath_in, verbose = verbose)
      # Convert BLB to matrix
      blb_mat_raw = to_mat(blb = blb_rep, delta = delta)
      blb_mat_dc = to_dc(out = blb_mat_raw, delta = delta)
      # Extract diagonals at or above k_min
      k_min_blb_raw = blb_mat_raw[k_min_ix]
      k_min_blb_dc = blb_mat_dc[k_min_ix]
      # Compute correlations
      corr_v = rep(0.0, NUM_CORR)
      corr_v[IX_PEARS_RAW] = cor(x = k_min_full_raw, y = k_min_blb_raw,
                                 method = "pearson")
      corr_v[IX_PEARS_DC] = cor(x = k_min_full_dc, y = k_min_blb_dc,
                                method = "pearson")
      corr_v[IX_SPEAR_RAW] = cor(x = k_min_full_raw, y = k_min_blb_raw,
                                 method = "spearman")
      corr_v[IX_SPEAR_DC] = cor(x = k_min_full_dc, y = k_min_blb_dc,
                                method = "spearman")
      # Generate PNG heatmap
      fid = corr_df$fid[i]
      if (png_enable && is.character(out_heat_png_dir)) {
        png_path = file.path(out_heat_png_dir, paste0(fid, ".png"))
        make_subdirs(png_path)
        png(filename = png_path,
            width = png_pix,
            height = png_pix)
        heat_ligc(
          mat = blb_mat_raw,
          vmax = vmax,
          as_rank = as_rank,
          div_by_max = div_by_max,
          ...
        )
        dev.off()
      }
      # Generate PDF heatmap
      if (pdf_enable && is.character(out_heat_pdf_dir)) {
        pdf_path = file.path(out_heat_pdf_dir, paste0(fid, ".pdf"))
        make_subdirs(pdf_path)
        pdf(filename = pdf_path,
            width = pdf_in,
            height = pdf_in)
        heat_ligc(
          mat = blb_mat_raw,
          vmax = vmax,
          as_rank = as_rank,
          div_by_max = div_by_max,
          ...
        )
        dev.off()
      }
      return(corr_v)
    }
    # Analyze BLB replicates
    corr_mat = sapply_router(
      X = 1:length(fnames_in),
      FUN = process_blb_rep,
      parallel_opt = parallel_opt,
      ncpu = ncpu,
      mc.preschedule = TRUE,
      verbose = verbose,
      ...
    )
    # Extract correlations
    stopifnot(all(is.finite(corr_mat)))
    stopifnot(nrow(corr_mat) == NUM_CORR)
    stopifnot(ncol(corr_mat) == nrow(corr_df))
    corr_df$pears_raw = corr_mat[IX_PEARS_RAW, ]
    corr_df$pears_dc = corr_mat[IX_PEARS_DC, ]
    corr_df$spear_raw = corr_mat[IX_SPEAR_RAW, ]
    corr_df$spear_dc = corr_mat[IX_SPEAR_DC, ]
    # Save correlations
    save_csv(
      x = corr_df,
      csv_path = out_corr_csv_path,
      row.names = FALSE,
      col.names = TRUE,
      verbose = verbose
    )
    return(invisible(corr_df))
  }
