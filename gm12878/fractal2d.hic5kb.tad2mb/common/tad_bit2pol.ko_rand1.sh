#!/bin/bash

# Script runs bit2pol for CMX pseudo-populations with random knock-out folding

# Usage:
#   nohup <path_to_this_script> &> <path_to_log_file> &

###################################################
# SCRIPT PATHS
###################################################

# Path to this script's directory
SCRIPT_PATH="$(readlink -f $0)"
SCRIPT_DIR="$(dirname $SCRIPT_PATH)"
# Path to corresonding python shell script
PY_SCRIPT_PATH="$SCRIPT_DIR/tad_bit2pol.py"

###################################################
# RUN TARGET
###################################################

cmd="python $PY_SCRIPT_PATH --ko_rand"
echo "Running: $cmd"
$cmd
echo "Finished."
