#!/usr/bin/env Rscript

###################################################################
# tad_mbi_shell.R
#
# Command-line interface for null many body interaction routines
###################################################################

# Similar to python optparse package
library(optparse)

###################################################################
# Path to this script
###################################################################

# Cached directory to script
# https://stackoverflow.com/questions/3452086/getting-path-of-an-r-script
TAD_MBI_SHELL_SCRIPT_DIR = getSrcDirectory(function(x) {
  x
})

# getSrcDirectory trick will not work via Rscript. If this is the
# case, then TAD_MBI_SHELL_SCRIPT_DIR will be length zero.
# http://stackoverflow.com/questions/1815606/rscript-determine-path-of-the-executing-script
if (length(TAD_MBI_SHELL_SCRIPT_DIR) == 0) {
  get_script_dir <- function() {
    cmdArgs = commandArgs(trailingOnly = FALSE)
    needle = "--file="
    ixmatch = grep(needle, cmdArgs)
    if (length(match) > 0) {
      # Rscript
      return(dirname(normalizePath(sub(
        needle, "", cmdArgs[ixmatch]
      ))))
    }
    return(dirname(scriptName::current_filename()))
  }
  TAD_MBI_SHELL_SCRIPT_DIR = get_script_dir()
}

# @return Path to directory containing this script
get_tad_mbi_shell_script_dir <- function() {
  return(TAD_MBI_SHELL_SCRIPT_DIR)
}

###################################################################
# Source utility script(s)
###################################################################

# Core many-body utilities
if (!exists("SOURCED_tad_mbi")) {
  source(file.path(get_tad_mbi_shell_script_dir(), "tad_mbi.R"))
}

###################################################################
# Parse command line
###################################################################

# https://www.r-bloggers.com/passing-arguments-to-an-r-script-from-command-lines/
option_list = list(
  # Utilities (in order encountered)
  make_option(
    c("-mbi_cache_null_counts",
      "--util_mbi_cache_null_counts"),
    action = "store_true",
    default = FALSE,
    help = "Utility converts random ligc (i,j) tuples into many body interaction counts"
  ),
  make_option(
    c("-mbi_cache_b2p_counts",
      "--util_mbi_cache_b2p_counts"),
    action = "store_true",
    default = FALSE,
    help = "Utility converts bit2pol ligc (i,j) tuples into many body interaction counts"
  ),
  make_option(
    c("-mbi_cache_boot",
      "--util_mbi_cache_boot"),
    action = "store_true",
    default = FALSE,
    help = "Utility computes random (null) bootstrap distribution over many body interactions"
  ),
  make_option(
    c("-mbi_cache_pvals",
      "--util_mbi_cache_pvals"),
    action = "store_true",
    default = FALSE,
    help = "Utility captures p-values of observed many body interactions"
  ),
  make_option(
    c("-mbi_heat_prin_loop",
      "--util_mbi_heat_prin_loop"),
    action = "store_true",
    default = FALSE,
    help = "Utility generates heat maps of many-body principal loop interactions"
  ),
  # Options (in order encountered)
  ################## TAD MODEL FILTERING
  # model_index
  make_option(
    c("-mix0", "--model_index0"),
    type = "integer",
    default = NULL,
    help = "If not NULL, will only process model at this *0-based* index"
  ),
  make_option(
    c("-mflt", "--model_filt_id"),
    type = "character",
    default = tad_core$MBI_MODEL_FILT_ID,
    help = paste0(
      "Optional name of TAD model filter, ",
      paste0(TAD_MODEL_FILT_IDS, collapse = "|")
    )
  ),
  ################## CORE OPTIONS
  # tad base_dir
  make_option(
    c("-tbd", "--tad_base_dir"),
    type = "character",
    default = tad_core$TAD_BASE_OUT_DIR,
    help = "Base TAD loci directory"
  ),
  # from_table
  make_option(
    c("-ft", "--from_table"),
    type = "integer",
    default = DEF_MBI_FROM_TABLE,
    help = "1: load data from scale table, 0: load from file system listing"
  ),
  # null_rmvout
  make_option(
    c("-no-nullrmo", "--no-null_rmvout"),
    action = "store_false",
    dest = "null_rmvout",
    default = tad_core$NULL_REMOVE_OUTLIERS,
    help = "Disable outlier removal"
  ),
  make_option(
    c("-nullrmo", "--null_rmvout"),
    action = "store_true",
    dest = "null_rmvout",
    default = tad_core$NULL_REMOVE_OUTLIERS,
    help = "Enable outlier removal"
  ),
  # null ens_sz
  make_option(
    c("-nensz", "--null_ens_sz"),
    type = "integer",
    default = tad_core$MBI_NULL_ENS_SZ,
    help = "Max size of random ensemble"
  ),
  # should_crop
  make_option(
    c("-no-cr", "--no-crop"),
    action = "store_false",
    dest = "crop",
    default = tad_core$B2P_CROP,
    help = "Data not assumed (nor will be) trimmed to TAD model region"
  ),
  make_option(
    c("-cr", "--crop"),
    action = "store_true",
    dest = "crop",
    default = tad_core$B2P_CROP,
    help = "Data assumed (or will be) trimmed to TAD model region"
  ),
  # edge_min_gd
  make_option(
    c("-edgd", "--edge_min_gd"),
    type = "integer",
    default = tad_core$MBI_EDGE_MIN_GD,
    help = "All |j-i| tuples < min_gd are filtered"
  ),
  # clq_min
  make_option(
    c("-cqmn", "--clq_min"),
    type = "integer",
    default = tad_core$MBI_CLQ_MIN,
    help = "Minimum clique size"
  ),
  # clq_max
  make_option(
    c("-cqmx", "--clq_max"),
    type = "integer",
    default = tad_core$MBI_CLQ_MAX,
    help = "Maximum clique size"
  ),
  # clq_map_id
  make_option(
    c("-cqmp", "--clq_map_id"),
    type = "character",
    default = tad_core$MBI_CLQ_MAP_ID,
    help = paste0(
      "Clique mapping method identifier, ",
      paste0(MBI_CLQ_MAP_IDS, collapse = "|")
    )
  ),
  # clq_type
  make_option(
    c("-cqty", "--clq_type"),
    type = "character",
    default = tad_core$MBI_CLQ_TYPE,
    help = paste0(
      "Clique calling algorithm, ",
      paste0(MBI_CLQ_TYPES, collapse = "|")
    )
  ),
  # mbi_pvtest_by_id
  make_option(
    c("-mbipvtby", "--mbi_pvtest_by_id"),
    type = "character",
    default = tad_core$MBI_PVTEST_BY_ID,
    help = paste0(
      "MBI p-value testing stratification, ",
      paste0(MBI_PVTEST_BY_IDS, collapse = "|")
    )
  ),
  # mbi tag
  make_option(
    c("-mbitg", "--mbi_tag"),
    type = "character",
    default = tad_core$MBI_TAG,
    help = "Optional suffix for many body interaction data"
  ),
  # ncpu
  make_option(
    c("-ncpu", "--num_cpu"),
    type = "integer",
    default = DEF_NCPU,
    help = "Number of CPUs for parallel processing"
  ),
  # verbose
  make_option(
    c("-no-verb", "--no-verbose"),
    action = "store_false",
    dest = "verbose",
    default = DEF_VERBOSE,
    help = "Disable verbose logging to stdout"
  ),
  make_option(
    c("-verb", "--verbose"),
    action = "store_true",
    dest = "verbose",
    default = DEF_VERBOSE,
    help = "Enable verbose logging to stdout"
  ),
  # overwrite
  make_option(
    c("-no-ov", "--no-overwrite"),
    action = "store_false",
    dest = "overwrite",
    default = DEF_SHOULD_OVERWRITE,
    help = "Disable overwrite of cached data"
  ),
  make_option(
    c("-ov", "--overwrite"),
    action = "store_true",
    dest = "overwrite",
    default = DEF_SHOULD_OVERWRITE,
    help = "Enable overwrite of cached data"
  ),
  ##############################
  # bit2pol options
  # ko_rand
  make_option(
    c("-no-kor", "--no-ko_rand"),
    action = "store_false",
    dest = "ko_rand",
    default = tad_core$B2P_KO_RAND,
    help = "Assume knock-outs enforced"
  ),
  make_option(
    c("-kor", "--ko_rand"),
    action = "store_true",
    dest = "ko_rand",
    default = tad_core$B2P_KO_RAND,
    help = "Assume knock-outs not enforced (random)"
  ),
  # b2p tag
  make_option(
    c("-b2ptg", "--b2p_tag"),
    type = "character",
    default = tad_core$B2P_TAG,
    help = "Optional suffix for bit2pol geometry directories"
  ),
  # pop_key
  make_option(
    c("-pop", "--pop_key"),
    type = "character",
    default = tad_core$B2P_POP_KEY,
    help = "Assumed CMX population model (spop|mpop)"
  ),
  # cmx tag
  make_option(
    c("-cmxtg", "--cmx_tag"),
    type = "character",
    default = tad_core$CMX_TAG,
    help = "Optional suffix for CMX results directories"
  ),
  # top_only
  make_option(
    c("-no-to", "--no-top_only"),
    action = "store_false",
    dest = "top_only",
    default = tad_core$B2P_TOP_ONLY,
    help = "Process all encountered bit2pol geometry folders"
  ),
  make_option(
    c("-to", "--top_only"),
    action = "store_true",
    dest = "top_only",
    default = tad_core$B2P_TOP_ONLY,
    help = "Process only top-level (lowest depth) bit2pol geometry folders"
  ),
  # b2p_rmvout
  make_option(
    c("-no-b2prmo", "--no-b2p_rmvout"),
    action = "store_false",
    dest = "b2p_rmvout",
    default = tad_core$B2P_REMOVE_OUTLIERS,
    help = "Disable outlier removal"
  ),
  make_option(
    c("-b2prmo", "--b2p_rmvout"),
    action = "store_true",
    dest = "b2p_rmvout",
    default = tad_core$B2P_REMOVE_OUTLIERS,
    help = "Enable outlier removal"
  ),
  # b2p_eqw
  make_option(
    c("-no-b2pew", "--no-b2p_eqw"),
    action = "store_false",
    dest = "b2p_eqw",
    default = tad_core$B2P_EQW,
    help = "Empirical data not assumed to be from target distribution"
  ),
  make_option(
    c("-b2pew", "--b2p_eqw"),
    action = "store_true",
    dest = "b2p_eqw",
    default = tad_core$B2P_EQW,
    help = "Empirical data assumed to be from target distribution"
  ),
  ##############################
  # boot options
  # stat_id
  make_option(
    c("-btstid", "--boot_stat_id"),
    type = "character",
    default = tad_core$MBI_BOOT_STAT_ID,
    help = paste0("Bootstrap statistic, ",
                  paste0(MBI_STAT_IDS, collapse = "|"))
  ),
  # reps
  make_option(
    c("-boot_reps", "--boot_reps"),
    type = "integer",
    default = tad_core$MBI_BOOT_REPS,
    help = "Number of boostrap replicates"
  ),
  ################## PLOT ARGUMENTS
  make_option(
    c("-hf", "--heat_format"),
    type = "character",
    default = DEF_TAD_HEAT_FORMAT,
    help = "Heatmap image format, either png|pdf|svg"
  ),
  make_option(
    c("-hwp", "--heat_width_pix"),
    type = "double",
    default = DEF_TAD_HEAT_DIM_PIX,
    help = "Heatmap pixel width, for raster formats such as png"
  ),
  make_option(
    c("-hhp", "--heat_height_pix"),
    type = "double",
    default = DEF_TAD_HEAT_DIM_PIX,
    help = "Heatmap pixel height, for raster formats such as png"
  ),
  make_option(
    c("-hwi", "--heat_width_in"),
    type = "double",
    default = DEF_TAD_HEAT_DIM_IN,
    help = "Heatmap inch width, for vector formats such as pdf"
  ),
  make_option(
    c("-hhi", "--heat_height_in"),
    type = "double",
    default = DEF_TAD_HEAT_DIM_IN,
    help = "Heatmap inch height, for vector formats such as pdf"
  ),
  make_option(
    c("-no-har", "--no-heat_as_rank"),
    action = "store_false",
    dest = "heat_as_rank",
    default = DEF_B2P_HEAT_AS_RANK,
    help = "Disable heatmap over data set ranks"
  ),
  make_option(
    c("-har", "--heat_as_rank"),
    action = "store_true",
    dest = "heat_as_rank",
    default = DEF_B2P_HEAT_AS_RANK,
    help = "Enable heatmap over data set ranks"
  ),
  make_option(
    c("-no-hdbm", "--no-heat_div_by_max"),
    action = "store_false",
    dest = "heat_div_by_max",
    default = DEF_B2P_HEAT_AS_RANK,
    help = "Disable divide heatmap data set by max value"
  ),
  make_option(
    c("-hdbm", "--heat_div_by_max"),
    action = "store_true",
    dest = "heat_div_by_max",
    default = DEF_B2P_HEAT_AS_RANK,
    help = "Enable divide heatmap data set by max value"
  ),
  make_option(
    c("-hvmi", "--heat_vmin"),
    type = "double",
    default = DEF_TAD_HEAT_VMIN,
    help = "Heatmap min anchor value ala seaborn"
  ),
  make_option(
    c("-hvma", "--heat_vmax"),
    type = "double",
    default = get_def_tad_heat_vmax(DEF_B2P_HEAT_AS_RANK),
    help = "Heatmap max anchor value ala seaborn"
  ),
  make_option(
    c("-hvdi", "--heat_vdia"),
    type = "double",
    default = get_def_mbi_heat_vdia(DEF_B2P_HEAT_AS_RANK),
    help = "Heatmap main diagonal standard value, ignored if < 0"
  ),
  make_option(
    c("-hvdf", "--heat_vdia_full"),
    action = "store_true",
    dest = "heat_vdia_full",
    default = get_def_mbi_heat_vdia_full(get_def_mbi_heat_vdia(DEF_B2P_HEAT_AS_RANK)),
    help = "Full diagonal is ovewritten by 'heat_vdia' if >= 0"
  ),
  make_option(
    c("-no-hvdf", "--no-heat_vdia_full"),
    action = "store_false",
    dest = "heat_vdia_full",
    default = get_def_mbi_heat_vdia_full(get_def_mbi_heat_vdia(DEF_B2P_HEAT_AS_RANK)),
    help = "Top, left cell is overwritten by 'heat_vdia' if >= 0"
  ),
  make_option(
    c("-htb", "--heat_tick_by"),
    type = "double",
    default = DEF_HEAT_TICK_BY,
    help = "Heatmap axis tick interval"
  ),
  make_option(
    c("-no-hkey", "--no-heat_key"),
    action = "store_false",
    dest = "heat_show_key",
    default = DEF_HEAT_SHOW_KEY,
    help = "Disable heatmap legend key"
  ),
  make_option(
    c("-hkey", "--heat_key"),
    action = "store_true",
    dest = "heat_show_key",
    default = DEF_HEAT_SHOW_KEY,
    help = "Enable heatmap legend key"
  ),
  make_option(
    c("-no-hfp", "--no-heat_fdr_ptsov"),
    action = "store_false",
    dest = "heat_fdr_ptsov",
    default = DEF_TAD_HEAT_FDR_PTSOV,
    help = "Disable heatmap overlay of specific contacts according to '--heat_fdr_thresh'"
  ),
  make_option(
    c("-hfp", "--heat_fdr_ptsov"),
    action = "store_true",
    dest = "heat_fdr_ptsov",
    default = DEF_TAD_HEAT_FDR_PTSOV,
    help = "Enable heatmap overlay of specific contacts according to '--heat_fdr_thresh'"
  ),
  make_option(
    c("-hft", "--heat_fdr_thresh"),
    type = "double",
    default = get_def_tad_heat_fdr_thresh(),
    help = "If specific contacts heatmap overlay enabled ('--heat_fdr_ptsov'), highlight points with FDR < than this value"
  ),
  make_option(
    c("-ht", "--heat_tag"),
    type = "character",
    default = DEF_TAD_HEAT_TAG,
    help = "[Optional] Additional heatmap identifier string"
  )
)

opt_parser = OptionParser(option_list = option_list)

opt = parse_args(opt_parser)

###################################################################
# Determine which utility to run
###################################################################

# Print command-line options
if (opt$verbose) {
  print("Options:")
  print(opt)
}

# Cache null (random) mbi counts
if (opt$util_mbi_cache_null_counts) {
  print("Running mbi_cache_null_counts()")
  launch_mbi_cache_null_counts(
    model_index0 = opt$model_index0,
    model_filt_id = opt$model_filt_id,
    base_dir = opt$tad_base_dir,
    from_table = opt$from_table,
    null_rmvout = opt$null_rmvout,
    ens_sz = opt$null_ens_sz,
    should_crop = opt$crop,
    edge_min_gd = opt$edge_min_gd,
    clq_min = opt$clq_min,
    clq_max = opt$clq_max,
    clq_map_id = opt$clq_map_id,
    clq_type = opt$clq_type,
    mbi_tag = opt$mbi_tag,
    ncpu = opt$num_cpu,
    verbose = opt$verbose,
    overwrite = opt$overwrite
  )
}

# Cache bit2pop (constrained) mbi counts
if (opt$util_mbi_cache_b2p_counts) {
  print("Running mbi_cache_b2p_counts()")
  launch_mbi_cache_b2p_counts(
    model_index0 = opt$model_index0,
    model_filt_id = opt$model_filt_id,
    base_dir = opt$tad_base_dir,
    from_table = opt$from_table,
    ko_rand = opt$ko_rand,
    b2p_tag = opt$b2p_tag,
    pop_key = opt$pop_key,
    cmx_tag = opt$cmx_tag,
    top_only = opt$top_only,
    b2p_rmvout = opt$b2p_rmvout,
    ens_sz = opt$null_ens_sz,
    should_crop = opt$crop,
    edge_min_gd = opt$edge_min_gd,
    clq_min = opt$clq_min,
    clq_max = opt$clq_max,
    clq_map_id = opt$clq_map_id,
    clq_type = opt$clq_type,
    mbi_tag = opt$mbi_tag,
    ncpu = opt$num_cpu,
    verbose = opt$verbose,
    overwrite = opt$overwrite
  )
}

# Cache random mbi bootstrap distribution
if (opt$util_mbi_cache_boot) {
  print("Running mbi_cache_boot()")
  launch_mbi_cache_boot(
    model_index0 =  opt$model_index0,
    model_filt_id = opt$model_filt_id,
    base_dir = opt$tad_base_dir,
    from_table = opt$from_table,
    stat_id = opt$boot_stat_id,
    reps = opt$boot_reps,
    null_rmvout = opt$null_rmvout,
    should_crop = opt$crop,
    edge_min_gd = opt$edge_min_gd,
    clq_min = opt$clq_min,
    clq_max = opt$clq_max,
    clq_map_id = opt$clq_map_id,
    clq_type = opt$clq_type,
    mbi_tag = opt$mbi_tag,
    ncpu = opt$num_cpu,
    verbose = opt$verbose,
    overwrite = opt$overwrite
  )
}

# Cache observed mbi p-values
if (opt$util_mbi_cache_pvals) {
  print("Running mbi_cache_pvals()")
  launch_mbi_cache_pvals(
    model_index0 =  opt$model_index0,
    model_filt_id = opt$model_filt_id,
    base_dir = opt$tad_base_dir,
    from_table = opt$from_table,
    ko_rand = opt$ko_rand,
    b2p_tag = opt$b2p_tag,
    pop_key = opt$pop_key,
    cmx_tag = opt$cmx_tag,
    top_only = opt$top_only,
    b2p_rmvout = opt$b2p_rmvout,
    b2p_eqw = opt$b2p_eqw,
    stat_id = opt$boot_stat_id,
    reps = opt$boot_reps,
    null_rmvout = opt$null_rmvout,
    should_crop = opt$crop,
    edge_min_gd = opt$edge_min_gd,
    clq_min = opt$clq_min,
    clq_max = opt$clq_max,
    clq_map_id = opt$clq_map_id,
    clq_type = opt$clq_type,
    mbi_pvtest_by_id = opt$mbi_pvtest_by_id,
    mbi_tag = opt$mbi_tag,
    ncpu = opt$num_cpu,
    verbose = opt$verbose,
    overwrite = opt$overwrite
  )
}

# Heatmap of mbi principal loops
if (opt$util_mbi_heat_prin_loop) {
  print("Running mbi_heat_prin_loop()")
  launch_mbi_heat_prin_loop(
    model_index0 =  opt$model_index0,
    model_filt_id = opt$model_filt_id,
    base_dir = opt$tad_base_dir,
    from_table = opt$from_table,
    ko_rand = opt$ko_rand,
    b2p_tag = opt$b2p_tag,
    pop_key = opt$pop_key,
    cmx_tag = opt$cmx_tag,
    top_only = opt$top_only,
    b2p_rmvout = opt$b2p_rmvout,
    b2p_eqw = opt$b2p_eqw,
    stat_id = opt$boot_stat_id,
    reps = opt$boot_reps,
    null_rmvout = opt$null_rmvout,
    should_crop = opt$crop,
    edge_min_gd = opt$edge_min_gd,
    clq_min = opt$clq_min,
    clq_max = opt$clq_max,
    clq_map_id = opt$clq_map_id,
    clq_type = opt$clq_type,
    mbi_pvtest_by_id = opt$mbi_pvtest_by_id,
    mbi_tag = opt$mbi_tag,
    heat_format = opt$heat_format,
    heat_width_pix = opt$heat_width_pix,
    heat_height_pix = opt$heat_height_pix,
    heat_width_in = opt$heat_width_in,
    heat_height_in = opt$heat_height_in,
    heat_as_rank = opt$heat_as_rank,
    heat_div_by_max = opt$heat_div_by_max,
    heat_vmin = opt$heat_vmin,
    heat_vmax = opt$heat_vmax,
    heat_vdia = opt$heat_vdia,
    heat_vdia_full = opt$heat_vdia_full,
    heat_tick_by = opt$heat_tick_by,
    heat_show_key = opt$heat_show_key,
    heat_fdr_ptsov = opt$heat_fdr_ptsov,
    heat_fdr_thresh = opt$heat_fdr_thresh,
    heat_tag = opt$heat_tag,
    ncpu = opt$num_cpu,
    verbose = opt$verbose
  )
}
