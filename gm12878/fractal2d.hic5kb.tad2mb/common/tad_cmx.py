#!/usr/bin/python
# -*- coding: utf-8 -*-

# Script runs CMX Gibbs sampler
#
# The following arguments are optional: (short-hand|long-hand)

#   [-bd|--base_dir]: [Optional] TAD base, non-versioned, output directory,
#       default is specified in tad_core.py
#   [-e|--exe] <path>: [Optional] Path to executable CMX binary with 'fea'
#       utility, default is given by tad_core.py
#   [-ft|--from_table <None|0|1|path>]: [Optional] If None|0, will use folder
#       listing in TAD base output directory for list of file names. If 1,
#       then will use default scale table path for obtaining model names. If
#       <path>, then will use user path to table file (must have 'name'
#       column) - if from table, then models are processed in order they are
#       listed in table file, else they are processed in order listed in file
#       system.
#   [-h|--help]: [Optional] Display help (i.e. usage information)
#   [-mf|--max_fail] <integer>: [Optional] Maximum number of failures allowed
#       for a single job configuration. If a job error code is encountered
#       more than this threshold, the job command line is reported and program
#       terminates
#   [-mix|--model_index] <integer>: [Optional] If specified, then will only
#       launch for parameter 0-based index into list returned by
#       tad_core.resolve_tad_model_names(). If not specified (default
#       behavior), then all TAD models are processed.
#   [-mp|--max_proc] <+integer>: [Optional] Maximum number of parallel
#       processes (or equivalently threads) to use to work through each
#       CMX Gibbs sampler run. Note: <--max_proc> should not exceed CPU
#       logical cores
#   [-pop|--pop_key]: [Optional] Population type: 'spop' (default) or 'mpop'
#   [-tg|--tag]: [Optional] Output folder tag suffix
#   [-nv|--naive]: [Optional] Output independent Bernoulli if no edge
#       restrictions found

###################################################
# Imports
###################################################

# For parsing user supplied arguments
import argparse

# For path testing
import os

# For default configuration settings
import tad_core

# For generating random unique identifiers
import uuid
import base64

# For Bernoulli coin flips
import numpy as np

###################################################
# Defaults
###################################################

# Default number of parallel processes, should not exceed logical cores
DEF_MAX_PROC = tad_core.MAX_PROC

# Default number of times a single job may return an abnormal error code. In
# my experience working on public compute clusters (with possibly ancient
# operating systems), background subprocesses may occasionally fail to launch
# and return with a non-zero error code. In this case, we can try to re-launch
# the process a few times to help mitigate this issue. If the subprocess still
# fails with an abnormal error code, then it's likely to be a user
# configuration error causing the folder simulation itself to crash!
DEF_MAX_FAIL = 2

# Default model index
DEF_MIX = None

###################################################
# TAD cmx
###################################################

# Exit program with error message if condition is true
# @param b_yes - If True, program is terminated
# @param msg - [Optional] error message to display prior to exit
# @param code - [Optional] error code to return on exit, default is 1
def exit_if(b_yes, msg="", code=1):
    '''If condition met, prints error message and exits program'''
    if b_yes:
        if msg:
            print "[tad_cmx] ERROR: " + msg
        print "[tad_cmx] EXITING (" + str(code) + ")"
        sys.exit(code)

# Load target Hi-C frequencies
def load_hic(hic_path):
    print "[tad_cmx] Loading:"
    print str(hic_path)
    assert os.path.exists(hic_path)
    out = []
    with open(hic_path) as hicfp:
        for line in hicfp:
            line = line.strip()
            # Ignore empty (all whitespace) lines
            if not line:
                continue
            # Ignore comment lines
            if line.startswith('#'):
                continue
            f = float(line)
            assert (f >= 0.0) and (f <= 1.0)
            out.append(f)
    print "[tad_cmx] Found " + str(len(out)) + " target Hi-C values"
    return out

# Load infeasible corpus
# @param infea_path - Path to infeasible corpus
# @param num_intr - Number of proximity interactions
# @param exit_on_error - Will exit if any format error encountered, else will
#   warn and skip to next row
def load_infea(infea_path, num_intr, exit_on_error=False):
    '''Determines if corpus is available and if it should be used'''
    assert num_intr > 0
    corp = set()
    num_skip = 0
    if os.path.isfile(infea_path):
        with open(infea_path) as fin:
            for line in fin:
                line = line.strip()
                # Ignore empty and comment lines
                if (not line) or line.startswith('#'):
                    continue
                # Parse CSV bit row
                bits = tuple([int(bit.strip()) for bit in line.split(',')])
                # Check expected number of bits read
                if len(bits) != num_intr:
                    msg = "Invalid corpus, expected row with " + \
                        str(num_intr) + " entries but encounted row with " + \
                        str(len(bits)) + " in file: " + infea_path
                    exit_if(exit_on_error, msg=msg)
                    print "WARNING: " + msg
                    print "(Skipping)"
                    num_skip = num_skip + 1
                    continue
                # Check all values are in {0,1}
                if not all(((bit == 0) or (bit == 1)) for bit in bits):
                    msg = "Invalid corpus, encountered non 0|1 entry in file: " + \
                        infea_path
                    exit_if(exit_on_error, msg=msg)
                    print "WARNING: " + msg
                    print "(Skipping)"
                    num_skip = num_skip + 1
                    continue
                # Append new corpus entry
                corp.add(bits)
        print "[tad_cmx] Loaded infeasible corpus from file:\n\t" + infea_path
        print "[tad_cmx] Infeasible corpus size: " + str(len(corp))
        print "[tad_cmx] Skipped misformatted corpus entries: " + str(num_skip)
    return corp

# Resolve pseudo-population size
def load_pseudopop_size(conf_path):
    print "[tad_cmx] Loading:"
    print str(conf_path)
    assert os.path.exists(conf_path)
    pseudopop_size = int(0)
    with open(conf_path) as conffp:
        for line in conffp:
            line = line.strip()
            if line and line.startswith('pseudopop_size'):
                pseudopop_size = int(line.split('=')[1].strip())
                assert pseudopop_size > 0
                break
    print "[tad_cmx] Pseudo-population size: " + str(pseudopop_size)
    return pseudopop_size

# Generates truncated Bernoulli bit matrix that is void of 'known' infeasible
#   configurations - returns COLUMN-MAJOR numpy matrix (i.e. each column is a
#   pseudo-population sample)
def get_bern_pseudopop(hic, pseudopop_size, infea_corp):
    '''Generates truncated Bernoulli pseudo-population'''
    np.random.seed()
    out = []
    while len(out) < pseudopop_size:
        bern_sample = []
        for f in hic:
            r = np.random.binomial(size=1,n=1, p=f)
            bern_sample.append(r[0])
        bern_sample = tuple(bern_sample)
        if bern_sample not in infea_corp:
            out.append(list(bern_sample))
    # Transpose to make column-major
    out = np.array(out).transpose()
    return out

# @param hic_path - Target Hi-C frequencies
# @param infea_path - Infeasible corpus
# @param results_dir - Base CMX results directory
# @param conf_path - CMX configuration INI
# @param args - User arguments
def naive_bern(hic_path, infea_path, results_dir, conf_path, args):
    hic = load_hic(hic_path)
    pseudopop_size = load_pseudopop_size(conf_path = conf_path)
    if (not hic) or (pseudopop_size <= 0):
        print "[tad_cmx] Unable to load Hi-C or pseudo-population size"
        return
    infea_corp = load_infea(infea_path=infea_path, num_intr=len(hic))
    pseudopop = get_bern_pseudopop(hic = hic,
                                   pseudopop_size=pseudopop_size,
                                   infea_corp =infea_corp)
    assert pseudopop.shape == (len(hic), pseudopop_size)
    subd = "bern" + "." + \
        base64.urlsafe_b64encode(uuid.uuid4().bytes).rstrip('=')
    target_dir = os.path.join(results_dir, subd)
    tad_core.make_dir(target_dir)
    pseu_path = os.path.join(target_dir, tad_core.B2P_BITPOP_FNAME)
    print "[tad_cmx] Exporting pseudo-population " + str(pseudopop.shape) + " to:"
    print pseu_path
    # Open as binary to make sure line feed is consistent between platforms
    with open(pseu_path, 'wb') as pseuf:
        np.savetxt(pseuf, X=pseudopop, fmt='%d', delimiter=",")
    # Compute correlation of sampled Hi-C with target Hi-C
    hic_bern = np.sum(pseudopop, axis=1) / float(pseudopop_size)
    pears = np.corrcoef(x=hic, y=hic_bern)[0,1]
    print "[tad_cmx] Generated Bernoulli pseudo-population with Pearson correlation:" + str(pears)
    hic_bern_path = os.path.join(target_dir, "map.pseu.hic.csv")
    print "[tad_cmx] Exporting psuedo-Hi-C:"
    print str(hic_bern_path)
    with open(hic_bern_path, 'wb') as hic_bernf:
        np.savetxt(hic_bernf, X=hic_bern, delimiter=",")

# @param mix - Model 0-based index
# @param mid - Model identifier
# @param args - User arguments
def launch_cmx(mix, mid, args):
    '''Process a single TAD model'''
    print '[tad_cmx] Processing ' + str(mix) + ' : ' +  str(mid)
    # Base directory with unversioned TAD model data
    base_dir = args.base_dir
    # Mixture model population type
    pop_key = args.pop_key
     # Target Hi-C frequencies
    hic_path = tad_core.get_cmx_hic_path(model_name=mid, base_dir=base_dir)
    # Edges path
    edges_path = tad_core.get_cmx_edge_ro_path(model_name=mid,
                                               base_dir=base_dir)
    # Infeasible corpus path
    infea_path = tad_core.get_tad_infea_corp_path(model_name=mid,
                                                  base_dir=base_dir)
    # Results directory
    results_dir = tad_core.get_cmx_results_dir(model_name=mid,
                                               pop_key=pop_key,
                                               tag=args.tag,
                                               base_dir=base_dir)
    # Configuration (top level) INI path
    conf_path = tad_core.get_cmx_ini_path(pop_key=pop_key)
    # If no edge restrictions, see if naive Bernoulli is possible
    if not os.path.exists(edges_path):
        print "[tad_cmx] Warning, restricted edge file path not found:"
        print str(edges_path)
        if os.path.exists(hic_path) and args.naive:
            print "[tad_cmx] Exporting pseudo-population as naive Bernoulli"
            naive_bern(hic_path=hic_path, infea_path = infea_path,
                       results_dir=results_dir, conf_path=conf_path,
                       args=args)
        else:
            print "[tad_cmx] Skipping, is '--naive' enabled?"
        return
    # Executable path
    assert os.path.exists(args.exe)
    cmd = [args.exe]
    # App dispatch
    app_key = tad_core.get_cmx_app_key(pop_key=pop_key)
    cmd.append('-' + app_key)
    # Worker threads
    cmd.extend(["--num_worker_threads", args.max_proc])
    # Job identifier
    job_id = "gibbs" + "." + \
                base64.urlsafe_b64encode(uuid.uuid4().bytes).rstrip('=')
    cmd.extend(["--job_id", job_id])
    cmd.extend(["--output_dir", results_dir])
    assert os.path.exists(conf_path)
    cmd.extend(["--conf", conf_path])    
    assert os.path.exists(edges_path)
    cmd.extend(["--Bnet_dag_edges_path", edges_path])
    assert os.path.exists(hic_path)
    cmd.extend(["--hic_path", hic_path])
    assert os.path.exists(infea_path)
    cmd.extend(["--Bnet_infeas_path", infea_path])
    # Blocking call
    tad_core.run_cmd(cmd=cmd, max_fail=args.max_fail)

# @param args - User arguments
def tad_cmx(args):
    '''Calls CMX Gibss sampler utility at each TAD model'''
    # List of active TAD models
    model_names = tad_core.resolve_tad_model_names(from_table=args.from_table,
                                                   base_dir=args.base_dir)
    if args.model_index is not None:
        # Process single TAD model
        mix = int(args.model_index)
        assert (mix >= 0) and (mix < len(model_names))
        mid = model_names[mix]
        launch_cmx(mix=mix, mid=mid, args=args)
    else:
        # Process all TAD model
        for mix, mid in enumerate(model_names):
            launch_cmx(mix=mix, mid=mid, args=args)

###################################################
# Main
###################################################

# Main script entry point
def __main__():
    print "======================= tad_cmx ======================="
    # Initialize command line parser
    parser = argparse.ArgumentParser()
    parser.add_argument('-bd', '--base_dir', default=tad_core.TAD_BASE_OUT_DIR,
                        help='Base directory storing unversioned TAD model data')
    parser.add_argument('-e', '--exe', default=tad_core.CMX_EXE_PATH,
                          help='Path to CMX Gibbs sampler executable binary')
    parser.add_argument('-ft', '--from_table', default=0,
                          help="If '1' or <path>, will attempt to load table and process TAD model names in row order")
    parser.add_argument('-mf', '--max_fail', default=DEF_MAX_FAIL,
                        help='Maximum number of times a single job configuration may fail with error return code')
    parser.add_argument('-mix', '--model_index', default=DEF_MIX,
                        help='Optional 0-based index into tad_core.resolve_tad_model_names(). If specified, other TADs will be ignored, else (default) all TADs will be processed.')
    parser.add_argument('-mp', '--max_proc', default=DEF_MAX_PROC,
                        help='Maximum number of active fold simulations')
    parser.add_argument('-pop', '--pop_key', default=tad_core.CMX_POP_KEY,
                        choices=['spop', 'mpop'],
                        help="Mixture model type, spop: single-population, mpop: multi-population")
    parser.add_argument('-tg', '--tag', default=tad_core.CMX_TAG,
                        help="Output directory tag suffix")
    parser.add_argument('-nv', '--naive', action='store_true',
                        help="Allow independent Bernoulli if no edge restrictions found")
    # Parse command line
    args = parser.parse_args()
    tad_core.print_cmd(args)
    # Run Gibbs sampler
    tad_cmx(args)
    print "Finished."

# Run main if we are the active script
if __name__ == '__main__':
    __main__()
