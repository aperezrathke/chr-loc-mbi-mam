#!/bin/bash

# Path to this script's directory
SCRIPT_PATH="$(readlink -f $0)"
SCRIPT_DIR="$(dirname $SCRIPT_PATH)"
SCRIPT_DIR=$(cd "$SCRIPT_DIR"; pwd)

#### Post-process bit2pol folding geometry
RSHELL_SCRIPT="$SCRIPT_DIR/tad_bit2pol_post_shell.R"

# Parse command line for [optional] 0-based model index
MIX0=""
if [ $# -gt 0 ]; then
    MIX0="--model_index0 $1"
fi

# Compute frequency matrices
cmd="Rscript --vanilla $RSHELL_SCRIPT"
cmd="$cmd $MIX0"
cmd="$cmd --util_b2p_cache_ligc_csv"
cmd="$cmd --util_b2p_cache_ligc_merge"
cmd="$cmd --util_b2p_cache_ligc_freqmat"
# Run command line
echo "-------------"
echo "Running:"
echo $cmd
$cmd

# Plot heat as rank
cmd="Rscript --vanilla $RSHELL_SCRIPT"
cmd="$cmd $MIX0"
cmd="$cmd --util_b2p_heat --heat_as_rank --heat_vmax 1.0"
# Run command line
echo "-------------"
echo "Running:"
echo $cmd
$cmd

# Plot heat as non-rank
cmd="Rscript --vanilla $RSHELL_SCRIPT"
cmd="$cmd $MIX0"
cmd="$cmd --util_b2p_heat --no-heat_as_rank --heat_vmax 0.35"
# Run command line
echo "-------------"
echo "Running:"
echo $cmd
$cmd

echo "Finished"
