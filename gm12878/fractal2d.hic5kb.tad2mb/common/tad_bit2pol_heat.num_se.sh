#!/bin/bash

# Path to this script's directory
SCRIPT_PATH="$(readlink -f $0)"
SCRIPT_DIR="$(dirname $SCRIPT_PATH)"
SCRIPT_DIR=$(cd "$SCRIPT_DIR"; pwd)

#### Post-process bit2pol folding geometry
RSHELL_SCRIPT="$SCRIPT_DIR/tad_bit2pol_post_shell.R"

# Model filter (at least 2 SEs per region)
MODEL_FILT_ID='num_se'

# Number of CPUs for parallel heat map generation, <= 0 will use all cores
HEAT_NUM_CPU="-1"
# Heatmap min anchor value (ala seaborn) for non-rank plots
HEAT_VMIN_VAL="0.0"
# Heatmap max anchor value (ala seaborn) for non-rank plots
HEAT_VMAX_VAL="0.35"
# Heatmap pixel width (used if format is png)
HEAT_WIDTH_PIX="512"
# Heatmap pixel height (used if format is png)
HEAT_HEIGHT_PIX="512"
# Heatmap inches width (used if format is pdf)
HEAT_WIDTH_IN="7"
# Heatmap inches height (used if format is pdf)
HEAT_HEIGHT_IN="7"

# Heatmap image format(s)
HEAT_FORMAT=("png" "pdf" "svg")

 # Iterate over image formats
for heat_format in "${HEAT_FORMAT[@]}"
do
    # Plot B2P heat as non-rank with empirical Hi-C upper tri
    cmd="Rscript --vanilla $RSHELL_SCRIPT"
    cmd="$cmd --util_b2p_heat"
    cmd="$cmd --model_filt_id $MODEL_FILT_ID"
    cmd="$cmd --heat_format $heat_format"
    cmd="$cmd --heat_width_pix $HEAT_WIDTH_PIX"
    cmd="$cmd --heat_height_pix $HEAT_HEIGHT_PIX"
    cmd="$cmd --heat_width_in $HEAT_WIDTH_IN"
    cmd="$cmd --heat_height_in $HEAT_HEIGHT_IN"
    cmd="$cmd --no-heat_as_rank"
    cmd="$cmd --no-heat_div_by_max"
    cmd="$cmd --heat_vmin $HEAT_VMIN_VAL"
    cmd="$cmd --heat_vmax $HEAT_VMAX_VAL"
    cmd="$cmd --no-heat_intr_ptsov"
    cmd="$cmd --heat_hic_utri"
    cmd="$cmd --no-heat_hic_ltri"
    cmd="$cmd --num_cpu $HEAT_NUM_CPU"
    
    # Run command line
    echo "-------------"
    echo "Running:"
    echo $cmd
    $cmd
done #End iteration over image formats

echo "Finished"
