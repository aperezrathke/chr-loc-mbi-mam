#!/usr/bin/env Rscript

###################################################################
# tad_bit2pol_post_shell.R
#
# Command-line interface for post-processing bit2pol folding data
###################################################################

# Similar to python optparse package
library(optparse)

###################################################################
# Path to this script
###################################################################

# Cached directory to script
# https://stackoverflow.com/questions/3452086/getting-path-of-an-r-script
TAD_BIT2POL_POST_SHELL_SCRIPT_DIR = getSrcDirectory(function(x) {
  x
})

# getSrcDirectory trick will not work via Rscript. If this is the
# case, then TAD_BIT2POL_POST_SHELL_SCRIPT_DIR will be length zero.
# http://stackoverflow.com/questions/1815606/rscript-determine-path-of-the-executing-script
if (length(TAD_BIT2POL_POST_SHELL_SCRIPT_DIR) == 0) {
  get_script_dir <- function() {
    cmdArgs = commandArgs(trailingOnly = FALSE)
    needle = "--file="
    ixmatch = grep(needle, cmdArgs)
    if (length(match) > 0) {
      # Rscript
      return(dirname(normalizePath(sub(
        needle, "", cmdArgs[ixmatch]
      ))))
    }
    return(dirname(scriptName::current_filename()))
  }
  TAD_BIT2POL_POST_SHELL_SCRIPT_DIR = get_script_dir()
}

# @return Path to directory containing this script
get_tad_bit2pol_post_shell_script_dir <- function() {
  return(TAD_BIT2POL_POST_SHELL_SCRIPT_DIR)
}

###################################################################
# Source utility script(s)
###################################################################

# Cache utilities
if (!exists("SOURCED_tad_bit2pol_post")) {
  source(file.path(
    get_tad_bit2pol_post_shell_script_dir(),
    "tad_bit2pol_post.R"
  ))
}

###################################################################
# Parse command line
###################################################################

# https://www.r-bloggers.com/passing-arguments-to-an-r-script-from-command-lines/
option_list = list(
  # Utilities (in order encountered)
  make_option(
    c("--util_b2p_cache_ligc_csv"),
    action = "store_true",
    default = FALSE,
    help = "Utility converts CSV formatted ligation contacts into binary RData"
  ),
  make_option(
    c("--util_b2p_cache_ligc_merge"),
    action = "store_true",
    default = FALSE,
    help = "Utility combines all individual ligc RData files into single RData file"
  ),
  make_option(
    c("--util_b2p_cache_ligc_freqmat"),
    action = "store_true",
    default = FALSE,
    help = "Utility computes contact frequency matrix for merged ligc RData"
  ),
  make_option(
    c("--util_b2p_heat"),
    action = "store_true",
    default = FALSE,
    help = "Utility plots heat maps of bit2pol contact frequency matrices"
  ),
  make_option(
    c("--util_b2p_cor"),
    action = "store_true",
    default = FALSE,
    help = "Utility to summarize bit2pol correlations"
  ),
  make_option(
    c("--util_b2p_unlink"),
    action = "store_true",
    default = FALSE,
    help = "Delete target bit population geometry directories"
  ),
  # Options (in order encountered)
  # model_index0
  make_option(
    c("--model_index0"),
    type = "integer",
    default = NULL,
    help = "0-BASE MODEL INDEX - if valid, only process model at this index"
  ),
  # model_filt_id
  make_option(
    c("--model_filt_id"),
    type = "character",
    default = NULL,
    help = paste0(
      "Optional name of TAD model filter, ",
      paste0(TAD_MODEL_FILT_IDS, collapse = "|")
    )
  ),
  # from_table
  make_option(
    c("--from_table"),
    type = "integer",
    default = DEF_B2P_FROM_TABLE,
    help = "1: load data from scale table, 0: load from file system listing"
  ),
  # ko_rand
  make_option(
    c("--no-ko_rand"),
    action = "store_false",
    dest = "ko_rand",
    default = tad_core$B2P_KO_RAND,
    help = "Assume knock-outs enforced"
  ),
  make_option(
    c("--ko_rand"),
    action = "store_true",
    dest = "ko_rand",
    default = tad_core$B2P_KO_RAND,
    help = "Assume knock-outs not enforced (random)"
  ),
  # b2p tag
  make_option(
    c("--b2p_tag"),
    type = "character",
    default = tad_core$B2P_TAG,
    help = "Optional suffix for bit2pol geometry directories"
  ),
  # pop_key
  make_option(
    c("--pop_key"),
    type = "character",
    default = tad_core$B2P_POP_KEY,
    help = "Assumed CMX population model (spop|mpop)"
  ),
  # cmx tag
  make_option(
    c("--cmx_tag"),
    type = "character",
    default = tad_core$CMX_TAG,
    help = "Optional suffix for CMX results directories"
  ),
  # top_only
  make_option(
    c("--no-top_only"),
    action = "store_false",
    dest = "top_only",
    default = tad_core$B2P_TOP_ONLY,
    help = "Process all encountered bit2pol geometry folders"
  ),
  make_option(
    c("--top_only"),
    action = "store_true",
    dest = "top_only",
    default = tad_core$B2P_TOP_ONLY,
    help = "Process only top-level (lowest depth) bit2pol geometry folders"
  ),
  # base_dir
  make_option(
    c("--tad_base_dir"),
    type = "character",
    default = tad_core$TAD_BASE_OUT_DIR,
    help = "Base TAD loci directory"
  ),
  # should_crop
  make_option(
    c("--no-crop"),
    action = "store_false",
    dest = "crop",
    default = tad_core$B2P_CROP,
    help = "Data not assumed (nor will be) trimmed to TAD model region"
  ),
  make_option(
    c("--crop"),
    action = "store_true",
    dest = "crop",
    default = tad_core$B2P_CROP,
    help = "Data assumed (or will be) trimmed to TAD model region"
  ),
  # num_cpu
  make_option(
    c("--num_cpu"),
    type = "integer",
    default = DEF_NCPU,
    help = "Number of CPUs for parallel processing. If <= 0, then all CPUs are used!"
  ),
  # verbose
  make_option(
    c("--no-verbose"),
    action = "store_false",
    dest = "verbose",
    default = DEF_VERBOSE,
    help = "Disable verbose logging to stdout"
  ),
  make_option(
    c("--verbose"),
    action = "store_true",
    dest = "verbose",
    default = DEF_VERBOSE,
    help = "Enable verbose logging to stdout"
  ),
  # rmvout
  make_option(
    c("--no-rmvout"),
    action = "store_false",
    dest = "rmvout",
    default = tad_core$B2P_REMOVE_OUTLIERS,
    help = "Disable outlier removal"
  ),
  make_option(
    c("--rmvout"),
    action = "store_true",
    dest = "rmvout",
    default = tad_core$B2P_REMOVE_OUTLIERS,
    help = "Enable outlier removal"
  ),
  # should_overwrite
  make_option(
    c("--no-overwrite"),
    action = "store_false",
    dest = "overwrite",
    default = DEF_SHOULD_OVERWRITE,
    help = "Disable overwrite of cached data"
  ),
  make_option(
    c("--overwrite"),
    action = "store_true",
    dest = "overwrite",
    default = DEF_SHOULD_OVERWRITE,
    help = "Enable overwrite of cached data"
  ),
  # eqw
  make_option(
    c("--no-eqw"),
    action = "store_false",
    dest = "eqw",
    default = tad_core$B2P_EQW,
    help = "Empirical data not assumed to be from target distribution"
  ),
  make_option(
    c("--eqw"),
    action = "store_true",
    dest = "eqw",
    default = tad_core$B2P_EQW,
    help = "Empirical data assumed to be from target distribution"
  ),
  # diag_til (correlation)
  make_option(
    c("--diag_til"),
    type = "integer",
    default = DEF_B2P_COR_DIAG_TIL,
    help = "Correlations computed up to this diagonal removed"
  ),
  ################## PLOT ARGUMENTS
  make_option(
    c("--heat_format"),
    type = "character",
    default = DEF_TAD_HEAT_FORMAT,
    help = "Heatmap image format, either 'png', 'pdf', or 'svg'"
  ),
  make_option(
    c("--heat_width_pix"),
    type = "double",
    default = DEF_TAD_HEAT_DIM_PIX,
    help = "Heatmap pixel width, for raster formats such as png"
  ),
  make_option(
    c("--heat_height_pix"),
    type = "double",
    default = DEF_TAD_HEAT_DIM_PIX,
    help = "Heatmap pixel height, for raster formats such as png"
  ),
  make_option(
    c("--heat_width_in"),
    type = "double",
    default = DEF_TAD_HEAT_DIM_IN,
    help = "Heatmap inch width, for vector formats such as pdf|svg"
  ),
  make_option(
    c("--heat_height_in"),
    type = "double",
    default = DEF_TAD_HEAT_DIM_IN,
    help = "Heatmap inch height, for vector formats such as pdf|svg"
  ),
  make_option(
    c("--no-heat_as_rank"),
    action = "store_false",
    dest = "heat_as_rank",
    default = DEF_B2P_HEAT_AS_RANK,
    help = "Disable heatmap over data set ranks"
  ),
  make_option(
    c("--heat_as_rank"),
    action = "store_true",
    dest = "heat_as_rank",
    default = DEF_B2P_HEAT_AS_RANK,
    help = "Enable heatmap over data set ranks"
  ),
  make_option(
    c("--no-heat_div_by_max"),
    action = "store_false",
    dest = "heat_div_by_max",
    default = DEF_B2P_HEAT_AS_RANK,
    help = "Disable divide heatmap data set by max value"
  ),
  make_option(
    c("--heat_div_by_max"),
    action = "store_true",
    dest = "heat_div_by_max",
    default = DEF_B2P_HEAT_AS_RANK,
    help = "Enable divide heatmap data set by max value"
  ),
  make_option(
    c("--heat_vmin"),
    type = "double",
    default = DEF_TAD_HEAT_VMIN,
    help = "Heatmap min anchor value ala seaborn"
  ),
  make_option(
    c("--heat_vmax"),
    type = "double",
    default = get_def_tad_heat_vmax(DEF_B2P_HEAT_AS_RANK),
    help = "Heatmap max anchor value ala seaborn"
  ),
  make_option(
    c("--heat_tick_by"),
    type = "double",
    default = DEF_HEAT_TICK_BY,
    help = "Heatmap axis tick interval"
  ),
  make_option(
    c("--no-heat_key"),
    action = "store_false",
    dest = "heat_show_key",
    default = DEF_HEAT_SHOW_KEY,
    help = "Disable heatmap legend key"
  ),
  make_option(
    c("--heat_key"),
    action = "store_true",
    dest = "heat_show_key",
    default = DEF_HEAT_SHOW_KEY,
    help = "Enable heatmap legend key"
  ),
  make_option(
    c("--no-heat_intr_ptsov"),
    action = "store_false",
    dest = "heat_intr_ptsov",
    default = DEF_B2P_HEAT_INTR_PTSOV,
    help = "Disable heatmap overlay of exemplar (i,j) constraints"
  ),
  make_option(
    c("--heat_intr_ptsov"),
    action = "store_true",
    dest = "heat_intr_ptsov",
    default = DEF_B2P_HEAT_INTR_PTSOV,
    help = "Enable heatmap overlay of exemplar (i,j) constraints"
  ),
  make_option(
    c("--no-heat_hic_utri"),
    action = "store_false",
    dest = "heat_hic_utri",
    default = DEF_B2P_HEAT_HIC_UTRI,
    help = "Disable empirical Hi-C plotting on heatmap upper triangle"
  ),
  make_option(
    c("--heat_hic_utri"),
    action = "store_true",
    dest = "heat_hic_utri",
    default = DEF_B2P_HEAT_HIC_UTRI,
    help = "Enable empirical Hi-C plotting on heatmap upper triangle"
  ),
  make_option(
    c("--no-heat_hic_ltri"),
    action = "store_false",
    dest = "heat_hic_ltri",
    default = DEF_B2P_HEAT_HIC_LTRI,
    help = "Disable empirical Hi-C plotting on heatmap lower triangle"
  ),
  make_option(
    c("--heat_hic_ltri"),
    action = "store_true",
    dest = "heat_hic_ltri",
    default = DEF_B2P_HEAT_HIC_LTRI,
    help = "Enable empirical Hi-C plotting on heatmap lower triangle"
  ),
  make_option(
    c("--heat_tag"),
    type = "character",
    default = DEF_TAD_HEAT_TAG,
    help = "[Optional] Additional heatmap identifier string"
  )
)

opt_parser = OptionParser(option_list = option_list)

opt = parse_args(opt_parser)

###################################################################
# Determine which utility to run
###################################################################

# Print command-line options
if (opt$verbose) {
  print("Options:")
  print(opt)
}

# Cache b2p ligc csv -> rdata
if (opt$util_b2p_cache_ligc_csv) {
  print("Running b2p_cache_ligc_csv()")
  launch_b2p_cache_ligc_csv(
    model_index0 = opt$model_index0,
    model_filt_id = opt$model_filt_id,
    from_table = opt$from_table,
    ko_rand = opt$ko_rand,
    b2p_tag = opt$b2p_tag,
    pop_key = opt$pop_key,
    cmx_tag = opt$cmx_tag,
    top_only = opt$top_only,
    base_dir = opt$tad_base_dir,
    should_crop = opt$crop,
    ncpu = opt$num_cpu,
    should_overwrite = opt$overwrite,
    verbose = opt$verbose
  )
}

# Cache b2p merged ligc
if (opt$util_b2p_cache_ligc_merge) {
  print("Running b2p_cache_ligc_merge()")
  launch_b2p_cache_ligc_merge(
    model_index0 = opt$model_index0,
    model_filt_id = opt$model_filt_id,
    from_table = opt$from_table,
    ko_rand = opt$ko_rand,
    b2p_tag = opt$b2p_tag,
    pop_key = opt$pop_key,
    cmx_tag = opt$cmx_tag,
    top_only = opt$top_only,
    base_dir = opt$tad_base_dir,
    should_crop = opt$crop,
    rmvout = opt$rmvout,
    should_overwrite = opt$overwrite,
    verbose = opt$verbose
  )
}

# Cache b2p ligc freqmat
if (opt$util_b2p_cache_ligc_freqmat) {
  print("Running b2p_cache_ligc_freqmat()")
  launch_b2p_cache_ligc_freqmat(
    model_index0 = opt$model_index0,
    model_filt_id = opt$model_filt_id,
    from_table = opt$from_table,
    ko_rand = opt$ko_rand,
    b2p_tag = opt$b2p_tag,
    pop_key = opt$pop_key,
    cmx_tag = opt$cmx_tag,
    top_only = opt$top_only,
    base_dir = opt$tad_base_dir,
    eqw = opt$eqw,
    should_crop = opt$crop,
    rmvout = opt$rmvout,
    should_overwrite = opt$overwrite,
    verbose = opt$verbose
  )
}

# Plot b2p heat map
if (opt$util_b2p_heat) {
  print("Running b2p_heat()")
  launch_b2p_heat(
    model_index0 = opt$model_index0,
    model_filt_id = opt$model_filt_id,
    from_table = opt$from_table,
    ko_rand = opt$ko_rand,
    b2p_tag = opt$b2p_tag,
    pop_key = opt$pop_key,
    cmx_tag = opt$cmx_tag,
    top_only = opt$top_only,
    base_dir = opt$tad_base_dir,
    eqw = opt$eqw,
    should_crop = opt$crop,
    rmvout = opt$rmvout,
    heat_format = opt$heat_format,
    heat_width_pix = opt$heat_width_pix,
    heat_height_pix = opt$heat_height_pix,
    heat_width_in = opt$heat_width_in,
    heat_height_in = opt$heat_height_in,
    heat_as_rank = opt$heat_as_rank,
    heat_div_by_max = opt$heat_div_by_max,
    heat_vmin = opt$heat_vmin,
    heat_vmax = opt$heat_vmax,
    heat_tick_by = opt$heat_tick_by,
    heat_show_key = opt$heat_show_key,
    heat_intr_ptsov = opt$heat_intr_ptsov,
    heat_hic_utri = opt$heat_hic_utri,
    heat_hic_ltri = opt$heat_hic_ltri,
    heat_tag = opt$heat_tag,
    ncpu = opt$num_cpu,
    verbose = opt$verbose
  )
}

# Summarize bit population correlation
if (opt$util_b2p_cor) {
  print("Running b2p_cor()")
  launch_b2p_cor(
    model_index0 = opt$model_index0,
    model_filt_id = opt$model_filt_id,
    from_table = opt$from_table,
    diag_til = opt$diag_til,
    ko_rand = opt$ko_rand,
    b2p_tag = opt$b2p_tag,
    pop_key = opt$pop_key,
    cmx_tag = opt$cmx_tag,
    top_only = opt$top_only,
    base_dir = opt$tad_base_dir,
    eqw = opt$eqw,
    should_crop = opt$crop,
    rmvout = opt$rmvout,
    verbose = opt$verbose
  )
}

# Delete bit population geometry
if (opt$util_b2p_unlink) {
  print("Running b2p_unlink()")
  launch_b2p_unlink(
    model_index0 = opt$model_index0,
    model_filt_id = opt$model_filt_id,
    from_table = opt$from_table,
    ko_rand = opt$ko_rand,
    b2p_tag = opt$b2p_tag,
    pop_key = opt$pop_key,
    cmx_tag = opt$cmx_tag,
    top_only = opt$top_only,
    base_dir = opt$tad_base_dir,
    verbose = opt$verbose
  )
}
