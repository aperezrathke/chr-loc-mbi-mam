#!/bin/bash

# Path to this script's directory
SCRIPT_PATH="$(readlink -f $0)"
SCRIPT_DIR="$(dirname $SCRIPT_PATH)"
SCRIPT_DIR=$(cd "$SCRIPT_DIR"; pwd)

# MBI annotation R utilities
RSHELL_SCRIPT="$SCRIPT_DIR/tad_mbi_sunburst_prin_loop_shell.R"

# Build command line
cmd="Rscript --vanilla $RSHELL_SCRIPT"

# Run command line
echo "Running:"
echo $cmd
$cmd
echo "Finished."
