###################################################################
# tad_filt.R
#
# Additional TAD model region filtering R utilities
#   - Not same as tad_filt.py which filters by SPRITE hits
###################################################################

# Guard variable to avoid multiple sourcing
SOURCED_tad_filt = TRUE

###################################################################
# Path to this script
###################################################################

# Cached directory to script
# https://stackoverflow.com/questions/3452086/getting-path-of-an-r-script
TAD_FILT_SCRIPT_DIR =
  getSrcDirectory(function(x) {
    x
  })

# getSrcDirectory trick will not work via Rscript. If this is the
# case, then TAD_FILT_SCRIPT_DIR will be length zero.
# http://stackoverflow.com/questions/1815606/rscript-determine-path-of-the-executing-script
if (length(TAD_FILT_SCRIPT_DIR) == 0) {
  get_script_dir <- function() {
    cmdArgs = commandArgs(trailingOnly = FALSE)
    needle = "--file="
    ixmatch = grep(needle, cmdArgs)
    if (length(match) > 0) {
      # Rscript
      return(dirname(normalizePath(sub(
        needle, "", cmdArgs[ixmatch]
      ))))
    }
    return(dirname(scriptName::current_filename()))
  }
  TAD_FILT_SCRIPT_DIR = get_script_dir()
}

# @return Path to directory containing this script
get_tad_filt_script_dir <- function() {
  return(TAD_FILT_SCRIPT_DIR)
}

###################################################################
# Utility scripts
###################################################################

# TAD loci utilities
source(file.path(get_tad_filt_script_dir(), "tad_core.R"))

###################################################################
# TAD model region filters
###################################################################

# Filter TAD regions by minimum number of Super-enhancers (SE)
# @param model_names - Target genomic regions (char vector)
# @param k - Minimum number of SE at retained TAD model regions
# @param scale_df - Data.frame with columns:
#     $name - Name of TAD region, ideally overlaps 'model_names' arg
#     $n_se_med - Number of super enhancers at TAD region
# @param verbose - If TRUE, enable logging to stdout
# @return Char vector of TAD region model names which meet minimum
#   Super-enhancer count cutoff
tad_model_filt_num_se <-
  function(model_names,
           k = 2,
           scale_df = tad_core$load_tad_scale(),
           verbose = tad_core$DEF_VERBOSE) {
    if (verbose) {
      print(paste0(
        "Filtering TAD models to have at least ",
        k,
        " super enhancers ..."
      ))
    }
    return(model_names[model_names %in% scale_df$name[scale_df$n_se_med >= k]])
  }

# Filter TAD regions by SPRITE hits
# @param model_names - Target genomic regions (char vector)
# @param k - Only retain top 'k_spr' TAD model regions based on
#   number of SPRITE hits associated to the Super-enhancer, IGNORED
#   IF K < 1!
# @param scale_df - Data.frame with columns:
#     $name - Name of TAD region, ideally overlaps 'model_names' arg
#     $n_se_med - Number of super enhancers at TAD region
# @param verbose - If TRUE, enable logging to stdout
# @return Char vector of TAD region model names which meet minimum
#   Super-enhancer count cutoff
tad_model_filt_top_k_spr <-
  function(model_names,
           k = 5,
           scale_df = tad_core$load_tad_scale(),
           verbose = tad_core$DEF_VERBOSE) {
    if (verbose) {
      print(paste0("Filtering TAD models to top ",
                   k,
                   " observed SPRITE SE hits ..."))
    }
    if (k < 1) {
      return(model_names)
    }
    top_k_names = scale_df$name[1:min(k, nrow(scale_df))]
    return(model_names[model_names %in% top_k_names])
  }

# @HACK - HARD-CODED FILTER TO TARGET RESIDUAL REGIONS
# Filter TAD regions to residual regions that need modeling
# @param model_names - Target genomic regions (char vector)
# @return Char vector of TAD model names overlapping residuals
tad_model_filt_residual <-
  function(model_names,
           ...) {
    residual_names = c("chr18.60675000.61120000",
                       "chr6.26040000.27880000")
    return(model_names[model_names %in% residual_names])
  }

# Filter TAD regions to those selected for distance boundary analysis
# @param model_names - Target genomic regions (char vector)
# @return Char vector of TAD model names overlapping residuals
tad_model_filt_distb <-
  function(model_names,
           ...) {
    distb_names = c(
      "chrX.19560000.20170000"
    )
    return(model_names[model_names %in% distb_names])
  }

# List of TAD model filter callbacks
TAD_MODEL_FILT_FUNCS = list(
  'num_se' = tad_model_filt_num_se,
  'top_k_spr' = tad_model_filt_top_k_spr,
  'residual' = tad_model_filt_residual,
  'distb' = tad_model_filt_distb
)

# Identifiers for dispatching TAD model filters
TAD_MODEL_FILT_IDS = names(TAD_MODEL_FILT_FUNCS)

# Dispatch TAD model filter from 'filt_id' character, useful for
# command-line based utilities
# @param model_names - Character vector of TAD model names
# @param filt_id - Model filter character identifier
# @param ... - Additional argumetns to TAD model filter callback
# @return Conditionally filtered TAD model names
tad_model_filt_dispatch <-
  function(model_names, filt_id = NULL, ...) {
    # Early out if no names to filter
    if (length(model_names) < 1) {
      return(model_names)
    }
    # Early out if NULL
    if (is.null(filt_id)) {
      return(model_names)
    }
    # Early out if filt_id is not a character
    if (!is.character(filt_id)) {
      return(model_names)
    }
    # Early out if filt_id is not recognized
    if (!(filt_id %in% TAD_MODEL_FILT_IDS)) {
      return(model_names)
    }
    filter_func = TAD_MODEL_FILT_FUNCS[[filt_id]]
    return(filter_func(model_names = model_names, ...))
  }

###################################################################
# TAD overlap stats
###################################################################

# Print overlap percentages among regions
tad_model_check_overlap <-
  function(scale_df = tad_core$load_tad_scale()) {
    if (nrow(scale_df) <= 1) {
      return(invisible(TRUE))
    }
    # Process models
    for (i in 1:(nrow(scale_df) - 1)) {
      model_name = scale_df$name[i]
      print(paste0("Processing: ", model_name))
      chr_df = scale_df[(i + 1):nrow(scale_df), ]
      chr_df = chr_df[chr_df$chr == scale_df$chr[i], ]
      if (nrow(chr_df) < 1) {
        next
      }
      # Check overlaps
      x1 = scale_df$bp_start_tad_med[i]
      x2 = scale_df$bp_end_tad_med[i]
      for (j in 1:nrow(chr_df)) {
        y1 = chr_df$bp_start_tad_med[j]
        y2 = chr_df$bp_end_tad_med[j]
        has_overlap = ((x1 <= y2) && (y1 <= x2))
        if (has_overlap) {
          z1 = max(x1, y1)
          z2 = min(x2, y2)
          fx = 100.0 * ((z2 - z1) / (x2 - x1))
          fy = 100.0 * ((z2 - z1) / (y2 - y1))
          print(paste0(
            "-overlap of (",
            format(fx, digits = 3),
            ", ",
            format(fy, digits = 3),
            ")% with: ",
            chr_df$name[j]
          ))
        }
      }
    }
    return(invisible(TRUE))
  }

# Genomic coordinate metadata, each bin has unique coordinate:
#   chr, [bp_start0, bp_end0) where bp_start0 is INCLUSIVE and
#   bp_end0 is EXCLUSIVE!
# - Chromosome
BIO_ID_CHR = "CHR"
# - INCLUSIVE base pair start coordinate
BIO_ID_BP_START0 = "BP_START0"
# - EXCLUSIVE base pair end coordinate
BIO_ID_BP_END0 = "BP_END0"

# @HACK - function parses model name of form:
#   "chr<X|Y|int>.<bp_start0>.<bp_end0>"
# @return list with elements
#   <BIO_ID_CHR> : character value of chromosome with 'chr' prefix
#   <BIO_ID_BP_START0> : INCLUSIVE integer base pair start
#     coordinate
#   <BIO_ID_BP_END0> : EXCLUSIVE integer base pair end coordinate
split_tad_model_name <- function(model_name) {
  # Convert single character string into character vector
  IX_MODID_CHR = 1
  IX_MODID_BP_START0 = 2
  IX_MODID_BP_END0 = 3
  modid_vec = unlist(strsplit(x = model_name, split = '.', fixed = TRUE))
  stopifnot(length(modid_vec) == 3)
  # Convert characters into typed list
  out = list()
  out[[BIO_ID_CHR]] = as.character(modid_vec[IX_MODID_CHR])
  out[[BIO_ID_BP_START0]] = as.integer(modid_vec[IX_MODID_BP_START0])
  out[[BIO_ID_BP_END0]] = as.integer(modid_vec[IX_MODID_BP_END0])
  stopifnot(out[[BIO_ID_BP_START0]] <= out[[BIO_ID_BP_END0]])
  return(out)
}
