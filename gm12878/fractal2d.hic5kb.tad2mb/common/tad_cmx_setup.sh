#!/bin/bash

# Script configures CMX Gibbs sampler and subsequent bit2pol 3-D folding
#
# Example usage:
#
#   nohup <path_to_script> [-cpu <int>] &> <path_to_log> &
#   : nohup means to keep job running even when logged out
#   : &> means to redirect stdout and stderr to log file
#   : & means to run script in background
#
# User may specify optional command-line overrides for number of CPUs:
#   -cpu <int>: Specify total number of R processes to spawn, must be positive
#       integer. Should not be greater than the number of CPU cores available
#       on the machine running this script.

# Path to this script's directory
SCRIPT_PATH="$(readlink -f $0)"
SCRIPT_DIR="$(dirname $SCRIPT_PATH)"
SCRIPT_DIR=$(cd "$SCRIPT_DIR"; pwd)


# @WARNING - On extreme, this should not exceed the number of CPUs per node
# This value can be overridden with command line: -procs <integer>
NUM_CPU=5

###################################################
# Command line overrides
###################################################

while [ $# -gt 1 ]
do
    case "$1" in
    -cpu)
        NUM_CPU=$2
        shift
        ;;
    esac
    shift
done

# Validate number of processes per job
if [ "$NUM_CPU" -le "0" ]; then
    echo "Error: Invalid process count: $NUM_CPU <= 0. Exiting." 1>&2
    exit 1
fi

###################################################
# Configure CMX at multiple TAD loci
###################################################

# Build command line
cmd="Rscript --vanilla $SCRIPT_DIR/tad_cmx_setup_shell.R"
cmd="$cmd --util_cmx_setup_loci --num_cpu $NUM_CPU"
cmd="$cmd --util_b2p_setup_loci"

# Run command line
echo "Running:"
echo $cmd
$cmd

 echo "Finished"
 