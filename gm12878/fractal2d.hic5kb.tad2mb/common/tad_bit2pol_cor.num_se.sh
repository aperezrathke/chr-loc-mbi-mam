#!/bin/bash

# Path to this script's directory
SCRIPT_PATH="$(readlink -f $0)"
SCRIPT_DIR="$(dirname $SCRIPT_PATH)"
SCRIPT_DIR=$(cd "$SCRIPT_DIR"; pwd)

#### Post-process bit2pol folding geometry
RSHELL_SCRIPT="$SCRIPT_DIR/tad_bit2pol_post_shell.R"

# Model filter (at least 2 SEs per region)
MODEL_FILT_ID='num_se'

# Build command line
cmd="Rscript --vanilla $RSHELL_SCRIPT"
cmd="$cmd --util_b2p_cor"
cmd="$cmd --model_filt_id $MODEL_FILT_ID"

# Run command line
echo "-------------"
echo "Running:"
echo $cmd
$cmd

echo "Finished"
