#!/bin/bash

# Path to this script's directory
SCRIPT_PATH="$(readlink -f $0)"
SCRIPT_DIR="$(dirname $SCRIPT_PATH)"
SCRIPT_DIR=$(cd "$SCRIPT_DIR"; pwd)

# Parse command line for [optional] 0-based model index
MIX0=""
if [ $# -gt 0 ]; then
    MIX0="$1"
fi

# Many body null counts
cmd="$SCRIPT_DIR/tad_mbi_null.sh $MIX0"
$cmd

# Many body null boot distribution
cmd="$SCRIPT_DIR/tad_mbi_boot.sh $MIX0"
$cmd

# Many body b2p counts
cmd="$SCRIPT_DIR/tad_mbi_b2p.sh $MIX0"
$cmd

# Many body p-values
cmd="$SCRIPT_DIR/tad_mbi_pvals.sh $MIX0"
$cmd

# @TODO - consider heat maps (no support on extreme)
#cmd="$SCRIPT_DIR/tad_mbi_heat.sh $MIX0"
#$cmd

echo "Finished MBI pipeline"
