###################################################################
# tad_cmx_setup.R
#
# Utilities for generating CMX configuration inputs such as:
# - edge restrictions (will also call p-values)
# - target Hi-C frequencies
#
# ASSUMES tad_perturb.R HAS ALREADY BEEN RUN FOR TAD MODEL
###################################################################

# Guard variable to avoid multiple sourcing
SOURCED_tad_cmx_setup = TRUE

###################################################################
# Path to this script
###################################################################

# Cached directory to script
# https://stackoverflow.com/questions/3452086/getting-path-of-an-r-script
TAD_CMX_SETUP_SCRIPT_DIR = getSrcDirectory(function(x) {
  x
})

# getSrcDirectory trick will not work via Rscript. If this is the
# case, then TAD_CMX_SETUP_SCRIPT_DIR will be length zero.
# http://stackoverflow.com/questions/1815606/rscript-determine-path-of-the-executing-script
if (length(TAD_CMX_SETUP_SCRIPT_DIR) == 0) {
  get_script_dir <- function() {
    cmdArgs = commandArgs(trailingOnly = FALSE)
    needle = "--file="
    ixmatch = grep(needle, cmdArgs)
    if (length(match) > 0) {
      # Rscript
      return(dirname(normalizePath(sub(
        needle, "", cmdArgs[ixmatch]
      ))))
    }
    return(dirname(scriptName::current_filename()))
  }
  TAD_CMX_SETUP_SCRIPT_DIR = get_script_dir()
}

# @return Path to directory containing this script
get_tad_cmx_setup_script_dir <- function() {
  return(TAD_CMX_SETUP_SCRIPT_DIR)
}

###################################################################
# Source utility script(s)
###################################################################

# Common configuration - loads local python module "tad_core"
source(file.path(get_tad_cmx_setup_script_dir(), "tad_core.R"))

# Import/export utilities
source(file.path(tad_core$MODULE_COMMON_DIR, "scripts", "utils.io.R"))

# Parallel utilities
source(file.path(tad_core$MODULE_COMMON_DIR, "scripts", "utils.parallel.R"))

# Cache utilities
if (!exists("SOURCED_null_cache")) {
  source(file.path(tad_core$NULL_SCRIPT_DIR, "null_cache.R"))
}

###################################################################
# Index conversion utilities
###################################################################

# Convert data.frame with 0-based index columns (i0, j0) to 1-based
# index columns (i1, j1)
# @param iojo_df - Data.frame with 0-based index columns i0, j0
# @return Data.frame with i0 -> i1 and j0 -> j1
i0j0_to_i1j1 <- function(i0j0_df) {
  # Convert 0-base to 1-base index
  colnames(i0j0_df)[which(colnames(i0j0_df) == "i0")] = "i1"
  i0j0_df$i1 = as.integer(i0j0_df$i1 + 1)
  colnames(i0j0_df)[which(colnames(i0j0_df) == "j0")] = "j1"
  i0j0_df$j1 = as.integer(i0j0_df$j1 + 1)
  return(invisible(i0j0_df))
}

# Convert data.frame with 1-based index columns (i1, j1) to 0-based
# index columns (i0, j0)
# @param i1j1_df - Data.frame with 1-based index columns i1, j1
# @return Data.frame with i1 -> i0 and j1 -> j0
i1j1_to_i0j0 <- function(i1j1_df) {
  # Convert 0-base to 1-base index
  colnames(i1j1_df)[which(colnames(i1j1_df) == "i1")] = "i0"
  i1j1_df$i0 = as.integer(i1j1_df$i0 - 1)
  stopifnot(all(i1j1_df$i0 >= 0))
  colnames(i1j1_df)[which(colnames(i1j1_df) == "j1")] = "j0"
  i1j1_df$j0 = as.integer(i1j1_df$j0 - 1)
  stopifnot(all(i1j1_df$j0 >= 0))
  return(invisible(i1j1_df))
}

###################################################################
# Mock data
###################################################################

# @return Mock perturbation ligc frequency matrix list with members
#   $npre - Input number of ligc samples before any outlier removal
#   $npost - Output number of ligc samples after any outlier removal
#   $logw_max - Max retained log weight
#   $w_sum - Sum of retained exp(logw - logw_max)
#   $fmat - Symmetric numeric frequency matrix, each cell in [0,1]
load_mock_perturb_ligc_freqmat <- function(verbose = DEF_VERBOSE) {
  fpath = file.path(tad_core$MOCK_DIR, "perturb.ligc.freqmat.rdata")
  return(invisible(load_rdata(
    rdata_path = fpath, verbose = verbose
  )))
}

# @return Mock perturbation (i1, j1) test tuples data.frame
load_mock_perturb_i1j1_df <- function(verbose = DEF_VERBOSE) {
  fpath = file.path(tad_core$MOCK_DIR, "perturb.i0j0.clust.rdata")
  i0j0_df = load_rdata(rdata_path = fpath, verbose = verbose)
  # Convert 0-base to 1-base index
  return(invisible(i0j0_to_i1j1(i0j0_df = i0j0_df)))
}

###################################################################
# Pertubation p-values
###################################################################

# Computes p-values for target perturbation ensemble
# @param pert_ligc - Pertubation frequency matrix
# @param i1j1_df - Data.frame with columns i1, j1 - where each row
#   identifies the 1-based contact pairs to test
# @param null_blb - Bootstrap over null samples. List of matrices
#   where each index k is a matrix Mk of observed (i,j) contact
#   frequencies such that |j-i| == (k-1). In other words, all
#   elements of matrix Mk are from a (k-1) diagonal of a bootstrap
#   replicate's contact frequency matrix; specifically each row of
#   Mk is from a separate replicate. Also, M1 and M2, which
#   correspond to 0-diagonal and 1-diagonal (bonded interactions)
#   are likely not present (NULL) and therefore should not be
#   tested against (however, these values would all be 1 if present)
# @param parallel_opt - one of "multicore" | "snow" | "no"
#   Specifies parallel processing platform
#   - multicore: uses "multicore" package only available on linux
#   - snow: uses "snow" package available on all platforms
#   - no: disable parallel processing
# @param ncpu - Number of CPUs to use for parallel computation
#   If ncpu <= 0, then all available CPUs are used!
# @param cl - An optional parallel::cluster. If not supplied and
#   ncpu > 1, a local cluster is created for duration of call
# @param verbose - If TRUE, log to stdout
# @return Data.frame with columns:
#   $i1 - 1-based bin index of tested first contacting locus
#   $j1 - 1-based bin index of tested second contact locus where
#           (j1 >= i1)
#   $f - Frequency of (i1,j1) contact in perturbation ensemble
#   $pval - Upper-tail p-value of observed (i1,j1) perturbation
#     contact according to 'null_blb' distribution
#   $fdr - FDR adjusted p-value for tests performed
perturb_call_pvals <-
  function(pert_ligc = load_mock_perturb_ligc_freqmat()$fmat,
           i1j1_df = load_mock_perturb_i1j1_df(),
           null_blb = null_cache_ligc_popd_lband_merge(),
           parallel_opt = DEF_PARALLEL_OPT,
           ncpu = DEF_NCPU,
           cl = NULL,
           verbose = DEF_VERBOSE) {
    # Assume at least 1-test requested
    N_TEST = nrow(i1j1_df)
    stopifnot(N_TEST >= 1)
    # Assume symmetric matrix
    stopifnot(isSymmetric(pert_ligc))
    # Assume upper triangle, else indexing may not work
    GDPROF = i1j1_df$j1 - i1j1_df$i1
    stopifnot(all(GDPROF >= 0))
    # Assume null distribution can support max genomic distance; if
    # assert trips, then null simulation needs larger locus
    max_gd = max(GDPROF)
    stopifnot(gd2ixdiag(max_gd) <= length(null_blb))
    # Obtain index matrix
    i1j1_mat = as.matrix(i1j1_df[, c("i1", "j1")])
    # Bounds check tests
    stopifnot(max(i1j1_mat) <= nrow(pert_ligc))
    
    # Pre-allocate output data.frame
    out_df = data.frame(
      i1 = i1j1_df$i1,
      j1 = i1j1_df$j1,
      f = pert_ligc[i1j1_mat],
      pval = rep(1.0, N_TEST),
      fdr = rep(1.0, N_TEST)
    )
    stopifnot(all(out_df$f >= 0.0))
    
    # Map genomic distance to BLB test diagonal
    gdixd =  gd2ixdiag(GDPROF)
    # Inner loop callback that may be parallelized
    # @param i - 1-based index into gdixd and f_pert vectors
    # @param gdixd - Genomic distance null diagonal index vector
    # @param f_pert - Parallel to gdixd, associated perturbation frequency
    # @param null_blb - Bootstrap replicate data
    # @return p-value for 'i' test
    get_pval <- function(i, gdixd, f_pert, null_blb) {
      ixdiag = gdixd[i]
      f = f_pert[i]
      n = as.numeric(length(null_blb[[ixdiag]]))
      return(sum(null_blb[[ixdiag]] >= f) / n)
    }
    
    # Assign p-values
    out_df$pval = sapply_router(
      X = 1:N_TEST,
      FUN = get_pval,
      parallel_opt = parallel_opt,
      ncpu = ncpu,
      cl = cl,
      mc.preschedule = TRUE,
      verbose = verbose,
      gdixd,
      out_df$f,
      null_blb
    )
    # Adjust p-values
    stopifnot(all(is.finite(out_df$pval)))
    out_df$fdr = p.adjust(p = out_df$pval, method = "fdr")
    return(invisible(out_df))
  }

###################################################################
# CMX configure
###################################################################

# Default model name for CMX configuration
DEF_CMX_MODEL_NAME = "chr1.234635000.235415000"

# Load 1-based Hi-C clustering 1-based indexing results from
#   0-based indexing RData path
# @param hic_clust0_rdata_path - Path to 0-based indexing Hi-C
#   clustering results data.frame
# @param verbose - If TRUE, log to stdout
# @return Data.frame with (i1, j1), etc. columns
load_hic_clust1from0 <- function(hic_clust0_rdata_path,
                                 verbose = DEF_VERBOSE) {
  i0j0_df = load_rdata(rdata_path = hic_clust0_rdata_path,
                       verbose = verbose)
  # Convert 0-base to 1-base index
  return(invisible(i0j0_to_i1j1(i0j0_df = i0j0_df)))
}

# @TODO - consider using cluster mean|median|max for target Hi-C frequencies
# Generates CMX configuration files at single locus:
#   cmx.edge.ro.csv - Column-major list of allowed edges
#   cmx.hic.csv - Target Hi-C frequencies
# Additionally generates p-value files:
#   perturb.pvals.all.csv|rdata - P-values and FDR results for locus
#     exemplar specific contacts to locus all specific contacts
#   perturb.pvals.ex.csv|rdata - P-values and FDR results for locus
#     exemplar specific contacts to each other
# @WARNING - ASSUMES 1:1 fragment to monomer bead mapping!
# @WARNING - ASSUMES PERTURBATION REMOVE OUTLIERS = 1
# @WARNING - CMX edges and target Hi-C frequencies always overwritten!
# @param model_name -  Model name for resolving locus specific paths
# @param tad_base_dir - TAD base directory with all models
# @param fdr_thresh - False discovery rate threshold for calling
#   allowed edges
# @param perturb_base_dir_freqmat - Input base perturbations
#   directory, such that each subdirectory is a separate
#   perturbation experiment with summarized frequency matrix results
# @param null_blb - Null bootstrap distribution
# @param parallel_opt - one of "multicore" | "snow" | "no"
#   Specifies parallel processing platform
#   - multicore: uses "multicore" package only available on linux
#   - snow: uses "snow" package available on all platforms
#   - no: disable parallel processing
# @param ncpu - Number of CPUs to use for parallel computation
#   If ncpu <= 0, then all available CPUs are used!
# @param cl - An optional parallel::cluster. If not supplied and
#   ncpu > 1, a local cluster is created for duration of call
# @param overwrite - If TRUE, overwrites cached p-values
# @param verbose - If TRUE, log to stdout
# @return list with p-value results:
#   all - Data.frame with ex-to-all p-values
#   ex - Data.frame with ex-to-ex p-values
cmx_setup_locus <- function(model_name = DEF_CMX_MODEL_NAME,
                            tad_base_dir = tad_core$TAD_BASE_OUT_DIR,
                            fdr_thresh = tad_core$CMX_EDGE_FDR_THRESH,
                            perturb_base_dir_freqmat = tad_core$PERTURB_BASE_DIR,
                            null_blb = null_cache_ligc_popd_lband_merge(),
                            parallel_opt = DEF_PARALLEL_OPT,
                            ncpu = DEF_NCPU,
                            cl = NULL,
                            overwrite = DEF_SHOULD_OVERWRITE,
                            verbose = DEF_VERBOSE) {
  stopifnot(dir.exists(tad_base_dir))
  stopifnot(dir.exists(perturb_base_dir_freqmat))
  # Clustering results path (0-based indexing)
  clust0_path = tad_core$get_hic_clust_path(model_name = model_name,
                                            base_dir = tad_base_dir,
                                            ext_no_dot = "rdata")
  stopifnot(file.exists(clust0_path))
  # Load 1-based indexing clustering results (from 0-based index path)
  clust1_df_all = load_hic_clust1from0(hic_clust0_rdata_path = clust0_path,
                                       verbose = verbose)
  clust1_df_ex = clust1_df_all[which(clust1_df_all$ex == 1), ]
  # P-value rdata file paths
  pvals_all_rdata_path = tad_core$get_tad_perturb_pvals_path(
    model_name = model_name,
    b_all = TRUE,
    ext_no_dot = "rdata",
    base_dir = tad_base_dir
  )
  pvals_ex_rdata_path = tad_core$get_tad_perturb_pvals_path(
    model_name = model_name,
    b_all = FALSE,
    ext_no_dot = "rdata",
    base_dir = tad_base_dir
  )
  # Check for cached p-values
  pvals_all = data.frame()
  pvals_ex = data.frame()
  if (!overwrite &&
      file.exists(pvals_all_rdata_path) &&
      file.exists(pvals_ex_rdata_path)) {
    # Cached values found!
    if (verbose) {
      print("Loading cached p-values...")
    }
    pvals_all = load_rdata(rdata_path = pvals_all_rdata_path,
                           verbose = verbose)
    pvals_ex = load_rdata(rdata_path = pvals_ex_rdata_path,
                          verbose = verbose)
  } else {
    # Converts to 4 tuple data.frame (i1, j1, k1, l1)
    i1j1_to_i1j1k1l1 <- function(i1, j1, i1j1_df) {
      colnames(i1j1_df)[which(colnames(i1j1_df) == "i1")] = "k1"
      colnames(i1j1_df)[which(colnames(i1j1_df) == "j1")] = "l1"
      return(data.frame(i1 = i1, j1 = j1, i1j1_df))
    }
    # Call p-values at each exemplar
    # @TODO - make this parallel!
    for (iex in 1:nrow(clust1_df_ex)) {
      # Load perturbation frequency matrix (0-based index convention)
      i0 = as.integer(clust1_df_ex$i1[iex] - 1)
      j0 = as.integer(clust1_df_ex$j1[iex] - 1)
      # Assume 1:1 fragment to monomer bead mapping!
      ex_perturb_dir = tad_core$get_perturb_dir(
        frag_a_lo = i0,
        frag_a_hi = i0,
        frag_b_lo = j0,
        frag_b_hi = j0,
        perturb_base_dir = perturb_base_dir_freqmat
      )
      # Path to frequency matrix list data
      ex_freqmat_rdata_path =
        tad_core$get_perturb_ligc_freqmat_rdata_path(rmvout = tad_core$PERTURB_REMOVE_OUTLIERS,
                                                     perturb_dir = ex_perturb_dir)
      stopifnot(file.exists(ex_freqmat_rdata_path))
      # Load frequency matrix list
      ex_freqmat_lst = load_rdata(rdata_path = ex_freqmat_rdata_path,
                                  verbose = verbose)
      # Call ex-to-all p-values
      ex2all_df = perturb_call_pvals(
        pert_ligc = ex_freqmat_lst$fmat,
        i1j1_df = clust1_df_all,
        null_blb = null_blb,
        parallel_opt = parallel_opt,
        ncpu = ncpu,
        cl = cl,
        verbose = verbose
      )
      ex2all_df = i1j1_to_i1j1k1l1(i1 = clust1_df_ex$i1[iex],
                                   j1 = clust1_df_ex$j1[iex],
                                   i1j1_df = ex2all_df)
      pvals_all = rbind(pvals_all, ex2all_df)
      # Call ex-to-ex p-values
      ex2ex_df = perturb_call_pvals(
        pert_ligc = ex_freqmat_lst$fmat,
        i1j1_df = clust1_df_ex,
        null_blb = null_blb,
        parallel_opt = parallel_opt,
        ncpu = ncpu,
        cl = cl,
        verbose = verbose
      )
      ex2ex_df = i1j1_to_i1j1k1l1(i1 = clust1_df_ex$i1[iex],
                                  j1 = clust1_df_ex$j1[iex],
                                  i1j1_df = ex2ex_df)
      pvals_ex = rbind(pvals_ex, ex2ex_df)
    }
    # Utility converts 1-base (i1,j1,k1,l1) to 0-base (i0,j0,k0,l0)
    i1j1k1l1_to_i0j0k0l0 <- function(i1j1k1l1_df) {
      colnames(i1j1k1l1_df)[which(colnames(i1j1k1l1_df) == "i1")] = "i0"
      i1j1k1l1_df$i0 = as.integer(i1j1k1l1_df$i0 - 1)
      stopifnot(all(i1j1k1l1_df$i0 >= as.integer(0)))
      colnames(i1j1k1l1_df)[which(colnames(i1j1k1l1_df) == "j1")] = "j0"
      i1j1k1l1_df$j0 = as.integer(i1j1k1l1_df$j0 - 1)
      stopifnot(all(i1j1k1l1_df$j0 >= as.integer(0)))
      colnames(i1j1k1l1_df)[which(colnames(i1j1k1l1_df) == "k1")] = "k0"
      i1j1k1l1_df$k0 = as.integer(i1j1k1l1_df$k0 - 1)
      stopifnot(all(i1j1k1l1_df$k0 >= as.integer(0)))
      colnames(i1j1k1l1_df)[which(colnames(i1j1k1l1_df) == "l1")] = "l0"
      i1j1k1l1_df$l0 = as.integer(i1j1k1l1_df$l0 - 1)
      stopifnot(all(i1j1k1l1_df$l0 >= as.integer(0)))
      return(i1j1k1l1_df)
    }
    # Finalize formatting - convert to 0-base indexing, set self
    # p-values and FDRs to 1.0, compute locus-wide FDR
    finalize_format <- function(i1j1k1l1_df) {
      # Convert to 0-base indexing
      i0j0k0l0 = i1j1k1l1_to_i0j0k0l0(i1j1k1l1_df)
      # Avoid testing effect on self contacts
      self = (i0j0k0l0$i0 == i0j0k0l0$k0) &
        (i0j0k0l0$j0 == i0j0k0l0$l0)
      i0j0k0l0$pval[self] = 1.0
      i0j0k0l0$fdr[self] = 1.0
      not_self = !self
      i0j0k0l0["fdr_locus"] = 1.0
      i0j0k0l0$fdr_locus[not_self] = p.adjust(p = i0j0k0l0$pval[not_self],
                                              method = "fdr")
      return(i0j0k0l0)
    }
    # Ex-to-all to 0-base and locus-wide FDR
    pvals_all = finalize_format(pvals_all)
    # Ex-to-ex to 0-base and locus-wide FDR
    pvals_ex = finalize_format(pvals_ex)
    # Cache CSV GZ p-values
    pvals_all_csv_gz_path = tad_core$get_tad_perturb_pvals_path(
      model_name = model_name,
      b_all = TRUE,
      ext_no_dot = "csv.gz",
      base_dir = tad_base_dir
    )
    pvals_ex_csv_gz_path = tad_core$get_tad_perturb_pvals_path(
      model_name = model_name,
      b_all = FALSE,
      ext_no_dot = "csv.gz",
      base_dir = tad_base_dir
    )
    save_csv_gz(
      x = pvals_all,
      gz_path = pvals_all_csv_gz_path,
      col.names = TRUE,
      verbose = verbose
    )
    save_csv_gz(
      x = pvals_ex,
      gz_path = pvals_ex_csv_gz_path,
      col.names = TRUE,
      verbose = verbose
    )
    # Cache rdata p-values
    save_rdata(data = pvals_all,
               rdata_path = pvals_all_rdata_path,
               verbose = verbose)
    save_rdata(data = pvals_ex,
               rdata_path = pvals_ex_rdata_path,
               verbose = verbose)
  }
  stopifnot(nrow(pvals_ex) > 0)
  stopifnot(nrow(pvals_all) >= nrow(pvals_ex))
  # Map (i,j) chromatin interactions to 0-based index
  intr_chr_path = tad_core$get_tad_intr_chr_path(model_name = model_name,
                                                 base_dir = tad_base_dir)
  stopifnot(file.exists(intr_chr_path))
  intr_chr_df = read.csv(file = intr_chr_path,
                         header = FALSE,
                         stringsAsFactors = FALSE)
  colnames(intr_chr_df) = c("i0", "j0")
  intr_chr_df$i0 = as.integer(intr_chr_df$i0)
  intr_chr_df$j0 = as.integer(intr_chr_df$j0)
  rownames(intr_chr_df) = paste0(intr_chr_df$i0, '.', intr_chr_df$j0)
  intr_chr_df['id0'] = as.integer((1:nrow(intr_chr_df)) - 1)
  # Generate CMX edges
  cmx_edge_ix = which(pvals_ex$fdr < fdr_thresh)
  N_CMX_EDGE = length(cmx_edge_ix)
  if (N_CMX_EDGE >= 1) {
    pvals_edge_df = pvals_ex[cmx_edge_ix,]
    pvals_edge_df['src_name'] = paste0(pvals_edge_df$i0, '.', pvals_edge_df$j0)
    stopifnot(all(pvals_edge_df$src_name %in% rownames(intr_chr_df)))
    pvals_edge_df['src_id0'] = intr_chr_df[pvals_edge_df$src_name, "id0"]
    pvals_edge_df['dst_name'] = paste0(pvals_edge_df$k0, '.', pvals_edge_df$l0)
    stopifnot(all(pvals_edge_df$dst_name %in% rownames(intr_chr_df)))
    pvals_edge_df['dst_id0'] = intr_chr_df[pvals_edge_df$dst_name, "id0"]
    # Should not have any self edges
    stopifnot(!any(pvals_edge_df$src_id0 == pvals_edge_df$dst_id0))
    cmx_edge_mat = matrix(0, nrow = 2, ncol = N_CMX_EDGE)
    mode(cmx_edge_mat) <- "integer"
    cmx_edge_mat[1,] = as.integer(pvals_edge_df$src_id0)
    cmx_edge_mat[2,] = as.integer(pvals_edge_df$dst_id0)
    # Save CMX edges
    cmx_edge_path = tad_core$get_cmx_edge_ro_path(model_name = model_name,
                                                  base_dir = tad_base_dir)
    save_csv(x = cmx_edge_mat,
             csv_path = cmx_edge_path,
             verbose = verbose)
  } else {
    print(paste0("WARNING: NO CMX EDGES DETECTED AT ", model_name))
    print(paste0("CONSIDER SETTING FDR > ", fdr_thresh))
  }
  # Generate CMX target Hi-C frequences
  clust0_df_ex = i1j1_to_i0j0(clust1_df_ex)
  rownames(clust0_df_ex) = paste0(clust0_df_ex$i0, '.', clust0_df_ex$j0)
  stopifnot(all(rownames(clust0_df_ex) %in% rownames(intr_chr_df)))
  cmx_hic_fq = clust0_df_ex[rownames(intr_chr_df), "fq"]
  stopifnot(length(cmx_hic_fq) == nrow(intr_chr_df))
  stopifnot(all(cmx_hic_fq >= 0.0))
  stopifnot(all(cmx_hic_fq <= 1.0))
  cmx_hic_fq = as.matrix(cmx_hic_fq, nrow = length(cmx_hic_fq), ncol = 1)
  cmx_hic_path = tad_core$get_cmx_hic_path(model_name = model_name,
                                           base_dir = tad_base_dir)
  save_csv(x = cmx_hic_fq,
           csv_path = cmx_hic_path,
           verbose = verbose)
  return(invisible(list(all = pvals_all, ex = pvals_ex)))
}

# Generates CMX configuration files at each TAD locus:
#   cmx.edge.ro.csv - Column-major list of allowed edges
#   cmx.hic.csv - Target Hi-C frequencies
# Additionally generates p-value files:
#   perturb.pvals.all.csv|rdata - P-values and FDR results for locus
#     exemplar specific contacts to locus all specific contacts
#   perturb.pvals.ex.csv|rdata - P-values and FDR results for locus
#     exemplar specific contacts to each other
# @WARNING - ASSUMES 1:1 fragment to monomer bead mapping!
# @WARNING - ASSUUMES PERTURBATION REMOVE OUTLIERS = 1
# @WARNING - CMX edges and target Hi-C frequencies always overwritten!
# @param model_name -  Model name for resolving locus specific paths
# @param tad_base_dir - TAD base directory with all models
# @param fdr_thresh - False discovery rate threshold for calling
#   allowed edges
# @param perturb_base_dir_freqmat - Input base perturbations
#   directory, such that each subdirectory is a separate
#   perturbation experiment with summarized frequency matrix results
# @param null_blb - Null bootstrap distribution
# @param parallel_opt - one of "multicore" | "snow" | "no"
#   Specifies parallel processing platform
#   - multicore: uses "multicore" package only available on linux
#   - snow: uses "snow" package available on all platforms
#   - no: disable parallel processing
# @param ncpu - Number of CPUs to use for parallel computation
#   If ncpu <= 0, then all available CPUs are used!
# @param cl - An optional parallel::cluster. If not supplied and
#   ncpu > 1, a local cluster is created for duration of call
# @param overwrite - If TRUE, overwrites cached p-values
# @param verbose - If TRUE, log to stdout
# @return TRUE if loci processed, FALSE o/w
cmx_setup_loci <- function(tad_base_dir = tad_core$TAD_BASE_OUT_DIR,
                           fdr_thresh = tad_core$CMX_EDGE_FDR_THRESH,
                           perturb_base_dir_freqmat = tad_core$PERTURB_BASE_DIR,
                           null_blb = null_cache_ligc_popd_lband_merge(),
                           parallel_opt = DEF_PARALLEL_OPT,
                           ncpu = DEF_NCPU,
                           cl = NULL,
                           overwrite = DEF_SHOULD_OVERWRITE,
                           verbose = DEF_VERBOSE) {
  tad_ids = list.files(path = tad_base_dir,
                       include.dirs = FALSE,
                       recursive = FALSE)
  if (length(tad_ids) < 1) {
    return(invisible(FALSE))
  }
  # Process each individual TAD locus
  N_TADS = length(tad_ids)
  for (i in 1:N_TADS) {
    tad_id = tad_ids[i]
    if (verbose) {
      print("#####################################################")
      print(paste0(i, " of ", N_TADS, " : ", tad_id))
      print("#####################################################")
    }
    cmx_setup_locus(
      model_name = tad_id,
      tad_base_dir = tad_base_dir,
      fdr_thresh = fdr_thresh,
      perturb_base_dir_freqmat = perturb_base_dir_freqmat,
      null_blb = null_blb,
      parallel_opt = parallel_opt,
      ncpu = ncpu,
      cl = cl,
      overwrite = overwrite,
      verbose = verbose
    )
  }
  return(invisible(TRUE))
}

###################################################################
# B2P configure
###################################################################

# Generates B2P intr_chr configuration files at single locus. Note,
# exemplar config is always written (if num_intr_chr_config > 0)!
# @param model_name -  Model name for resolving locus specific paths
# @param tad_base_dir - TAD base directory with all models
# @param num_intr_chr_config - Number of cluster resamples for
#   chromatin-chromatin interaction configurations
# @param verbose - If TRUE, log to stdout
# @param FALSE if now configs written, TRUE o/w
b2p_setup_locus <- function(model_name = DEF_CMX_MODEL_NAME,
                            tad_base_dir = tad_core$TAD_BASE_OUT_DIR,
                            num_intr_chr_config = tad_core$B2P_NUM_INTR_CHR_CONFIG,
                            verbose = DEF_VERBOSE) {
  if (num_intr_chr_config < 1) {
    return(invisible(FALSE))
  }
  # Directory for chromatin-chromatin interaction configs
  b2p_intr_chr_dir = tad_core$get_b2p_intr_chr_dir(model_name = model_name,
                                                   base_dir = tad_base_dir)
  # Determines destination file name based on counter value
  get_b2p_intr_fid <- function(counter) {
    paste0(as.character(as.integer(counter)), '.csv')
  }
  # Copy core interactions
  intr_chr0_path = tad_core$get_tad_intr_chr_path(model_name = model_name,
                                                  base_dir = tad_base_dir)
  stopifnot(file.exists(intr_chr0_path))
  dst_path = file.path(b2p_intr_chr_dir, get_b2p_intr_fid(0))
  if (verbose) {
    print("Copying core interactions...")
    print(paste0("source: ", intr_chr0_path))
    print(paste0("destination: ", dst_path))
  }
  make_subdirs(dst_path)
  result = file.copy(from = intr_chr0_path,
                     to = dst_path,
                     overwrite = TRUE)
  stopifnot(result)
  num_intr_chr_config = as.integer(num_intr_chr_config - 1)
  if (num_intr_chr_config < 1) {
    # Early out, no resampling needed
    return(invisible(TRUE))
  }
  # Clustering results path (0-based indexing)
  clust0_path = tad_core$get_hic_clust_path(model_name = model_name,
                                            base_dir = tad_base_dir,
                                            ext_no_dot = "rdata")
  stopifnot(file.exists(clust0_path))
  clust0_df = load_rdata(clust0_path, verbose = verbose)
  clust0_df$i0 = as.integer(clust0_df$i0)
  clust0_df$j0 = as.integer(clust0_df$j0)
  rownames(clust0_df) = paste0(clust0_df$i0, '.', clust0_df$j0)
  # Map (i,j) chromatin interactions to 0-based index
  intr_chr0_df = read.csv(file = intr_chr0_path,
                          header = FALSE,
                          stringsAsFactors = FALSE)
  colnames(intr_chr0_df) = c("i0", "j0")
  intr_chr0_df$i0 = as.integer(intr_chr0_df$i0)
  intr_chr0_df$j0 = as.integer(intr_chr0_df$j0)
  rownames(intr_chr0_df) = paste0(intr_chr0_df$i0, '.', intr_chr0_df$j0)
  stopifnot(all(rownames(intr_chr0_df) %in% rownames(clust0_df)))
  # Get cid for core, i.e. exemplar, (i,j) tuples
  cid0 = clust0_df[rownames(intr_chr0_df), "cid0"]
  # Matrix of |NUM_CLUST| x |NUM_REP| where each row represents
  # cluster samples and rows are in same order as 'cid0'
  cmat = matrix(data = as.integer(0),
                nrow = length(cid0),
                ncol = num_intr_chr_config)
  # Resample from each cluster
  irow = rep(as.integer(1), num_intr_chr_config)
  for (i in 1:length(cid0)) {
    iclust = which(clust0_df$cid0 == cid0[i])
    if (length(iclust) == 1) {
      # Special case - cluster of size 1. The sample() method will
      # crash if trying to resample from cluster of size 1!
      irow = rep(iclust, num_intr_chr_config)
    } else {
      # Else, resample with replacement from cluster
      stopifnot(length(iclust) > 1)
      prob = clust0_df$fq[iclust]
      prob = prob / sum(prob)
      irow = sample(
        x = iclust,
        size = num_intr_chr_config,
        replace = TRUE,
        prob = prob
      )
    }
    cmat[i,] = irow
  }
  # Export resampled results
  for (counter in 1:num_intr_chr_config) {
    dst_path = file.path(b2p_intr_chr_dir, get_b2p_intr_fid(counter))
    rep_df = clust0_df[cmat[, counter], c("i0", "j0")]
    save_csv(
      x = rep_df,
      csv_path = dst_path,
      row.names = FALSE,
      col.names = FALSE,
      verbose = verbose
    )
  }
  return(invisible(TRUE))
}

# Generates B2P intr_chr configuration files at multiple TAD loci
# @param tad_base_dir - TAD base directory with all models
# @param num_intr_chr_config - Number of cluster resamples for
#   chromatin-chromatin interaction configurations
# @param verbose - If TRUE, log to stdout
b2p_setup_loci <- function(tad_base_dir = tad_core$TAD_BASE_OUT_DIR,
                           num_intr_chr_config = tad_core$B2P_NUM_INTR_CHR_CONFIG,
                           verbose = DEF_VERBOSE) {
  tad_ids = list.files(path = tad_base_dir,
                       include.dirs = FALSE,
                       recursive = FALSE)
  if (length(tad_ids) < 1) {
    return(invisible(FALSE))
  }
  # Process each individual TAD locus
  N_TADS = length(tad_ids)
  for (i in 1:N_TADS) {
    tad_id = tad_ids[i]
    if (verbose) {
      print("#####################################################")
      print(paste0(i, " of ", N_TADS, " : ", tad_id))
      print("#####################################################")
    }
    b2p_setup_locus(
      model_name = tad_id,
      tad_base_dir = tad_base_dir,
      num_intr_chr_config = num_intr_chr_config,
      verbose = verbose
    )
  }
  return(invisible(TRUE))
}

###################################################################
# CMX unlink
###################################################################

# Delete target CMX results at TAD locus
# @param model_name -  Model name for resolving locus specific paths
# @param tad_base_dir - TAD base directory with all models
# @param pop_key - CMX population model (spop|mpop)
# @param cmx_tag - Optional suffix string CMX results directory
# @param verbose - If TRUE, log to stdout
cmx_unlink_locus <-
  function(model_name = DEF_CMX_MODEL_NAME,
           tad_base_dir = tad_core$TAD_BASE_OUT_DIR,
           pop_key = tad_core$CMX_POP_KEY,
           tag = tad_core$CMX_TAG,
           verbose = DEF_VERBOSE) {
    cmx_results_dir = tad_core$get_cmx_results_dir(
      model_name = model_name,
      pop_key = pop_key,
      tag = tag,
      base_dir = tad_base_dir
    )
    if (dir.exists(cmx_results_dir)) {
      if (verbose) {
        print(paste0("Unlinking ", cmx_results_dir))
      }
      unlink(x = cmx_results_dir,
             recursive = TRUE,
             force = TRUE)
    } else {
      if (verbose) {
        print(paste0("Skipping (not found) ", cmx_results_dir))
      }
    }
  }

# Delete target CMX results at mutliple TAD loci
# @param tad_base_dir - TAD base directory with all models
# @param pop_key - CMX population model (spop|mpop)
# @param cmx_tag - Optional suffix string CMX results directory
# @param verbose - If TRUE, log to stdout
cmx_unlink_loci <-
  function(tad_base_dir = tad_core$TAD_BASE_OUT_DIR,
           pop_key = tad_core$CMX_POP_KEY,
           tag = tad_core$CMX_TAG,
           verbose = DEF_VERBOSE) {
    tad_ids = list.files(path = tad_base_dir,
                         include.dirs = FALSE,
                         recursive = FALSE)
    if (length(tad_ids) < 1) {
      return(invisible(FALSE))
    }
    for (model_name in tad_ids) {
      cmx_unlink_locus(
        model_name = model_name,
        tad_base_dir = tad_base_dir,
        pop_key = pop_key,
        tag = tag,
        verbose = verbose
      )
    }
    return(invisible(TRUE))
  }
