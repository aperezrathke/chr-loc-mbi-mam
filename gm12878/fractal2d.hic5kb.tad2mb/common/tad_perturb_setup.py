#!/usr/bin/python
# -*- coding: utf-8 -*-

# Script merges all TAD model interactions into single file for use with
#   null simulation setup.
#
# The following arguments are optional: (short-hand|long-hand)
#
#   [-bd|--base_dir]: [Optional] TAD base, non-versioned, output directory,
#       default is specified in tad_core.py
#   [-h|--help]: [Optional] Display help (i.e. usage information)
#   [-o|--out]: [Optional] Output master interactions file path

###################################################
# Imports
###################################################

# For parsing user supplied arguments
import argparse

# For path utilities
import os

# For exiting if inputs invalid
import sys

# For default configuration settings
import tad_core

###################################################
# TAD perturb setup
###################################################

# @param args - User arguments
def tad_perturb_setup(args):
    '''Process chr-chr interactions at each TAD model'''
    # List of active TAD models
    base_dir = args.base_dir
    model_names = tad_core.resolve_tad_model_names(from_table=0,
                                                   base_dir=base_dir)
    # Master interactions
    intrtup_master = set([])
    n_total = 0
    for mix, mid in enumerate(model_names):
        # Load chr-chr interactions
        intr_path = tad_core.get_tad_intr_chr_path(model_name=mid,
                                                   base_dir=base_dir)
        intr = tad_core.parse_csv_pairs(fpath=intr_path)
        intrtup = zip(intr['i'], intr['j'])
        n_model = len(intrtup)
        n_master_pre = len(intrtup_master)
        intrtup_master.update(intrtup)
        n_master_post = len(intrtup_master)
        n_kept = n_master_post - n_master_pre
        perc_kept = 100.0 * float(n_kept) / float(n_model)
        print "Loaded " + str(n_model) + ", kept " + str(n_kept) + " (" + \
            str(perc_kept) + "%) from:\n\t" + intr_path
        n_total = n_total + n_model
    # Sort
    intrtup_master = sorted(list(intrtup_master))
    n_kept = len(intrtup_master)
    perc_kept = 100.0 * float(n_kept) / float(n_total)
    # Save master interactions
    print "Loaded " + str(n_total) + " interactions"
    print "Kept " + str(n_kept) + " (" + str(perc_kept) + "%) interactions"
    print "Saving master file at " + args.out
    tad_core.make_dir(os.path.dirname(args.out))
    # Open in binary mode to force consistent new line termination
    with open(args.out, 'wb') as f:
        for tup in intrtup_master:
            f.write(str(tup[0]) + ',' + str(tup[1]) + '\n')
 
###################################################
# Main
###################################################

# Main script entry point
def __main__():
    print "======================= tad_perturb ======================="
    # Initialize command line parser
    parser = argparse.ArgumentParser()
    parser.add_argument('-bd', '--base_dir', default=tad_core.TAD_BASE_OUT_DIR,
                        help='Base directory storing unversioned TAD model data')
    parser.add_argument('-o', '--out', default=tad_core.PERTURB_INTR_CHR_PATH,
                        help='Output master interactions file path')
    # Parse command line
    args = parser.parse_args()
    tad_core.print_cmd(args)
    # Run perturbations
    tad_perturb_setup(args)
    print "Finished."

# Run main if we are the active script
if __name__ == '__main__':
    __main__()
