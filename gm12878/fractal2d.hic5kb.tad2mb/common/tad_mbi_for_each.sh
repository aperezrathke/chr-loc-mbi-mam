#!/bin/bash

# Path to this script's directory
SCRIPT_PATH="$(readlink -f $0)"
SCRIPT_DIR="$(dirname $SCRIPT_PATH)"
SCRIPT_DIR=$(cd "$SCRIPT_DIR"; pwd)

# @HACK - mirror value in tad_core.py
TAD_BASE_OUT_DIR="$SCRIPT_DIR/../output/tad"

# Count number of TAD model directories
# @HACK - assume all files are directories
NUM_TAD=$(ls -1 "$TAD_BASE_OUT_DIR/" | wc -l)

# Target MBI pipeline
SH_TARGET="$SCRIPT_DIR/tad_mbi.sh"

# Iterate over TAD regions
echo "Processing $NUM_TAD TAD regions"
for ((i=0;i<NUM_TAD;i++)); do
    cmd="$SH_TARGET $i"
    echo "Running: $cmd"
    $cmd
done

echo "Finished MBI for each"
