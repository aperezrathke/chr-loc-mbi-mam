#!/bin/bash

# Path to this script's directory
SCRIPT_PATH="$(readlink -f $0)"
SCRIPT_DIR="$(dirname $SCRIPT_PATH)"
SCRIPT_DIR=$(cd "$SCRIPT_DIR"; pwd)

# MBI R utilities
RSHELL_SCRIPT="$SCRIPT_DIR/tad_mbi_shell.R"

####################################################################
# MBI 3-body options

# 'top_k_spr' : Limit to default top k=5 regions based on # of SE SPRITE hits
# 'num_se' : Limit to TAD regions with k=2 SEs
MODEL_FILT_ID="num_se"

# Only use first 'ens_sz' random samples for null modeling many body
#   interactions at each TAD locus, this is a performance vs quality tradeoff
# Note, if None or < 1, all samples used
NULL_ENS_SZ=75000

# Only retain ligc (i,j) tuples with |j-i| > min_gd
EDGE_MIN_GD=4

# Minimum clique size
CLQ_MIN=3

# Maximum clique size
CLQ_MAX=3

# Method used to summarize each clique and map to a bin
CLQ_MAP_ID="3b"

# Clique type - 'any' means clique does not have to be maximal
CLQ_TYPE="any"

# Bootstrap statistic
BOOT_STAT_ID="mea"

# Number of bootstrap replicates
BOOT_REPS=1000

# P-value startification method, all bootstrap elements with same test label
#   as observed test label will be used to compute p-value
PVTEST_BY_ID='body_maxgd_mingd'

####################################################################
# MBI 3-body NULL counts

# @HACK - Hard-coded CPU count to avoid out of RAM errors
NCPU=20

cmd="Rscript --vanilla $RSHELL_SCRIPT"
cmd="$cmd --util_mbi_cache_null_counts"
cmd="$cmd --num_cpu $NCPU"
cmd="$cmd --model_filt_id $MODEL_FILT_ID"
cmd="$cmd --null_ens_sz $NULL_ENS_SZ"
cmd="$cmd --edge_min_gd $EDGE_MIN_GD"
cmd="$cmd --clq_min $CLQ_MIN"
cmd="$cmd --clq_max $CLQ_MAX"
cmd="$cmd --clq_map_id $CLQ_MAP_ID"
cmd="$cmd --clq_type $CLQ_TYPE"

# Run command line
echo "Running:"
echo $cmd
$cmd
echo "Finished NULL counts."

####################################################################
# MBI 3-body bootstrap

# Number of CPU cores for bootstrap, reduce this number if RAM usage is issue
# @TODO - Determine maximum number of CPU cores implicitly within mbi_boot()
#   based on memory status after gc() call!
NCPU=5

cmd="Rscript --vanilla $RSHELL_SCRIPT"
cmd="$cmd --util_mbi_cache_boot"
cmd="$cmd --num_cpu $NCPU"
cmd="$cmd --model_filt_id $MODEL_FILT_ID"
cmd="$cmd --boot_stat_id $BOOT_STAT_ID"
cmd="$cmd --boot_reps $BOOT_REPS"
cmd="$cmd --edge_min_gd $EDGE_MIN_GD"
cmd="$cmd --clq_min $CLQ_MIN"
cmd="$cmd --clq_max $CLQ_MAX"
cmd="$cmd --clq_map_id $CLQ_MAP_ID"
cmd="$cmd --clq_type $CLQ_TYPE"

# Run command line
echo "Running:"
echo $cmd
$cmd
echo "Finished bootstrap."

####################################################################
# MBI 3-body B2P counts

# @HACK - Hard-coded CPU count to avoid out of RAM errors
NCPU=20

cmd="Rscript --vanilla $RSHELL_SCRIPT"
cmd="$cmd --util_mbi_cache_b2p_counts"
cmd="$cmd --num_cpu $NCPU"
cmd="$cmd --model_filt_id $MODEL_FILT_ID"
cmd="$cmd --null_ens_sz $NULL_ENS_SZ"
cmd="$cmd --edge_min_gd $EDGE_MIN_GD"
cmd="$cmd --clq_min $CLQ_MIN"
cmd="$cmd --clq_max $CLQ_MAX"
cmd="$cmd --clq_map_id $CLQ_MAP_ID"
cmd="$cmd --clq_type $CLQ_TYPE"

# Run command line
echo "Running:"
echo $cmd
$cmd
echo "Finished B2P counts."

####################################################################
# MBI 3-body p-values

# @HACK - avoid out of RAM errors
NCPU=10

# Many body bootstrap
cmd="Rscript --vanilla $RSHELL_SCRIPT"
cmd="$cmd --util_mbi_cache_pvals"
cmd="$cmd --num_cpu $NCPU"
cmd="$cmd --model_filt_id $MODEL_FILT_ID"
cmd="$cmd --boot_stat_id $BOOT_STAT_ID"
cmd="$cmd --boot_reps $BOOT_REPS"
cmd="$cmd --edge_min_gd $EDGE_MIN_GD"
cmd="$cmd --clq_min $CLQ_MIN"
cmd="$cmd --clq_max $CLQ_MAX"
cmd="$cmd --clq_map_id $CLQ_MAP_ID"
cmd="$cmd --clq_type $CLQ_TYPE"
cmd="$cmd --mbi_pvtest_by_id $PVTEST_BY_ID"

# Run command line
echo "Running:"
echo $cmd
$cmd
echo "Finished p-values."
