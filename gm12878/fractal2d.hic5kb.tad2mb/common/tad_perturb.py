#!/usr/bin/python
# -*- coding: utf-8 -*-

# Script runs knock-in perturbations to assess contact dependencies.
# ASSUMES tad_perturb_setup.py HAS ALREADY BEEN RUN!
# ASSUMES 1:1 fragment-to-monomer mapping
#
# The following arguments are optional: (short-hand|long-hand)
#
#   [-a|--append]: [Optional] Will always generate requested ensemble size
#       and update any existing frequency matrix with new ensemble. If ligc
#       ensemble already exists, will only generate new samples until target
#       size is reached. DEFAULT IS TO SKIP FOLD IF FREQMAT RDATA EXISTS!
#   [-arch|--arch_conf]: Overloads archetype configuration INI path
#   [-clobfld|--clobber_fold]: [Optional] If switch is present, any ligc data
#       is erased at program start and replaced by results of program run.
#       Default is to:
#           i) Avoid folding if frequency matrix RData present or
#           ii) Resume folding if no matrix RData
#   [-clobfmt|--clobber_freqmat]: [Optional] If switch is present, existing
#       frequency matrix (fmat) data is erased at program start and replaced
#       by results of program run.
#   [-cmd|--cmd_key]: [Optional] Model command key passed to the 'main'
#       dispatch utility, default is tad_core.PERTURB_CMD_KEY
#   [-cnf|--conf]: [Optional] Path to top-level model INI configuration,
#       default is given by PERTURB_TOP_INI_PATH in tad_core.py
#   [-dki|--dist_ki] <+real>: [Optional] Scalar knock-in distance in Angstroms.
#       A knock-in constraint is satisfied if at least one monomer node from
#       each fragment region is within this distance. May also be specified as
#       'chr_knock_in_dist = <+real>' within a configuration INI; the folder
#       binary also has a hard-coded default if not explicitly set by user.
#   [-e|--exe] <path>: [Optional] Path to executable folder binary with 'main'
#       dispatch utility with chr-chr interaction support, default is given by
#       tad_core.PERTURB_EXE_PATH
#   [-ens|--ensemble_size] <integer>: Target perturbation ensemble size
#   [-h|--help]: [Optional] Display help (i.e. usage information)
#   [-i|--intr]: [Optional] Input file with [significant] chromatin-to-
#       chromatin proximity interactions encoded as pairs of 0-based fragment
#       identifiers. Default is given by tad_core.PERTURB_INTR_CHR_PATH
#   [-kix|--knock_in_index] <integer>: [Optional] If specified, then will only
#       launch infea for parameter 0-based index into file at 'intr_chr' path.
#       If not specified (default behavior), all interactions are processed -
#       where 'intr_chr' is given by tad_core.get_tad_intr_chr_path(). ONLY
#       USED IF '--model_index' ALSO USED!
#   [-mf|--max_fail] <integer>: [Optional] Maximum number of failures allowed
#       for a single job configuration. If a job error code is encountered
#       more than this threshold, the job command line is reported and program
#       terminates
#   [-mp|--max_proc] <+integer>: [Optional] Maximum number of parallel
#       processes (or equivalently threads) to use to work through each
#       perturbation ensemble. Note: <--max_proc> should not exceed CPU
#       logical cores
#   [-mt|--max_trial] <+integer>: [Optional] Max number of trials to attempt
#       folding a given perturbation constraint configuration before giving up
#   [-pbd_fold|--perturb_base_dir_fold]: [Optional] Perturbation base output
#       directory for ligc folding, default is tad_core.PERTURB_BASE_DIR
#   [-pdb_fmt|--perturb_base_dir_freqmat]: [Optional] Perturbation base output
#       directory for frequency matrix calculation, default is
#       tad_core.PERTURB_BASE_DIR
#
# SUMMARY of major control flow options:
#
# clobber_fold - Erases any ligc and logw data at program start
# clobber_freqmat - Erases any freqmat data at program start
# append - Always generates minimum requested ensemble size where:
#       i) If existing ligc data, will generate up to target ensemble unless
#           existing ensemble is already at or greater than target
#       ii) Will create or update existing frequency matrix with new ensemble
#
# NOTE: If 'append' is False, then default behavior is to AVOID generating any
#   fold or frequency matrix data WHEN frequency matrix RData is present. If
#   no matrix data, then default behavior is as if append is True.

###################################################
# Imports
###################################################

# For parsing user supplied arguments
import argparse

# For path utilities
import os

# For cleaning unneeded ligc data
import shutil

# For exiting if inputs invalid
import sys

# For default configuration settings
import tad_core

# For generating random unique identifiers
import uuid
import base64

###################################################
# Defaults
###################################################

# Default number of parallel processes, should not exceed logical cores
DEF_MAX_PROC = tad_core.MAX_PROC

# Default number of times a single job may return an abnormal error code. In
# my experience working on public compute clusters (with possibly ancient
# operating systems), background subprocesses may occasionally fail to launch
# and return with a non-zero error code. In this case, we can try to re-launch
# the process a few times to help mitigate this issue. If the subprocess still
# fails with an abnormal error code, then it's likely to be a user
# configuration error causing the folder simulation itself to crash!
DEF_MAX_FAIL = 2

# Default knock-in perturbation index
DEF_KNOCK_IN_INDEX = None

###################################################
# TAD perturb
###################################################

# @param cmd - Command line to execute
# @param max_fail - Max number of failures
def run_cmd(cmd, max_fail):
    '''Run blocking subprocess'''
    tad_core.run_cmd(cmd=cmd, max_fail=max_fail)

# @param dirpath - Path to directory
def try_rmtree(dirpath):
    """Attempts to remove directory tree"""
    if os.path.exists(dirpath):
        print "[tad_perturb] Removing directory tree:\n\t" + dirpath
        shutil.rmtree(dirpath, ignore_errors=True)

# ASSUMES 1:1 fragment-to-monomer mapping
# @param kix - Knock-in perturbation index
# @param intr - Dictionary { "i": [], "j": [] } of chr-chr interaction tuples
# @param perturb_base_dir - TAD base directory
def get_perturb_dir(kix, intr, perturb_base_dir):
    """Return perturbation ensemble output directory"""
    i = intr["i"][kix]
    j = intr["j"][kix]
    return tad_core.get_perturb_dir(frag_a_lo=i, frag_a_hi=i, frag_b_lo=j,
                                    frag_b_hi=j,
                                    perturb_base_dir=perturb_base_dir)

# @param perturb_dir - Perturbation ensemble output directory
def has_freqmat(perturb_dir):
    """Return True if cached frequency matrix RData exists, False o/w"""
    ligc_freqmat_rdata_path = \
        tad_core.get_perturb_ligc_freqmat_rdata_path(
            rmvout=tad_core.PERTURB_REMOVE_OUTLIERS,
            perturb_dir=perturb_dir)
    return os.path.exists(ligc_freqmat_rdata_path)

# @param perturb_dir_fold - Perturbation folding ensemble output directory
# @param perturb_dir_freqmat - Frequeny matrix summary output directory
# @param args - User arguments
def get_fold_ens_size(perturb_dir_fold, perturb_dir_freqmat, args):
    """Determine [remaining] ensemble size"""
    if has_freqmat(perturb_dir_freqmat) and (not args.append):
        print "[tad_perturb] Avoiding folding as cached matrix RData exists"
        return 0
    # Determine number of existing ligc samples
    ensemble_size = int(args.ensemble_size)
    ligc_csv_gz_dir = \
        tad_core.get_perturb_ligc_csv_gz_dir(perturb_dir=perturb_dir_fold)
    if os.path.exists(ligc_csv_gz_dir):
        ligc_files = os.listdir(ligc_csv_gz_dir)
        prev_size = len(ligc_files)
        ensemble_size = ensemble_size - prev_size
        print "[tad_perturb] Found " + str(prev_size) + \
                " existing ligc samples"
    return max(ensemble_size, 0)

# @param perturb_dir_fold - Perturbation folding ensemble output directory
# @param perturb_dir_freqmat - Frequeny matrix summary output directory
# @param clobber_fold - If True, erase any existing ligc data
# @param clobber_freqmat - If True, erase any existing matrix data
def clobber(perturb_dir_fold, perturb_dir_freqmat, clobber_fold=False,
            clobber_freqmat=False):
    """Conditionally clobber any requested subfolders"""
    # Check fold data
    if clobber_fold:
        ligc_csv_gz_dir = \
            tad_core.get_perturb_ligc_csv_gz_dir(perturb_dir=perturb_dir_fold)
        try_rmtree(ligc_csv_gz_dir)
        logw_dir = \
            tad_core.get_perturb_logw_dir(perturb_dir=perturb_dir_fold)
        try_rmtree(logw_dir)
    # Check frequency matrix data
    if clobber_freqmat:
        freqmat_csv_gz_dir = \
            tad_core.get_perturb_ligc_freqmat_csv_gz_dir(perturb_dir= \
                                                            perturb_dir_freqmat)
        try_rmtree(freqmat_csv_gz_dir)
        freqmat_rdata_dir = \
            tad_core.get_perturb_ligc_freqmat_rdata_dir(perturb_dir= \
                                                            perturb_dir_freqmat)
        try_rmtree(freqmat_rdata_dir)

# @param kix - Knock-in perturbation index
# @param intr_path - Path to chr-chr interactions file
# @param perturb_dir_fold - Perturbation folding ensemble output directory
# @param perturb_dir_freqmat - Frequeny matrix summary output directory
# @param args - User arguments
def fold(kix, intr_path, perturb_dir_fold, perturb_dir_freqmat, args):
    '''Blocking call to generate perturbation ensemble'''
    # Determine ensemble size
    ensemble_size = get_fold_ens_size(perturb_dir_fold=perturb_dir_fold,
                                      perturb_dir_freqmat=perturb_dir_freqmat,
                                      args=args)
    if ensemble_size < 1:
        # Early out if no folding needed
        return
    ################################################################
    # Arguments
    # Executable path
    cmd = [args.exe]
    # Dispatch
    cmd.extend(["--main_dispatch", args.cmd_key])
    # Conf (top level) INI path
    cmd.extend(["--conf", args.conf])
    # Max trials
    cmd.extend(["--max_trials", str(args.max_trial)])
    # Top-level ensemble size
    cmd.extend(["--ensemble_size", str(ensemble_size)])
    # Worker threads
    cmd.extend(["--num_worker_threads", str(args.max_proc)])
    # Archetype INI path
    arch = args.arch_conf
    cmd.extend(["--arch", arch])
    # Note, not setting frags, assuming 1:1 with monomer beads
    # Knock-in chr-chr interactions path
    cmd.extend(["--chr_knock_in", intr_path])
    # Knock-in distance (assumed scalar)
    cmd.extend(["--chr_knock_in_dist", str(args.dist_ki)])
    # Knock-in index mask
    cmd.extend(["--chr_knock_in_index_mask", str(kix)])
    # Perturbation output directory
    cmd.extend(["--output_dir", perturb_dir_fold])
    # Job identifier
    job_id = "prtrb" + "." + \
                base64.urlsafe_b64encode(uuid.uuid4().bytes).rstrip('=')
    cmd.extend(["--job_id", job_id])
    # Export arg
    cmd.extend(["-export_ligc_csv_gz", "-export_log_weights"])
    # Blocking call
    run_cmd(cmd=cmd, max_fail=args.max_fail)

# @param perturb_dir_fold - Perturbation folding ensemble output directory
# @param perturb_dir_freqmat - Frequeny matrix summary output directory
# @param args - User arguments
def ligc2freqmat(perturb_dir_fold, perturb_dir_freqmat, args):
    """Summarize ligc data as contact frequency matrix"""
    # Check if we can avoid this call
    if has_freqmat(perturb_dir_freqmat) and (not args.append):
        print "[tad_perturb] Avoiding ligc2freqmat as cached matrix RData exists"
        return
    # Build command line
    scdir = tad_core.SCRIPT_DIR
    scname = "tad_perturb_shell.R"
    scpath = os.path.join(scdir, scname)
    cmd = ["Rscript", "--vanilla", scpath,
           "--util_perturb_cache_ligc_freqmat"]
    ligc_csv_dir = \
        tad_core.get_perturb_ligc_csv_gz_dir(perturb_dir=perturb_dir_fold)
    cmd.extend(["--ligc_csv_dir", ligc_csv_dir])
    logw_dir = tad_core.get_perturb_logw_dir(perturb_dir=perturb_dir_fold)
    cmd.extend(["--logw_dir", logw_dir])
    rmvout = tad_core.PERTURB_REMOVE_OUTLIERS
    ligc_freqmat_csv_gz_path = \
        tad_core.get_perturb_ligc_freqmat_csv_gz_path(rmvout=rmvout,
                                                      perturb_dir= \
                                                        perturb_dir_freqmat)
    cmd.extend(["--ligc_freqmat_csv_gz_path", ligc_freqmat_csv_gz_path])
    ligc_freqmat_rdata_path = \
        tad_core.get_perturb_ligc_freqmat_rdata_path(rmvout=rmvout,
                                                     perturb_dir= \
                                                        perturb_dir_freqmat)
    cmd.extend(["--ligc_freqmat_rdata_path", ligc_freqmat_rdata_path])
    if rmvout:
        cmd.append("--rmvout")
    else:
        cmd.append("--no-rmvout")
    if args.append:
        cmd.append("--append")
    else:
        cmd.append("--no-append")
    # Blocking call
    run_cmd(cmd=cmd, max_fail=args.max_fail)

# @param perturb_dir_fold - Perturbation folding ensemble output directory
# @param perturb_dir_freqmat - Frequeny matrix summary output directory
def clean(perturb_dir_fold, perturb_dir_freqmat):
    """Cleans ligc data if frequency matrix data exists"""
    if not has_freqmat(perturb_dir=perturb_dir_freqmat):
        print "[tad_perturb] Avoiding clean, no frequency matrix RData found"
        return
    # Remove ligc fold data to save disk space
    clobber(perturb_dir_fold=perturb_dir_fold,
            perturb_dir_freqmat=perturb_dir_freqmat,
            clobber_fold=True,
            clobber_freqmat=False)

# @param kix - Knock-in perturbation index
# @param intr - Dictionary { "i": [], "j": [] } of chr-chr interaction tuples
# @param intr_path - Path to chr-chr interactions file
# @param args - User arguments
def launch_perturb(kix, intr, intr_path, args):
    '''Process a single TAD model'''
    print '[tad_perturb] Processing kix = ' + str(kix)
    # Determine perturbation ensemble output directory
    perturb_dir_fold = get_perturb_dir(kix=kix, intr=intr,
                                       perturb_base_dir= \
                                        args.perturb_base_dir_fold)
    # Determine pertubation frequency matrix output directory
    perturb_dir_freqmat = get_perturb_dir(kix=kix, intr=intr,
                                          perturb_base_dir= \
                                            args.perturb_base_dir_freqmat)
    # Perform clobber(s) if requested
    clobber(perturb_dir_fold=perturb_dir_fold,
            perturb_dir_freqmat=perturb_dir_freqmat,
            clobber_fold=args.clobber_fold,
            clobber_freqmat=args.clobber_freqmat)
    # Generate perturbation ensemble
    fold(kix=kix, intr_path=intr_path, perturb_dir_fold=perturb_dir_fold,
         perturb_dir_freqmat=perturb_dir_freqmat, args=args)
    # Summarize ligc into frequency matrix
    ligc2freqmat(perturb_dir_fold=perturb_dir_fold,
                 perturb_dir_freqmat=perturb_dir_freqmat, args=args)
    # Check if clean-up possible
    clean(perturb_dir_fold=perturb_dir_fold,
          perturb_dir_freqmat=perturb_dir_freqmat)
    # @TODO - compute p-values and FDR

# @param args - User arguments
def tad_perturb(args):
    '''Calls fea utility at each TAD model'''
    # Load chr-chr interactions
    intr_path = args.intr
    print "[tad_perturb] Loading " + str(intr_path)
    intr = tad_core.parse_csv_pairs(fpath=intr_path)
    assert len(intr["i"]) == len(intr["j"])
    NUM_INTR = len(intr["i"])
    if args.knock_in_index is not None:
        # Process single perturbation
        kix = int(args.knock_in_index)
        assert (kix >= 0) and (kix < NUM_INTR)
        launch_perturb(kix=kix, intr=intr, intr_path=intr_path, args=args)
    else:
        # Process all perturbations
        for kix in xrange(NUM_INTR):
            launch_perturb(kix=kix, intr=intr, intr_path=intr_path, args=args)

###################################################
# Main
###################################################

# Main script entry point
def __main__():
    print "======================= tad_perturb ======================="
    # Initialize command line parser
    parser = argparse.ArgumentParser()
    parser.add_argument('-a', '--append', action='store_true',
                        help='If present, will always generate target ensemble size and update any existing frequency matrix, else program avoids folding and frequency matrix computation if matrix RData already present.')
    parser.add_argument('-arch', '--arch_conf',
                        default=tad_core.PERTURB_ARCH_INI_PATH,
                        help='Model INI archetype configuration file path passed to the folder')
    parser.add_argument('-clobfld', '--clobber_fold', action='store_true',
                        help='If present, any ligc ensemble is erased at program start and replaced with results of program run.')
    parser.add_argument('-clobfmt', '--clobber_freqmat', action='store_true',
                        help='If present, any existing frequency matrix is erased at program start and replaced with results of program run.')
    parser.add_argument('-cmd', '--cmd_key', default=tad_core.PERTURB_CMD_KEY,
                        help='Model command key passed to the folding simulation "main" utility')
    parser.add_argument('-cnf', '--conf',
                        default=tad_core.PERTURB_TOP_INI_PATH,
                        help='Model INI configuration file path passed to the "main" utility')
    parser.add_argument('-dki', '--dist_ki', default=tad_core.PERTURB_DIST_KI,
                        help='Scalar knock-in distance in Angstroms')
    parser.add_argument('-e', '--exe', default=tad_core.PERTURB_EXE_PATH,
                        help='Path to folding simulation executable binary with "main" utility')
    parser.add_argument('-ens', '--ensemble_size',
                        default=tad_core.PERTURB_ENSEMBLE_SIZE,
                        help='Perturbation ensemble size')
    # Default interactions file created by tad_perturb_setup.py!
    parser.add_argument('-i', '--intr', default=tad_core.PERTURB_INTR_CHR_PATH,
                        help='Input file with [significant] chromatin-to-chromatin proximity interactions encoded as pairs of 0-based fragment identifiers')
    parser.add_argument('-mf', '--max_fail', default=DEF_MAX_FAIL,
                        help='Maximum number of times a single job configuration may fail with error return code')
    parser.add_argument('-kix', '--knock_in_index', default=DEF_KNOCK_IN_INDEX,
                        help='Optional 0-based index into chr-chr interactions file, only used if model index also specified. If not specified (default), all interactions will be perturbed.')
    parser.add_argument('-mp', '--max_proc', default=DEF_MAX_PROC,
                        help='Maximum number of parallel processes (or threads)')
    parser.add_argument('-mt', '--max_trial', default=tad_core.PERTURB_MAX_TRIAL,
                        help='Max number of trials to attempt folding a given perturbation constraint configuration before giving up')
    parser.add_argument('-pbd_fold', '--perturb_base_dir_fold',
                        default=tad_core.PERTURB_BASE_DIR,
                        help='Perturbation ligc folding base output directory')
    parser.add_argument('-pbd_fmt', '--perturb_base_dir_freqmat',
                        default=tad_core.PERTURB_BASE_DIR,
                        help='Perturbation ligc frequency matrix base output directory')
    # Parse command line
    args = parser.parse_args()
    tad_core.print_cmd(args)
    # Run perturbations
    tad_perturb(args)
    print "Finished."

# Run main if we are the active script
if __name__ == '__main__':
    __main__()
