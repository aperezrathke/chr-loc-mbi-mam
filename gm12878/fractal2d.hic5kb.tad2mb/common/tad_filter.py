#!/usr/bin/python
# -*- coding: utf-8 -*-

# Filters TAD models - removes TAD-bounded bounded SPRITE clusters which
# exceed overlap threshold with higher-ranked (i.e. more SPRITE hits)
# bounded clusters. Also removes TAD regions with too few SPRITE hits.
#
# Script accepts the following arguments (short|long):
#   -spr|--sprite <path> : Path to input processed SPRITE tabular data
#   -fo|--fout <path> : Path to write filtered TAD tabular data
#   -maxov|--max_overlap <real in [0,1]>: Max fractional overlap with previous
#       models, any models which exceed this overlap fraction are culled.
#   -minspr|--min_sprite_hits <integer>: Minimum number of associated SPRITE
#       clusters needed to retain a model, any models with less than this
#       value are culled.

###################################################
# Imports
###################################################

# For parsing user supplied arguments
import argparse

# For vector operations
import numpy as np

# For parsing tabular data
import pandas as pd

# Named records
from collections import namedtuple

# For default configuration settings
import tad_core

###################################################
# Named tuples
###################################################

# Overlap record
# .ix - Row index key containing max overlap
# .ov - Overlap fraction in [0,1]
OvRec = namedtuple('OvRec', "ix, ov")

###################################################
# Utilities
###################################################

# @param row - Data.frame row as namedtuple
def get_model_name(row):
    '''Return model name from row entry'''
    return '.'.join([str(row.chr), str(int(row.bp_start_tad_med)),
                     str(int(row.bp_end_tad_med))])

# @param row - Data.frame row as namedtuple within df
# @param df - Data.frame containing row
# @return OvRec named tuple
def get_max_overlap(row, df):
    '''Return maximum overlap fraction of row with previous rows'''
    # Get preceding rows
    # @HACK - explicitly check: row.Index < 1. Am seeing different behavior on
    #   Windows vs CentOs. On Windows, df.head(0) will return empty data.frame
    #   but on CentOs, it is returning first row which leads to 100% overlap
    #   and first row erroneously being culled.
    if row.Index < 1:
        return OvRec(ix=None, ov=0.0)
    df_prev = df.head(row.Index)
    if df_prev.empty:
        return OvRec(ix=None, ov=0.0)
    # Keep only items on same chromosome
    df_chr = df_prev.loc[df_prev.chr == row.chr]
    if df_chr.empty:
        return OvRec(ix=None, ov=0.0)
    # Keep only overlapping rows
    has_overlap = (df_chr.bp_start_tad_med <= row.bp_end_tad_med) & \
                    (df_chr.bp_end_tad_med >= row.bp_start_tad_med)
    df_overlap = df_chr.loc[has_overlap]
    if df_overlap.empty:
        return OvRec(ix=None, ov=0.0)
    # Get overlap range
    bp_start_ov = np.maximum(df_overlap.bp_start_tad_med, row.bp_start_tad_med)
    bp_end_ov = np.minimum(df_overlap.bp_end_tad_med, row.bp_end_tad_med)
    bp_span_ov = bp_end_ov - bp_start_ov
    overlap = bp_span_ov / row.bp_span_tad_med
    max_overlap_arg = np.argmax(overlap)
    max_overlap = overlap[max_overlap_arg]
    return OvRec(ix=max_overlap_arg, ov=max_overlap)

# @param df_sprite - Data.frame with processed SPRITE data
# @param max_overlap - Rows of df_sprite must not exceed this overlap fraction
# @param min_sprite_hits - Rows of df_sprite must have at least this many
#   SPRITE cluster associations
# @return Data.frame of filtered records as well as column for model name
def get_filtered_tads(df_sprite, max_overlap=tad_core.FILT_MAX_OVERLAP,
                      min_sprite_hits=tad_core.FILT_MIN_SPRITE_HITS):
    '''Returns data.frame of TADs meeting filter criteria'''
    # Assume max overlap is in [0,1]
    assert 0.0 <= max_overlap <= 1.0
    # Keep track of retained records
    df_filt = pd.DataFrame(index=df_sprite.index.copy())
    df_filt['name'] = ""
    df_filt['kept'] = False
    # Filter TADs
    for row in df_sprite.itertuples(index=True, name='Pandas'):
        if row.n_spr < min_sprite_hits:
            continue
        ov_rec = get_max_overlap(row, df_sprite)
        if ov_rec.ov > max_overlap:
            continue
        df_filt.ix[row.Index, 'kept'] = True
        df_filt.ix[row.Index, 'name'] = get_model_name(row)
    df_filt = pd.concat([df_filt, df_sprite], axis=1)
    df_filt = df_filt.loc[df_filt.kept]
    df_filt.drop(axis=1, labels='kept', inplace=True)
    return df_filt

# @param df_filt - Data.frame of filtered TAD records
# @param fout - Output file path to write df_filt
def save_filtered_tads(df_filt, fout=tad_core.FILT_PATH):
    '''Writes filtered TAD records'''
    tad_core.save_table(df=df_filt, fout=fout, index=False)

###################################################
# TAD filter
###################################################

# @param sprite - Path to processed SPRITE tabular data
# @param out - Path to write filtered TAD tabular data
# @param max_overlap - Max fractional overlap with previous models in [0,1]
# @param min_sprite_hits - Minimum number of associated SPRITE clusters
def tad_filter(sprite = tad_core.SPRITE_PATH,
               fout = tad_core.FILT_PATH,
               max_overlap = tad_core.FILT_MAX_OVERLAP,
               min_sprite_hits = tad_core.FILT_MIN_SPRITE_HITS):
    '''Filters TAD bounded SPRITE records'''
    # Assume max overlap is in [0,1]
    assert 0.0 <= max_overlap <= 1.0
    # Load processed SPRITE records
    df_sprite = tad_core.load_sprite(fin=sprite)
    # Filter records
    df_filt = get_filtered_tads(df_sprite=df_sprite, max_overlap=max_overlap,
                                min_sprite_hits=min_sprite_hits)
    # Save records
    save_filtered_tads(df_filt=df_filt, fout=fout)

###################################################
# Main
###################################################

# Main script entry point
def __main__():
    print "======================== tad_filter ========================"
    # Initialize command line parser
    parser = argparse.ArgumentParser()
    # Optional format arguments
    parser.add_argument('-spr', '--sprite', default=tad_core.SPRITE_PATH,
                        help='Path to input processed SPRITE tabular data')
    parser.add_argument('-fo', '--fout', default=tad_core.FILT_PATH,
                        help='Path to write filtered TAD records')
    parser.add_argument('-maxov', '--max_overlap',
                        default=tad_core.FILT_MAX_OVERLAP,
                        help='Max fractional overlap with previous models in [0,1]')
    parser.add_argument('-minspr', '--min_sprite_hits',
                        default=tad_core.FILT_MIN_SPRITE_HITS,
                        help='Min number of associated SPRITE clusters')
    # Parse command line
    args = parser.parse_args()
    tad_core.print_cmd(args)
    # Feed to utility
    # https://stackoverflow.com/questions/16878315/what-is-the-right-way-to-treat-python-argparse-namespace-as-a-dictionary
    tad_filter(**vars(args))

# Run main if we are the active script
if __name__ == '__main__':
    __main__()
