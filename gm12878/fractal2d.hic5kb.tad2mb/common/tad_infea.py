#!/usr/bin/python
# -*- coding: utf-8 -*-

# Script searches for infeasible chromatin-to-chromatin proximity constraints
#
# The following arguments are optional: (short-hand|long-hand)

#   [-bd|--base_dir]: [Optional] TAD base, non-versioned, output directory,
#       default is specified in tad_core.py
#   [-clob|--clobber]: [Optional] If switch is present, any previous corpus of
#       infeasible constraints will be replaced by program run. If switch is
#       not present, the program behavior is to resume generation of the
#       infeasible corpus using the previously generated corpus (if present)
#       and the unique results will simply be appended to the previous file
#       (if present).
#   [-cmd|--cmd_key]: [Optional] Model command key passed to the 'fea'
#       dispatch utility, default is 'loc_fea'
#   [-cnf|--conf]: [Optional] *Sub-path* to top-level model INI configuration,
#       default is given by FEA_TOP_INI_PATH in tad_core.py
#   [-dki|--dist_ki] <+real>: [Optional] Scalar knock-in distance in Angstroms.
#       A knock-in constraint is satisfied if at least one monomer node from
#       each fragment region is within this distance. May also be specified as
#       'chr_knock_in_dist = <+real>' within a configuration INI; the folder
#       binary also has a hard-coded default if not explicitly set by user.
#   [-dko|--dist_ko] <+real>: [Optional] Scalar knock-out distance in
#       Angstroms. A knock-out constraint is satisfied if all monomer nodes
#       from each fragment region are greater than this distance from each
#       other. May also be specified as 'chr_knock_out_dist = <+real>' within
#       a configuration INI; the folder binary also has a hard-coded default
#       if not explicitly set by user.
#   [-e|--exe] <path>: [Optional] Path to executable folder binary with 'fea'
#       utility, default is given by tad_core.py
#   [-exc|--exit_corp]: [Optional] Default is to ignore resumed corpus entries
#       which have errors (simply give warning and skip those entries); if
#       this switch is present, then will hard exit if any error encountered
#       in resumed corpus format
#   [-ft|--from_table <None|0|1|path>]: [Optional] If None|0, will use folder
#       listing in TAD base output directory for list of file names. If 1,
#       then will use default scale table path for obtaining model names. If
#       <path>, then will use user path to table file (must have 'name'
#       column) - if from table, then models are processed in order they are
#       listed in table file, else they are processed in order listed in file
#       system.
#   [-h|--help]: [Optional] Display help (i.e. usage information)
#   [-hb|--heartb] <+integer>: [Optional] Heartbeat interval, program status
#       is reported on intervals defined by this many folding attempts
#   [-kor|--ko_rand]: [Optional] Switch if present will treat '0's as random
#       folding instead of constrained knock-outs
#   [-ma|--max_attempt] <+integer>: [Optional] Maximum number of attempted
#       configurations, this is same as max number of calls to 'fea' utility
#   [-mb|--max_batch] <+integer>: [Optional] Maximum number of constraint
#       configurations to generate prior to fold testing with 'fea'
#       utility. All configurations within this batch size should be unique
#   [-mc|--max_corp] <+integer>: [Optional] Target maximum infeasible corpus
#       size, may be slightly exceeded depending on batch size resolution.
#       Fea search at a TAD locus terminates when first of:
#           1.) maximum infeasible corpus size is reached -or-
#           2.) maximum number of attempted folds is reached
#   [-mf|--max_fail] <integer>: [Optional] Maximum number of failures allowed
#       for a single job configuration. If a job error code is encountered
#       more than this threshold, the job command line is reported and program
#       terminates
#   [-mix|--model_index] <integer>: [Optional] If specified, then will only
#       launch infea for parameter 0-based index into list returned by
#       tad_core.resolve_tad_model_names(). If not specified (default
#       behavior), then all TAD models are processed.
#   [-mp|--max_proc] <+integer>: [Optional] Maximum number of parallel
#       processes to use to work through each configuration batch. Note:
#           <--max_proc> should not exceed CPU logical cores
#   [-mt|--max_trial] <+integer>: [Optional] Number of trials to attempt
#       folding a given proximity constraint configuration before labeling as
#       infeasible
#   [-ssp|--sleep_sec_poll] <+real>: [Optional] Number of seconds to wait
#       before polling active folding simulations for completion

###################################################
# Imports
###################################################

# For parsing user supplied arguments
import argparse

# For default configuration settings
import tad_core

# Import local 'infea' script
# THIS MUST BE IMPORTED AFTER 'tad_core'!
#   - tad_core modifies sys path so that this script may be found
import infea

###################################################
# Defaults
###################################################

# Default number of parallel processes, should not exceed logical cores
DEF_MAX_PROC = tad_core.MAX_PROC

###################################################
# TAD infea
###################################################

# @param mix - Model 0-based index
# @param mid - Model identifier
# @param args - User arguments
def launch_infea(mix, mid, args):
    '''Process a single TAD model'''
    print '[tad_infea] Processing ' + str(mix) + ' : ' +  str(mid)
    # Base directory with unversioned TAD model data
    base_dir = args.base_dir
    # TAD chr-chr interactions
    intr_path = tad_core.get_tad_intr_chr_path(model_name = mid,
                                               base_dir = base_dir)
    # TAD infeasible chr-chr interaction corpus
    out_path = tad_core.get_tad_infea_corp_path(model_name = mid,
                                                base_dir = base_dir)
    # TAD archetype ini
    arch_path = tad_core.get_tad_folder_arch_ini_path(model_name = mid,
                                                      base_dir = base_dir)
    # Infea arguments list
    infea_args = infea.Args(cmd_key = args.cmd_key,
                            conf = args.conf,
                            intr = intr_path,
                            out = out_path,
                            arch_conf = arch_path,
                            clobber = args.clobber,
                            dist_ki = args.dist_ki,
                            dist_ki_file = None,
                            dist_ko = args.dist_ko,
                            dist_ko_file = None,
                            exe = args.exe,
                            exit_corp = args.exit_corp,
                            frag = None,
                            heartb = args.heartb,
                            ko_rand = args.ko_rand,
                            max_attempt = args.max_attempt,
                            max_batch = args.max_batch,
                            max_corp = args.max_corp,
                            max_fail = args.max_fail,
                            max_proc = args.max_proc,
                            max_trial = args.max_trial,
                            sleep_sec_poll = args.sleep_sec_poll)
    # Call infea!
    infea.infea(args=infea_args)

# @param args - User arguments
def tad_infea(args):
    '''Calls fea utility at each TAD model'''
    # List of active TAD models
    model_names = tad_core.resolve_tad_model_names(from_table=args.from_table,
                                                   base_dir=args.base_dir)
    if args.model_index is not None:
        # Process single TAD model
        mix = int(args.model_index)
        assert (mix >= 0) and (mix < len(model_names))
        mid = model_names[mix]
        launch_infea(mix=mix, mid=mid, args=args)
    else:
        # Process all TAD model
        for mix, mid in enumerate(model_names):
            launch_infea(mix=mix, mid=mid, args=args)

###################################################
# Main
###################################################

# Main script entry point
def __main__():
    print "======================= tad_infea ======================="
    # Initialize command line parser
    parser = argparse.ArgumentParser()
    parser.add_argument('-bd', '--base_dir', default=tad_core.TAD_BASE_OUT_DIR,
                        help='Base directory storing unversioned TAD model data')
    parser.add_argument('-clob', '--clobber', action='store_true',
                        help='If present, any previous infeasible corpus will be overwritten with new data, default (switch not present) is to resume using any previous corpus located at <--out>') 
    parser.add_argument('-cmd', '--cmd_key', default=tad_core.FEA_CMD_KEY,
                        help='Model command key passed to the folding simulation "fea" utility')
    parser.add_argument('-cnf', '--conf', default=tad_core.FEA_TOP_INI_PATH,
                        help='Model INI configuration file path passed to the "fea" utility')       
    parser.add_argument('-dki', '--dist_ki', default=tad_core.FEA_DIST_KI,
                        help='Scalar knock-in distance in Angstroms')
    parser.add_argument('-dko', '--dist_ko', default=tad_core.FEA_DIST_KO,
                        help='Scalar knock-out distance in Angstroms')
    parser.add_argument('-e', '--exe', default=tad_core.FEA_EXE_PATH,
                          help='Path to folding simulation executable binary with "fea" utility')
    parser.add_argument('-exc', '--exit_corp', action='store_true',
                        help='If present, will hard exit if any format error encountered in a resumed corpus (default is to warn and skip any misformatted rows)')
    parser.add_argument('-ft', '--from_table', default=1,
                          help="If '1' or <path>, will attempt to load table and process TAD model names in row order")
    parser.add_argument('-hb', '--heartb', default=infea.DEF_HEARTB,
                        help='Heartbeat interval, program status is reported on intervals defined by this many folding attempts')
    parser.add_argument('-kor', '--ko_rand', action='store_true',
                        help="Switch, if present, will treat '0's as random folding instead of constrained knock-outs")
    parser.add_argument('-ma', '--max_attempt', default=tad_core.FEA_MAX_ATTEMPT,
                        help='Maximum number of attempted configurations, this is same as maximum number of calls to folding simulation tool')
    parser.add_argument('-mb', '--max_batch', default=infea.DEF_MAX_BATCH,
                        help='Maximum number of constraint configurations to generate prior to fold testing')
    parser.add_argument('-mc', '--max_corp', default=tad_core.FEA_MAX_CORP,
                        help='Target maximum infeasible corpus size (may be exceeded slightly based on batch size), program terminates when either max attempts or max corpus size is reached')
    parser.add_argument('-mf', '--max_fail', default=infea.DEF_MAX_FAIL,
                        help='Maximum number of times a single job configuration may fail with error return code')
    parser.add_argument('-mix', '--model_index', default=None,
                        help='Optional 0-based index into tad_core.resolve_tad_model_names(). If specified, other TADs will be ignored, else (default) all TADs will be processed.')
    parser.add_argument('-mp', '--max_proc', default=DEF_MAX_PROC,
                        help='Maximum number of active fold simulations')
    parser.add_argument('-mt', '--max_trial', default=tad_core.FEA_MAX_TRIAL,
                        help='Maximum number of trials per fold attempt of a given constraint configuration before labeling as infeasible')
    parser.add_argument('-ssp', '--sleep_sec_poll', default=infea.DEF_SLEEP_SEC_POLL,
                        help='Polling interval in seconds to check for completed active folding simulations')
    # Parse command line
    args = parser.parse_args()
    tad_core.print_cmd(args)
    # Find infeasible proximity constraint configurations!
    tad_infea(args)
    print "Finished."

# Run main if we are the active script
if __name__ == '__main__':
    __main__()
