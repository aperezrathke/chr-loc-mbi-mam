#!/bin/bash

# Path to this script's directory
SCRIPT_PATH="$(readlink -f $0)"
SCRIPT_DIR="$(dirname $SCRIPT_PATH)"
SCRIPT_DIR=$(cd "$SCRIPT_DIR"; pwd)

#### Delete CMX results

# Build command line
cmd="Rscript --vanilla $SCRIPT_DIR/tad_cmx_setup_shell.R"
cmd="$cmd --util_cmx_unlink_loci"

# Run command line
echo "Running:"
echo $cmd
$cmd

echo "Finished"
