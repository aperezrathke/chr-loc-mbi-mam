#!/bin/bash

# Path to this script's directory
SCRIPT_PATH="$(readlink -f $0)"
SCRIPT_DIR="$(dirname $SCRIPT_PATH)"
SCRIPT_DIR=$(cd "$SCRIPT_DIR"; pwd)

#### CLUSTER CLEANED HI-C

# Number of parallel processes for TAD clustering. 
TAD_CLUST_NUM_CPU="100"

# Build command line
cmd="Rscript --vanilla $SCRIPT_DIR/tad_clust_shell.R"
cmd="$cmd --util_tad_apclust"
cmd="$cmd --tad_apclust_num_cpu $TAD_CLUST_NUM_CPU"
# Run command line
echo "Running:"
echo $cmd
$cmd

#### PLOT SETTINGS

# Number of CPUs for parallel heat map generation, <= 0 will use all cores
TAD_PLOT_CLUST_NUM_CPU="100"

# Heatmap min anchor value (ala seaborn) for rank plots
HEAT_VMIN_RANK="0.0"
# Heatmap max anchor value (ala seaborn) for rank plots
HEAT_VMAX_RANK="1.0"
# Heatmap min anchor value (ala seaborn) for non-rank plots
HEAT_VMIN_VAL="0.0"
# Heatmap max anchor value (ala seaborn) for non-rank plots
HEAT_VMAX_VAL="0.35"
# Heatmap image format (png or pdf)
HEAT_FORMAT="png"
# Heatmap pixel width (used if format is png)
HEAT_WIDTH_PIX="1024"
# Heatmap pixel height (used if format is png)
HEAT_HEIGHT_PIX="1024"
# Heatmap inches width (used if format is pdf)
HEAT_WIDTH_IN="7"
# Heatmap inches height (used if format is pdf)
HEAT_HEIGHT_IN="7"
# Cluster sample scatter plot point char
TAD_PLOT_CLUST_PCH_SA="20"
# Cluster sample scatter plot point char scale
TAD_PLOT_CLUST_CEX_SA="0.6"
# Cluster exemplar scatter plot point char
TAD_PLOT_CLUST_PCH_EX="22"
# Cluster exemplar scatter plot point char scale
TAD_PLOT_CLUST_CEX_EX="0.8"

#### PLOT COMBINED SCATTER PLOT WITH HEATMAP

# Build command line - Plot quantile normalized ranks
cmd="Rscript --vanilla $SCRIPT_DIR/tad_clust_shell.R"
cmd="$cmd --util_tad_plot_apclust"
cmd="$cmd --tad_heat_col_id fq"
cmd="$cmd --heat_format $HEAT_FORMAT"
cmd="$cmd --heat_width_pix $HEAT_WIDTH_PIX"
cmd="$cmd --heat_height_pix $HEAT_HEIGHT_PIX"
cmd="$cmd --heat_width_in $HEAT_WIDTH_IN"
cmd="$cmd --heat_height_in $HEAT_HEIGHT_IN"
cmd="$cmd --heat_as_rank"
cmd="$cmd --heat_div_by_max"
cmd="$cmd --heat_vmin $HEAT_VMIN_RANK"
cmd="$cmd --heat_vmax $HEAT_VMAX_RANK"
cmd="$cmd --tad_plot_clust_pch_sa $TAD_PLOT_CLUST_PCH_SA"
cmd="$cmd --tad_plot_clust_cex_sa $TAD_PLOT_CLUST_CEX_SA"
cmd="$cmd --tad_plot_clust_pch_ex $TAD_PLOT_CLUST_PCH_EX"
cmd="$cmd --tad_plot_clust_cex_ex $TAD_PLOT_CLUST_CEX_EX"
cmd="$cmd --tad_plot_clust_num_cpu $TAD_PLOT_CLUST_NUM_CPU"
# Run command line
echo "Running:"
echo $cmd
$cmd

# Build command line - Plot quantile normalized values
cmd="Rscript --vanilla $SCRIPT_DIR/tad_clust_shell.R"
cmd="$cmd --util_tad_plot_apclust"
cmd="$cmd --tad_heat_col_id fq"
cmd="$cmd --heat_format $HEAT_FORMAT"
cmd="$cmd --heat_width_pix $HEAT_WIDTH_PIX"
cmd="$cmd --heat_height_pix $HEAT_HEIGHT_PIX"
cmd="$cmd --heat_width_in $HEAT_WIDTH_IN"
cmd="$cmd --heat_height_in $HEAT_HEIGHT_IN"
cmd="$cmd --no-heat_as_rank"
cmd="$cmd --heat_div_by_max"
cmd="$cmd --heat_vmin $HEAT_VMIN_VAL"
cmd="$cmd --heat_vmax $HEAT_VMAX_VAL"
cmd="$cmd --tad_plot_clust_pch_sa $TAD_PLOT_CLUST_PCH_SA"
cmd="$cmd --tad_plot_clust_cex_sa $TAD_PLOT_CLUST_CEX_SA"
cmd="$cmd --tad_plot_clust_pch_ex $TAD_PLOT_CLUST_PCH_EX"
cmd="$cmd --tad_plot_clust_cex_ex $TAD_PLOT_CLUST_CEX_EX"
cmd="$cmd --tad_plot_clust_num_cpu $TAD_PLOT_CLUST_NUM_CPU"
# Run command line
echo "Running:"
echo $cmd
$cmd

# Build command line - Plot raw count ranks
cmd="Rscript --vanilla $SCRIPT_DIR/tad_clust_shell.R"
cmd="$cmd --util_tad_plot_apclust"
cmd="$cmd --tad_heat_col_id rawc"
cmd="$cmd --heat_format $HEAT_FORMAT"
cmd="$cmd --heat_width_pix $HEAT_WIDTH_PIX"
cmd="$cmd --heat_height_pix $HEAT_HEIGHT_PIX"
cmd="$cmd --heat_width_in $HEAT_WIDTH_IN"
cmd="$cmd --heat_height_in $HEAT_HEIGHT_IN"
cmd="$cmd --heat_as_rank"
cmd="$cmd --heat_div_by_max"
cmd="$cmd --heat_vmin $HEAT_VMIN_RANK"
cmd="$cmd --heat_vmax $HEAT_VMAX_RANK"
cmd="$cmd --tad_plot_clust_pch_sa $TAD_PLOT_CLUST_PCH_SA"
cmd="$cmd --tad_plot_clust_cex_sa $TAD_PLOT_CLUST_CEX_SA"
cmd="$cmd --tad_plot_clust_pch_ex $TAD_PLOT_CLUST_PCH_EX"
cmd="$cmd --tad_plot_clust_cex_ex $TAD_PLOT_CLUST_CEX_EX"
cmd="$cmd --tad_plot_clust_num_cpu $TAD_PLOT_CLUST_NUM_CPU"
# Run command line
echo "Running:"
echo $cmd
$cmd

# Build command line - Plot raw count values
cmd="Rscript --vanilla $SCRIPT_DIR/tad_clust_shell.R"
cmd="$cmd --util_tad_plot_apclust"
cmd="$cmd --tad_heat_col_id rawc"
cmd="$cmd --heat_format $HEAT_FORMAT"
cmd="$cmd --heat_width_pix $HEAT_WIDTH_PIX"
cmd="$cmd --heat_height_pix $HEAT_HEIGHT_PIX"
cmd="$cmd --heat_width_in $HEAT_WIDTH_IN"
cmd="$cmd --heat_height_in $HEAT_HEIGHT_IN"
cmd="$cmd --no-heat_as_rank"
cmd="$cmd --heat_div_by_max"
cmd="$cmd --heat_vmin $HEAT_VMIN_VAL"
cmd="$cmd --heat_vmax $HEAT_VMAX_VAL"
cmd="$cmd --tad_plot_clust_pch_sa $TAD_PLOT_CLUST_PCH_SA"
cmd="$cmd --tad_plot_clust_cex_sa $TAD_PLOT_CLUST_CEX_SA"
cmd="$cmd --tad_plot_clust_pch_ex $TAD_PLOT_CLUST_PCH_EX"
cmd="$cmd --tad_plot_clust_cex_ex $TAD_PLOT_CLUST_CEX_EX"
cmd="$cmd --tad_plot_clust_num_cpu $TAD_PLOT_CLUST_NUM_CPU"
# Run command line
echo "Running:"
echo $cmd
$cmd

echo "Finished."
