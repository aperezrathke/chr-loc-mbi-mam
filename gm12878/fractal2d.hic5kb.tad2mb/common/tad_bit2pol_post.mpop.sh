#!/bin/bash

# Script for B2P folding any residual TAD regions

# Path to this script's directory
SCRIPT_PATH="$(readlink -f $0)"
SCRIPT_DIR="$(dirname $SCRIPT_PATH)"
SCRIPT_DIR=$(cd "$SCRIPT_DIR"; pwd)

#### Post-process bit2pol folding geometry
RSHELL_SCRIPT="$SCRIPT_DIR/tad_bit2pol_post_shell.R"

# If '1' or <path>, will attempt to load table and process TAD model names in
# row order. If '0', will check existing folders in TAD base output directory.
# @WARNING - THIS MAY CAUSE PRINT MISMATCH OF 'MID' IN 'tad_cmx.queue.py'
#   WHICH IS OKAY, JUST LOGGING STATEMENTS OF WHICH TAD IS BEING PROCESSED
#   MAY NOT BE CORRECT
FROM_TABLE=1

# num_se : SPRITE SE regions with median cluster 2
# top_k_spr : top [5] SPRITE regions
MODEL_FILT_ID='num_se'

# CMX population key
POP_KEY='mpop'

## Compute frequency matrices
cmd="Rscript --vanilla $RSHELL_SCRIPT"
cmd="$cmd --model_filt_id $MODEL_FILT_ID"
cmd="$cmd --from_table $FROM_TABLE"
cmd="$cmd --top_only"
cmd="$cmd --pop_key $POP_KEY"
cmd="$cmd --util_b2p_cache_ligc_csv"
cmd="$cmd --util_b2p_cache_ligc_merge"
cmd="$cmd --util_b2p_cache_ligc_freqmat"
cmd="$cmd --util_b2p_cor"
# Run command line
echo "-------------"
echo "Running:"
echo $cmd
$cmd

echo "Finished $POP_KEY : $MODEL_FILT_ID B2P post"
