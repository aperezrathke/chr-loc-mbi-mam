#!/bin/bash

# Path to this script's directory
SCRIPT_PATH="$(readlink -f $0)"
SCRIPT_DIR="$(dirname $SCRIPT_PATH)"
SCRIPT_DIR=$(cd "$SCRIPT_DIR"; pwd)

# MBI R utilities
RSHELL_SCRIPT="$SCRIPT_DIR/tad_mbi_shell.R"

# Parse command line for [optional] 0-based model index
MIX0=""
if [ $# -gt 0 ]; then
    MIX0="--model_index0 $1"
fi

# Model filter (at least 2 SEs per region)
MODEL_FILT_ID='num_se'

# Number of CPUs for parallel heat map generation, <= 0 will use all cores
HEAT_NUM_CPU="-1"
# Heatmap pixel width (used if format is png)
HEAT_WIDTH_PIX="512"
# Heatmap pixel height (used if format is png)
HEAT_HEIGHT_PIX="512"
# Heatmap inches width (used if format is pdf)
HEAT_WIDTH_IN="3.3"
# Heatmap inches height (used if format is pdf)
HEAT_HEIGHT_IN="3.3"
# Heatmap FDR threshold for points overlay
HEAT_FDR_THRESH="0.05"

# Heatmap image format(s)
HEAT_FORMAT=("svg" "png")

# Heatmap min anchor value (ala seaborn) for non-rank plots
HEAT_VMIN_VAL="0.0"
# Heatmap max anchor value (ala seaborn) for non-rank plots
HEAT_VMAX_VAL=("0.35" "0.25" "0.2" "0.15" "0.1" "0.05")

 # Iterate over image formats
for heat_format in "${HEAT_FORMAT[@]}"
do
    for heat_vmax in "${HEAT_VMAX_VAL[@]}"
    do
        # Plot counts without specific contacts
        cmd="Rscript --vanilla $RSHELL_SCRIPT"
        cmd="$cmd --util_mbi_heat_prin_loop $MIX0"
        cmd="$cmd --model_filt_id $MODEL_FILT_ID"
        cmd="$cmd --heat_format $heat_format"
        cmd="$cmd --heat_width_pix $HEAT_WIDTH_PIX"
        cmd="$cmd --heat_height_pix $HEAT_HEIGHT_PIX"
        cmd="$cmd --heat_width_in $HEAT_WIDTH_IN"
        cmd="$cmd --heat_height_in $HEAT_HEIGHT_IN"
        cmd="$cmd --no-heat_as_rank"
        cmd="$cmd --no-heat_div_by_max"
        cmd="$cmd --heat_vmin $HEAT_VMIN_VAL"
        cmd="$cmd --heat_vmax $heat_vmax"
        cmd="$cmd --heat_key"
        cmd="$cmd --no-heat_fdr_ptsov"
        cmd="$cmd --heat_fdr_thresh -1.0"
        # Run command line
        echo "Running:"
        echo $cmd
        $cmd

        # # Plot counts with specific contacts
        # cmd="Rscript --vanilla $RSHELL_SCRIPT"
        # cmd="$cmd --util_mbi_heat_prin_loop $MIX0"
        # cmd="$cmd --model_filt_id $MODEL_FILT_ID"
        # cmd="$cmd --heat_format $heat_format"
        # cmd="$cmd --heat_width_pix $HEAT_WIDTH_PIX"
        # cmd="$cmd --heat_height_pix $HEAT_HEIGHT_PIX"
        # cmd="$cmd --heat_width_in $HEAT_WIDTH_IN"
        # cmd="$cmd --heat_height_in $HEAT_HEIGHT_IN"
        # cmd="$cmd --no-heat_as_rank"
        # cmd="$cmd --no-heat_div_by_max"
        # cmd="$cmd --heat_vmin $HEAT_VMIN_VAL"
        # cmd="$cmd --heat_vmax $heat_vmax"
        # cmd="$cmd --heat_key"
        # cmd="$cmd --heat_fdr_ptsov"
        # cmd="$cmd --heat_fdr_thresh $HEAT_FDR_THRESH"
        # # Run command line
        # echo "Running:"
        # echo $cmd
        # $cmd

        # Run command line
        echo "-------------"
        echo "Running:"
        echo $cmd
        $cmd
    done # End iteration over vmax
done # End iteration over image formats

echo "Finished"
