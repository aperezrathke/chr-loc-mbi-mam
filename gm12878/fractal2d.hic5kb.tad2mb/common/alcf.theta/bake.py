#!/usr/bin/python
# -*- coding: utf-8 -*-

# Bake simulation JSON specifications

###################################################
# Imports
###################################################

# For path manipulation 
import os

# For launching shell scripts
import subprocess

###################################################
# Globals
###################################################

# Path to directory containing this script
# https://stackoverflow.com/questions/4934806/how-can-i-find-scripts-directory-with-python
SCRIPT_DIR = os.path.abspath(os.path.dirname(os.path.realpath(__file__)))

# Root module directory
MODULE_DIR = os.path.join(SCRIPT_DIR, "..", "..", "..", "..")

# Common utilities directory
MODULE_COMMON_DIR = os.path.join(MODULE_DIR, "common")

# Configuration type
CONFIG_ID = "fractal2d"

# Path to .bun file
BUN_PATH=os.path.join(MODULE_COMMON_DIR, "scripts", "config", CONFIG_ID,
                      "folder.bun")

# Output directory
OUT_DIR = os.path.join(SCRIPT_DIR, "..", "output")

# Workspace directory
WORKSPACE_DIR = os.path.join(MODULE_DIR, "..", "..")

# Path to workspace bake script
BAKE_SCRIPT_PATH = os.path.join(WORKSPACE_DIR, "folder", "scripts",
                                "Template", "bake.py")

# Compilation target
COMPILE = ["alcf-theta-intel", "alcf-theta-intel-threaded",
           "alcf-theta-intel-cmdlets", "alcf-theta-intel-threaded-cmdlets"]

###################################################
# Bake
###################################################

cmd = ["python", BAKE_SCRIPT_PATH, "--bun", BUN_PATH, "--out", OUT_DIR,
       "--force", "--compile"]
cmd.extend(COMPILE)
print "Running: " + " ".join(cmd)
subprocess.call(cmd)
