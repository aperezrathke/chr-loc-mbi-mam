#!/bin/bash

# Path to this script's directory
SCRIPT_PATH="$(readlink -f $0)"
SCRIPT_DIR="$(dirname $SCRIPT_PATH)"
SCRIPT_DIR=$(cd "$SCRIPT_DIR"; pwd)

# NOTE: If running on extreme, will need to load modules:
#   module load apps/R-3.5.1
#   module load compilers/python-2.7.13-gcc

# Build command line
cmd="Rscript --vanilla $SCRIPT_DIR/tad_sprite_cache_shell.R"

# Run command line
echo "Running:"
echo $cmd
$cmd
echo "Finished"
