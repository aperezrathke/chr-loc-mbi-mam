###################################################################
# tad_bio_binMbiCount.prin_loop.R
#
# Computes binned MBI contact summary over principal loops
#
# @WARNING - If running on Windows, may encounter path length
#   limits leading to erroneous 'file.exists(<path>) == FALSE'
###################################################################

# Guard variable to avoid multiple sourcing
SOURCED_tad_bio_binMbiCount_prin_loop = TRUE

###################################################################
# Path to this script
###################################################################

# Cached directory to script
# https://stackoverflow.com/questions/3452086/getting-path-of-an-r-script
TAD_BIO_BINMBICOUNT_PRIN_LOOP_SCRIPT_DIR =
  getSrcDirectory(function(x) {
    x
  })

# getSrcDirectory trick will not work via Rscript. If this is the
# case, then TAD_BIO_BINMBICOUNT_PRIN_LOOP_SCRIPT_DIR will be length zero.
# http://stackoverflow.com/questions/1815606/rscript-determine-path-of-the-executing-script
if (length(TAD_BIO_BINMBICOUNT_PRIN_LOOP_SCRIPT_DIR) == 0) {
  get_script_dir <- function() {
    cmdArgs = commandArgs(trailingOnly = FALSE)
    needle = "--file="
    ixmatch = grep(needle, cmdArgs)
    if (length(match) > 0) {
      # Rscript
      return(dirname(normalizePath(sub(
        needle, "", cmdArgs[ixmatch]
      ))))
    }
    return(dirname(scriptName::current_filename()))
  }
  TAD_BIO_BINMBICOUNT_PRIN_LOOP_SCRIPT_DIR = get_script_dir()
}

# @return Path to directory containing this script
get_tad_bio_binMbiCount_prin_loop_script_dir <- function() {
  return(TAD_BIO_BINMBICOUNT_PRIN_LOOP_SCRIPT_DIR)
}

###################################################################
# Source utility script(s)
###################################################################

# MBI utilities
if (!exists("SOURCED_tad_mbi")) {
  source(file.path(
    get_tad_bio_binMbiCount_prin_loop_script_dir(),
    "tad_mbi.R"
  ))
}

###################################################################
# Misc utilities
###################################################################

# MBI binning identifier
BIO_BIN_MBI_PRIN_LOOP_ID = "prin_loop"

# @param mbi_df - Data.frame of many body interactions with cols:
#   $body - Number of beads participating in many body
#   $max_gd - Bead span of principal loop
#   $fdr - False discovery rate of many body
# @param min_body - Minimum many body size, if <= 0, then no lower
#   bound (all bodies <= max_body will be retained)
# @param max_body - Maximum many body size, if <= 0, then no upper
#   bound (all bodies >= min_body will be retained)
# @param min_loop - Minimum principal loop bead span, if <= 0, then
#   no lower bound (all loops <= max_loop will be retained)
# @param max_loop - Maximum principal loop bead span, if <= 0, then
#   no upper bound (all loops >= min_body will be retained)
# @param fdr - False discovery rate threshold, only retain
#   interactions < fdr
# @return Filtered many body records
get_bio_bin_mbi_recs_prin_loop <-
  function(mbi_df = load_mock_mbi_pvals(),
           min_body = tad_core$BIO_BIN_MBI_MIN_BODIES[1],
           max_body = tad_core$BIO_BIN_MBI_MAX_BODIES[1],
           min_loop = tad_core$BIO_BIN_MBI_MIN_LOOPS[1],
           max_loop = tad_core$BIO_BIN_MBI_MAX_LOOPS[1],
           fdr = tad_core$BIO_BIN_MBI_FDR) {
    keep_min_body = rep(TRUE, nrow(mbi_df))
    keep_max_body = rep(TRUE, nrow(mbi_df))
    keep_min_loop = rep(TRUE, nrow(mbi_df))
    keep_max_loop = rep(TRUE, nrow(mbi_df))
    if (min_body > 0) {
      keep_min_body = mbi_df$body >= min_body
    }
    if (max_body > 0) {
      keep_max_body = mbi_df$body <= max_body
    }
    if (min_loop > 0) {
      keep_min_loop = mbi_df$max_gd >= min_loop
    }
    if (max_loop > 0) {
      keep_max_loop = mbi_df$max_gd <= max_loop
    }
    keep_fdr = mbi_df$fdr < fdr
    keep = keep_min_body &
      keep_max_body & keep_min_loop & keep_max_loop & keep_fdr
    return(mbi_df[keep,])
  }

###################################################################
# binMbiCount_prin_loop
###################################################################

# Utility loads cached binned MBI counts (RData)
# @WARNING - MOSTLY DEFERS TO DEFAULTS IN tad_core.py
# @return Numeric vector of MBI counts or NULL if not found
load_bio_binMbiCount_prin_loop <-
  function(model_name = tad_core$get_tad_model_names()[1],
           min_body = tad_core$BIO_BIN_MBI_MIN_BODIES[1],
           max_body = tad_core$BIO_BIN_MBI_MAX_BODIES[1],
           min_loop = tad_core$BIO_BIN_MBI_MIN_LOOPS[1],
           max_loop = tad_core$BIO_BIN_MBI_MAX_LOOPS[1],
           fdr = tad_core$BIO_BIN_MBI_FDR,
           verbose = FALSE) {
    # @WARNING - DEFER TO tad_core.py FOR DEFAULT ARGUMENTS
    rdata_path = tad_core$get_bio_bin_mbi_counts_path(
      model_name = model_name,
      min_body = min_body,
      max_body = max_body,
      min_loop = min_loop,
      max_loop = max_loop,
      fdr = fdr,
      bin_id = BIO_BIN_MBI_PRIN_LOOP_ID,
      ext_no_dot = 'rdata'
    )
    if (file.exists(rdata_path)) {
      return(load_rdata(rdata_path = rdata_path, verbose = verbose))
    }
    # File not found!
    return(NULL)
  }

# Bin linear many body counts at single genomic region, if multiple
# MBI p-value files are present, they are simply averaged! If no
# p-values are detected, then no counts are written.
# @WARNING - MOSTLY DEFERS TO DEFAULTS IN tad_core.py
cache_bio_binMbiCount_prin_loop_at <-
  function(model_index = 1,
           tad_df = tad_core$load_tad_scale(),
           min_body = tad_core$BIO_BIN_MBI_MIN_BODIES[1],
           max_body = tad_core$BIO_BIN_MBI_MAX_BODIES[1],
           min_loop = tad_core$BIO_BIN_MBI_MIN_LOOPS[1],
           max_loop = tad_core$BIO_BIN_MBI_MAX_LOOPS[1],
           fdr = tad_core$BIO_BIN_MBI_FDR,
           overwrite = FALSE) {
    # Bounds check
    stopifnot(model_index >= 1)
    stopifnot(model_index <= nrow(tad_df))
    # Determine region id
    model_name = tad_df$name[model_index]
    # Skip if cached data exists
    # @WARNING - DEFER TO tad_core.py FOR DEFAULT ARGUMENTS
    out_rdata_path = tad_core$get_bio_bin_mbi_counts_path(
      model_name = model_name,
      min_body = min_body,
      max_body = max_body,
      min_loop = min_loop,
      max_loop = max_loop,
      fdr = fdr,
      bin_id = BIO_BIN_MBI_PRIN_LOOP_ID,
      ext_no_dot = 'rdata'
    )
    if (!overwrite && file.exists(out_rdata_path)) {
      return(invisible(TRUE))
    }
    # Determine B2P geometry folders
    # @WARNING - DEFER TO tad_core.py FOR DEFAULT ARGUMENTS
    geom_dirs = b2p_get_geom_dirs(model_name = model_name)
    # Initialize bin counts
    bin_counts = rep(0.0, tad_df$mon_num[model_index])
    # Keep track of p-value files encountered
    pvals_count = 0
    # Process B2P geometry
    for (geom_dir in geom_dirs) {
      # Resolve MBI p-values rdata path
      # @WARNING - DEFER TO tad_core.py FOR DEFAULT ARGUMENTS
      pvals_rdata_path = tad_core$get_mbi_pvals_path(locus_dir = geom_dir,
                                                     ext_no_dot = 'rdata')
      # Skip geometry folder if no p-values are present
      if (!file.exists(pvals_rdata_path)) {
        next
      }
      # Update processed count
      pvals_count = pvals_count + 1
      # Load p-values
      mbi_df = load_rdata(rdata_path = pvals_rdata_path,
                          verbose = FALSE)
      # Filter p-values
      mbi_df = get_bio_bin_mbi_recs_prin_loop(
        mbi_df = mbi_df,
        min_body = min_body,
        max_body = max_body,
        min_loop = min_loop,
        max_loop = max_loop,
        fdr = fdr
      )
      # Skip degenerate results
      if (nrow(mbi_df) < 1) {
        next
      }
      # Update bin counts corresponding to principal loops
      stopifnot(max(mbi_df$i1) <= length(bin_counts))
      stopifnot(max(mbi_df$j1) <= length(bin_counts))
      tabs = tabulate(bin = c(mbi_df$i1, mbi_df$j1),
                      nbins = length(bin_counts))
      bin_counts = bin_counts + tabs
    }
    if (pvals_count > 0) {
      # Average results
      bin_counts = bin_counts / (as.numeric(pvals_count))
      # Resolve CSV path
      # @WARNING - DEFER TO tad_core.py FOR DEFAULT ARGUMENTS
      out_csv_gz_path = tad_core$get_bio_bin_mbi_counts_path(
        model_name = model_name,
        min_body = min_body,
        max_body = max_body,
        min_loop = min_loop,
        max_loop = max_loop,
        fdr = fdr,
        bin_id = BIO_BIN_MBI_PRIN_LOOP_ID,
        ext_no_dot = 'csv.gz'
      )
      # Save results
      save_rdata(data = bin_counts,
                 rdata_path = out_rdata_path,
                 verbose = FALSE)
      save_csv_gz(
        x = bin_counts,
        gz_path = out_csv_gz_path,
        row.names = FALSE,
        col.names = FALSE,
        verbose = FALSE
      )
    }
    return(invisible(TRUE))
  }

# Bin linear many body counts at single genomic region, if multiple
# MBI p-value files are present, they are simply averaged! If no
# p-values are detected, then no counts are written.
# @WARNING - MOSTLY DEFERS TO DEFAULTS IN tad_core.py
# @param tad_df - Data.frame with expected columns:
#   $name - TAD region model identifier (character)
#   $mon_num - Size (number of beads|bins)
# @param min_bodies - Vector of minimum body sizes, SAME SIZE AS
#   'max_bodies'. If <= 0, then no lower bound
# @param min_bodies - Vector of maximum body sizes, SAME SIZE AS
#   'min_bodies'. If <= 0, then no upper bound
# @param min_loops - Vector of lower bounds on bin|bead span of
#   principal loop, SAME SIZE AS 'min_bodies'. If <= 0, then no
#   lower bound
# @param max_loops - Vector of upper bounds on bin|bead span of
#   principal loop, SAME SIZE AS 'min_bodies'. If <= 0, then no
#   upper bound
# @param fdr - Only many-body interactions with false discovery
#   rate < 'fdr' are counted at each bead|bin
# @param parallel_opt - one of "multicore" | "snow" | "no"
#   Specifies parallel processing platform
#   - multicore: uses "multicore" package only available on linux
#   - snow: uses "snow" package available on all platforms
#   - no: disable parallel processing
# @param ncpu - Number of CPUs to use for parallel computation
#   If ncpu <= 0, then all available CPUs are used!
# @param overwrite - If TRUE, overwrite cached data
cache_bio_binMbiCount_prin_loop <-
  function(tad_df = tad_core$load_tad_scale(),
           min_bodies = tad_core$BIO_BIN_MBI_MIN_BODIES,
           max_bodies = tad_core$BIO_BIN_MBI_MAX_BODIES,
           min_loops = tad_core$BIO_BIN_MBI_MIN_LOOPS,
           max_loops = tad_core$BIO_BIN_MBI_MAX_LOOPS,
           fdr = tad_core$BIO_BIN_MBI_FDR,
           parallel_opt = DEF_PARALLEL_OPT,
           ncpu = DEF_NCPU,
           overwrite = FALSE) {
    stopifnot(length(min_bodies) == length(max_bodies))
    stopifnot(length(min_bodies) == length(min_loops))
    stopifnot(length(min_bodies) == length(max_loops))
    n = length(min_bodies)
    stopifnot(n > 0)
    res = rep(TRUE, nrow(tad_df))
    for (i in 1:n) {
      min_body = min_bodies[i]
      max_body = max_bodies[i]
      min_loop = min_loops[i]
      max_loop = max_loops[i]
      res = res & sapply_router(
        X = 1:nrow(tad_df),
        FUN = cache_bio_binMbiCount_prin_loop_at,
        parallel_opt = parallel_opt,
        ncpu = ncpu,
        tad_df = tad_df,
        min_body = min_body,
        max_body = max_body,
        min_loop = min_loop,
        max_loop = max_loop,
        fdr = fdr,
        overwrite = overwrite
      )
    }
    return(invisible(all(res)))
  }
