#!/usr/bin/python
# -*- coding: utf-8 -*-

# Downloads Hi-C data from cloud storage at each scaled TAD record. Requires
# 'tad_scale.py' to be run first. This script is a wrapper around Aiden lab's
# juicer tools: https://github.com/aidenlab/juicer/wiki/Data-Extraction
#
# Script accepts the following arguments (short|long):
#   -fi|--fin <path> : Path to input scaled TAD tabular data
#   -fod|--fout_dir <path> : Base directory for writing Hi-C data at each TAD
#   -j|--juicer <path> : Path to juicer_tools.jar
#   -n|--norm NONE|VC|VC_SQRT|KR : Hi-C normalization type
#   -m|--mapq combined|combined_30 : Hi-C read mapping quality
#   -u|--url : Hi-C cloud storage base URL
#   -bp|--bin_bp : Hi-C bin resolution in base pairs

###################################################
# Imports
###################################################

# For parsing user supplied arguments
import argparse

# For path manipulation
import os

# For running shell commands
import subprocess

# For default configuration settings
import tad_core

###################################################
# Utilities
###################################################

# https://stackoverflow.com/questions/16891340/remove-a-prefix-from-a-string
def remove_prefix(text, prefix):
    if text.startswith(prefix):
        return text[len(prefix):]
    return text

# @param row - Row from scaled TAD data.frame
# @param juicer - Path to juicer_tools.jar
# @param fout_dir - Base directory to store Hi-C data at each TAD record
# @param norm - Hi-C normalization in {"NONE, "VC", "VC_SQRT", "KR"}
# @param mapq - Hi-C mapping quality in {"combined", "combined_30"}
# @param url - Hi-C cloud storage base URL
# @param bin_bp - Hi-C bin resolution in base pairs
def launch_juicer(row, juicer=tad_core.HIC_JUICER_PATH,
                  fout_dir=tad_core.TAD_BASE_OUT_DIR, norm=tad_core.HIC_NORM,
                  mapq=tad_core.HIC_MAPQ, url=tad_core.HIC_BASE_URL,
                  bin_bp=tad_core.HIC_BIN_BP):
    hic_path = tad_core.get_hic_raw_path(model_name=row.name,
                                         base_dir=fout_dir, norm=norm,
                                         mapq=mapq, bin_bp=bin_bp)
    tad_core.make_dir(os.path.dirname(hic_path))
    hic_url = os.path.join(url, mapq + ".hic")
    chr_id = remove_prefix(text=row.chr, prefix="chr")
    bp_start = int(row.bp_start_tad_med)
    # Account for bin span
    bp_end = int(row.bp_end_tad_med) - int(bin_bp)
    assert bp_end >= float(bp_start)
    chr_a = ':'.join([chr_id, str(bp_start), str(bp_end)])
    cmd = ["java", "-jar", juicer, "dump", "observed", norm, hic_url, chr_a,
           chr_a, "BP", str(bin_bp), hic_path]
    print "Running:"
    print ' '.join(cmd)
    subprocess.call(cmd)
    if os.path.exists(hic_path):
        print "Hi-C data written to:"
        print hic_path
    else:
        print "Failed to write Hi-C data!"

###################################################
# TAD juicer
###################################################

# @param fin - Path to input scaled TAD records
# @param fout_dir - Base directory to store Hi-C data at each TAD record
# @param juicer - Path to juicer_tools.jar
# @param norm - Hi-C normalization in {"NONE, "VC", "VC_SQRT", "KR"}
# @param mapq - Hi-C mapping quality in {"combined", "combined_30"}
# @param url - Hi-C cloud storage base URL
# @param bin_bp - Hi-C bin resolution in base pairs
def tad_juicer(fin = tad_core.SCALE_PATH,
               fout_dir = tad_core.TAD_BASE_OUT_DIR,
               juicer = tad_core.HIC_JUICER_PATH,
               norm = tad_core.HIC_NORM,
               mapq = tad_core.HIC_MAPQ,
               url = tad_core.HIC_BASE_URL,
               bin_bp = tad_core.HIC_BIN_BP):
    '''Retrieve Hi-C for scaled TAD models'''
    # Load scaled TAD records
    df_scale = tad_core.load_tad_scale(fin=fin)
    for row in df_scale.itertuples(index=True, name='Pandas'):
        # Pull Hi-C
        launch_juicer(row=row, juicer=juicer, fout_dir=fout_dir, norm=norm,
                      mapq=mapq, url=url, bin_bp = bin_bp)

###################################################
# Main
###################################################

# Main script entry point
def __main__():
    print "======================== tad_juicer ========================"
    # Initialize command line parser
    parser = argparse.ArgumentParser()
    # Optional arguments
    parser.add_argument('-fi', '--fin', default=tad_core.SCALE_PATH,
                        help='Path to input scaled TAD records')
    parser.add_argument('-fod', '--fout_dir', default=tad_core.TAD_BASE_OUT_DIR,
                        help='Base directory to store unversioned Hi-C data')
    parser.add_argument('-j', '--juicer', default=tad_core.HIC_JUICER_PATH,
                        help='Path to juicer_tools.jar')
    parser.add_argument('-n', '--norm', default=tad_core.HIC_NORM,
                        choices={"NONE", "VC", "VC_SQRT", "KR"},
                        help='Hi-C normalization type')
    parser.add_argument('-m', '--mapq', default=tad_core.HIC_MAPQ,
                        choices={"combined", "combined_30"},
                        help='Hi-C read mapping quality')
    parser.add_argument('-u', '--url', default=tad_core.HIC_BASE_URL,
                        help='Hi-C cloud storage base URL')
    parser.add_argument('-bp', '--bin_bp', default=tad_core.HIC_BIN_BP,
                        help='Hi-C bin resolution in base pairs')
    # Parse command line
    args = parser.parse_args()
    tad_core.print_cmd(args)
    # Feed to utility
    # https://stackoverflow.com/questions/16878315/what-is-the-right-way-to-treat-python-argparse-namespace-as-a-dictionary
    tad_juicer(**vars(args))

# Run main if we are the active script
if __name__ == '__main__':
    __main__()
