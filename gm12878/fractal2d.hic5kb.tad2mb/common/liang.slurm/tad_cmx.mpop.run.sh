#!/bin/bash

#SBATCH --job-name="cmx.mpop"
#SBATCH --output="cmx.mpop.%j.out"
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=288
#SBATCH --export=ALL
#SBATCH -t 75:30:00

# Usage:
#
#   sbatch <script_name.sh> <mix>
#
# where <mix> is the model index" which is a 0-based index specifying the
# target TAD region.
#
# Example:
#
#   sbatch <script_path> 0
#
# Above will generate jobs for the 0-th (first) TAD model.
#
# Monitor using either:
#   squeue -u <user_id>
#
# Delete using:
#   scancel <job_id>
#
# For more details, see:
#
# https://portal.xsede.org/sdsc-comet
# https://slurm.schedmd.com/sbatch.html

###################################################
# Timestamp utils
###################################################

# From http://stackoverflow.com/questions/17066250/create-timestamp-variable-in-bash-script
timestamp() {
  date +"%T"
}

###################################################
# Run
###################################################

# Determine path to script
HOME_DIR=$(echo ~)
WORK_DIR="$HOME_DIR/chr-working"
PROJ_DIR="$WORK_DIR/loci/mbi-mam"
SCRIPT_DIR="$PROJ_DIR/gm12878/fractal2d.hic5kb.tad2mb/common/liang.slurm"

# Max local CPU cores to use (make sure does not exceed PBS ppn)
MAX_PROC=100

# Parse command line (non-optional argument) for model index
MIX=-1
if [ $# -gt 0 ]; then
    MIX="$1"
fi

# Validate argument(s)
if [ "$MIX" -lt "0" ]; then
    # http://stackoverflow.com/questions/4381618/exit-a-script-on-error
    # Redirect to stderror (file descriptor 2)
    echo "Error: Invalid model index: $MIX < 0. Exiting." 1>&2
    # Exit status 1 indicates error
    exit 1
fi

# Output information about compute node
python "$SCRIPT_DIR/../cpu_info.py"

# Determine path to target local script
PY_SCRIPT_PATH="$SCRIPT_DIR/../tad_cmx.py"

# If '1' or <path>, will attempt to load table and process TAD model names in
# row order. If '0', will check existing folders in TAD base output directory.
# @WARNING - THIS MAY CAUSE PRINT MISMATCH OF 'MID' IN 'tad_cmx.queue.py'
#   WHICH IS OKAY, JUST LOGGING STATEMENTS OF WHICH TAD IS BEING PROCESSED
#   MAY NOT BE CORRECT
FROM_TABLE=1

# Generative model type
POP_KEY='mpop'

# Timestamp script execution
SCRIPT_ID="$(date +%Y%m%d%H%M%S).$RANDOM"
# Run script
cmd="python $PY_SCRIPT_PATH --model_index $MIX"
cmd="$cmd --max_proc $MAX_PROC"
cmd="$cmd --from_table $FROM_TABLE"
cmd="$cmd --pop_key $POP_KEY"
echo "$SCRIPT_ID : $(timestamp) : Running $cmd"
$cmd
echo "$SCRIPT_ID : $(timestamp) : Finished job"
