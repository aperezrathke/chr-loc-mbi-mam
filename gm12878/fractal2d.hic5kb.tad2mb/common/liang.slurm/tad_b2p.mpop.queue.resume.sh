#!/bin/bash

# Call python multi-population BIT2POL multiplexer
#
# Example usage:
#
#   nohup <path_to_script> [-nodes <int>] &> <path_to_log> &
#   : nohup means to keep job running even when logged out
#   : &> means to redirect stdout and stderr to log file
#   : & means to run script in background
#
# Optional argument(s)
#
#   -nodes <int>: Specify maximum number of compute nodes

###################################################
# Script paths and globals
###################################################

# Path to script directory
SCRIPT_PATH="$(readlink -f $0)"
SCRIPT_DIR="$(dirname $SCRIPT_PATH)"

# Path to python script
PY_SCRIPT_PATH="$SCRIPT_DIR/tad_b2p.mpop.queue.py"

# Max computes nodes
MAX_NODES=8

# Resume B2P generation from this model index
RESUME_INDEX=500

###################################################
# Command line overrides
###################################################

# Check if there are any parameter overrides
while [ $# -gt 1 ]
do
    case "$1" in
    -nodes)
        MAX_NODES=$2
        shift
        ;;
    esac
    shift
done

# Validate argument(s)
if [ "$MAX_NODES" -lt "1" ]; then
    # Use at least a single node
    MAX_NODES=1
fi

###################################################
# Run
###################################################

# Run script
cmd="python $PY_SCRIPT_PATH --max_compute_nodes $MAX_NODES"
cmd="$cmd --resume_index $RESUME_INDEX"
echo "Running $cmd"
$cmd
echo "Finished multi-population B2P queue"
