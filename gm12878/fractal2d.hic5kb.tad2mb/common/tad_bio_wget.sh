#!/bin/bash

# Retrieve raw ENCODE data sets

# Path to this script's directory
SCRIPT_PATH="$(readlink -f $0)"
SCRIPT_DIR="$(dirname $SCRIPT_PATH)"
SCRIPT_DIR=$(cd "$SCRIPT_DIR"; pwd)

# Make sure storage directory exists
OUT_DIR="$SCRIPT_DIR/../output/encode/bigWig"
mkdir -p "$OUT_DIR"

# Switch to output folder
pushd ./
cd "$OUT_DIR"

echo "-----------------------"
echo "RNA-seq (Poly A+) - LONG READS (> 200 BP) (Gingeras, CSHL)"
echo "ENCFF353AGE - plus strand signal of unique reads"
wget https://www.encodeproject.org/files/ENCFF353AGE/@@download/ENCFF353AGE.bigWig
echo "ENCFF970EOO - minus strand signal of unique reads"
wget https://www.encodeproject.org/files/ENCFF970EOO/@@download/ENCFF970EOO.bigWig
echo "ENCFF619YUK - plus strand signal of unique reads"
wget https://www.encodeproject.org/files/ENCFF619YUK/@@download/ENCFF619YUK.bigWig
echo "ENCFF736SXH - minus strand signal of unique reads"
wget https://www.encodeproject.org/files/ENCFF736SXH/@@download/ENCFF736SXH.bigWig
echo "ENCFF683NHK - plus strand signal of unique reads"
wget https://www.encodeproject.org/files/ENCFF683NHK/@@download/ENCFF683NHK.bigWig
echo "ENCFF658NYH - minus strand signal of unique reads"
wget https://www.encodeproject.org/files/ENCFF658NYH/@@download/ENCFF658NYH.bigWig
echo "ENCFF897KDK - plus strand signal of unique reads"
wget https://www.encodeproject.org/files/ENCFF897KDK/@@download/ENCFF897KDK.bigWig
echo "ENCFF050ZIR - minus strand signal of unique reads"
wget https://www.encodeproject.org/files/ENCFF050ZIR/@@download/ENCFF050ZIR.bigWig

echo "-----------------------"
echo "H3K27ac ChIP-seq (Bernstein, Broad)"
echo "ENCFF180LKW - pooled fold change over control"
wget https://www.encodeproject.org/files/ENCFF180LKW/@@download/ENCFF180LKW.bigWig

echo "-----------------------"
echo "H3K27me3 ChIP-seq (Bernstein, Broad)"
echo "ENCFF167NBF - pooled fold change over control"
wget https://www.encodeproject.org/files/ENCFF167NBF/@@download/ENCFF167NBF.bigWig

echo "-----------------------"
echo "H3K4me1 ChIP-seq (Bernstein, Broad)"
echo "ENCFF682WPF - pooled fold change over control"
wget https://www.encodeproject.org/files/ENCFF682WPF/@@download/ENCFF682WPF.bigWig

echo "-----------------------"
echo "H3K9me3 ChIP-seq (Bernstein, Broad)"
echo "GSM733664 - signal"
wget ftp://ftp.ncbi.nlm.nih.gov/geo/samples/GSM733nnn/GSM733664/suppl/GSM733664_hg19_wgEncodeBroadHistoneGm12878H3k9me3StdSig.bigWig

echo "-----------------------"
echo "POLR2AphosphoS5 ChIP-seq (Myers, HAIB)"
echo "ENCFF002UPS - pooled fold changed over control"
wget https://www.encodeproject.org/files/ENCFF002UPS/@@download/ENCFF002UPS.bigWig

echo "-----------------------"
echo "RNA-seq ((Poly A+) - SMALL READS (< 200 BP) (Gingeras, CSHL)"
echo "ENCFF497GXW - plus strand signal of unique reads"
wget https://www.encodeproject.org/files/ENCFF497GXW/@@download/ENCFF497GXW.bigWig
echo "ENCFF898YSM - minus strand signal of unique reads"
wget https://www.encodeproject.org/files/ENCFF898YSM/@@download/ENCFF898YSM.bigWig
echo "ENCFF883JLV - plus strand signal of unique reads"
wget https://www.encodeproject.org/files/ENCFF883JLV/@@download/ENCFF883JLV.bigWig
echo "ENCFF791HSP - minus strand signal of unique reads"
wget https://www.encodeproject.org/files/ENCFF791HSP/@@download/ENCFF791HSP.bigWig

echo "-----------------------"
echo "H3K36me3 ChIP-seq (Stamatoyannopoulos, UW)"
echo "ENCFF662QFK - pooled fold change over control"
wget https://www.encodeproject.org/files/ENCFF662QFK/@@download/ENCFF662QFK.bigWig

echo "-----------------------"
echo "EP300 ChIP-seq (Snyder, Stanford)"
echo "ENCFF820BXH - pooled fold change over control"
wget https://www.encodeproject.org/files/ENCFF820BXH/@@download/ENCFF820BXH.bigWig

echo "-----------------------"
echo "CTCF ChIP-seq (Snyder, Stanford)"
echo "ENCFF312KXX - pooled fold change over control"
wget https://www.encodeproject.org/files/ENCFF312KXX/@@download/ENCFF312KXX.bigWig

echo "-----------------------"
echo "DNase1 DNase-seq (Stamatoyannopoulos, UW)"
echo "ENCFF264NMW - read-depth normalized signal"
wget https://www.encodeproject.org/files/ENCFF264NMW/@@download/ENCFF264NMW.bigWig
echo "ENCFF901GZH - read-depth normalized signal"
wget https://www.encodeproject.org/files/ENCFF901GZH/@@download/ENCFF901GZH.bigWig

echo "-----------------------"
echo "H3K4me3 ChIP-seq (Bernstein, Broad)"
echo "ENCFF818GNV - pooled fold change over control"
wget https://www.encodeproject.org/files/ENCFF818GNV/@@download/ENCFF818GNV.bigWig

# NOTE: microRNA-seq (ENCSR770HBF) - NOT DOWNLOADED - is GRCh38 and must be
#   converted to hg19 via CrossMap tool

# NOTE: whole genome bisulfite sequencing (ENCSR890UQO) - NOT DOWNLOADED - is
#   GRCh38 and must be converted to hg19 via CrossMap tool

echo "-----------------------"
echo "CBX5 (HP1-alpha) ChIP-seq (Myers, HAIB)"
echo "ENCFF442AII - pooled fold change over control"
wget https://www.encodeproject.org/files/ENCFF442AII/@@download/ENCFF442AII.bigWig

echo "-----------------------"
echo "STAT3 ChIP-seq (Snyder, Stanford)"
echo "ENCFF133SDO - pooled fold change over control"
wget https://www.encodeproject.org/files/ENCFF133SDO/@@download/ENCFF133SDO.bigWig

echo "-----------------------"
echo "H3K9ac Chip-seq (Bernstein, Broad)"
echo "ENCFF465KNK - pooled fold change over control"
wget https://www.encodeproject.org/files/ENCFF465KNK/@@download/ENCFF465KNK.bigWig

echo "-----------------------"
echo "RNA-seq (Poly A-) - LONG READS (> 200 BP) (Gingeras, CSHL)"
echo "ENCFF276GYO - plus strand signal of unique reads"
wget https://www.encodeproject.org/files/ENCFF276GYO/@@download/ENCFF276GYO.bigWig
echo "ENCFF487QGW - minus strand signal of unique reads"
wget https://www.encodeproject.org/files/ENCFF487QGW/@@download/ENCFF487QGW.bigWig
echo "ENCFF681MXE - plus strand signal of unique reads"
wget https://www.encodeproject.org/files/ENCFF681MXE/@@download/ENCFF681MXE.bigWig
echo "ENCFF161SKT - minus strand signal of unique reads"
wget https://www.encodeproject.org/files/ENCFF161SKT/@@download/ENCFF161SKT.bigWig

echo "-----------------------"
echo "PAX5 ChIP-seq (Myers, HAIB)"
echo "ENCFF945PRR - pooled fold change over control"
wget https://www.encodeproject.org/files/ENCFF945PRR/@@download/ENCFF945PRR.bigWig

echo "-----------------------"
echo "RAD21 ChIP-seq (Myers, HAIB)"
echo "ENCFF567EGK - pooled fold change over control"
wget https://www.encodeproject.org/files/ENCFF567EGK/@@download/ENCFF567EGK.bigWig

echo "-----------------------"
echo "H3K79me2 ChIP-seq (Bernstein, Broad)"
echo "ENCFF396JIR - pooled fold change over control"
wget https://www.encodeproject.org/files/ENCFF396JIR/@@download/ENCFF396JIR.bigWig

echo "-----------------------"
echo "POLR3"
echo "GSM935316 - signal"
wget ftp://ftp.ncbi.nlm.nih.gov/geo/samples/GSM935nnn/GSM935316/suppl/GSM935316_hg19_wgEncodeSydhTfbsGm12878Pol3StdSig.bigWig

echo "-----------------------"
echo "MYC"
echo "GSM822290 - signal"
wget ftp://ftp.ncbi.nlm.nih.gov/geo/samples/GSM822nnn/GSM822290/suppl/GSM822290_hg19_wgEncodeOpenChromChipGm12878CmycSig.bigWig

echo "-----------------------"
echo "RNA-seq - SMALL READS (< 200 BP) - Nuclear Fraction (Gingeras, CSHL)"
echo "ENCFF937BBI - plus strand signal of unique reads"
wget https://www.encodeproject.org/files/ENCFF937BBI/@@download/ENCFF937BBI.bigWig
echo "ENCFF395XGO - minus strand signal of unique reads"
wget https://www.encodeproject.org/files/ENCFF395XGO/@@download/ENCFF395XGO.bigWig
echo "ENCFF957PZK - plus strand signal of unique reads"
wget https://www.encodeproject.org/files/ENCFF957PZK/@@download/ENCFF957PZK.bigWig
echo "ENCFF163IQD - minus strand signal of unique reads"
wget https://www.encodeproject.org/files/ENCFF163IQD/@@download/ENCFF163IQD.bigWig

echo "-----------------------"
echo "RNA-seq - LONG READS (Poly A-) (> 200 BP) - Nuclear Fraction (Gingeras, CSHL)"
echo "ENCFF663PUB - plus strand signal of unique reads"
wget https://www.encodeproject.org/files/ENCFF663PUB/@@download/ENCFF663PUB.bigWig
echo "ENCFF271WFW - minus strand signal of unique reads"
wget https://www.encodeproject.org/files/ENCFF271WFW/@@download/ENCFF271WFW.bigWig
echo "ENCFF932MVQ - plus strand signal of unique reads"
wget https://www.encodeproject.org/files/ENCFF932MVQ/@@download/ENCFF932MVQ.bigWig
# NOTE: ENCFF064LBB - minus strand signal of unique reads - NOT DOWNLOADED -
#   is GRCh38 and must be converted to hg19 via CrossMap tool

echo "-----------------------"
echo "RNA-seq - LONG READS (Poly A+) (> 200 BP) - Nuclear Fraction (Gingeras, CSHL)"
echo "ENCFF572JQA - plus strand signal of unique reads"
wget https://www.encodeproject.org/files/ENCFF572JQA/@@download/ENCFF572JQA.bigWig
echo "ENCFF121WAL - minus strand signal of unique reads"
wget https://www.encodeproject.org/files/ENCFF121WAL/@@download/ENCFF121WAL.bigWig
echo "ENCFF613JUB - plus strand signal of unique reads"
wget https://www.encodeproject.org/files/ENCFF613JUB/@@download/ENCFF613JUB.bigWig
echo "ENCFF862GCF - minus strand signal of unique reads"
wget https://www.encodeproject.org/files/ENCFF862GCF/@@download/ENCFF862GCF.bigWig

# Return to original directory 
popd

echo "Finished"
