#!/usr/bin/python
# -*- coding: utf-8 -*-

# Common configuration settings and utilities

###################################################
# Imports
###################################################

# For path manipulation 
import os

# For determining number of CPU cores
from multiprocessing import cpu_count

# For running shell commands
import subprocess

# For system manipulation
import sys

# For parsing tabular data
import pandas as pd

###################################################
# Globals
###################################################

# Path to directory containing this script
# https://stackoverflow.com/questions/4934806/how-can-i-find-scripts-directory-with-python
SCRIPT_DIR = os.path.abspath(os.path.dirname(os.path.realpath(__file__)))

# Root module directory
MODULE_DIR = os.path.join(SCRIPT_DIR, "..", "..", "..")
MODULE_DIR = os.path.abspath(MODULE_DIR)

# Common utilities directory
MODULE_COMMON_DIR = os.path.join(MODULE_DIR, "common")
# Common scripts directory
MODULE_COMMON_SCRIPTS_DIR = os.path.join(MODULE_COMMON_DIR, "scripts")
# Common config directory
MODULE_COMMON_CONFIG_DIR = os.path.join(MODULE_COMMON_SCRIPTS_DIR, "config",
                                        "fractal2d")

# Output base directory
OUT_BASE_DIR = os.path.join(SCRIPT_DIR, "..", "output")
OUT_BASE_DIR = os.path.abspath(OUT_BASE_DIR)

# Build directory
BUILD_DIR = os.path.join(OUT_BASE_DIR, "build")

# Import local 'scale_nucleus' script
# https://stackoverflow.com/questions/4383571/importing-files-from-different-folder
LIB_PATH = os.path.abspath(os.path.join(BUILD_DIR, "scripts", "Local"))
sys.path.append(LIB_PATH)
import scale_nucleus

# Default number of parallel processes, should not exceed logical cores
MAX_PROC = int(max(cpu_count() / 2, 1))

###################################################
# Settings
###################################################

# Configure default R script overwrite setting
DEF_SHOULD_OVERWRITE = False

# Configure default R script verbose setting
DEF_VERBOSE = True

# Directory path to mock-up data sets
MOCK_DIR = os.path.join(SCRIPT_DIR, "..", "mock")

# Path to mock-up Hi-C data from juicer tools
MOCK_HIC_RAW_PATH = os.path.join(MOCK_DIR, "hic.raw.txt")

# Path to mock-up Hi-C cleaned data.frame
MOCK_HIC_CLEAN_PATH = os.path.join(MOCK_DIR, "hic.clean.csv.gz")

# Path to mock-up Hi-C clustered data.frame
MOCK_HIC_CLUST_PATH = os.path.join(MOCK_DIR, "hic.clust.csv.gz")

# Directory path for null scripts
NULL_SCRIPT_DIR = os.path.join(SCRIPT_DIR, "..", "null")

# Path to null output base directory
NULL_BASE_OUT_DIR = os.path.join(OUT_BASE_DIR, "null")

# If True, samples with weights outside +/- 1.5 IQR are removed
NULL_REMOVE_OUTLIERS = True

# If True, bonded ligations are retained
NULL_BONDED = False

# Directory with null ligation contact data in gzipped CSV format
NULL_LIGC_CSV_GZ_DIR = os.path.join(NULL_BASE_OUT_DIR, "ligc.csv.gz")

# Directory with null ligation contact data in RData format
NULL_LIGC_RDATA_DIR = os.path.join(NULL_BASE_OUT_DIR, "ligc.rdata")

# Directory with null ligation contact data in RData format merged into single
# file
NULL_LIGC_MERGE_RDATA_DIR = \
    os.path.join(NULL_BASE_OUT_DIR, "ligc.merge.rdata")

# Directory for storing null ligation contact frequency matrix in gzipped CSV
# format
NULL_LIGC_FREQMAT_CSV_GZ_DIR = \
    os.path.join(NULL_BASE_OUT_DIR, "ligc.freqmat.csv.gz")

# Directory for storing null ligation contact frequency matrix in RData format
NULL_LIGC_FREQMAT_RDATA_DIR = \
    os.path.join(NULL_BASE_OUT_DIR, "ligc.freqmat.rdata")

# Directory with null ligation contact BLB replicate data in lower-banded,
# gzipped CSV format
NULL_LIGC_POPD_LBAND_CSV_GZ_DIR = \
    os.path.join(NULL_BASE_OUT_DIR, "ligc.popd.lband.csv.gz")

# Directory with null ligation contact BLB replicate data in lower-banded,
# RData format
NULL_LIGC_POPD_LBAND_RDATA_DIR = \
    os.path.join(NULL_BASE_OUT_DIR, "ligc.popd.lband.rdata")

# Directory with null ligation contact BLB replicate data in lower-banded,
# RData format merged into single file
NULL_LIGC_POPD_LBAND_MERGE_RDATA_DIR = \
    os.path.join(NULL_BASE_OUT_DIR, "ligc.popd.lband.merge.rdata")

# Path to ligation contact BLB replicate data in lower-banded, RData format
# merged into single file
NULL_LIGC_POPD_LBAND_MERGE_RDATA_PATH = \
    os.path.join(NULL_LIGC_POPD_LBAND_MERGE_RDATA_DIR,
        os.path.basename(NULL_LIGC_POPD_LBAND_MERGE_RDATA_DIR))

# Path to processed SPRITE data
SPRITE_PATH = os.path.join(MODULE_COMMON_DIR, "sprite",
    "gm12878.mapq10.cluster.spr5.hic10.gd40.tad20.refG100.homg0.75.csv")

# Base directory to create TAD loci models
TAD_BASE_CONFIG_DIR = os.path.join(SCRIPT_DIR, "..", "tad")

# Base directory to store unversioned data (e.g. Hi-C)
TAD_BASE_OUT_DIR = \
    os.path.join(OUT_BASE_DIR, os.path.basename(TAD_BASE_CONFIG_DIR))

# Base directory to store TAD related plots
TAD_BASE_PLOT_DIR = \
    os.path.join(OUT_BASE_DIR, "tad.plot")

# Number of lattice dimensions when growing polymer chains, corresponds to
# folder INI argument 'num_unit_sphere_sample_points'
TAD_LAT_DIM = int(50)

# Maximum overlap factor, model may not overlap previous models by more than
# this factor else it will be culled
FILT_MAX_OVERLAP = 0.20

# Minimum number of SPRITE cluster hits, any models with associated SPRITE
# records less than this value are culled
FILT_MIN_SPRITE_HITS = 10

# Identifier for subdirectory containing filtered data
FILT_ID = '.'.join(["tad", "filt", "ov" + str(int(100.0 * FILT_MAX_OVERLAP)),
                    "sprh" + str(FILT_MIN_SPRITE_HITS)])

# Path to filtered TAD tabular data
FILT_PATH = os.path.join(OUT_BASE_DIR, FILT_ID, os.path.basename(SPRITE_PATH))

# Unscaled nuclear volume in microns (+real)
SCALE_NUC_VOL = scale_nucleus.NV_GM12878_SANBORN

# Whole genome length in base pairs (+real)
SCALE_GEN_BP = scale_nucleus.GBP_HG19_DIPLOID

# Target monomer unit density in base pairs
SCALE_MON_BP = 5000

# Intrinsic fiber unit density in base pairs
SCALE_FIB_BP = scale_nucleus.FBP_11_NM_RIPPE

# Intrinsic fiber unit length in Angstroms
SCALE_FIB_LEN = scale_nucleus.FL_11_NM

# Identifier for subdirectory containing scaled data
SCALE_ID = '.'.join(["tad", "scale", str(SCALE_MON_BP / 1000) + "kb"])

# Path to [filtered and] scaled TAD tabular data
SCALE_PATH = os.path.join(OUT_BASE_DIR, SCALE_ID, os.path.basename(FILT_PATH))

# Juicer tools path
# https://github.com/aidenlab/juicer/wiki/Juicer-Tools-Quick-Start
HIC_JUICER_PATH = os.path.join(BUILD_DIR, "tools", "juicer_tools.jar")

# Hi-C normalization: norm NONE|VC|VC_SQRT|KR
#   "NONE" is no normalization, "VC" is for vanilla coverage normalization
#   (old Aiden lab algorithm), "VC_SQRT" is square root of vanilla coverage,
#   and KR is Knight-Ruiz matrix balancing (recommended).
HIC_NORM = "KR"

# Hi-C read mapping quality (mapq): combined|combined_30
#   For mapq > 0, select "combined"; for mapq > 30, select "combined_30".
#   Higher mapq score has better read mapping confidence, but read counts are
#   likely more sparse and bias may be increased due to differences in primer
#   efficiencies throughout genome.
HIC_MAPQ = "combined"

# Hi-C cloud storage base URL
HIC_BASE_URL = "https://hicfiles.s3.amazonaws.com/hiseq/gm12878/in-situ/"

# Hi-C bin resolution in base pairs
HIC_BIN_BP = SCALE_MON_BP

# False discovery rate threshold for specific contacts
HIC_FDR_THRESH = 0.05

# Lower target fraction of Hi-C specific contacts to retain from clustering
HIC_CLUST_FRACT_LO = 0.14

# Upper target fraction of Hi-C specific contacts to retain from clustering
HIC_CLUST_FRACT_HI = 0.15

# Hard upper bound on number of clusters, ignored if < 1
HIC_CLUST_K_MAX = 35

# File identifier for chr-chr interactions within a locus
INTR_CHR_FID = "intr.chr.0.csv"

#################
# Fea

# Command key to folder 'fea' dispatch utility
FEA_CMD_KEY = "loc_fea"

# Path to top-level configuration INI for infeasible chr-chr constraint search
FEA_TOP_INI_PATH = os.path.join(MODULE_COMMON_CONFIG_DIR,
                                "folder.fea.parent.ini")

# Path to folder executable for 'fea' dispatch
FEA_EXE_PATH = os.path.join(BUILD_DIR, "bin", "Release", "u_sac")

# Knock-in distance threshold in Angstroms
#   Giorgetti, Luca, et al. "Predictive polymer modeling reveals coupled
#       fluctuations in chromosome conformation and transcription." Cell 157.4
#       (2014): 950-963.
FEA_DIST_KI = 795.0

# Knock-out distance threshold in Angstroms
FEA_DIST_KO_SCALE = 1.05
FEA_DIST_KO = FEA_DIST_KO_SCALE * FEA_DIST_KI

# Max number of tested KI/KO configurations
FEA_MAX_ATTEMPT = 10000

# Maximum infeasible corpus size
FEA_MAX_CORP = 100

# Max number of folder trials before a KI/KO config is labeled 'infeasible'
FEA_MAX_TRIAL = 5000

#################
# Perturb

# If True, samples with weights outside +/- 1.5 IQR are removed
PERTURB_REMOVE_OUTLIERS = NULL_REMOVE_OUTLIERS

# Command key
PERTURB_CMD_KEY = "loc_intr_chr"

# Path to perturbation archetype INI
PERTURB_ARCH_INI_PATH = os.path.join(NULL_SCRIPT_DIR, "config",
                                     "folder.arch.ini")

# Path to top-level configuration INI for intr_chr chr-chr constraint
PERTURB_TOP_INI_PATH = os.path.join(MODULE_COMMON_CONFIG_DIR,
                                    "folder.intr_chr.parent.ini")

# Path to folder executable for 'fea' dispatch
PERTURB_EXE_PATH = os.path.join(BUILD_DIR, "bin", "Release_threaded",
                                "u_sac")

# Knock-in distance threshold in Angstroms
PERTURB_DIST_KI = FEA_DIST_KI

# Max number of folder trials
PERTURB_MAX_TRIAL = 25000

# Perturbation ensemble size
PERTURB_ENSEMBLE_SIZE = 10000

# Base perturbations folder
PERTURB_BASE_DIR = os.path.join(NULL_BASE_OUT_DIR, "perturb")

# Path to master interactions list
# Created by tad_perturb_setup.py!
PERTURB_INTR_CHR_PATH = os.path.join(NULL_BASE_OUT_DIR, INTR_CHR_FID)

#################
# CMX

# Optional suffix string to tag output CMX folders
CMX_TAG = ""

# FDR threshold for calling allowed CMX edges
CMX_EDGE_FDR_THRESH = 0.05

# CMX Gibbs sampler model: spop|mpop
CMX_POP_KEY = "spop"

# CMX executable path
CMX_EXE_PATH = os.path.join(SCRIPT_DIR, "..", "..", "..", "..", "..", "cmx",
                            "bin", "Release_threaded", "cmx")

#################
# Bit2Pol

# If TRUE, use cluster resampling for interaction contstraints
# If FALSE, use exemplar interaction constrations only
B2P_USE_INTR_CHR_DIR = False

# If TRUE, generate log file for each bit2pol batch job
B2P_USE_LOGGING = True

# Number of cluster resamples for chromatin-chromatin interaction configs
B2P_NUM_INTR_CHR_CONFIG = 500

# Path to bit2pol.py script
B2P_SCRIPT_PATH = os.path.join(BUILD_DIR, "scripts", "Local", "bit2pol.py")

# Path to folder executable for bit2pol 3-D modeling
B2P_EXE_PATH = os.path.join(BUILD_DIR, "bin", "Release", "u_sac")

# Command key to main dispatch for bit2pol folding
B2P_CMD_KEY = PERTURB_CMD_KEY

# Path to archetype INI configuration for bit2pol folding
B2P_ARCH_INI_PATH = PERTURB_ARCH_INI_PATH

# Path to top-level INI configuration for bit2pol folding
B2P_TOP_INI_PATH = PERTURB_TOP_INI_PATH

# If True, only process b2p|cmx folders at lowest depth
# If False, process all encountered b2p|cmx folders
B2P_TOP_ONLY = True

# Optional suffix string to tag output bit2pol folders
B2P_TAG = "clustw" if B2P_USE_INTR_CHR_DIR else ""

# Ensemble size per bit config
B2P_ENSEMBLE_SIZE = 50

# Number of processes per bit config
B2P_NUM_PROC = 1

# If True, treat '0' bits as random folding
# If False, enforce '0' bits as constrained knock-outs
B2P_KO_RAND = True

# Knock-in distance threshold in Angstroms
B2P_DIST_KI = PERTURB_DIST_KI

# CMX Gibbs sampler model: spop|mpop to transform
B2P_POP_KEY = CMX_POP_KEY

# Name of pseudo-population bit matrix file
B2P_BITPOP_FNAME = "map.pseu.frame.csv"

# Max number of folding trials for bit2pol 3-D modelings
B2P_MAX_TRIAL = 50000

# If True, crop b2p ligc data to match modeled TAD region
B2P_CROP = True

# If TRUE, assume empirical distribution is target
B2P_EQW = True

# Remove geometry samples with outlier weights
B2P_REMOVE_OUTLIERS = False

#################
# MBI

# TAD region filtering
MBI_MODEL_FILT_ID = None

# Only use first 'ens_sz' random samples for null modeling many body
#   interactions at each TAD locus, this is a performance vs quality tradeoff
# Note, if None or < 1, all samples used
MBI_NULL_ENS_SZ = 75000

# Only retain ligc (i,j) tuples with |j-i| > min_gd
MBI_EDGE_MIN_GD = 4

# Minimum clique size
MBI_CLQ_MIN = 3

# Maximum clique size
MBI_CLQ_MAX = None

# Method used to summarize each clique and map to a bin
MBI_CLQ_MAP_ID = "minb_maxgd"

# Clique type - 'max' means clique is cannot be extended
MBI_CLQ_TYPE = "max"

# Bootstrap statistic
MBI_BOOT_STAT_ID = "mea"

# Number of bootstrap replicates
MBI_BOOT_REPS = 1000

# P-value startification method, all bootstrap elements with same test label
#   as observed test label will be used to compute p-value
MBI_PVTEST_BY_ID = 'body_maxgd'

# Miscellaneous data tag
MBI_TAG = ""

#################
# BIO

# Binning method for many-body interactions
BIO_BIN_MBI_ID = "prin_loop"

# Binning minimum body size
BIO_BIN_MBI_MIN_BODIES = [ 3,  3,  4,  5,  6,  3,  5,  3,  6]

# Binning maximum body size
BIO_BIN_MBI_MAX_BODIES = [-1,  3,  4,  5,  6,  4, -1,  5, -1]

# Binning minimum principal loop bead span
BIO_BIN_MBI_MIN_LOOPS =  [-1, -1, -1, -1, -1, -1, -1, -1, -1]

# Binning maximum principal loop bead span
BIO_BIN_MBI_MAX_LOOPS =  [-1, -1, -1, -1, -1, -1, -1, -1, -1]

# FDR threshold for binning a many-body interaction
BIO_BIN_MBI_FDR = HIC_FDR_THRESH

# Path to Super-enhancer LILY data
BIO_SE_LILY_PATH = os.path.join(MODULE_COMMON_DIR, 'lily', 'gm12878.bed')

# Default TAD region filter
BIO_MODEL_FILT_ID = 'num_se'

###################################################
# Utilities
###################################################

# @param args - parsed argparse object
def print_cmd(args):
    '''Prints command-line to stdout'''
    for arg in vars(args):
        print "[--" + arg + "]"
        print str(getattr(args, arg))

# Will automatically convert argumetns to string
# @param cmd - Command list to execute
# @param max_fail - Max number of failures
def run_cmd(cmd, max_fail):
    '''Run blocking subprocess'''
    cmd = map(str, cmd)
    RC_OKAY = 0
    cmd_str = " ".join(cmd)
    print "Running: " + cmd_str
    rc = 1 - RC_OKAY
    NUM_ATTEMPT = max(1, max_fail)
    for i in xrange(NUM_ATTEMPT):
        # Blocking call!
        rc = subprocess.call(cmd)
        if rc == RC_OKAY:
            break
    if rc != RC_OKAY:
        print "ERROR: Unexpected return code (" + str(rc) + ") from command:"
        print cmd_str
        print "EXITING (" + str(rc) + ")"
        sys.exit(rc)

# Recursively creates directory such that all parent directories are created
# if necessary
# http://stackoverflow.com/questions/273192/in-python-check-if-a-directory-exists-and-create-it-if-necessary
def make_dir(d):
   '''Utility for making a directory if not existing.'''
   if not os.path.exists(d):
       os.makedirs(d)

# @param df - Data.frame to save
# @param fout - Output file path
# @param index - Flag to save index column
def save_table(df, fout, index=False):
    '''Writes data.frame'''
    print "Saving: " + str(fout)
    make_dir(os.path.dirname(fout))
    df.to_csv(path_or_buf=fout, index=index)

# @param fin - File path or handle
# @param header - Header row (or None)
# @param index_col - index column if any (else False)
# @return loaded Pandas data.frame
def load_table(fin, header=0, index_col=False):
    '''Reads data.frame'''
    print "Loading: " + str(fin)
    df = pd.read_csv(filepath_or_buffer=fin, header=header, index_col=index_col)
    return df

# @param fin - File path or handle to processed SPRITE data
def load_sprite(fin=SPRITE_PATH):
    '''Return data.frame of processed SPRITE tabular data'''
    return load_table(fin=fin, header=0, index_col=False)

# @param fin - File path or handle to filtered TAD tabular data
def load_tad_filt(fin=FILT_PATH):
    '''Return data.frame of filtered TAD tabular data'''
    return load_table(fin=fin, header=0, index_col=False)

# @param fin - File path or handle to [filtered and] scaled TAD tabular data
def load_tad_scale(fin=SCALE_PATH):
    '''Return data.frame of [filtered and] scaled TAD tabular data'''
    return load_table(fin=fin, header=0, index_col=False)

# @param prefix - File name prefix
# @param ext_no_dot - File extension without dot prefix
# @param data_dir - Target directory to store data
# @param rmvout - True for outlier removal, False o/w
def get_ligc_path_util(prefix,
                       ext_no_dot,
                       data_dir,
                       rmvout):
    '''Return path to ligc related data set from ensemble data dir'''
    fname = '.'.join([prefix, 'rmvout', str(int(rmvout)), ext_no_dot])
    return os.path.join(data_dir, fname)

# @param rmvout - True for outlier removal, False o/w
# @param data_dir - Target directory to store data
def get_null_ligc_merge_rdata_path(rmvout = NULL_REMOVE_OUTLIERS,
                                   data_dir = NULL_LIGC_MERGE_RDATA_DIR):
    '''Return path to merged ligc data from null ensemble'''
    return get_ligc_path_util(prefix='ligc.merge',
                              ext_no_dot='rdata',
                              data_dir=data_dir,
                              rmvout=rmvout)

# @param rmvout - True for outlier removal, False o/w
# @param data_dir - Target directory to store data
def get_null_ligc_freqmat_csv_gz_path(rmvout = NULL_REMOVE_OUTLIERS,
                                      data_dir = NULL_LIGC_FREQMAT_CSV_GZ_DIR):
    '''Return path to frequency matrix ligc CSV GZ data from null ensemble'''
    return get_ligc_path_util(prefix='ligc.freqmat',
                              ext_no_dot='csv.gz',
                              data_dir=data_dir,
                              rmvout=rmvout)

# @param rmvout - True for outlier removal, False o/w
# @param data_dir - Target directory to store data
def get_null_ligc_freqmat_rdata_path(rmvout = NULL_REMOVE_OUTLIERS,
                                     data_dir = NULL_LIGC_FREQMAT_RDATA_DIR):
    '''Return path to frequency matrix ligc RData from null ensemble'''
    return get_ligc_path_util(prefix='ligc.freqmat',
                              ext_no_dot='rdata',
                              data_dir=data_dir,
                              rmvout=rmvout)

# @param model_name - Model identifier
# @param base_dir - Base directory for storing generated models
def get_tad_model_dir(model_name, base_dir=TAD_BASE_OUT_DIR):
    '''Return TAD model base directory'''
    return os.path.join(base_dir, model_name)

# @param base_dir - Base directory for storing generated models
def get_tad_model_names(base_dir=TAD_BASE_OUT_DIR):
    '''Return all sub-directories within base dir'''
    return sorted([name for name in os.listdir(base_dir)
            if os.path.isdir(os.path.join(base_dir, name))])

# @param fin - File path or handle to filtered and scaled TAD tabular data
# @return list of model names, ordered by occurrence in table
def get_tad_model_names_from_table(fin=SCALE_PATH):
    '''Return model names according to order listed in table file'''
    df = load_tad_scale(fin=fin)
    names = df['name']
    return list(names)

# @WARNING - WILL TRIGGER READ FROM DISK OF SCALED SPREADSHEET!
# @param model_name - Model identifier
def get_tad_mon_num(model_name):
    '''Scaled monomer number for TAD model name'''
    df = load_tad_scale(fin=SCALE_PATH)
    # https://stackoverflow.com/questions/17071871/select-rows-from-a-dataframe-based-on-values-in-a-column-in-pandas
    row = df.loc[df['name'] == model_name]
    # https://stackoverflow.com/questions/16729574/how-to-get-a-value-from-a-cell-of-a-dataframe
    return int(row.iloc[0]['mon_num'])

# @param from_table - [None|0|1|<path>] If equivalent to 0 or None, or if set
#   to non-existant file path, then model names will be from output directory
#   listing. Else, model names will be from 'name' column of loaded table file.
# @param base_dir - Base directory for storing generated models
# @return List of model names according to parameters
def resolve_tad_model_names(from_table=1, base_dir=TAD_BASE_OUT_DIR):
    '''Resolve model names from user parameters'''
    if not from_table is None:
        ft_str = str(from_table).strip()
        if (ft_str not in ["0", "0L", "0.0"]) and (ft_str.lower() != "none"):
            if ft_str in ["1", "1L", "1.0"]:
                # Use default table path
                return get_tad_model_names_from_table()
            elif os.path.isfile(ft_str):
                # Use custom table path
                return get_tad_model_names_from_table(fin=ft_str)
    # Use directory listing
    return get_tad_model_names(base_dir=base_dir)

# @param model_filt_id - Model filter identifier in tad_filt.R
# See 'tad_filt.R' and 'tad_filt_export.R'
def get_tad_model_filt_ex_list_path(model_filt_id):
    '''Path to pre-filtered TAD model names'''
    fname = '.'.join(['tad', model_filt_id, 'csv'])
    fdir = 'tad_filt_ex'
    fpath = os.path.join(OUT_BASE_DIR, fdir, fname)
    return fpath

# @param model_filt_id - Model filter identifier in tad_filt.R
# See 'tad_filt.R' and 'tad_filt_export.R'
def load_tad_model_filt_ex_list(model_filt_id):
    '''Load list of pre-filtered TAD model names'''
    if not model_filt_id is None:
        fpath = get_tad_model_filt_ex_list_path(model_filt_id=model_filt_id)
        if os.path.isfile(fpath):
            out = []
            with open(fpath, 'r') as f:
                for line in f:
                    line = line.strip()
                    # Skip empty lines
                    if not line:
                        continue
                    # Skip comments
                    if line.startswith("#"):
                        continue
                    out.append(line)
            return(out)
    return None

def resolve_tad_model_names_filt_ex(model_filt_id='',
                                    base_dir=TAD_BASE_OUT_DIR):
    '''Pre-filtered TAD model regions for use in Python existing on disk'''
    # Get models from disk
    disk_names = get_tad_model_names(base_dir=base_dir)
    # Check if filtering
    filt_names = load_tad_model_filt_ex_list(model_filt_id)
    if filt_names:
        disk_names = list(set(disk_names) & set(filt_names))
    return disk_names

# @param model_name - Model identifier
# @param base_dir - Base directory for storing generated models
def get_tad_config_dir(model_name, base_dir=TAD_BASE_OUT_DIR):
    '''Return TAD model configuration directory'''
    return os.path.join(get_tad_model_dir(model_name=model_name,
                                          base_dir=base_dir), 'config')

# @param model_name - Model identifier
# @param base_dir - Base directory for storing generated models
def get_tad_folder_arch_ini_path(model_name, base_dir=TAD_BASE_OUT_DIR):
    '''Return path to TAD arch INI config for polymer folding'''
    return os.path.join(get_tad_config_dir(model_name=model_name,
                                           base_dir=base_dir),
                                           'folder.arch.ini')

# @param model_name - Model identifier
# @param base_dir - Base directory for storing generated models
def get_hic_raw_dir(model_name, base_dir=TAD_BASE_OUT_DIR):
    '''Return directory for storing Hi-C raw data'''
    model_dir = get_tad_model_dir(model_name=model_name, base_dir=base_dir)
    return os.path.join(model_dir, "hic.raw")

# @param norm - Hi-C normalization
# @param mapq - Hi-C read mapping quality
# @param bin_bp - Hi-C bin resolution
def get_hic_raw_file(norm=HIC_NORM, mapq=HIC_MAPQ, bin_bp=HIC_BIN_BP):
    '''Return file name of Hi-C raw data'''
    return '.'.join(['hic', norm, mapq, str(bin_bp), 'txt'])

# @param model_name - Model identifier
# @param base_dir - Base directory for storing generated models
# @param norm - Hi-C normalization
# @param mapq - Hi-C mapping quality
# @param bin_bp - Hi-C bin resolution
def get_hic_raw_path(model_name, base_dir=TAD_BASE_OUT_DIR, norm=HIC_NORM,
                     mapq=HIC_MAPQ, bin_bp=HIC_BIN_BP):
    '''Return file path of Hi-C raw data'''
    raw_dir = get_hic_raw_dir(model_name=model_name, base_dir=base_dir)
    raw_file = get_hic_raw_file(norm=norm, mapq=mapq, bin_bp=bin_bp)
    return os.path.join(raw_dir, raw_file)

# @param model_name - Model identifier
# @param base_dir - Base directory for storing generated models
def get_hic_clean_dir(model_name, base_dir=TAD_BASE_OUT_DIR):
    '''Return directory for storing Hi-C cleaned data'''
    model_dir = get_tad_model_dir(model_name=model_name, base_dir=base_dir)
    # Note the '1' suffix is because indexing is 1-based
    return os.path.join(model_dir, "hic.clean.1")

# @param ext_no_dot - File extension without dot prefix
def get_hic_clean_file(ext_no_dot="csv.gz"):
    '''Return file name of Hi-C cleaned data'''
    return '.'.join(["hic", "clean", ext_no_dot])

# @param model_name - Model identifier
# @param base_dir - Base directory for storing generated models
# @param ext_no_dot - File extension without dot prefix
def get_hic_clean_path(model_name, base_dir=TAD_BASE_OUT_DIR,
                       ext_no_dot="csv.gz"):
    '''Return path to cleaned Hi-C data'''
    clean_dir = get_hic_clean_dir(model_name=model_name, base_dir=base_dir)
    fid = get_hic_clean_file(ext_no_dot=ext_no_dot)
    return os.path.join(clean_dir, fid)

# @param model_name - Model identifier
# @param base_dir - Base directory for storing generated models
def get_hic_clust_dir(model_name, base_dir=TAD_BASE_OUT_DIR):
    '''Return directory for storing Hi-C specific contact clustering data'''
    model_dir = get_tad_model_dir(model_name=model_name, base_dir=base_dir)
    # Note the '0' suffix is because indexing is 0-based
    return os.path.join(model_dir, "hic.clust.0")

# @param model_name - Model identifier
# @param base_dir - Base directory for storing generated models
def get_hic_APResult_path(model_name, base_dir=TAD_BASE_OUT_DIR):
    '''Return path to binary Hi-C affinity propagation clustering data'''
    clust_dir = get_hic_clust_dir(model_name=model_name, base_dir=base_dir)
    fid = "apres.rdata"
    return os.path.join(clust_dir, fid)

# @param model_name - Model identifier
# @param base_dir - Base directory for storing generated models
def get_hic_clust_path(model_name, base_dir=TAD_BASE_OUT_DIR,
                       ext_no_dot="csv.gz"):
    '''Return path to Hi-C clustering data.frame for specific contacts'''
    clust_dir = get_hic_clust_dir(model_name=model_name, base_dir=base_dir)
    fid = '.'.join(["hic", "clust", ext_no_dot])
    return os.path.join(clust_dir, fid)

def get_tad_clust_sum_path():
    '''Return path to clustering summary over all tad regions'''
    return os.path.join(OUT_BASE_DIR, "tad.clust.sum",
                        "tad.clust.sum.csv")

# @param model_name - Model identifier
# @param base_dir - Base directory for storing generated models
def get_tad_intr_chr_path(model_name, base_dir=TAD_BASE_OUT_DIR):
    '''Return path to chromatin-chromatin interactions file'''
    model_dir = get_tad_model_dir(model_name=model_name, base_dir=base_dir)
    fid = INTR_CHR_FID
    return os.path.join(model_dir, fid)

# @param model_name - Model identifier
# @param base_dir - Base directory for storing generated models
def get_tad_infea_dir(model_name, base_dir=TAD_BASE_OUT_DIR):
    '''Return directory for infeasible chr-chr interaction analysis'''
    model_dir = get_tad_model_dir(model_name=model_name, base_dir=base_dir)
    return os.path.join(model_dir, "infea")

# @param model_name - Model identifier
# @param base_dir - Base directory for storing generated models
def get_tad_infea_corp_path(model_name, base_dir=TAD_BASE_OUT_DIR):
    '''Return path containing infeasible chr-chr interaction corpus'''
    infea_dir = get_tad_infea_dir(model_name=model_name, base_dir=base_dir)
    fid = "infea.csv"
    return os.path.join(infea_dir, fid)

# Load (i,j) tuples into dictionary { i: [], j: [] }
def parse_csv_pairs(fpath):
    """Returns list of interaction tuples"""
    pairs = { "i" : [], "j" : [] }
    with open(fpath, 'r') as f:
        for line in f:
            line = line.strip()
            # Skip empty lines
            if not line:
                continue
            # Skip comments
            if line.startswith("#"):
                continue
            # Split by comma
            ij = line.split(",")
            # Relevant (i,j) are assumed to be first two elements
            if len(ij) >= 2:
                i = int(ij[0].strip())
                j = int(ij[1].strip())
                pairs["i"].append(i)
                pairs["j"].append(j)
    return(pairs)

# @param frag_a_lo - First bead of interacting fragment A
# @param frag_a_hi - Last bead of interacting fragment A
# @param frag_b_lo - First bead of interacting fragment B
# @param frag_b_hi - Last bead of interacting fragment B
# @param perturb_base_dir - Base directory for storing perturbation data
def get_perturb_dir(frag_a_lo, frag_a_hi, frag_b_lo, frag_b_hi,
                    perturb_base_dir=PERTURB_BASE_DIR):
    """Returns specific perturbation subfolder"""
    frags = map(str, [frag_a_lo, frag_a_hi, frag_b_lo, frag_b_hi])
    perturb_id = '.'.join(frags)
    return os.path.join(perturb_base_dir, perturb_id)

# @param pertub_dir - Directory storing perturbation ensemble (e.g.
#   get_perturb_dir(...))
def get_perturb_logw_dir(perturb_dir):
    """Return perturbation subfolder with log weight files"""
    return os.path.join(perturb_dir, "log_weights")

# @param pertub_dir - Directory storing perturbation ensemble (e.g.
#   get_perturb_dir(...))
def get_perturb_ligc_csv_gz_dir(perturb_dir):
    """Return perturbation subfolder with [compressed] ligation contacts"""
    return os.path.join(perturb_dir, "ligc.csv.gz")

# @param pertub_dir - Directory storing perturbation ensemble (e.g.
#   get_perturb_dir(...))
def get_perturb_ligc_freqmat_csv_gz_dir(perturb_dir):
    """Return perturbation CSV GZ subfolder with ensemble contact frequencies"""
    return os.path.join(perturb_dir, "ligc.freqmat.csv.gz")

# @param rmvout - True for outlier removal, False o/w
# @param pertub_dir - Directory storing perturbation ensemble (e.g.
#   get_perturb_dir(...))
def get_perturb_ligc_freqmat_csv_gz_path(rmvout, perturb_dir):
    """Return perturbed ensemble contact frequency matrix CSV GZ path"""
    data_dir = get_perturb_ligc_freqmat_csv_gz_dir(perturb_dir=perturb_dir)
    return get_ligc_path_util(prefix='ligc.freqmat',
                              ext_no_dot='csv.gz',
                              data_dir=data_dir,
                              rmvout=rmvout)

# @param pertub_dir - Directory storing perturbation ensemble (e.g.
#   get_perturb_dir(...))
def get_perturb_ligc_freqmat_rdata_dir(perturb_dir):
    """Return perturbation RData subfolder with ensemble contact frequencies"""
    return os.path.join(perturb_dir, "ligc.freqmat.rdata")

# @param rmvout - True for outlier removal, False o/w
# @param pertub_dir - Directory storing perturbation ensemble (e.g.
#   get_perturb_dir(...))
def get_perturb_ligc_freqmat_rdata_path(rmvout, perturb_dir):
    """Return perturbed ensemble contact frequency matrix RData path"""
    data_dir = get_perturb_ligc_freqmat_rdata_dir(perturb_dir=perturb_dir)
    return get_ligc_path_util(prefix='ligc.freqmat',
                              ext_no_dot='rdata',
                              data_dir=data_dir,
                              rmvout=rmvout)

# @param model_name - Model identifier
# @param base_dir - Base directory for storing generated models
def get_tad_perturb_pvals_dir(model_name, base_dir=TAD_BASE_OUT_DIR):
    """Return directory to store TAD model specific perturbation p-values"""
    model_dir = get_tad_model_dir(model_name=model_name, base_dir=base_dir)
    return os.path.join(model_dir, "perturb.pvals")

# @param model_name - Model identifier
# @param b_all - True: All specific, False: Exemplar specific contacts
# @param ext_no_dot - File extension without dot prefix
# @param base_dir - Base directory for storing generated models
def get_tad_perturb_pvals_path(model_name, b_all, ext_no_dot,
                               base_dir = TAD_BASE_OUT_DIR):
    '''Return path to store TAD model specifc perturbation p-values'''
    pv_dir = get_tad_perturb_pvals_dir(model_name=model_name,
                                       base_dir=base_dir)
    desc = "all" if b_all else "ex"
    pv_file = '.'.join(["pvals", desc, ext_no_dot])
    return os.path.join(pv_dir, pv_file)

# @param model_name - Model identifier
# @param base_dir - Base directory for storing generated models
def get_cmx_edge_ro_path(model_name, base_dir=TAD_BASE_OUT_DIR):
    '''Return file path for CMX read only allowed edges'''
    model_dir = get_tad_model_dir(model_name=model_name, base_dir=base_dir)
    return os.path.join(model_dir, "cmx.edge.ro.csv")

# @param model_name - Model identifier
# @param base_dir - Base directory for storing generated models
def get_cmx_hic_path(model_name, base_dir=TAD_BASE_OUT_DIR):
    '''Return path to CMX target Hi-C frequencies'''
    model_dir = get_tad_model_dir(model_name=model_name, base_dir=base_dir)
    return os.path.join(model_dir, "cmx.hic.csv")

# @param pop_key - Model population key
def get_cmx_ini_path(pop_key=CMX_POP_KEY):
    '''Return CMX top-level configuraiton INI path'''
    ini_dir = MODULE_COMMON_CONFIG_DIR
    ini_file = '.'.join(['cmx', pop_key, 'ini'])
    return os.path.join(ini_dir, ini_file)

# @param pop_key - Model population key
def get_cmx_app_key(pop_key=CMX_POP_KEY):
    '''CMX application key for population type'''
    app_key = '_'.join(['app', pop_key, 'canon'])
    return app_key

# @param model_name - Model identifier
# @param pop_key - Model population key
# @param tag - Optional suffix string for CMX results folder
# @WARNING: If tag is valid, then it will be prefixed with a '.'
# @param base_dir - Base directory for storing generated models
def get_cmx_results_dir(model_name, pop_key=CMX_POP_KEY, tag=CMX_TAG,
                        base_dir = TAD_BASE_OUT_DIR):
    '''TAD locus subdirectory for storing CMX chain runs'''
    model_dir = get_tad_model_dir(model_name=model_name, base_dir=base_dir)
    res_subd = '.'.join(['cmx', pop_key])
    if tag:
        res_subd = res_subd + '.' + tag
    return os.path.join(model_dir, res_subd)

# @param model_name - Model identifier
# @param base_dir - Base directory for storing generated models
def get_b2p_intr_chr_dir(model_name, base_dir=TAD_BASE_OUT_DIR):
    '''Return cluster sampled chromatin-chromatin interactions directory'''
    model_dir = get_tad_model_dir(model_name=model_name, base_dir=base_dir)
    subd = "intr.chr.0.b2p"
    return os.path.join(model_dir, subd)

# @param ko_rand - If True: KO ignored (random folding), If False: KO enforced
# @param tag - Optional suffix string, may be None or empty string. Note, tag
#   is assumed to be string or None, unexpected behavior may occur if tag is
#   is not properly type. For instance, if tag is integer 0, then this will be
#   evaluate to a False expression and will not be appended to subdir.
# @WARNING: If tag is valid, then it will be prefixed with a '.'
def get_b2p_geom_subdir(ko_rand=B2P_KO_RAND, tag=B2P_TAG):
    """Return name of bit2pol subfolder for storing raw geometry"""
    subd = "geom.kor" + str(int(ko_rand))
    if tag:
        subd = subd + '.' + tag
    return subd

# @param model_name - Model identifier
# @param ko_rand - If True: KO ignored (random folding), If False: KO enforced
# @param b2p_tag - Optional bit2pol suffix string, see get_b2p_geom_subdir()
# @param pop_key - Model population key
# @param cmx_tag - Optional CMX suffix string, see get_cmx_results_dir()
# @param base_dir - Base directory for storing generated models
def get_b2p_geom_dirs(model_name, ko_rand=B2P_KO_RAND, b2p_tag=B2P_TAG,
                      pop_key=CMX_POP_KEY, cmx_tag=CMX_TAG,
                      base_dir=TAD_BASE_OUT_DIR):
    '''Return all directory paths containing bit2pol folding geometry'''
    cmx_results_dir = get_cmx_results_dir(model_name=model_name,
                                          pop_key=pop_key, tag=cmx_tag,
                                          base_dir=base_dir)
    geom_subdir = get_b2p_geom_subdir(ko_rand=ko_rand, tag=b2p_tag)
    out = []
    for dirpath, dirnames, filenames in os.walk(cmx_results_dir):
        # Check for existing bit2pol folding geometry
        if  geom_subdir in dirnames:
            geom_dirpath = os.path.join(dirpath, geom_subdir)
            out.append(geom_dirpath)
    return out

# @param model_name - Model identifier
# @param ko_rand - If True: KO ignored (random folding), If False: KO enforced
# @param b2p_tag - Optional suffix string, see get_b2p_geom_subdir()
# @param pop_key - Model population key
# @param cmx_tag - Optional CMX suffix string, see get_cmx_results_dir()
# @param base_dir - Base directory for storing generated models
def get_b2p_geom_topdirs(model_name, ko_rand=B2P_KO_RAND, b2p_tag=B2P_TAG,
                         pop_key=CMX_POP_KEY, cmx_tag=CMX_TAG,
                         base_dir=TAD_BASE_OUT_DIR):
    '''Return top-level directory paths containing bit2pol folding geometry'''
    geom_dirs = get_b2p_geom_dirs(model_name=model_name, ko_rand=ko_rand,
                                  b2p_tag=b2p_tag, pop_key=pop_key,
                                  cmx_tag=cmx_tag, base_dir=base_dir)
    if len(geom_dirs) < 1:
        return []
    depth = []
    for gd in geom_dirs:
        depth.append(gd.count(os.sep))
    min_depth = min(depth)
    out = []
    for i, d in enumerate(depth):
        if min_depth == d:
            out.append(geom_dirs[i])
    return out

# @param geom_dir - Path to bit2pol base geometry folder
# @param b_crop - If TRUE, data is assumed cropped to match TAD region
# @param prefix - Data prefix
# @param ext_no_dot - Data file extension without leading dot
def get_b2p_geom_data_dir_util(geom_dir, b_crop, prefix, ext_no_dot):
    if b_crop:
        return os.path.join(geom_dir, '.'.join([prefix, 'crop', ext_no_dot]))
    return os.path.join(geom_dir, '.'.join([prefix, ext_no_dot]))

# @param geom_dir - Path to bit2pol base geometry folder
# @param b_crop - If TRUE, data is assumed cropped to match TAD region
def get_b2p_geom_coord_csv_dir(geom_dir, b_crop=B2P_CROP):
    return get_b2p_geom_data_dir_util(geom_dir=geom_dir, b_crop=b_crop,
                                      prefix="csv", ext_no_dot="gz")

# @param geom_dir - Path to bit2pol base geometry folder
# @param b_crop - If TRUE, data is assumed cropped to match TAD region
def get_b2p_geom_coord_rdata_dir(geom_dir, b_crop=B2P_CROP):
    return get_b2p_geom_data_dir_util(geom_dir=geom_dir, b_crop=b_crop,
                                      prefix="coord", ext_no_dot="rdata")

# @param geom_dir - Path to bit2pol base geometry folder
# @param b_crop - If TRUE, data is assumed cropped to match TAD region
def get_b2p_geom_coord_merge_rdata_dir(geom_dir, b_crop=B2P_CROP):
    return get_b2p_geom_data_dir_util(geom_dir=geom_dir, b_crop=b_crop,
                                      prefix="coord.merge", ext_no_dot="rdata")

# @param geom_dir - Path to bit2pol base geometry folder
# @param b_crop - If TRUE, data is assumed cropped to match TAD region
# @param rmvout - If TRUE, samples with outlier weights assumed removed
def get_b2p_geom_coord_merge_rdata_path(geom_dir, b_crop=B2P_CROP,
                                        rmvout=B2P_REMOVE_OUTLIERS):
    '''Path to geometry coordinates merge data'''
    data_dir = get_b2p_geom_coord_merge_rdata_dir(geom_dir=geom_dir,
                                                  b_crop=b_crop)
    fname = '.'.join(['coord.merge', 'rmvout', str(int(rmvout)), 'rdata'])
    return os.path.join(data_dir, fname)

# @param geom_dir - Path to bit2pol base geometry folder
# @param b_crop - If TRUE, data is assumed cropped to match TAD region
def get_b2p_geom_dist_merge_rdata_dir(geom_dir, b_crop=B2P_CROP):
    return get_b2p_geom_data_dir_util(geom_dir=geom_dir, b_crop=b_crop,
                                      prefix="dist.merge", ext_no_dot="rdata")
                                      
# @param geom_dir - Path to bit2pol base geometry folder
# @param b_crop - If TRUE, data is assumed cropped to match TAD region
# @param rmvout - If TRUE, samples with outlier weights assumed removed
def get_b2p_geom_dist_merge_rdata_path(geom_dir, b_crop=B2P_CROP,
                                       rmvout=B2P_REMOVE_OUTLIERS):
    '''Path to geometry distances merge data'''
    data_dir = get_b2p_geom_dist_merge_rdata_dir(geom_dir=geom_dir,
                                                 b_crop=b_crop)
    fname = '.'.join(['dist.merge', 'rmvout', str(int(rmvout)), 'rdata'])
    return os.path.join(data_dir, fname)

# @param geom_dir - Path to bit2pol base geometry folder
# @param b_crop - If TRUE, data is assumed cropped to match TAD region
def get_b2p_geom_dist_mean_rdata_dir(geom_dir, b_crop=B2P_CROP):
    return get_b2p_geom_data_dir_util(geom_dir=geom_dir, b_crop=b_crop,
                                      prefix="dist.mean", ext_no_dot="rdata")
                                      
# @param geom_dir - Path to bit2pol base geometry folder
# @param eqw - If TRUE, assume equal weights (empirical matches target)
# @param b_crop - If TRUE, data is assumed cropped to match TAD region
# @param rmvout - If TRUE, samples with outlier weights assumed removed
def get_b2p_geom_dist_mean_rdata_path(geom_dir, eqw=B2P_EQW, b_crop=B2P_CROP,
                                      rmvout=B2P_REMOVE_OUTLIERS):
    '''Path to geometry distances mean data'''
    data_dir = get_b2p_geom_dist_mean_rdata_dir(geom_dir=geom_dir,
                                                b_crop=b_crop)
    fname = '.'.join(['dist.mean', 'rmvout', str(int(rmvout))])
    if eqw:
        fname = fname + '.eqw'
    fname = fname + '.rdata'
    return os.path.join(data_dir, fname)

# @param geom_dir - Path to bit2pol base geometry folder
# @param b_crop - If TRUE, data is assumed cropped to match TAD region
def get_b2p_geom_dist_mean_csv_gz_dir(geom_dir, b_crop=B2P_CROP):
    return get_b2p_geom_data_dir_util(geom_dir=geom_dir, b_crop=b_crop,
                                      prefix="dist.mean", ext_no_dot="csv.gz")
                                      
# @param geom_dir - Path to bit2pol base geometry folder
# @param eqw - If TRUE, assume equal weights (empirical matches target)
# @param b_crop - If TRUE, data is assumed cropped to match TAD region
# @param rmvout - If TRUE, samples with outlier weights assumed removed
def get_b2p_geom_dist_mean_csv_gz_path(geom_dir, eqw=B2P_EQW, b_crop=B2P_CROP,
                                    rmvout=B2P_REMOVE_OUTLIERS):
    '''Path to geometry distances mean data'''
    data_dir = get_b2p_geom_dist_mean_csv_gz_dir(geom_dir=geom_dir,
                                              b_crop=b_crop)
    fname = '.'.join(['dist.mean', 'rmvout', str(int(rmvout))])
    if eqw:
        fname = fname + '.eqw'
    fname = fname + '.csv.gz'
    return os.path.join(data_dir, fname)

# @param geom_dir - Path to bit2pol base geometry folder
# @param b_crop - If TRUE, data is assumed cropped to match TAD region
def get_b2p_geom_dist_cor_sc_rdata_dir(geom_dir, b_crop=B2P_CROP):
    return get_b2p_geom_data_dir_util(geom_dir=geom_dir, b_crop=b_crop,
                                      prefix="dist.cor_sc",
                                      ext_no_dot="rdata")
                                      
# @param geom_dir - Path to bit2pol base geometry folder
# @param eqw - If TRUE, assume equal weights (empirical matches target)
# @param b_crop - If TRUE, data is assumed cropped to match TAD region
# @param rmvout - If TRUE, samples with outlier weights assumed removed
def get_b2p_geom_dist_cor_sc_rdata_path(geom_dir, eqw=B2P_EQW, b_crop=B2P_CROP,
                                        rmvout=B2P_REMOVE_OUTLIERS):
    '''Path to geometry distances single-cell correlation data'''
    data_dir = get_b2p_geom_dist_cor_sc_rdata_dir(geom_dir=geom_dir,
                                                  b_crop=b_crop)
    fname = '.'.join(['dist.cor_sc', 'rmvout', str(int(rmvout))])
    if eqw:
        fname = fname + '.eqw'
    fname = fname + '.rdata'
    return os.path.join(data_dir, fname)

# @param geom_dir - Path to bit2pol base geometry folder
# @param b_crop - If TRUE, data is assumed cropped to match TAD region
def get_b2p_geom_dist_cor_sc_csv_gz_dir(geom_dir, b_crop=B2P_CROP):
    return get_b2p_geom_data_dir_util(geom_dir=geom_dir, b_crop=b_crop,
                                      prefix="dist.cor_sc",
                                      ext_no_dot="csv.gz")
                                      
# @param geom_dir - Path to bit2pol base geometry folder
# @param eqw - If TRUE, assume equal weights (empirical matches target)
# @param b_crop - If TRUE, data is assumed cropped to match TAD region
# @param rmvout - If TRUE, samples with outlier weights assumed removed
def get_b2p_geom_dist_cor_sc_csv_gz_path(geom_dir, eqw=B2P_EQW, b_crop=B2P_CROP,
                                         rmvout=B2P_REMOVE_OUTLIERS):
    '''Path to geometry distances single-cell correlation data'''
    data_dir = get_b2p_geom_dist_cor_sc_csv_gz_dir(geom_dir=geom_dir,
                                                   b_crop=b_crop)
    fname = '.'.join(['dist.cor_sc', 'rmvout', str(int(rmvout))])
    if eqw:
        fname = fname + '.eqw'
    fname = fname + '.csv.gz'
    return os.path.join(data_dir, fname)

# @param geom_dir - Path to bit2pol base geometry folder
# @param b_crop - If TRUE, data is assumed cropped to match TAD region
def get_b2p_geom_dist_bound_rdata_dir(geom_dir, b_crop=B2P_CROP):
    return get_b2p_geom_data_dir_util(geom_dir=geom_dir, b_crop=b_crop,
                                      prefix="dist.bound",
                                      ext_no_dot="rdata")
                                      
# @param geom_dir - Path to bit2pol base geometry folder
# @param eqw - If TRUE, assume equal weights (empirical matches target)
# @param b_crop - If TRUE, data is assumed cropped to match TAD region
# @param rmvout - If TRUE, samples with outlier weights assumed removed
def get_b2p_geom_dist_bound_rdata_path(geom_dir, eqw=B2P_EQW, b_crop=B2P_CROP,
                                       rmvout=B2P_REMOVE_OUTLIERS):
    '''Path to geometry distances boundary data'''
    data_dir = get_b2p_geom_dist_bound_rdata_dir(geom_dir=geom_dir,
                                                 b_crop=b_crop)
    fname = '.'.join(['dist.bound', 'rmvout', str(int(rmvout))])
    if eqw:
        fname = fname + '.eqw'
    fname = fname + '.rdata'
    return os.path.join(data_dir, fname)

# @param geom_dir - Path to bit2pol base geometry folder
# @param b_crop - If TRUE, data is assumed cropped to match TAD region
def get_b2p_geom_dist_bound_csv_gz_dir(geom_dir, b_crop=B2P_CROP):
    return get_b2p_geom_data_dir_util(geom_dir=geom_dir, b_crop=b_crop,
                                      prefix="dist.bound",
                                      ext_no_dot="csv.gz")
                                      
# @param geom_dir - Path to bit2pol base geometry folder
# @param eqw - If TRUE, assume equal weights (empirical matches target)
# @param b_crop - If TRUE, data is assumed cropped to match TAD region
# @param rmvout - If TRUE, samples with outlier weights assumed removed
def get_b2p_geom_dist_bound_csv_gz_path(geom_dir, eqw=B2P_EQW, b_crop=B2P_CROP,
                                         rmvout=B2P_REMOVE_OUTLIERS):
    '''Path to geometry distances boundary data'''
    data_dir = get_b2p_geom_dist_bound_csv_gz_dir(geom_dir=geom_dir,
                                                  b_crop=b_crop)
    fname = '.'.join(['dist.bound', 'rmvout', str(int(rmvout))])
    if eqw:
        fname = fname + '.eqw'
    fname = fname + '.csv.gz'
    return os.path.join(data_dir, fname)

# @param geom_dir - Path to bit2pol base geometry folder
# @param b_crop - If TRUE, data is assumed cropped to match TAD region
def get_b2p_geom_ligc_csv_gz_dir(geom_dir, b_crop=B2P_CROP):
    return get_b2p_geom_data_dir_util(geom_dir=geom_dir, b_crop=b_crop,
                                      prefix="ligc", ext_no_dot="csv.gz")

# @param geom_dir - Path to bit2pol base geometry folder
# @param b_crop - If TRUE, data is assumed cropped to match TAD region
def get_b2p_geom_ligc_rdata_dir(geom_dir, b_crop=B2P_CROP):
    return get_b2p_geom_data_dir_util(geom_dir=geom_dir, b_crop=b_crop,
                                      prefix="ligc", ext_no_dot="rdata")

# @param geom_dir - Path to bit2pol base geometry folder
# @param b_crop - If TRUE, data is assumed cropped to match TAD region
def get_b2p_geom_ligc_merge_rdata_dir(geom_dir, b_crop=B2P_CROP):
    return get_b2p_geom_data_dir_util(geom_dir=geom_dir, b_crop=b_crop,
                                      prefix="ligc.merge", ext_no_dot="rdata")

# @param geom_dir - Path to bit2pol base geometry folder
# @param b_crop - If TRUE, data is assumed cropped to match TAD region
# @param rmvout - If TRUE, ligc samples with outlier weights assumed removed
def get_b2p_geom_ligc_merge_rdata_path(geom_dir, b_crop=B2P_CROP,
                                       rmvout=B2P_REMOVE_OUTLIERS):
    '''Path to ligc merge data'''
    data_dir = get_b2p_geom_ligc_merge_rdata_dir(geom_dir=geom_dir,
                                                 b_crop=b_crop)
    return get_null_ligc_merge_rdata_path(rmvout=rmvout, data_dir=data_dir)

# @param geom_dir - Path to bit2pol base geometry folder
# @param b_crop - If TRUE, data is assumed cropped to match TAD region
def get_b2p_geom_freqmat_csv_gz_dir(geom_dir, b_crop=B2P_CROP):
    return get_b2p_geom_data_dir_util(geom_dir=geom_dir, b_crop=b_crop,
                                      prefix="ligc.freqmat", ext_no_dot="csv.gz")

# @param geom_dir - Path to bit2pol base geometry folder
# @param b_crop - If TRUE, data is assumed cropped to match TAD region
def get_b2p_geom_freqmat_rdata_dir(geom_dir, b_crop=B2P_CROP):
    return get_b2p_geom_data_dir_util(geom_dir=geom_dir, b_crop=b_crop,
                                      prefix="ligc.freqmat", ext_no_dot="rdata")

# @param geom_dir - Path to bit2pol base geometry folder
# @param eqw - If TRUE, assume empirical distribution is target
# @param b_crop - If TRUE, data is assumed cropped to match TAD region
# @param rmvout - If TRUE, ligc samples with outlier weights assumed removed
def get_b2p_geom_freqmat_csv_gz_path(geom_dir, eqw=B2P_EQW, b_crop=B2P_CROP,
                                     rmvout=B2P_REMOVE_OUTLIERS):
    data_dir = get_b2p_geom_freqmat_csv_gz_dir(geom_dir=geom_dir,
                                               b_crop=b_crop)
    prefix = "ligc.freqmat"
    if eqw:
        prefix = prefix + ".eqw"
    return get_ligc_path_util(prefix=prefix,
                              ext_no_dot='csv.gz',
                              data_dir=data_dir,
                              rmvout=rmvout)

# @param geom_dir - Path to bit2pol base geometry folder
# @param eqw - If TRUE, assume empirical distribution is target
# @param b_crop - If TRUE, data is assumed cropped to match TAD region
# @param rmvout - If TRUE, ligc samples with outlier weights assumed removed
def get_b2p_geom_freqmat_rdata_path(geom_dir, eqw=B2P_EQW, b_crop=B2P_CROP,
                                    rmvout=B2P_REMOVE_OUTLIERS):
    data_dir = get_b2p_geom_freqmat_rdata_dir(geom_dir=geom_dir,
                                              b_crop=b_crop)
    prefix = "ligc.freqmat"
    if eqw:
        prefix = prefix + ".eqw"
    return get_ligc_path_util(prefix=prefix,
                              ext_no_dot='rdata',
                              data_dir=data_dir,
                              rmvout=rmvout)

# Subdirectory name for storing many-body interaction (MBI) counts
# @param locus_dir - Base locus directory
# @param should_crop - Is input ligc ensemble cropped to locus region?
def get_mbi_counts_dir(locus_dir, should_crop = B2P_CROP):
    '''Many body interaction counts subdirectory'''
    d = os.path.join(locus_dir, "mbi.counts")
    if should_crop:
        d = d + '.crop'
    return d

# @param locus_dir - Base locus directory
# @param rmvout - Did input ligc have outliers removed?
# @param should_crop - Is input ligc ensemble cropped to locus region?
# @param edge_min_gd - |j-i| tuples < edge_min_gd are filtered
# @param clq_min - Minimum clique size
# @param clq_max - Maximum clique size
# @param clq_map_id - Clique mapping method identifier
# @param clq_type - Clique calling algorithm
# @param mbi_tag - Miscellaneous tag suffix
def get_mbi_counts_path(locus_dir,
                        rmvout,
                        should_crop = B2P_CROP,
                        edge_min_gd = MBI_EDGE_MIN_GD,
                        clq_min = MBI_CLQ_MIN,
                        clq_max = MBI_CLQ_MAX,
                        clq_map_id = MBI_CLQ_MAP_ID,
                        clq_type = MBI_CLQ_TYPE,
                        mbi_tag = MBI_TAG):
    '''Path to many body interaction counts data'''
    if isinstance(clq_max, (int, float)):
        clq_max = int(clq_max)
    props = ['rmvo', str(int(rmvout)),
             'edgd', str(int(edge_min_gd)),
             'cqmn', str(int(clq_min)),
             'cqmx', str(clq_max),
             'cqmp', str(clq_map_id)]
    # HACK - only append type if it's not 'max' to avoid having to rename
    #   previously generated data (implicitly assume 'max' if missing)
    if clq_type != 'max':
        props.extend(['cqty', clq_type])
    if mbi_tag:
        props.append(mbi_tag)
    props.append('rdata')
    fid = '.'.join(props)
    d = get_mbi_counts_dir(locus_dir=locus_dir,
                           should_crop=should_crop)
    return os.path.join(d, fid)

# Subdirectory name for storing many-body interaction (MBI) bootstrap
# @param locus_dir - Base locus directory
# @param should_crop - Is input ligc ensemble cropped to locus region?
def get_mbi_boot_dir(locus_dir, should_crop = B2P_CROP):
    '''Many body interaction bootstrap subdirectory'''
    d = os.path.join(locus_dir, "mbi.boot")
    if should_crop:
        d = d + '.crop'
    return d

# @param locus_dir - Base locus directory
# @param stat_id - Bootstrap statistic identifier
# @param reps - Number of boostrap replicates
# @param rmvout - Did input ligc have outliers removed?
# @param should_crop - Is input ligc ensemble cropped to locus region?
# @param edge_min_gd - |j-i| tuples < edge_min_gd are filtered
# @param clq_min - Minimum clique size
# @param clq_max - Maximum clique size
# @param clq_map_id - Clique mapping method identifier
# @param clq_type - Clique calling algorithm
# @param mbi_tag - Miscellaneous tag suffix
def get_mbi_boot_path(locus_dir,
                      stat_id = MBI_BOOT_STAT_ID,
                      reps = MBI_BOOT_REPS,
                      rmvout = NULL_REMOVE_OUTLIERS,
                      should_crop = B2P_CROP,
                      edge_min_gd = MBI_EDGE_MIN_GD,
                      clq_min = MBI_CLQ_MIN,
                      clq_max = MBI_CLQ_MAX,
                      clq_map_id = MBI_CLQ_MAP_ID,
                      clq_type = MBI_CLQ_TYPE,
                      mbi_tag = MBI_TAG):
    '''Path to many body interactions bootstrap data'''
    if isinstance(clq_max, (int, float)):
        clq_max = int(clq_max)
    props = ['btst', str(stat_id),
             'btrp', str(int(reps)),
             'rmvo', str(int(rmvout)),
             'edgd', str(int(edge_min_gd)),
             'cqmn', str(int(clq_min)),
             'cqmx', str(clq_max),
             'cqmp', str(clq_map_id)]
    # HACK - only append type if it's not 'max' to avoid having to rename
    #   previously generated data (implicitly assume 'max' if missing)
    if clq_type != 'max':
        props.extend(['cqty', clq_type])
    if mbi_tag:
        props.append(mbi_tag)
    props.append('rdata')
    fid = '.'.join(props)
    d = get_mbi_boot_dir(locus_dir=locus_dir,
                         should_crop=should_crop)
    return os.path.join(d, fid)

# Subdirectory name for storing many-body interaction (MBI) p-values
# @param locus_dir - Base locus directory
# @param should_crop - Is input ligc ensemble cropped to locus region?
def get_mbi_pvals_dir(locus_dir, should_crop = B2P_CROP):
    '''Many body interaction p-values subdirectory'''
    d = os.path.join(locus_dir, "mbi.pvals")
    if should_crop:
        d = d + '.crop'
    return d

# @param locus_dir - Base locus directory
# @param stat_id - Bootstrap statistic identifier
# @param reps - Number of boostrap replicates
# @param null_rmvout - Did random ligc have outliers removed?
# @param should_crop - Are ligc ensembles cropped to locus region?
# @param b2p_rmvout - Did b2p likgc have outliers removed?
# @param b2p_eqw - Is b2p assumed target?
# @param edge_min_gd - |j-i| tuples < edge_min_gd are filtered
# @param clq_min - Minimum clique size
# @param clq_max - Maximum clique size
# @param clq_map_id - Clique mapping method identifier
# @param clq_type - Clique calling algorithm
# @parma mbi_pvtest_by_id - P-value test stratification
# @param mbi_tag - Miscellaneous tag suffix
# @param ext_no_dot - File extension sans dot prefix
def get_mbi_pvals_path(locus_dir,
                       stat_id = MBI_BOOT_STAT_ID,
                       reps = MBI_BOOT_REPS,
                       null_rmvout = NULL_REMOVE_OUTLIERS,
                       should_crop = B2P_CROP,
                       b2p_rmvout = B2P_REMOVE_OUTLIERS,
                       b2p_eqw = B2P_EQW,
                       edge_min_gd = MBI_EDGE_MIN_GD,
                       clq_min = MBI_CLQ_MIN,
                       clq_max = MBI_CLQ_MAX,
                       clq_map_id = MBI_CLQ_MAP_ID,
                       clq_type = MBI_CLQ_TYPE,
                       mbi_pvtest_by_id = MBI_PVTEST_BY_ID,
                       mbi_tag = MBI_TAG,
                       ext_no_dot = 'rdata'):
    '''Path to many body interactions p-values data'''
    if isinstance(clq_max, (int, float)):
        clq_max = int(clq_max)
    props = ['btst', str(stat_id),
             'btrp', str(int(reps)),
             'nrvo', str(int(null_rmvout)),
             'brvo', str(int(b2p_rmvout)),
             'beqw', str(int(b2p_eqw)),
             'edgd', str(int(edge_min_gd)),
             'cqmn', str(int(clq_min)),
             'cqmx', str(clq_max),
             'cqmp', str(clq_map_id)]
    # HACK - only append type if it's not 'max' to avoid having to rename
    #   previously generated data (implicitly assume 'max' if missing)
    if clq_type != 'max':
        props.extend(['cqty', clq_type])
    # HACK - similarly, only append test stratification if it's not
    #   'body_maxgd' to avoid having to rename previously generated data
    #   (implicitly assume 'body_maxgd' if missing)
    if mbi_pvtest_by_id != 'body_maxgd':
        props.extend(['pvby', mbi_pvtest_by_id])
    if mbi_tag:
        props.append(mbi_tag)
    props.append(ext_no_dot)
    fid = '.'.join(props)
    d = get_mbi_pvals_dir(locus_dir=locus_dir,
                          should_crop=should_crop)
    return os.path.join(d, fid)

# @param min_body - Minimum body count
# @param max_body - Maximum body count
# @param min_loop - Minimum principal loop bead span
# @param max_loop - Maximum principal loop bead span
# @param fdr - False discovery rate threshold
# @param bin_id - Binning method identifier
# @param clq_map_id - Clique mapping method identifier
# @param ext_no_dot - File extension sans dot prefix
def get_bio_bin_mbi_counts_dir(min_body,
                               max_body,
                               min_loop,
                               max_loop,
                               fdr = BIO_BIN_MBI_FDR,
                               bin_id = BIO_BIN_MBI_ID,
                               clq_map_id = MBI_CLQ_MAP_ID,
                               ext_no_dot = 'rdata'):
    '''Directory for linear genome binned many body interaction counts'''
    subd = '.'.join([bin_id,
                     clq_map_id,
                     'mnbd',
                     str(int(min_body)),
                     'mxbd',
                     str(int(max_body)),
                     'mnlo',
                     str(int(min_loop)),
                     'mxlo',
                     str(int(max_loop)),
                     'fdr',
                     str(fdr),
                     ext_no_dot])
    return os.path.join(OUT_BASE_DIR, "bin.mbi", subd)

# @param model_name - Model identifier
# @param min_body - Minimum body count
# @param max_body - Maximum body count
# @param min_loop - Minimum principal loop bead span
# @param max_loop - Maximum principal loop bead span
# @param fdr - False discovery rate threshold
# @param bin_id - Binning method identifier
# @param clq_map_id - Clique mapping method identifier
# @param ext_no_dot - File extension sans dot prefix
def get_bio_bin_mbi_counts_path(model_name,
                                min_body,
                                max_body,
                                min_loop,
                                max_loop,
                                fdr = BIO_BIN_MBI_FDR,
                                bin_id = BIO_BIN_MBI_ID,
                                clq_map_id = MBI_CLQ_MAP_ID,
                                ext_no_dot = 'rdata'):
    '''File path to linear genome binned many body interaction counts'''
    d = get_bio_bin_mbi_counts_dir(min_body=min_body,
                                   max_body=max_body,
                                   min_loop=min_loop,
                                   max_loop=max_loop,
                                   fdr=fdr,
                                   bin_id=bin_id,
                                   clq_map_id=clq_map_id,
                                   ext_no_dot=ext_no_dot)
    fid = '.'.join([model_name, ext_no_dot])
    return os.path.join(d, fid)
