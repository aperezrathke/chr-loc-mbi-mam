###################################################################
# tad_core.R
#
# R wrapper around common configuration settings and utilities
###################################################################

# Guard against multiple sourcing of this file
if (!exists("SOURCED_tad_core")) {
  # For interfacing with python
  library(reticulate)
  
  # Cached directory to script
  # https://stackoverflow.com/questions/3452086/getting-path-of-an-r-script
  TAD_CORE_SCRIPT_DIR = getSrcDirectory(function(x) {
    x
  })
  
  # getSrcDirectory trick will not work via Rscript. If this is the
  # case, then TAD_CORE_SCRIPT_DIR will be length zero.
  # http://stackoverflow.com/questions/1815606/rscript-determine-path-of-the-executing-script
  if (length(TAD_CORE_SCRIPT_DIR) == 0) {
    get_script_dir <- function() {
      cmdArgs = commandArgs(trailingOnly = FALSE)
      needle = "--file="
      ixmatch = grep(needle, cmdArgs)
      if (length(match) > 0) {
        # Rscript
        return(dirname(normalizePath(sub(
          needle, "", cmdArgs[ixmatch]
        ))))
      }
      return(dirname(TAD_CORE_SCRIPT_DIR::current_filename()))
    }
    TAD_CORE_SCRIPT_DIR = get_script_dir()
  }
  
  # Common configuration - loads local python module "tad_core"
  tad_core = import_from_path(module = "tad_core", path = TAD_CORE_SCRIPT_DIR)
}

# Guard variable to prevent multiple sourcing
SOURCED_tad_core = TRUE
