#!/usr/bin/env Rscript

###################################################################
# tad_bit2pol_coord_shell.R
#
# Command-line interface for caching bit2pol coordinate data
###################################################################

# Similar to python optparse package
library(optparse)

###################################################################
# Path to this script
###################################################################

# Cached directory to script
# https://stackoverflow.com/questions/3452086/getting-path-of-an-r-script
TAD_BIT2POL_COORD_SHELL_SCRIPT_DIR = getSrcDirectory(function(x) {
  x
})

# getSrcDirectory trick will not work via Rscript. If this is the
# case, then TAD_BIT2POL_COORD_SHELL_SCRIPT_DIR will be length zero.
# http://stackoverflow.com/questions/1815606/rscript-determine-path-of-the-executing-script
if (length(TAD_BIT2POL_COORD_SHELL_SCRIPT_DIR) == 0) {
  get_script_dir <- function() {
    cmdArgs = commandArgs(trailingOnly = FALSE)
    needle = "--file="
    ixmatch = grep(needle, cmdArgs)
    if (length(match) > 0) {
      # Rscript
      return(dirname(normalizePath(sub(
        needle, "", cmdArgs[ixmatch]
      ))))
    }
    return(dirname(scriptName::current_filename()))
  }
  TAD_BIT2POL_COORD_SHELL_SCRIPT_DIR = get_script_dir()
}

# @return Path to directory containing this script
get_tad_bit2pol_coord_shell_script_dir <- function() {
  return(TAD_BIT2POL_COORD_SHELL_SCRIPT_DIR)
}

###################################################################
# Source utility script(s)
###################################################################

# Cache utilities
if (!exists("SOURCED_tad_bit2pol_coord")) {
  source(file.path(
    get_tad_bit2pol_coord_shell_script_dir(),
    "tad_bit2pol_coord.R"
  ))
}

###################################################################
# Parse command line
###################################################################

# https://www.r-bloggers.com/passing-arguments-to-an-r-script-from-command-lines/
option_list = list(
  # Utilities (in order encountered)
  make_option(
    c("--util_b2p_cache_coord_csv"),
    action = "store_true",
    default = FALSE,
    help = "Utility converts CSV formatted coordinates into binary RData"
  ),
  make_option(
    c("--util_b2p_cache_coord_merge"),
    action = "store_true",
    default = FALSE,
    help = "Utility combines all individual coordinate RData files into single RData file"
  ),
  # Options (in order encountered)
  # model_index0
  make_option(
    c("--model_index0"),
    type = "integer",
    default = NULL,
    help = "0-BASE MODEL INDEX - if valid, only process model at this index"
  ),
  # model_filt_id
  make_option(
    c("--model_filt_id"),
    type = "character",
    default = NULL,
    help = paste0(
      "Optional name of TAD model filter, ",
      paste0(TAD_MODEL_FILT_IDS, collapse = "|")
    )
  ),
  # from_table
  make_option(
    c("--from_table"),
    type = "integer",
    default = DEF_B2P_FROM_TABLE,
    help = "1: load data from scale table, 0: load from file system listing"
  ),
  # ko_rand
  make_option(
    c("--no-ko_rand"),
    action = "store_false",
    dest = "ko_rand",
    default = tad_core$B2P_KO_RAND,
    help = "Assume knock-outs enforced"
  ),
  make_option(
    c("--ko_rand"),
    action = "store_true",
    dest = "ko_rand",
    default = tad_core$B2P_KO_RAND,
    help = "Assume knock-outs not enforced (random)"
  ),
  # b2p tag
  make_option(
    c("--b2p_tag"),
    type = "character",
    default = tad_core$B2P_TAG,
    help = "Optional suffix for bit2pol geometry directories"
  ),
  # pop_key
  make_option(
    c("--pop_key"),
    type = "character",
    default = tad_core$B2P_POP_KEY,
    help = "Assumed CMX population model (spop|mpop)"
  ),
  # cmx tag
  make_option(
    c("--cmx_tag"),
    type = "character",
    default = tad_core$CMX_TAG,
    help = "Optional suffix for CMX results directories"
  ),
  # top_only
  make_option(
    c("--no-top_only"),
    action = "store_false",
    dest = "top_only",
    default = tad_core$B2P_TOP_ONLY,
    help = "Process all encountered bit2pol geometry folders"
  ),
  make_option(
    c("--top_only"),
    action = "store_true",
    dest = "top_only",
    default = tad_core$B2P_TOP_ONLY,
    help = "Process only top-level (lowest depth) bit2pol geometry folders"
  ),
  # base_dir
  make_option(
    c("--tad_base_dir"),
    type = "character",
    default = tad_core$TAD_BASE_OUT_DIR,
    help = "Base TAD loci directory"
  ),
  # should_crop
  make_option(
    c("--no-crop"),
    action = "store_false",
    dest = "crop",
    default = tad_core$B2P_CROP,
    help = "Data not assumed (nor will be) trimmed to TAD model region"
  ),
  make_option(
    c("--crop"),
    action = "store_true",
    dest = "crop",
    default = tad_core$B2P_CROP,
    help = "Data assumed (or will be) trimmed to TAD model region"
  ),
  # num_cpu
  make_option(
    c("--num_cpu"),
    type = "integer",
    default = DEF_NCPU,
    help = "Number of CPUs for parallel processing. If <= 0, then all CPUs are used!"
  ),
  # verbose
  make_option(
    c("--no-verbose"),
    action = "store_false",
    dest = "verbose",
    default = DEF_VERBOSE,
    help = "Disable verbose logging to stdout"
  ),
  make_option(
    c("--verbose"),
    action = "store_true",
    dest = "verbose",
    default = DEF_VERBOSE,
    help = "Enable verbose logging to stdout"
  ),
  # rmvout
  make_option(
    c("--no-rmvout"),
    action = "store_false",
    dest = "rmvout",
    default = tad_core$B2P_REMOVE_OUTLIERS,
    help = "Disable outlier removal"
  ),
  make_option(
    c("--rmvout"),
    action = "store_true",
    dest = "rmvout",
    default = tad_core$B2P_REMOVE_OUTLIERS,
    help = "Enable outlier removal"
  ),
  # check_merge_size
  make_option(
    c("--no-check_merge_size"),
    action = "store_false",
    dest = "check_merge_size",
    default = B2P_COORD_CHECK_MERGE_SIZE,
    help = "Merge will check for new ensemble RData files"
  ),
  make_option(
    c("--check_merge_size"),
    action = "store_true",
    dest = "check_merge_size",
    default = B2P_COORD_CHECK_MERGE_SIZE,
    help = "Merge will check for new ensemble RData files"
  ),
  # should_overwrite
  make_option(
    c("--no-overwrite"),
    action = "store_false",
    dest = "overwrite",
    default = DEF_SHOULD_OVERWRITE,
    help = "Disable overwrite of cached data"
  ),
  make_option(
    c("--overwrite"),
    action = "store_true",
    dest = "overwrite",
    default = DEF_SHOULD_OVERWRITE,
    help = "Enable overwrite of cached data"
  )
)

opt_parser = OptionParser(option_list = option_list)

opt = parse_args(opt_parser)

###################################################################
# Determine which utility to run
###################################################################

# Print command-line options
if (opt$verbose) {
  print("Options:")
  print(opt)
}

# Cache b2p coord csv -> rdata
if (opt$util_b2p_cache_coord_csv) {
  print("Running b2p_cache_coord_csv()")
  launch_b2p_cache_coord_csv(
    model_index0 = opt$model_index0,
    model_filt_id = opt$model_filt_id,
    from_table = opt$from_table,
    ko_rand = opt$ko_rand,
    b2p_tag = opt$b2p_tag,
    pop_key = opt$pop_key,
    cmx_tag = opt$cmx_tag,
    top_only = opt$top_only,
    base_dir = opt$tad_base_dir,
    should_crop = opt$crop,
    ncpu = opt$num_cpu,
    should_overwrite = opt$overwrite,
    verbose = opt$verbose
  )
}

# Cache b2p merged coord
if (opt$util_b2p_cache_coord_merge) {
  print("Running b2p_cache_coord_merge()")
  launch_b2p_cache_coord_merge(
    model_index0 = opt$model_index0,
    model_filt_id = opt$model_filt_id,
    from_table = opt$from_table,
    ko_rand = opt$ko_rand,
    b2p_tag = opt$b2p_tag,
    pop_key = opt$pop_key,
    cmx_tag = opt$cmx_tag,
    top_only = opt$top_only,
    base_dir = opt$tad_base_dir,
    should_crop = opt$crop,
    rmvout = opt$rmvout,
    check_merge_size = opt$check_merge_size,
    should_overwrite = opt$overwrite,
    verbose = opt$verbose
  )
}
