#!/usr/bin/python
# -*- coding: utf-8 -*-

# Script generates 3-D polymers from all pseudo-population bit matrices
#   encountered within directory hierarchy
#
# Arguments (shorthand|longhand):
#
#   [-arch|--arch_conf]: Overloads archetype configuration INI path, default
#       given by B2P_ARCH_INI_PATH in tad_core.py
#   [-bd|--base_dir]: [Optional] TAD base, non-versioned, output directory,
#       default is specified in tad_core.py
#   [-bpf|--bitpop_fname] [Optional] File name of pseudo-population bit matrix
#   [-cmd|--cmd_key]: [Optional] Model command key passed to the 'main'
#       dispatch utility, default is tad_core.B2P_CMD_KEY
#   [-cnf|--conf]: [Optional] Path to top-level model INI configuration,
#       default is given by B2P_TOP_INI_PATH in tad_core.py
#   [-dki|--dist_ki] <+real>: [Optional] Scalar knock-in distance in Angstroms.
#       A knock-in constraint is satisfied if at least one monomer node from
#       each fragment region is within this distance. May also be specified as
#       'chr_knock_in_dist = <+real>' within a configuration INI; the folder
#       binary also has a hard-coded default if not explicitly set by user.
#   [-e|--exe] <path>: [Optional] Path to executable folder binary with
#       support for chromatin-chromatin interaction constraints, default is
#       given by tad_core.B2P_EXE_PATH
#   [-ens|--ensemble_size] <integer>: Target ensemble size PER BIT CONFIG!
#   [-ft|--from_table <None|0|1|path>]: [Optional] If None|0, will use folder
#       listing in TAD base output directory for list of file names. If 1,
#       then will use default scale table path for obtaining model names. If
#       <path>, then will use user path to table file (must have 'name'
#       column) - if from table, then models are processed in order they are
#       listed in table file, else they are processed in order listed in file
#       system.
#   [-h|--help]: [Optional] Display help (i.e. usage information)
#   [-kor|--ko_rand]: [Optional] Switch if present will treat '0's as random
#       folding instead of constrained knock-outs, see also --no-ko_rand
#   [-no-kor|--no-ko_rand]: [Optional] Switch if present will enforce '0's as
#       constrained knock-outs, see also --ko_rand
#   [-mix|--model_index] <integer>: [Optional] If specified, then will only
#       launch for parameter 0-based index into list returned by
#       tad_core.resolve_tad_model_names(). If not specified (default
#       behavior), then all TAD models are processed.
#   [-mp|--max_proc] <+integer>: [Optional] Maximum number of parallel
#       processes (or equivalently threads) to use to work through each
#       CMX Gibbs sampler run. Note: <--max_proc> should not exceed CPU
#       logical cores
#   [-mt|--max_trial] <+integer>: [Optional] Number of trials to attempt
#       folding a given proximity constraint configuration before giving up
#   [-np|--num_proc] <+integer>: [Optional] Number of processes per
#       chromatin-to-chromatin bit configuration
#   [-pop|--pop_key]: [Optional] Population type: 'spop' (default) or 'mpop'
#   [-scp|--script_path]: [Optional] Path to 'bit2pol.py' script
#   [-top|--top_only]: [Optional] If switch present, only bitpop files at
#       lowest directory depth will be processed, see also --no-top_only
#   [-no-top|--no-top_only] : [Optional] If switch present, all encountered
#       bitpop files will be processed, see also --top_only
#   [-uichrd|--use_intr_chr_dir]: [Optional] If switch present, will use
#       resampled specific interaction constraints directory instead of
#       exemplar interactions; see also --no_use_intr_chr_dir
#   [-no-uichrd|--no-use_intr_chr_dir] : [Optional] If switch present, will
#       use exemplar interactions constraints instead of directory; see also
#       --use_intr_chr_dir
#   [-ulog|--use_logging]: [Optional] If switch present, will generate log
#       files for each bit2pol batch job, see also --no_use_logging
#   [-no-ulog|--no-use_logging] : [Optional] If switch present, will not
#       generate log files for each bit2pol batch job, see also --use_logging
#   [-xcsv|--xport_csv] <-1|0|1>: [Optional] 0: disable, 1: enable CSV polymer
#       export. -1: (default) Defer to INI and/or polymer folder defaults
#   [-xcsv_gz|--xport_csv_gz] <-1|0|1>: [Optional] 0: disable, 1: enable gzip
#       compressed CSV polymer export. -1: (default) Defer to INI and/or
#       polymer folder defaults
#   [-xintr|--xport_intr] <-1|0|1>: [Optional] 0: disable, 1: enable
#       chromatin-to-chromatin proximity interactions constraints to be
#       exported as PML selections. -1: (default) Defer to INI and/or polymer
#       folder defaults
#   [-xligc_bond|--xport_ligc_bond] <-1|0|1>: [Optional] 0: disable, 1: enable
#       explicit export of (i,i) and (i,i+1) bonded ligations - will increase
#       file size (these ligations always occur!)
#   [-xligc_csv|--xport_ligc_csv] <-1|0|1>: [Optional] 0: disable, 1: enable
#       CSV ligation contact tuple export. -1: (default) Defer to INI and/or
#       polymer folder defaults
#   [-xligc_csv_gz|--xport_ligc_csv_gz] <-1|0|1>: [Optional] 0: disable, 1:
#       enable gzip compressed CSV ligation contact tuple export. -1: (default)
#       Defer to INI and/or polymer folder defaults
#   [-xlogw|--xport_logw] <-1|0|1>: [Optional] 0: disable, 1: enable export of
#       population log importance weights. -1: (default) Defer to INI and/or
#       polymer folder defaults
#   [-xpdb|--xport_pdb]: [Optional] 0: disable, 1: enable PDB polymer export.
#       -1: (default) Defer to INI and/or polymer folder defaults
#   [-xpdb_gz|--xport_pdb_gz]: [Optional] 0: disable, 1: enable gzip
#       compressed PDB polymer export. -1: (default) Defer to INI and/or
#       polymer folder defaults
#   [-xpml|--xport_pml]: [[Optional] 0: disable, 1: enable PML polymer export.
#       -1: (default) Defer to INI and/or polymer folder defaults
#   [-xpml_gz|--xport_pml_gz]: [[Optional] 0: disable, 1: enable gzip
#       compressed PML polymer export. -1: (default) Defer to INI and/or
#       polymer folder defaults

###########################################
# Imports
###########################################

# For parsing user supplied arguments
import argparse

# For file path testing
import os

# For running external processes
import subprocess

# For flushing prints
import sys

# For default configuration settings
import tad_core

###########################################
# Defaults
###########################################

# Default number of parallel processes
DEF_MAX_PROC = tad_core.MAX_PROC

# Default model index
DEF_MIX = None

# Default export arguments
DEF_XPORT_CSV = 0
DEF_XPORT_CSV_GZ = 1
DEF_XPORT_INTR = 1
DEF_XPORT_LIGC_BOND = 0
DEF_XPORT_LIGC_CSV = 0
DEF_XPORT_LIGC_CSV_GZ = 1
DEF_XPORT_LOGW = 1
DEF_XPORT_PDB = 0
DEF_XPORT_PDB_GZ = 0
DEF_XPORT_PML = 0
DEF_XPORT_PML_GZ = 1

###########################################
# TAD bit2pol
###########################################

# @param results_dir - Base directory to recursively search for bitpops
# @param bitpop_fname - Target bit population file name
# @param top_only - If TRUE, only return results at lowest directory depth
# @return list of bitpop file paths
def get_bitpop_paths(results_dir, bitpop_fname, top_only):
    '''Return all bitpop file paths as list'''
    out = []
    for dirpath, dirnames, filenames in os.walk(results_dir):
        # Check for existing pseudo-population
        if  bitpop_fname in filenames:
            bitpop_path = os.path.join(dirpath, bitpop_fname)
            out.append(bitpop_path)
    if len(out) < 1:
        return []
    if not top_only:
        return out
    # Refine by depth
    depth = []
    for fp in out:
        depth.append(fp.count(os.sep))
    min_depth = min(depth)
    out_top = []
    for i, d in enumerate(depth):
        if min_depth == d:
            out_top.append(out[i])
    return out_top

# @param mix - Model 0-based index
# @param mid - Model identifier
# @param args - User arguments
def launch_bit2pol(mix, mid, geom_subd, cmd_invar, args):
    locus_id_str = str(mix) + ' : ' +  str(mid)
    print '[tad_bit2pol] Processing ' + locus_id_str
    # Base directory with unversioned TAD model data
    base_dir = args.base_dir
    # Mixture model population type
    pop_key = args.pop_key
    # Determine CMX results dir
    cmx_results_dir = tad_core.get_cmx_results_dir(model_name=mid,
                                                   pop_key=pop_key,
                                                   base_dir=base_dir)
    if not os.path.exists(cmx_results_dir):
        print "[tad_bit2pol] Skipping " + locus_id_str + \
            " as CMX results path not found:\n\t" + cmx_results_dir
        return

    # If file name matches this value, we assume it is a bit matrix
    bitpop_fname = args.bitpop_fname

    # Chromatin-chromatin interaction tuples path
    intr_path = None
    if args.use_intr_chr_dir:
        intr_path = tad_core.get_b2p_intr_chr_dir(model_name = mid,
                                                  base_dir=base_dir)
    else:
        intr_path = tad_core.get_tad_intr_chr_path(model_name = mid,
                                                   base_dir=base_dir)
    assert os.path.exists(intr_path)

    bitpop_paths = get_bitpop_paths(results_dir=cmx_results_dir,
                                    bitpop_fname = bitpop_fname,
                                    top_only = args.top_only)
    # Process pseudo-population bit matrices
    for bitpop_path in bitpop_paths:
        print "\tFound bit-population frame:\n\t\t" + str(bitpop_path)
        sys.stdout.flush()
        # Shallow copy invariant arguments
        cmd_args = cmd_invar[:]
        # Bit-population path argument
        cmd_args.extend(["--bitpop", bitpop_path])
        # Chromatin-chromatin interactions
        cmd_args.extend(["--intr", intr_path])
        # Output folder
        dirpath = os.path.dirname(bitpop_path)
        output_dir = os.path.join(dirpath, geom_subd)
        cmd_args.extend(["--outdir", output_dir])
        if args.use_logging:
            # Log prefix
            log_dir = os.path.join(output_dir, "logs")
            tad_core.make_dir(log_dir)
            log_prefix = os.path.join(log_dir, 'log')
            cmd_args.extend(['--log_prefix', log_prefix])
        # Call bit2pol!
        subprocess.check_call(cmd_args)

# @param args - User arguments
def tad_bit2pol(args):
    '''Run bit2pol for CMX pseudo-population at TAD model(s)'''

    # We cannot have empty pseudo-population file name
    assert args.bitpop_fname

    #######################################
    # Determine invariant command-line

    # Script path to bit2pol.py
    assert os.path.exists(args.script_path)
    cmd_invar = ["python", args.script_path]
    # Command-line key to main dispatch
    cmd_invar.extend(["--cmd_key", args.cmd_key])
    # Archetype INI configuration path
    if args.arch_conf:
        cmd_invar.extend(["--arch_conf", args.arch_conf])
    # Top-level INI configuration path
    assert os.path.exists(args.conf)
    cmd_invar.extend(["--conf", args.conf])
    # Polymer folder path
    assert os.path.exists(args.exe)
    cmd_invar.extend(["--exe", args.exe])
    # Ensemble size per bit configuration
    assert int(args.ensemble_size) >= 1
    cmd_invar.extend(["--ens_size", str(int(args.ensemble_size))])
    # Number of CPU procs per bit config
    assert int(args.num_proc) >= 1
    cmd_invar.extend(["--num_proc", str(int(args.num_proc))])
    # Check if modeling random knock-outs instead of explicit
    if args.ko_rand:
        cmd_invar.append("--ko_rand")
    else:
        cmd_invar.append("--no-ko_rand")
    # Max number of parallel processes
    assert int(args.max_proc) >= 1
    cmd_invar.extend(["--max_proc", str(args.max_proc)])
    # Maximum number of folding trials per configuration
    assert int(args.max_trial) >= 1
    cmd_invar.extend(["--max_trial", str(int(args.max_trial))])
    # Knock-in distance
    cmd_invar.extend(["--dist_ki", str(args.dist_ki)])
    # Export options
    cmd_invar.extend(["--xport_csv", str(args.xport_csv)])
    cmd_invar.extend(["--xport_csv_gz", str(args.xport_csv_gz)])
    cmd_invar.extend(["--xport_intr", str(args.xport_intr)])
    cmd_invar.extend(["--xport_ligc_bond", str(args.xport_ligc_bond)])
    cmd_invar.extend(["--xport_ligc_csv", str(args.xport_ligc_csv)])
    cmd_invar.extend(["--xport_ligc_csv_gz", str(args.xport_ligc_csv_gz)])
    cmd_invar.extend(["--xport_pdb", str(args.xport_pdb)])
    cmd_invar.extend(["--xport_pdb_gz", str(args.xport_pdb_gz)])
    cmd_invar.extend(["--xport_pml", str(args.xport_pml)])
    cmd_invar.extend(["--xport_pml_gz", str(args.xport_pml_gz)])
    cmd_invar.extend(["--xport_logw", str(args.xport_logw)])

    # Determine subfolder to store simulation geometry
    geom_subd = tad_core.get_b2p_geom_subdir(ko_rand=args.ko_rand,
                                             tag=args.tag)

    #######################################
    # Process TADs

    # List of active TAD models
    assert os.path.exists(args.base_dir)
    model_names = tad_core.resolve_tad_model_names(from_table=args.from_table,
                                                   base_dir=args.base_dir)
    if args.model_index is not None:
        # Process single TAD model
        mix = int(args.model_index)
        assert (mix >= 0) and (mix < len(model_names))
        mid = model_names[mix]
        launch_bit2pol(mix=mix, mid=mid, geom_subd=geom_subd,
                       cmd_invar=cmd_invar, args=args)
    else:
        # Process all TAD model
        for mix, mid in enumerate(model_names):
            launch_bit2pol(mix=mix, mid=mid, geom_subd=geom_subd,
                           cmd_invar=cmd_invar, args=args)

###########################################
# Main
###########################################

# Main script entry point
def __main__():
    print "======================= tad_bit2pol ======================="
    # Initialize command line parser
    parser = argparse.ArgumentParser()
    # Optional argument(s)
    parser.add_argument('-arch', '--arch_conf',
                        default=tad_core.B2P_ARCH_INI_PATH,
                        help='Model INI archetype configuration file path passed to folder')
    parser.add_argument('-bd', '--base_dir', default=tad_core.TAD_BASE_OUT_DIR,
                        help='Base directory storing unversioned TAD model data')
    parser.add_argument('-bpf', '--bitpop_fname',
                        default=tad_core.B2P_BITPOP_FNAME,
                        help="File name of pseudo-population bit matrix")
    parser.add_argument('-cmd', '--cmd_key', default=tad_core.B2P_CMD_KEY,
                        help='Model command key passed to the folding simulation "main" utility')
    parser.add_argument('-cnf', '--conf',
                        default=tad_core.B2P_TOP_INI_PATH,
                        help='Model top-level INI configuration file path passed to the "main" utility')
    parser.add_argument('-dki', '--dist_ki', default=tad_core.B2P_DIST_KI,
                        help='Scalar knock-in distance in Angstroms')
    parser.add_argument('-e', '--exe', default=tad_core.B2P_EXE_PATH,
                          help='Path to folder binary with support for 3-D chromatin-chromatin constraints')
    parser.add_argument('-ens', '--ensemble_size',
                        default=tad_core.B2P_ENSEMBLE_SIZE,
                        help='Ensemble size PER BIT CONFIG!')
    parser.add_argument('-ft', '--from_table', default=0,
                          help="If '1' or <path>, will attempt to load table and process TAD model names in row order")
    # https://stackoverflow.com/questions/15008758/parsing-boolean-values-with-argparse
    parser.add_argument('-kor', '--ko_rand', dest='ko_rand', action='store_true',
                        help="Switch, if present, will treat '0's as random folding instead of constrained knock-outs")
    parser.add_argument('-no-kor', '--no-ko_rand', dest='ko_rand', action='store_false',
                        help="Switch, if present, will enforce '0's as constrained knock-outs")
    parser.set_defaults(ko_rand=tad_core.B2P_KO_RAND)
    parser.add_argument('-mix', '--model_index', default=DEF_MIX,
                        help='Optional 0-based index into tad_core.resolve_tad_model_names(). If specified, other TADs will be ignored, else (default) all TADs will be processed.')
    parser.add_argument('-mp', '--max_proc', default=DEF_MAX_PROC,
                        help='Maximum number of active fold simulations')
    parser.add_argument('-mt', '--max_trial', default=tad_core.B2P_MAX_TRIAL,
                        help='Maximum number of trials per fold attempt of a given constraint configuration before giving up')
    parser.add_argument('-np', '--num_proc', default=tad_core.B2P_NUM_PROC,
                        help='Number of independent CPU processes for each bit sample')
    parser.add_argument('-pop', '--pop_key', default=tad_core.B2P_POP_KEY,
                        choices=['spop', 'mpop'],
                        help="Mixture model type, spop: single-populatoin, mpop: multi-population")
    parser.add_argument('-scp', '--script_path', default=tad_core.B2P_SCRIPT_PATH,
                        help="Path to 'bit2pol.py'")
    parser.add_argument('-tg', '--tag', default=tad_core.B2P_TAG,
                        help='Optional suffix for bit2pol geometry directories')
    # https://stackoverflow.com/questions/15008758/parsing-boolean-values-with-argparse
    parser.add_argument('-top', '--top_only', dest='top_only', action='store_true',
                        help="Only process bitpop files at lowest depth")
    parser.add_argument('-no-top', '--no-top_only', dest='top_only', action='store_false',
                        help="Process all encountered bitpop files")
    parser.set_defaults(top_only=tad_core.B2P_TOP_ONLY)
    parser.add_argument('-uichrd', '--use_intr_chr_dir', dest='use_intr_chr_dir', action='store_true',
                        help="Use directory or resampled interaction constraints")
    parser.add_argument('-no-uichrd', '--no-use_intr_chr_dir', dest='use_intr_chr_dir', action='store_false',
                        help="Use exemplar interaction constraints only")
    parser.set_defaults(use_intr_chr_dir=tad_core.B2P_USE_INTR_CHR_DIR)
    parser.add_argument('-ulog', '--use_logging', dest='use_logging', action='store_true',
                        help="Generate log file for each bit2pol batch job")
    parser.add_argument('-no-ulog', '--no-use_logging', dest='use_logging', action='store_false',
                        help="Do not generate log file for each bit2pol batch job")
    parser.set_defaults(use_logging=tad_core.B2P_USE_LOGGING)
    parser.add_argument('-xcsv', '--xport_csv', default=DEF_XPORT_CSV,
                        help="-1: Defer to INI, 0: Disable, 1: Enable CSV export of polymer geometry")
    parser.add_argument('-xcsv_gz', '--xport_csv_gz', default=DEF_XPORT_CSV_GZ,
                        help="-1: Defer to INI, 0: Disable, 1: Enable gzip-compressed CSV export of polymer geometry")
    parser.add_argument('-xintr', '--xport_intr', default=DEF_XPORT_INTR,
                        help="-1: Defer to INI, 0: Disable, 1: Enable export of PML selections for constrained chromatin-to-chromatin proximity interactions")
    parser.add_argument('-xligc_bond', '--xport_ligc_bond', default=DEF_XPORT_LIGC_BOND,
                        help="-1: Defer to INI, 0: Disable, 1: Enable explicit export of (i,i) and (i,i+1) polymer ligation contact tuples (increases file size, these ligations always occur!)")
    parser.add_argument('-xligc_csv', '--xport_ligc_csv', default=DEF_XPORT_LIGC_CSV,
                        help="-1: Defer to INI, 0: Disable, 1: Enable CSV export of polymer ligation contact tuples")
    parser.add_argument('-xligc_csv_gz', '--xport_ligc_csv_gz', default=DEF_XPORT_LIGC_CSV_GZ,
                        help="-1: Defer to INI, 0: Disable, 1: Enable gzip-compressed CSV export of polymer ligation contact tuples")
    parser.add_argument('-xlogw', '--xport_logw', default=DEF_XPORT_LOGW,
                        help="-1: Defer to INI, 0: Disable, 1: Enable polymer log importance weight export")
    parser.add_argument('-xpdb', '--xport_pdb',default=DEF_XPORT_PDB,
                        help="-1: Defer to INI, 0: Disable, 1: Enable PDB export of polymer geometry")
    parser.add_argument('-xpdb_gz', '--xport_pdb_gz', default=DEF_XPORT_PDB_GZ,
                        help="-1: Defer to INI, 0: Disable, 1: Enable gzip-compressed PDB export of polymer geometry")
    parser.add_argument('-xpml', '--xport_pml', default=DEF_XPORT_PML,
                        help="-1: Defer to INI, 0: Disable, 1: Enable PML export of polymer geometry")
    parser.add_argument('-xpml_gz', '--xport_pml_gz', default=DEF_XPORT_PML_GZ,
                        help="-1: Defer to INI, 0: Disable, 1: Enable gzip-compressed PML export of polymer geometry")
    # Parse command line
    args = parser.parse_args()
    # Print command line
    tad_core.print_cmd(args)
    # Feed to run utility
    tad_bit2pol(args)

# Run main if we are the active script
if __name__ == '__main__':
    __main__()
