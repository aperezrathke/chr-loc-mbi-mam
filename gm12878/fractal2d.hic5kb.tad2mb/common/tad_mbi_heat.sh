#!/bin/bash

# Path to this script's directory
SCRIPT_PATH="$(readlink -f $0)"
SCRIPT_DIR="$(dirname $SCRIPT_PATH)"
SCRIPT_DIR=$(cd "$SCRIPT_DIR"; pwd)

# MBI R utilities
RSHELL_SCRIPT="$SCRIPT_DIR/tad_mbi_shell.R"

# Parse command line for [optional] 0-based model index
MIX0=""
if [ $# -gt 0 ]; then
    MIX0="--model_index0 $1"
fi

# Heatmap min anchor value (ala seaborn) for rank plots
HEAT_VMIN_RANK="0.0"
# Heatmap max anchor value (ala seaborn) for rank plots
HEAT_VMAX_RANK="1.0"
# Heatmap min anchor value (ala seaborn) for non-rank plots
HEAT_VMIN_VAL="0.0"
# Heatmap max anchor value (ala seaborn) for non-rank plots
HEAT_VMAX_VAL="0.35"
# Heatmap image format (png or pdf)
HEAT_FORMAT="png"
# Heatmap pixel width (used if format is png)
HEAT_WIDTH_PIX="512"
# Heatmap pixel height (used if format is png)
HEAT_HEIGHT_PIX="512"
# Heatmap inches width (used if format is pdf)
HEAT_WIDTH_IN="7"
# Heatmap inches height (used if format is pdf)
HEAT_HEIGHT_IN="7"
# Heatmap FDR threshold for points overlay
HEAT_FDR_THRESH="0.05"

# Plot ranks with specific contacts
cmd="Rscript --vanilla $RSHELL_SCRIPT"
cmd="$cmd --util_mbi_heat_prin_loop $MIX0"
cmd="$cmd --heat_format $HEAT_FORMAT"
cmd="$cmd --heat_width_pix $HEAT_WIDTH_PIX"
cmd="$cmd --heat_height_pix $HEAT_HEIGHT_PIX"
cmd="$cmd --heat_width_in $HEAT_WIDTH_IN"
cmd="$cmd --heat_height_in $HEAT_HEIGHT_IN"
cmd="$cmd --heat_as_rank"
cmd="$cmd --heat_div_by_max"
cmd="$cmd --heat_vmin $HEAT_VMIN_RANK"
cmd="$cmd --heat_vmax $HEAT_VMAX_RANK"
cmd="$cmd --heat_fdr_ptsov"
cmd="$cmd --heat_fdr_thresh $HEAT_FDR_THRESH"
# Run command line
echo "Running:"
echo $cmd
$cmd

# Plot ranks without specific contacts
cmd="Rscript --vanilla $RSHELL_SCRIPT"
cmd="$cmd --util_mbi_heat_prin_loop $MIX0"
cmd="$cmd --heat_format $HEAT_FORMAT"
cmd="$cmd --heat_width_pix $HEAT_WIDTH_PIX"
cmd="$cmd --heat_height_pix $HEAT_HEIGHT_PIX"
cmd="$cmd --heat_width_in $HEAT_WIDTH_IN"
cmd="$cmd --heat_height_in $HEAT_HEIGHT_IN"
cmd="$cmd --heat_as_rank"
cmd="$cmd --heat_div_by_max"
cmd="$cmd --heat_vmin $HEAT_VMIN_RANK"
cmd="$cmd --heat_vmax $HEAT_VMAX_RANK"
cmd="$cmd --no-heat_fdr_ptsov"
cmd="$cmd --heat_fdr_thresh -1.0"
# Run command line
echo "Running:"
echo $cmd
$cmd

# Plot counts with specific contacts
cmd="Rscript --vanilla $RSHELL_SCRIPT"
cmd="$cmd --util_mbi_heat_prin_loop $MIX0"
cmd="$cmd --heat_format $HEAT_FORMAT"
cmd="$cmd --heat_width_pix $HEAT_WIDTH_PIX"
cmd="$cmd --heat_height_pix $HEAT_HEIGHT_PIX"
cmd="$cmd --heat_width_in $HEAT_WIDTH_IN"
cmd="$cmd --heat_height_in $HEAT_HEIGHT_IN"
cmd="$cmd --no-heat_as_rank"
cmd="$cmd --no-heat_div_by_max"
cmd="$cmd --heat_vmin $HEAT_VMIN_VAL"
cmd="$cmd --heat_vmax $HEAT_VMAX_VAL"
cmd="$cmd --heat_fdr_ptsov"
cmd="$cmd --heat_fdr_thresh $HEAT_FDR_THRESH"
# Run command line
echo "Running:"
echo $cmd
$cmd

# Plot counts without specific contacts
cmd="Rscript --vanilla $RSHELL_SCRIPT"
cmd="$cmd --util_mbi_heat_prin_loop $MIX0"
cmd="$cmd --heat_format $HEAT_FORMAT"
cmd="$cmd --heat_width_pix $HEAT_WIDTH_PIX"
cmd="$cmd --heat_height_pix $HEAT_HEIGHT_PIX"
cmd="$cmd --heat_width_in $HEAT_WIDTH_IN"
cmd="$cmd --heat_height_in $HEAT_HEIGHT_IN"
cmd="$cmd --no-heat_as_rank"
cmd="$cmd --no-heat_div_by_max"
cmd="$cmd --heat_vmin $HEAT_VMIN_VAL"
cmd="$cmd --heat_vmax $HEAT_VMAX_VAL"
cmd="$cmd --no-heat_fdr_ptsov"
cmd="$cmd --heat_fdr_thresh -1.0"
# Run command line
echo "Running:"
echo $cmd
$cmd

echo "Finished."
