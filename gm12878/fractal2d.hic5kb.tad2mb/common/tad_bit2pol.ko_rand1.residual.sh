#!/bin/bash

# Script for B2P folding any residual TAD regions

# Path to this script's directory
SCRIPT_PATH="$(readlink -f $0)"
SCRIPT_DIR="$(dirname $SCRIPT_PATH)"
SCRIPT_DIR=$(cd "$SCRIPT_DIR"; pwd)

# Determine path to target local script
PY_SCRIPT_PATH="$SCRIPT_DIR/tad_bit2pol.py"

# Max local CPU cores to use
MAX_PROC=20

# If '1' or <path>, will attempt to load table and process TAD model names in
# row order. If '0', will check existing folders in TAD base output directory.
# @WARNING - THIS MAY CAUSE PRINT MISMATCH OF 'MID' IN 'tad_cmx.queue.py'
#   WHICH IS OKAY, JUST LOGGING STATEMENTS OF WHICH TAD IS BEING PROCESSED
#   MAY NOT BE CORRECT
FROM_TABLE=1

# 27 -> chr18.60675000.61120000
# 40 -> chr6.26040000.27880000
RESIDUAL_MIX=('27' '40')

 # Iterate over residual model regions
for mix in "${RESIDUAL_MIX[@]}"
do
    # Run B2P dispatcher
    cmd="python $PY_SCRIPT_PATH"
    cmd="$cmd --model_index $mix"
    cmd="$cmd --max_proc $MAX_PROC"
    cmd="$cmd --from_table $FROM_TABLE"
    cmd="$cmd --ko_rand"
    # Run command line
    echo "-------------"
    echo "Running:"
    echo $cmd
    $cmd
done # End iteration over residual regions

echo "Finished residual B2P"
