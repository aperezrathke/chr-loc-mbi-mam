#!/bin/bash

# Path to this script's directory
SCRIPT_PATH="$(readlink -f $0)"
SCRIPT_DIR="$(dirname $SCRIPT_PATH)"
SCRIPT_DIR=$(cd "$SCRIPT_DIR"; pwd)

#### Delete bit2pol folding geometry

# Build command line
cmd="Rscript --vanilla $SCRIPT_DIR/tad_bit2pol_post_shell.R"
cmd="$cmd --util_b2p_unlink"

# Run command line
echo "Running:"
echo $cmd
$cmd

echo "Finished"
