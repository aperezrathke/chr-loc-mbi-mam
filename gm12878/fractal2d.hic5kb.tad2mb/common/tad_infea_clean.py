#!/usr/bin/python
# -*- coding: utf-8 -*-

# Script cleans (deletes) existing infeasible corpus. Useful if corpus is
#   stale and simulation parameters have changed.
#
# The following arguments are optional: (short-hand|long-hand)

#   [-bd|--base_dir]: [Optional] TAD base, non-versioned, output directory,
#       default is specified in tad_core.py

###################################################
# Imports
###################################################

# For parsing user supplied arguments
import argparse

# For file utils
import os

# For default configuration settings
import tad_core

###################################################
# TAD infea clean
###################################################

# @param base_dir - Unversioned TAD model output base directory
def tad_infea_clean(base_dir = tad_core.TAD_BASE_OUT_DIR):
    '''Clean (stale) infeasible corpus at each TAD'''
    # List of active TAD models
    model_names = tad_core.get_tad_model_names(base_dir=base_dir)
    # Process each TAD model
    for ix, mid in enumerate(model_names):
        print '[tad_infea_clean] Processing ' + str(ix) + ' : ' +  str(mid)
        # TAD infeasible chr-chr interaction corpus
        corp_path = tad_core.get_tad_infea_corp_path(model_name = mid,
                                                     base_dir = base_dir)
        if os.path.exists(corp_path):
            try:
                print '[tad_infea_clean] Removing:\n\t' + corp_path
                os.remove(corp_path)
            except OSError:
                pass

###################################################
# Main
###################################################

# Main script entry point
def __main__():
    print "======================= tad_infea_clean ======================="
    # Initialize command line parser
    parser = argparse.ArgumentParser()
    parser.add_argument('-bd', '--base_dir', default=tad_core.TAD_BASE_OUT_DIR,
                        help='Base directory storing unversioned TAD model data')
    # Parse command line
    args = parser.parse_args()
    tad_core.print_cmd(args)
    # Clean (stale) infeasible corpus at each TAD
    tad_infea_clean(**vars(args))
    print "Finished."

# Run main if we are the active script
if __name__ == '__main__':
    __main__()
