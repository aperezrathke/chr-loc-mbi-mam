#!/bin/bash

# Path to this script's directory
SCRIPT_PATH="$(readlink -f $0)"
SCRIPT_DIR="$(dirname $SCRIPT_PATH)"
SCRIPT_DIR=$(cd "$SCRIPT_DIR"; pwd)

#### Bit2pol coordinate geometry cache
RSHELL_SCRIPT="$SCRIPT_DIR/tad_bit2pol_coord_shell.R"

# Parse command line for [optional] 0-based model index
MIX0=""
if [ $# -gt 0 ]; then
    MIX0="--model_index0 $1"
fi

# Max local cores
NCPU=10

# Cache coordinate files
cmd="Rscript --vanilla $RSHELL_SCRIPT"
cmd="$cmd $MIX0"
cmd="$cmd --num_cpu $NCPU"
cmd="$cmd --util_b2p_cache_coord_csv"
cmd="$cmd --util_b2p_cache_coord_merge"
# Run command line
echo "-------------"
echo "Running:"
echo $cmd
$cmd

echo "Finished"
