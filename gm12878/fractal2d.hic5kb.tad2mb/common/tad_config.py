#!/usr/bin/python
# -*- coding: utf-8 -*-

# Creates TAD configuration INIs. Requires 'tad_scale.py' to be run first.
#
# Script accepts the following arguments (short|long):
#   -fi|--fin <path> : Path to input scaled TAD tabular data, defaults to
#       SCALE_PATH in tad_core.py
#   -fod|--fout_dir <path> : Base directory for writing Hi-C data at each TAD,
#       defaults to TAD_BASE_OUT_DIR value in tad_core.py
#   -ld|--lat_dim <+integer> : (lattice dimension) Number of points to sample
#       on surface of monomer node when growing a polymer chain and deciding
#       where to add the next node, defaults to TAD_LAT_DIM in tad_core.py
#   -cr|--create : If TAD model subfolder does not exist, then will be created;
#       else model will be skipped with warning (default)

###################################################
# Imports
###################################################

# For parsing user supplied arguments
import argparse

# For file path utils
import os

# For default configuration settings
import tad_core

###################################################
# Utilities
###################################################

# @param row - Data.frame row as namedtuple
# @param lat_dim - Lattice dimension for polymer folding (must be positive int)
def get_folder_arch_ini_body(row, lat_dim):
    '''Return folder archetype INI configuration'''
    assert lat_dim > 0
    nuc_diam_str = str(row.nuc_diam)
    mon_diam_str = str(row.mon_diam)
    mon_num_str = str(int(row.mon_num))
    lat_dim_str = str(int(lat_dim))
    ini = """nuclear_diameter = %s
max_node_diameter = %s
num_nodes_0 = %s
num_unit_sphere_sample_points = %s
""" % (nuc_diam_str, mon_diam_str, mon_num_str, lat_dim_str)
    return ini

# @param body - text body of INI file
# @param fpath - path to write ini file
def write_ini(body, fpath):
    '''Utility creates INI file and any parent directories'''
    # Make sure parent folders exist
    tad_core.make_dir(os.path.dirname(fpath))
    # Open binary mode for consistent new lines between systems
    # https://stackoverflow.com/questions/9184107/how-can-i-force-pythons-file-write-to-use-the-same-newline-format-in-windows
    with open(fpath, 'wb') as fh:
        print "[tad_config] Writing:\n\t" + fpath
        fh.write(body)

###################################################
# TAD config
###################################################

# @param fin - Path to input scaled TAD records
# @param fout_dir - Base directory to store Hi-C data at each TAD record
# @param lat_dim - Lattice dimension for polymer folding (must be positive int)
# @param create - If TRUE, create any missing TAD model subdirectory, else warn
def tad_config(fin = tad_core.SCALE_PATH,
               fout_dir = tad_core.TAD_BASE_OUT_DIR,
               lat_dim = tad_core.TAD_LAT_DIM,
               create = False):
    '''Generates configuration file(s) at each TAD model'''
    assert lat_dim > 0
    # Load scaled TAD records
    df_scale = tad_core.load_tad_scale(fin=fin)
    # List of active TAD models
    model_names = tad_core.get_tad_model_names(base_dir=fout_dir)
    # Process each TAD model
    for row in df_scale.itertuples(index=False, name='Pandas'):
        mid = str(row.name)
        exists = mid in model_names
        if exists or create:
            if not exists:
                print "[tad_config] Warning, creating TAD subdir at " + mid
            # folder archetype INI
            write_ini(body = get_folder_arch_ini_body( \
                                row=row,
                                lat_dim=lat_dim),
                      fpath = tad_core.get_tad_folder_arch_ini_path( \
                                model_name=mid,
                                base_dir=fout_dir))
        else:
            print "[tad_config] Warning, no TAD model dir for " + mid + \
                    " (Skipping)"

###################################################
# Main
###################################################

# Main script entry point
def __main__():
    print "======================== tad_config ========================"
    # Initialize command line parser
    parser = argparse.ArgumentParser()
    # Optional arguments
    parser.add_argument('-fi', '--fin', default=tad_core.SCALE_PATH,
                        help='Path to input scaled TAD records')
    parser.add_argument('-fod', '--fout_dir', default=tad_core.TAD_BASE_OUT_DIR,
                        help='Base directory to store unversioned Hi-C data')
    parser.add_argument('-ld', '--lat_dim', default=tad_core.TAD_LAT_DIM,
                        type=int,
                        help='Lattice dimension for polymer folding (+int)')
    parser.add_argument('-cr', '--create', action='store_true',
                        help='If present, any non-existing TAD model subfolders will be created, else TAD will be skipped with warning') 
                        
    # Parse command line
    args = parser.parse_args()
    tad_core.print_cmd(args)
    # Feed to utility
    # https://stackoverflow.com/questions/16878315/what-is-the-right-way-to-treat-python-argparse-namespace-as-a-dictionary
    tad_config(**vars(args))

# Run main if we are the active script
if __name__ == '__main__':
    __main__()
