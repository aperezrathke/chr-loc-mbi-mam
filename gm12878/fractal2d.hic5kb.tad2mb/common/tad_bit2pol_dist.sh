#!/bin/bash

# Path to this script's directory
SCRIPT_PATH="$(readlink -f $0)"
SCRIPT_DIR="$(dirname $SCRIPT_PATH)"
SCRIPT_DIR=$(cd "$SCRIPT_DIR"; pwd)

#### Bit2pol distance geometry cache
RSHELL_SCRIPT="$SCRIPT_DIR/tad_bit2pol_dist_shell.R"

# Parse command line for [optional] 0-based model index
MIX0=""
if [ $# -gt 0 ]; then
    MIX0="--model_index0 $1"
fi

# Number of local CPUs
NUM_CPU=2

# Cache distance file(s)
cmd="Rscript --vanilla $RSHELL_SCRIPT"
cmd="$cmd $MIX0"
cmd="$cmd --num_cpu $NUM_CPU"
cmd="$cmd --util_b2p_cache_dist_merge"
cmd="$cmd --util_b2p_cache_dist_mean"
# Run command line
echo "-------------"
echo "Running:"
echo $cmd
$cmd

echo "Finished"
