#!/bin/bash

# Path to this script's directory
SCRIPT_PATH="$(readlink -f $0)"
SCRIPT_DIR="$(dirname $SCRIPT_PATH)"
SCRIPT_DIR=$(cd "$SCRIPT_DIR"; pwd)

#### Bit2pol distance geometry cache
RSHELL_SCRIPT="$SCRIPT_DIR/tad_bit2pol_dipc_shell.R"

# Parse command line for [optional] 0-based model index
MIX0=""
if [ $# -gt 0 ]; then
    # Ignore MIX0 if negative
    if [ $1 -ge 0 ]; then
        MIX0="--model_index0 $1"
    fi
fi

# Parse command line for model filter
# @HACK - MUST ALWAYS BE SECOND ARGUMENT!!!
#   (PASS FIRST ARGUMENT AS -1 TO IGNORE)
MFI=""
if [ $# -gt 1 ]; then
    MFI="--model_filt_id $2"
fi

# Number of local CPUs
NUM_CPU=10

# Cache Dip-C comparisons
cmd="Rscript --vanilla $RSHELL_SCRIPT"
cmd="$cmd $MIX0"
cmd="$cmd $MFI"
cmd="$cmd --num_cpu $NUM_CPU"
cmd="$cmd --util_dipc_to_b2p_compare"
# Run command line
echo "-------------"
echo "Running:"
echo $cmd
$cmd

echo "Finished"
