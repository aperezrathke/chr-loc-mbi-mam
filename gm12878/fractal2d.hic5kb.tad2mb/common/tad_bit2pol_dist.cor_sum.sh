#!/bin/bash

# Path to this script's directory
SCRIPT_PATH="$(readlink -f $0)"
SCRIPT_DIR="$(dirname $SCRIPT_PATH)"
SCRIPT_DIR=$(cd "$SCRIPT_DIR"; pwd)

#### Bit2pol distance geometry correlation cache
RSHELL_SCRIPT="$SCRIPT_DIR/tad_bit2pol_dist_shell.R"

# Parse command line for [optional] model filter
MODEL_FILT_ID=""
if [ $# -gt 0 ]; then
    MODEL_FILT_ID="--model_filt_id $1"
fi

# Cache distance file(s)
cmd="Rscript --vanilla $RSHELL_SCRIPT"
cmd="$cmd $MODEL_FILT_ID"
cmd="$cmd --util_b2p_dist_cor_sum"
# Run command line
echo "-------------"
echo "Running:"
echo $cmd
$cmd

echo "Finished"
