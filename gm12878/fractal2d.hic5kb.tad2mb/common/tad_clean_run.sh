#!/bin/bash

# Path to this script's directory
SCRIPT_PATH="$(readlink -f $0)"
SCRIPT_DIR="$(dirname $SCRIPT_PATH)"
SCRIPT_DIR=$(cd "$SCRIPT_DIR"; pwd)

#### CLEAN HI-C

# Number of parallel processes for TAD clean. Note we are likely
# overcommiting the amount of virtual memory as all BLB replicates are loaded.
# However, in practice this seems to work okay up to a threshold number of
# parallel R multicore processes. See discussions:
# https://stackoverflow.com/questions/15668893/r-multicore-mcfork-unable-to-fork-cannot-allocate-memory
# https://www.win.tue.nl/~aeb/linux/lk/lk-9.html
TAD_CLEAN_NUM_CPU="20"

# Build command line - Clean Hi-C
cmd="Rscript --vanilla $SCRIPT_DIR/tad_clean_shell.R"
cmd="$cmd --util_tad_clean"
cmd="$cmd --tad_clean_num_cpu $TAD_CLEAN_NUM_CPU"
# Run command line
echo "Running:"
echo $cmd
$cmd

#### PLOT SETTINGS

# Number of CPUs for parallel heat map generation, <= 0 will use all cores
TAD_HEAT_NUM_CPU="-1"

# Heatmap min anchor value (ala seaborn) for rank plots
HEAT_VMIN_RANK="0.0"
# Heatmap max anchor value (ala seaborn) for rank plots
HEAT_VMAX_RANK="1.0"
# Heatmap min anchor value (ala seaborn) for non-rank plots
HEAT_VMIN_VAL="0.0"
# Heatmap max anchor value (ala seaborn) for non-rank plots
HEAT_VMAX_VAL="0.35"
# Heatmap image format (png or pdf)
HEAT_FORMAT="png"
# Heatmap pixel width (used if format is png)
HEAT_WIDTH_PIX="512"
# Heatmap pixel height (used if format is png)
HEAT_HEIGHT_PIX="512"
# Heatmap inches width (used if format is pdf)
HEAT_WIDTH_IN="7"
# Heatmap inches height (used if format is pdf)
HEAT_HEIGHT_IN="7"
# Heatmap FDR threshold for points overlay
HEAT_FDR_THRESH="0.05"


#### PLOT HI-C QUANTILE NORMALIZED FREQUENCIES

# Build command line - Plot quantile normalized ranks with specific contacts
cmd="Rscript --vanilla $SCRIPT_DIR/tad_clean_shell.R"
cmd="$cmd --util_tad_heat"
cmd="$cmd --tad_heat_col_id fq"
cmd="$cmd --heat_format $HEAT_FORMAT"
cmd="$cmd --heat_width_pix $HEAT_WIDTH_PIX"
cmd="$cmd --heat_height_pix $HEAT_HEIGHT_PIX"
cmd="$cmd --heat_width_in $HEAT_WIDTH_IN"
cmd="$cmd --heat_height_in $HEAT_HEIGHT_IN"
cmd="$cmd --heat_as_rank"
cmd="$cmd --heat_div_by_max"
cmd="$cmd --heat_vmin $HEAT_VMIN_RANK"
cmd="$cmd --heat_vmax $HEAT_VMAX_RANK"
cmd="$cmd --heat_fdr_ptsov"
cmd="$cmd --heat_fdr_thresh $HEAT_FDR_THRESH"
cmd="$cmd --tad_heat_num_cpu $TAD_HEAT_NUM_CPU"
# Run command line
echo "Running:"
echo $cmd
$cmd

# Build command line - Plot quantile normalized ranks without specific contacts
cmd="Rscript --vanilla $SCRIPT_DIR/tad_clean_shell.R"
cmd="$cmd --util_tad_heat"
cmd="$cmd --tad_heat_col_id fq"
cmd="$cmd --heat_format $HEAT_FORMAT"
cmd="$cmd --heat_width_pix $HEAT_WIDTH_PIX"
cmd="$cmd --heat_height_pix $HEAT_HEIGHT_PIX"
cmd="$cmd --heat_width_in $HEAT_WIDTH_IN"
cmd="$cmd --heat_height_in $HEAT_HEIGHT_IN"
cmd="$cmd --heat_as_rank"
cmd="$cmd --heat_div_by_max"
cmd="$cmd --heat_vmin $HEAT_VMIN_RANK"
cmd="$cmd --heat_vmax $HEAT_VMAX_RANK"
cmd="$cmd --no-heat_fdr_ptsov"
cmd="$cmd --heat_fdr_thresh -1.0"
cmd="$cmd --tad_heat_num_cpu $TAD_HEAT_NUM_CPU"
# Run command line
echo "Running:"
echo $cmd
$cmd

# Build command line - Plot quantile normalized values with specific contacts
cmd="Rscript --vanilla $SCRIPT_DIR/tad_clean_shell.R"
cmd="$cmd --util_tad_heat"
cmd="$cmd --tad_heat_col_id fq"
cmd="$cmd --heat_format $HEAT_FORMAT"
cmd="$cmd --heat_width_pix $HEAT_WIDTH_PIX"
cmd="$cmd --heat_height_pix $HEAT_HEIGHT_PIX"
cmd="$cmd --heat_width_in $HEAT_WIDTH_IN"
cmd="$cmd --heat_height_in $HEAT_HEIGHT_IN"
cmd="$cmd --no-heat_as_rank"
cmd="$cmd --heat_div_by_max"
cmd="$cmd --heat_vmin $HEAT_VMIN_VAL"
cmd="$cmd --heat_vmax $HEAT_VMAX_VAL"
cmd="$cmd --heat_fdr_ptsov"
cmd="$cmd --heat_fdr_thresh $HEAT_FDR_THRESH"
cmd="$cmd --tad_heat_num_cpu $TAD_HEAT_NUM_CPU"
# Run command line
echo "Running:"
echo $cmd
$cmd

# Build command line - Plot quantile normalized values without specific contacts
cmd="Rscript --vanilla $SCRIPT_DIR/tad_clean_shell.R"
cmd="$cmd --util_tad_heat"
cmd="$cmd --tad_heat_col_id fq"
cmd="$cmd --heat_format $HEAT_FORMAT"
cmd="$cmd --heat_width_pix $HEAT_WIDTH_PIX"
cmd="$cmd --heat_height_pix $HEAT_HEIGHT_PIX"
cmd="$cmd --heat_width_in $HEAT_WIDTH_IN"
cmd="$cmd --heat_height_in $HEAT_HEIGHT_IN"
cmd="$cmd --no-heat_as_rank"
cmd="$cmd --heat_div_by_max"
cmd="$cmd --heat_vmin $HEAT_VMIN_VAL"
cmd="$cmd --heat_vmax $HEAT_VMAX_VAL"
cmd="$cmd --no-heat_fdr_ptsov"
cmd="$cmd --heat_fdr_thresh -1.0"
cmd="$cmd --tad_heat_num_cpu $TAD_HEAT_NUM_CPU"
# Run command line
echo "Running:"
echo $cmd
$cmd

#### PLOT HI-C RAW COUNTS

# Build command line - Plot raw count ranks with specific contacts
cmd="Rscript --vanilla $SCRIPT_DIR/tad_clean_shell.R"
cmd="$cmd --util_tad_heat"
cmd="$cmd --tad_heat_col_id rawc"
cmd="$cmd --heat_format $HEAT_FORMAT"
cmd="$cmd --heat_width_pix $HEAT_WIDTH_PIX"
cmd="$cmd --heat_height_pix $HEAT_HEIGHT_PIX"
cmd="$cmd --heat_width_in $HEAT_WIDTH_IN"
cmd="$cmd --heat_height_in $HEAT_HEIGHT_IN"
cmd="$cmd --heat_as_rank"
cmd="$cmd --heat_div_by_max"
cmd="$cmd --heat_vmin $HEAT_VMIN_RANK"
cmd="$cmd --heat_vmax $HEAT_VMAX_RANK"
cmd="$cmd --heat_fdr_ptsov"
cmd="$cmd --heat_fdr_thresh $HEAT_FDR_THRESH"
cmd="$cmd --tad_heat_num_cpu $TAD_HEAT_NUM_CPU"
# Run command line
echo "Running:"
echo $cmd
$cmd

# Build command line - Plot raw count ranks without specific contacts
cmd="Rscript --vanilla $SCRIPT_DIR/tad_clean_shell.R"
cmd="$cmd --util_tad_heat"
cmd="$cmd --tad_heat_col_id rawc"
cmd="$cmd --heat_format $HEAT_FORMAT"
cmd="$cmd --heat_width_pix $HEAT_WIDTH_PIX"
cmd="$cmd --heat_height_pix $HEAT_HEIGHT_PIX"
cmd="$cmd --heat_width_in $HEAT_WIDTH_IN"
cmd="$cmd --heat_height_in $HEAT_HEIGHT_IN"
cmd="$cmd --heat_as_rank"
cmd="$cmd --heat_div_by_max"
cmd="$cmd --heat_vmin $HEAT_VMIN_RANK"
cmd="$cmd --heat_vmax $HEAT_VMAX_RANK"
cmd="$cmd --no-heat_fdr_ptsov"
cmd="$cmd --heat_fdr_thresh -1.0"
cmd="$cmd --tad_heat_num_cpu $TAD_HEAT_NUM_CPU"
# Run command line
echo "Running:"
echo $cmd
$cmd

# Build command line - Plot raw count values with specific contacts
cmd="Rscript --vanilla $SCRIPT_DIR/tad_clean_shell.R"
cmd="$cmd --util_tad_heat"
cmd="$cmd --tad_heat_col_id rawc"
cmd="$cmd --heat_format $HEAT_FORMAT"
cmd="$cmd --heat_width_pix $HEAT_WIDTH_PIX"
cmd="$cmd --heat_height_pix $HEAT_HEIGHT_PIX"
cmd="$cmd --heat_width_in $HEAT_WIDTH_IN"
cmd="$cmd --heat_height_in $HEAT_HEIGHT_IN"
cmd="$cmd --no-heat_as_rank"
cmd="$cmd --heat_div_by_max"
cmd="$cmd --heat_vmin $HEAT_VMIN_VAL"
cmd="$cmd --heat_vmax $HEAT_VMAX_VAL"
cmd="$cmd --heat_fdr_ptsov"
cmd="$cmd --heat_fdr_thresh $HEAT_FDR_THRESH"
cmd="$cmd --tad_heat_num_cpu $TAD_HEAT_NUM_CPU"
# Run command line
echo "Running:"
echo $cmd
$cmd

# Build command line - Plot raw count values without specific contacts
cmd="Rscript --vanilla $SCRIPT_DIR/tad_clean_shell.R"
cmd="$cmd --util_tad_heat"
cmd="$cmd --tad_heat_col_id rawc"
cmd="$cmd --heat_format $HEAT_FORMAT"
cmd="$cmd --heat_width_pix $HEAT_WIDTH_PIX"
cmd="$cmd --heat_height_pix $HEAT_HEIGHT_PIX"
cmd="$cmd --heat_width_in $HEAT_WIDTH_IN"
cmd="$cmd --heat_height_in $HEAT_HEIGHT_IN"
cmd="$cmd --no-heat_as_rank"
cmd="$cmd --heat_div_by_max"
cmd="$cmd --heat_vmin $HEAT_VMIN_VAL"
cmd="$cmd --heat_vmax $HEAT_VMAX_VAL"
cmd="$cmd --no-heat_fdr_ptsov"
cmd="$cmd --heat_fdr_thresh -1.0"
cmd="$cmd --tad_heat_num_cpu $TAD_HEAT_NUM_CPU"
# Run command line
echo "Running:"
echo $cmd
$cmd

echo "Finished."
