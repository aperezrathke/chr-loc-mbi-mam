#!/bin/bash

# Path to this script's directory
SCRIPT_PATH="$(readlink -f $0)"
SCRIPT_DIR="$(dirname $SCRIPT_PATH)"
SCRIPT_DIR=$(cd "$SCRIPT_DIR"; pwd)

# MBI R utilities
RSHELL_SCRIPT="$SCRIPT_DIR/tad_mbi_shell.R"

# Parse command line for [optional] 0-based model index
MIX0=""
if [ $# -gt 0 ]; then
    MIX0="--model_index0 $1"
fi

# @HACK - avoid out of RAM errors
NCPU=10

# Many body bootstrap
cmd="Rscript --vanilla $RSHELL_SCRIPT"
cmd="$cmd --util_mbi_cache_pvals --num_cpu $NCPU $MIX0"
# Run command line
echo "Running:"
echo $cmd
$cmd
echo "Finished."
