#!/bin/bash

# Wrapper to source intel compiler environment variables
# Example usage:
#
#   nohup <path_to_script> &> <path_to_log> &
#   : nohup means to keep job running even when logged out
#   : &> means to redirect stdout and stderr to log file
#   : & means to run script in background

# Path to this script's directory
SCRIPT_PATH="$(readlink -f $0)"
SCRIPT_DIR="$(dirname $SCRIPT_PATH)"
SCRIPT_DIR=$(cd "$SCRIPT_DIR"; pwd)

# Setup intel compiler environment variables.
FC="ifort"
source /opt/intel/bin/compilervars.sh intel64

# Build command line
cmd="python $SCRIPT_DIR/tad_infea.py"
# Run command line
echo "Running:"
echo $cmd
$cmd
echo "Finished."
