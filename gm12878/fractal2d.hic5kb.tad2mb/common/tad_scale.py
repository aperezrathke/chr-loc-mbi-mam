#!/usr/bin/python
# -*- coding: utf-8 -*-

# Scales TAD models to target fiber density and monomer resolution. It is
# recommended that 'tad_filter.py' be run first. See tad_core.py for defaults
#
# Script accepts the following arguments (short|long):
#   -fi|--fin <path> : Path to input filtered TAD tabular data
#   -fo|--fout <path> : Path to write scaled TAD tabular data
#   -nv|--nuc_vol <+real microns> : Nuclear volume unscaled in microns
#   -gbp|--gen_bp <+real base pairs> : Whole genome length in base pairs
#   -mbp|--mon_bp <+real base pairs> : Target monomer unit density|densities
#       in base pairs, may be scalar (homogeneous) or vector (heterogeneous);
#       if heterogeneous vector, then vector must be tip-to-tail in same order
#       as loci present in 'loc_bp' and must sum to loci base pair lengths
#   -fbp|--fib_bp <+real base pairs> : Intrinsic fiber unit density in base
#       pairs, may be scalar (homogeneous) or vector (heterogeneous) fiber
#   -fl|--fib_len <+real Angstroms> : Intrinsic fiber unit linear length in
#       Angstroms, e.g 110 Angstroms for 11 nm fiber, may be scalar
#       (homogeneous) or  vector (heterogeneous) fiber

###################################################
# Imports
###################################################

# For parsing user supplied arguments
import argparse

# For default configuration settings
import tad_core

###################################################
# Utilities
###################################################

# @param row - Data.frame row as namedtuple
# @param nuc_vol - Unscaled nuclear volume in microns (+real)
# @param gen_bp - Whole genome length in base pairs, including ploidy
# @param mon_bp - Target monomer unit density in base pairs
# @param fib_bp - Intrinsic fiber unit density in base pairs
# @param fib_len - Intrinsic fiber unit length in Angstroms
def get_scaled_nucleus(row, nuc_vol=tad_core.SCALE_NUC_VOL,
                       gen_bp=tad_core.SCALE_GEN_BP,
                       mon_bp=tad_core.SCALE_MON_BP,
                       fib_bp=tad_core.SCALE_FIB_BP,
                       fib_len=tad_core.SCALE_FIB_LEN):
    '''Gets scaled nuclear arguments'''
    scaled_args = tad_core.scale_nucleus.scale_nucleus(
        nuc_vol=nuc_vol, gen_bp=gen_bp, loc_bp=row.bp_span_tad_med,
        mon_bp=mon_bp, fib_bp=fib_bp, fib_len=fib_len)
    return scaled_args

# @param df_filt - Data.frame with pre-filtered TAD data
# @param nuc_vol - Unscaled nuclear volume in microns (+real)
# @param gen_bp - Whole genome length in base pairs, including ploidy
# @param mon_bp - Target monomer unit density in base pairs
# @param fib_bp - Intrinsic fiber unit density in base pairs
# @param fib_len - Intrinsic fiber unit length in Angstroms
# @return Data.frame with columns for scaled nuclear arguments
def get_scaled_tads(df_filt, nuc_vol=tad_core.SCALE_NUC_VOL,
                    gen_bp=tad_core.SCALE_GEN_BP,
                    mon_bp=tad_core.SCALE_MON_BP,
                    fib_bp=tad_core.SCALE_FIB_BP,
                    fib_len=tad_core.SCALE_FIB_LEN):
    '''Returns data.frame of nuclear scaled TADs'''
    # Keep track of scaled arguments
    df_scale = df_filt
    df_scale['nuc_diam'] = 0.0
    df_scale['mon_diam'] = 0.0
    df_scale['mon_num'] = 0
    df_scale['mon_bp'] = 0.0
    df_scale['loc_span_bp'] = 0.0
    # Scale TADs
    for row in df_scale.itertuples(index=True, name='Pandas'):
        scaled_args = get_scaled_nucleus(row=row, nuc_vol=nuc_vol,
                                         gen_bp=gen_bp, mon_bp=mon_bp,
                                         fib_bp=fib_bp, fib_len=fib_len)
        df_scale.ix[row.Index, 'nuc_diam'] = scaled_args.nuc_diam
        df_scale.ix[row.Index, 'mon_diam'] = scaled_args.mon_diam[0]
        df_scale.ix[row.Index, 'mon_num'] = scaled_args.mon_num[0]
        df_scale.ix[row.Index, 'mon_bp'] = scaled_args.mon_bp[0]
        df_scale.ix[row.Index, 'loc_span_bp'] = \
            scaled_args.mon_bp[0] * scaled_args.mon_num[0]
    return df_scale

# @param df_scale - Data.frame of scaled TAD records
# @param fout - Output file path to write df_filt
def save_scaled_tads(df_scale, fout=tad_core.SCALE_PATH):
    '''Writes scaled TAD records'''
    tad_core.save_table(df=df_scale, fout=fout, index=False)

###################################################
# TAD scale
###################################################

# @param fin - Path to input filtered TAD records
# @param fout - Path to write scaled TAD records
# @param nuc_vol - Unscaled nuclear volume in microns (+real)
# @param gen_bp - Whole genome length in base pairs, including ploidy
# @param mon_bp - Target monomer unit density in base pairs
# @param fib_bp - Intrinsic fiber unit density in base pairs
# @param fib_len - Intrinsic fiber unit length in Angstroms
def tad_scale(fin = tad_core.FILT_PATH,
              fout = tad_core.SCALE_PATH,
              nuc_vol = tad_core.SCALE_NUC_VOL,
              gen_bp = tad_core.SCALE_GEN_BP,
              mon_bp = tad_core.SCALE_MON_BP,
              fib_bp = tad_core.SCALE_FIB_BP,
              fib_len = tad_core.SCALE_FIB_LEN):
    '''Scales TAD models'''
    # Load filtered TAD records
    df_filt = tad_core.load_tad_filt(fin=fin)
    # Scale TADs
    df_scale = get_scaled_tads(df_filt=df_filt, nuc_vol=nuc_vol,
                               gen_bp=gen_bp, mon_bp=mon_bp, fib_bp=fib_bp,
                               fib_len=fib_len)
    # Save results
    save_scaled_tads(df_scale=df_scale, fout=fout)

###################################################
# Main
###################################################

# Main script entry point
def __main__():
    print "======================== tad_scale ========================"
    # Initialize command line parser
    parser = argparse.ArgumentParser()
    # Optional arguments
    parser.add_argument('-fi', '--fin', default=tad_core.FILT_PATH,
                        help='Path to input filtered TAD records')
    parser.add_argument('-fo', '--fout', default=tad_core.SCALE_PATH,
                        help='Path to write scaled TAD records')
    parser.add_argument('-nv', '--nuc_vol', default=tad_core.SCALE_NUC_VOL,
                        help='Unscaled nuclear volume in microns (+real)')
    parser.add_argument('-gbp', '--gen_bp', default=tad_core.SCALE_GEN_BP,
                        help='Whole genome length in base pairs (+real)')
    parser.add_argument('-mbp', '--mon_bp', default=tad_core.SCALE_MON_BP,
                        help='Target monomer unit density in base pairs')
    parser.add_argument('-fbp', '--fib_bp', default=tad_core.SCALE_FIB_BP,
                        help='Intrinsic fiber unit density in base pairs')
    parser.add_argument('-fl', '--fib_len', default=tad_core.SCALE_FIB_LEN,
                        help='Intrinsic fiber unit length in Angstroms')
    # Parse command line
    args = parser.parse_args()
    tad_core.print_cmd(args)
    # Feed to utility
    # https://stackoverflow.com/questions/16878315/what-is-the-right-way-to-treat-python-argparse-namespace-as-a-dictionary
    tad_scale(**vars(args))

# Run main if we are the active script
if __name__ == '__main__':
    __main__()
