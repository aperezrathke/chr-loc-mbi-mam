#!/bin/bash

# Call python BIT2POL DIST multiplexer
#
# Example usage:
#
#   nohup <path_to_script> [-nodes <int>] &> <path_to_log> &
#   : nohup means to keep job running even when logged out
#   : &> means to redirect stdout and stderr to log file
#   : & means to run script in background
#
# Optional argument(s)
#
#   -nodes <int>: Specify maximum number of compute nodes

###################################################
# Script paths and globals
###################################################

# Path to script directory
SCRIPT_PATH="$(readlink -f $0)"
SCRIPT_DIR="$(dirname $SCRIPT_PATH)"

# Path to python script
PY_SCRIPT_PATH="$SCRIPT_DIR/tad_b2p_dist.queue.py"

# Max computes nodes
MAX_NODES=5

###################################################
# Command line overrides
###################################################

# Check if there are any parameter overrides
while [ $# -gt 1 ]
do
    case "$1" in
    -nodes)
        MAX_NODES=$2
        shift
        ;;
    esac
    shift
done

# Validate argument(s)
if [ "$MAX_NODES" -lt "1" ]; then
    # Use at least a single node
    MAX_NODES=1
fi

###################################################
# Run
###################################################

# Make sure proper version of python is loaded
module load compilers/python-2.7.13-gcc

# Run script
cmd="python $PY_SCRIPT_PATH --max_compute_nodes $MAX_NODES"
echo "Running $cmd"
$cmd
echo "Finished B2P DIST queue"
