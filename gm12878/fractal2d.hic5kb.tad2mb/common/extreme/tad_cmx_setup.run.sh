#!/bin/bash  

#PBS -l walltime=75:00:00
#PBS -l nodes=1:ppn=20
#PBS -j oe  
#PBS -m n  
#PBS -M perezrat@uic.edu
#PBS -q jliang
#PBS -N centro

# Usage:
#   qsub <script_name.sh>
#
# ----------------------------------------------------------------------------
#
# Monitor using either:
#
#   qstat -u <netid>
# or
#   showq -u <netid>
#
# Delete using:
#   qdel <job_id_from_qstat>
# or to delete all active jobs:
#   qselect -u <netid> | xargs qdel

###################################################
# Timestamp utils
###################################################

# From http://stackoverflow.com/questions/17066250/create-timestamp-variable-in-bash-script
timestamp() {
  date +"%T"
}

###################################################
# Run
###################################################

# Determine path to script
HOME_DIR=$(echo ~)
WORK_DIR="$HOME_DIR/chr-working"
PROJ_DIR="$WORK_DIR/loci/mbi-mam"
SCRIPT_DIR="$PROJ_DIR/gm12878/fractal2d.hic5kb.tad2mb/common/extreme"

# Max local CPU cores to use (make sure does not exceed PBS ppn)
NUM_CPU=5

# Make sure proper version of python is loaded
module load compilers/python-2.7.13-gcc

# Make sure R is loaded
module load apps/R-3.5.1

# Output information about compute node
python "$SCRIPT_DIR/../cpu_info.py"

# Determine path to target local script
SH_SCRIPT_PATH="$SCRIPT_DIR/../tad_cmx_setup.sh"

# Timestamp script execution
SCRIPT_ID="$(date +%Y%m%d%H%M%S).$RANDOM"
# Run script
cmd="$SH_SCRIPT_PATH -cpu $NUM_CPU"
echo "$SCRIPT_ID : $(timestamp) : Running $cmd"
$cmd
echo "$SCRIPT_ID : $(timestamp) : Finished job"
