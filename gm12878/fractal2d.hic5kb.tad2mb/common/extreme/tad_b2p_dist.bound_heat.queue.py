#!/usr/bin/python
# -*- coding: utf-8 -*-

# Script replicates BIT2POL (B2P) DIST BOUND HEAT for each TAD model over extreme cluster
#
# Usage:
#
#   [-od|--odir]: Directory for storing qsub .o log files
#   [-mcn|--max_compute_nodes]: Maximum number of compute nodes to use
#   [-mfi|--model_filt_id]: [Optional] TAD region filter identifier
#   [-mp|--max_polls]: Max number of attempts to determine finished jobs
#   [-shs|--sh_script]: Path to shell script wrapper for launching B2P DIST
#   [-ssp|--sleep_secs_poll]: Polling interval to check for finished jobs in
#       seconds
#   [-ssx|--sleep_secs_ex]: Polling interval if command line output returns
#       unexpected code
#   --user: User identifier

###################################################
# Imports
###################################################

# For parsing user supplied arguments
import argparse

# For file path testing
import os

# For running shell commands
import subprocess

# For exiting if inputs invalid
import sys

# For sleeping until jobs finish
import time

# For determining user name
import getpass

# Path to directory containing this script
# https://stackoverflow.com/questions/4934806/how-can-i-find-scripts-directory-with-python
SCRIPT_DIR = os.path.abspath(os.path.dirname(os.path.realpath(__file__)))

# Import local 'tad_core' script
# https://stackoverflow.com/questions/4383571/importing-files-from-different-folder
LIB_PATH = os.path.abspath(os.path.join(SCRIPT_DIR, ".."))
sys.path.append(LIB_PATH)
import tad_core

###################################################
# Defaults
###################################################

# Default directory for storing qsub log files
DEFAULT_ODIR = os.path.join(SCRIPT_DIR, "..", "..", "output", "logs", "o.b2p_dist.bound_heat")

# Default path to shell script wrapper for launching B2P DIST HEAT
DEFAULT_SH_SCRIPT_PATH = os.path.join(SCRIPT_DIR, "tad_b2p_dist.bound_heat.run.sh")

# Default max number of compute nodes to use
DEFAULT_MAX_COMPUTE_NODES = 10

# Default TAD region filter
DEFAULT_MODEL_FILT_ID = ''

# Default polling interval in seconds
DEFAULT_SLEEP_SECS_POLL = 5

# Default polling interval after a return code exception
DEFAULT_SLEEP_SECS_EX = 30

# Default maximum number of attempts to determine finished jobs
DEFAULT_MAX_POLLS = 1000

###########################################
# Globals
###########################################

# Set of currently running job identifiers
JOB_QUEUE = set()

###########################################
# Methods
###########################################

# @param args - User arguments
# @return Arguments, some which may have been modified during validation
def check_args(args):
    """Validates user arguments, exits program if invalid."""
    # Performs cursory argument validation, by no means exhaustive.
    if (not os.path.isfile(args.sh_script)):
        print "Error, invalid shell script path: " + str(args.sh_script)
        sys.exit(1)
    args.sh_script = os.path.abspath(args.sh_script)
    args.max_compute_nodes = int(args.max_compute_nodes)
    args.sleep_secs_poll = float(args.sleep_secs_poll)
    args.sleep_secs_ex = float(args.sleep_secs_ex)
    args.max_polls = int(args.max_polls)
    if (args.max_compute_nodes < 1):
        print "Error, max compute nodes must be positive integer"
        sys.exit(1)
    if (args.sleep_secs_poll <= 0.0):
        print "Error, polling interval must be positive real"
        sys.exit(1)
    if (args.sleep_secs_ex <= 0.0):
        print "Error, exception polling interval must be positive real"
        sys.exit(1)
    if (args.max_polls <= 0):
        print "Error, poll attempts must be positive integer"
        sys.exit(1)
    if not args.user:
        print "Error, could not determine user name"
        sys.exit(1)
    if not args.odir:
        print "Error, need valid path to qsub log directory"
        sys.exit(1)
    return args

# @param cmd - Command line to obtain stdout
# @param args - User arguments
# @return stdout from 'cmd'
def get_stdout(cmd, args):
    '''Obtain command line standard output'''
    stdout = ""
    success = False
    # https://stackoverflow.com/questions/2083987/how-to-retry-after-exception-in-python
    # https://stackoverflow.com/questions/28675138/python-check-output-fails-with-exit-status-1-but-popen-works-for-same-command
    # https://www.tutorialspoint.com/python/python_exceptions.htm
    for attempt in xrange(0, args.max_polls):
        try:
            stdout = subprocess.check_output(cmd)
        except:
            time.sleep(args.sleep_secs_ex)
            continue
        else:
            success = True
            break
    if not success:
        print "Error, command failed: " + ' '.join(cmd)
        sys.exit(0)
    return stdout

# @param args - User arguments
# @return set of active job identifiers
def get_active_job_ids(args):
    '''Determines active job identifiers'''
    job_ids = set()
    cmd = ["showq", "-u", args.user]
    stdout = get_stdout(cmd, args)
    lines = stdout.splitlines()
    iterlines = iter(lines)
    for line in iterlines:
        # Check if line is a job listing
        if (args.user in line):
            # JOBID is first column, so split by white space
            JOBID = line.split()[0]
            JOBID = ''.join(c for c in JOBID if c.isdigit())
            job_ids.add(int(JOBID))
    return job_ids

# @param args - User arguments
# @param mix - Model index
# @param mfi - [Optional] Model filter identifier
def queue_job(args, mix, mfi):
    '''Calls qsub if compute nodes are available, else polls until free.'''
    global JOB_QUEUE
    # Check if job queue is full
    while (len(JOB_QUEUE) >= args.max_compute_nodes):
        # Wait for a bit before checking if jobs have completed
        time.sleep(args.sleep_secs_poll)
        JOB_QUEUE = JOB_QUEUE & get_active_job_ids(args)
    # If we reach here, then a compute node is available
    F_arg = '"' + str(mix)
    if mfi:
        F_arg = F_arg + ' ' + str(mfi)
    F_arg = F_arg + '"'
    cmd = ["qsub", "-F", F_arg, args.sh_script]
    print "Queuing cmd:\n\t" + " ".join(cmd)
    job = get_stdout(cmd, args)
    # job now equals: '<integer>.admin.uic.edu', we want the <integer> part:
    job = job.split('.')[0]
    JOB_QUEUE.add(int(job))

###################################################
# Queue BIT2POL DIST BOUND HEAT
###################################################

# @param args - User arguments
def queue_b2p_dist_bound_heat(args):
    '''Calls BIT2POL DIST BOUND HEAT at each TAD model'''
    # Validate arguments
    args = check_args(args)

    # Set working directory to qsub log directory so that qsub files are
    # stored within this directory
    tad_core.make_dir(args.odir)
    owd = os.getcwd()
    os.chdir(args.odir)

    # Base directory with unversioned TAD model data
    base_dir = tad_core.TAD_BASE_OUT_DIR
    # List of active TAD models
    # @WARNING - If '--from_table 1', then 'mid' many not match 'mix' in print
    model_names = tad_core.resolve_tad_model_names_filt_ex(model_filt_id=args.model_filt_id,
                                                           base_dir=base_dir)
    # Process each TAD model
    for mix, mid in enumerate(model_names):
        print '[tad_b2p_dist.bound_heat.queue] Queuing ' + str(mix) + ' : ' +  str(mid)
        queue_job(args=args, mix=mix, mfi=args.model_filt_id)

    # Restore old working directory
    os.chdir(owd)

###################################################
# Main
###################################################

# Main script entry point
def __main__():
    print "======================= tad_b2p_dist.bound_heat.queue ======================="
    # Initialize command line parser
    parser = argparse.ArgumentParser()
    parser.add_argument('-od', '--odir', default=DEFAULT_ODIR,
        help='Directory for storing qsub log files')
    parser.add_argument('-mcn', '--max_compute_nodes',
        default=DEFAULT_MAX_COMPUTE_NODES,
        help='Maximum number of compute nodes to use')
    parser.add_argument('-mfi', '--model_filt_id', default=DEFAULT_MODEL_FILT_ID,
        help='[Optional] TAD region filter')
    parser.add_argument('-mp', '--max_polls', default=DEFAULT_MAX_POLLS,
        help='Maximum number of attempts to check for job completion')
    parser.add_argument('-shs', '--sh_script', default=DEFAULT_SH_SCRIPT_PATH,
        help='Path to shell script for launching B2P DIST')
    parser.add_argument('-ssp', '--sleep_secs_poll',
        default=DEFAULT_SLEEP_SECS_POLL,
        help='Polling interval to check for job completion in seconds')
    parser.add_argument('-ssx', '--sleep_secs_ex',
        default=DEFAULT_SLEEP_SECS_EX,
        help='Polling interval to check for job completion after command line exception in seconds')
    parser.add_argument('-usr', '--user', default=getpass.getuser(),
        help='User identifier')
    # Parse command line
    args = parser.parse_args()
    tad_core.print_cmd(args)
    # Run BIT2POL DIST BOUND HEAT
    queue_b2p_dist_bound_heat(args)
    print "Finished."

# Run main if we are the active script
if __name__ == '__main__':
    __main__()
