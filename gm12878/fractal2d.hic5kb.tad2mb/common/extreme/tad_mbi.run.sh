#!/bin/bash  

#PBS -l walltime=75:00:00
#PBS -l nodes=1:ppn=20
#PBS -j oe  
#PBS -m n  
#PBS -M perezrat@uic.edu
#PBS -q jliang
#PBS -N centro

# Usage:
#   qsub -F "<integer>" <script_name.sh>
#
# The -F "<integer>" are the arguments to this script. This corresponds to a
# 0-based index into the list of the model identifiers returned by
# tad_core.resolve_tad_model_names(). THE QUOTES ARE MANDATORY. Example:
#
#   qsub -F "0" <script_name>.sh
#
# Above will generate a job for the 0-th (first) TAD model.
#
# ----------------------------------------------------------------------------
# From:
# http://stackoverflow.com/questions/26487658/does-qsub-pass-command-line-arguments-to-my-script
# https://stackoverflow.com/questions/3504081/parameter-for-shell-scripts-that-is-started-with-qsub
# http://docs.adaptivecomputing.com/suite/8-0/basic/help.htm#topics/torque/commands/qsub.htm#-F
# Specifies the arguments that will be passed to the job script when the
# script is launched. The accepted syntax is:
#   qsub -F "myarg1 myarg2 myarg3=myarg3value" myscript2.sh
#
# Quotation marks are required. qsub will fail with an error message if the
# argument following -F is not a quoted value. The pbs_mom server will pass
# the quoted value as arguments to the job script when it launches the script.
# ----------------------------------------------------------------------------
#
# Monitor using either:
#
#   qstat -u <netid>
# or
#   showq -u <netid>
#
# Delete using:
#   qdel <job_id_from_qstat>
# or to delete all active jobs:
#   qselect -u <netid> | xargs qdel

###################################################
# Timestamp utils
###################################################

# From http://stackoverflow.com/questions/17066250/create-timestamp-variable-in-bash-script
timestamp() {
  date +"%T"
}

###################################################
# Run
###################################################

# Determine path to script
HOME_DIR=$(echo ~)
WORK_DIR="$HOME_DIR/chr-working"
PROJ_DIR="$WORK_DIR/loci/mbi-mam"
SCRIPT_DIR="$PROJ_DIR/gm12878/fractal2d.hic5kb.tad2mb/common/extreme"

# Parse command line (non-optional argument) for model index
MIX=-1
if [ $# -gt 0 ]; then
    MIX="$1"
fi

# Validate argument(s)
if [ "$MIX" -lt "0" ]; then
    # http://stackoverflow.com/questions/4381618/exit-a-script-on-error
    # Redirect to stderror (file descriptor 2)
    echo "Error: Invalid model index: $MIX < 0. Exiting." 1>&2
    # Exit status 1 indicates error
    exit 1
fi

# Make sure proper version of python is loaded
module load compilers/python-2.7.13-gcc

# Make sure R is loaded
module load apps/R-3.5.1

# Output information about compute node
python "$SCRIPT_DIR/../cpu_info.py"

# Determine path to target local script
SH_SCRIPT_PATH="$SCRIPT_DIR/../tad_mbi.sh"

# Timestamp script execution
SCRIPT_ID="$(date +%Y%m%d%H%M%S).$RANDOM"
# Run script
cmd="$SH_SCRIPT_PATH $MIX"
echo "$SCRIPT_ID : $(timestamp) : Running $cmd"
$cmd
echo "$SCRIPT_ID : $(timestamp) : Finished job"
