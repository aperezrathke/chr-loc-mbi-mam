#!/bin/bash  

#PBS -l walltime=75:00:00
#PBS -l nodes=1:ppn=20
#PBS -j oe  
#PBS -m n  
#PBS -M perezrat@uic.edu
#PBS -q jliang
#PBS -N centro

# Usage:
#   qsub -F "-mix <integer> -kix <integer>" <script_name.sh>
#
# The -F "-mix <integer> -kix <integer>" are the arguments to this script. For
# perturbation modeling, '-mix' corresponds to a 0-based index into the list
# of the model identifiers returned by tad_core.resolve_tad_model_names(); the
# argument 'kix' corresponds to the 'knock-in' interaction index to perturb at
# the corresponding 'mix' model. THE QUOTES ARE MANDATORY. Example:
#
#   qsub -F "-mix 0 -kix 0" <script_name>.sh
#
# Above will generate a job for pertubing the 0-th  (first)interaction at the
#   0-th (first) TAD model
#
# ----------------------------------------------------------------------------
# From:
# http://stackoverflow.com/questions/26487658/does-qsub-pass-command-line-arguments-to-my-script
# https://stackoverflow.com/questions/3504081/parameter-for-shell-scripts-that-is-started-with-qsub
# http://docs.adaptivecomputing.com/suite/8-0/basic/help.htm#topics/torque/commands/qsub.htm#-F
# Specifies the arguments that will be passed to the job script when the
# script is launched. The accepted syntax is:
#   qsub -F "myarg1 myarg2 myarg3=myarg3value" myscript2.sh
#
# Quotation marks are required. qsub will fail with an error message if the
# argument following -F is not a quoted value. The pbs_mom server will pass
# the quoted value as arguments to the job script when it launches the script.
# ----------------------------------------------------------------------------
#
# Monitor using either:
#
#   qstat -u <netid>
# or
#   showq -u <netid>
#
# Delete using:
#   qdel <job_id_from_qstat>
# or to delete all active jobs:
#   qselect -u <netid> | xargs qdel

###################################################
# Timestamp utils
###################################################

# From http://stackoverflow.com/questions/17066250/create-timestamp-variable-in-bash-script
timestamp() {
  date +"%T"
}

###################################################
# Run
###################################################

# Determine path to script
HOME_DIR=$(echo ~)
WORK_DIR="$HOME_DIR/chr-working"
PROJ_DIR="$WORK_DIR/loci/mbi-mam"
SCRIPT_DIR="$PROJ_DIR/gm12878/fractal2d.hic5kb.tad2mb/common/extreme"

# Max local CPU cores to use (make sure matches PBS ppn)
MAX_PROC=20

# Parse command line (non-optional arguments) for model and perturb index
MIX=-1
KIX=-1
# @HACK - Command line is passed as single quoted argument, must break up
#   manually using whitespace
MIX_IS_NEXT=false
KIX_IS_NEXT=false
for arg in $@
do
    case "$arg" in
    -mix)
        MIX_IS_NEXT=true
        KIX_IS_NEXT=false
        ;;
    -kix)
        KIX_IS_NEXT=true
        MIX_IS_NEXT=false
        ;;
    *)
        if [ "$MIX_IS_NEXT" = true ]; then
            MIX="$arg"
        elif [ "$KIX_IS_NEXT" = true ]; then
            KIX="$arg"
        fi
        ;;
    esac
    shift
done

# Validate argument(s)
if [ "$MIX" -lt "0" ]; then
    # http://stackoverflow.com/questions/4381618/exit-a-script-on-error
    # Redirect to stderror (file descriptor 2)
    echo "Error: Invalid model index: $MIX < 0. Exiting." 1>&2
    # Exit status 1 indicates error
    exit 1
fi

# Validate argument(s)
if [ "$KIX" -lt "0" ]; then
    # http://stackoverflow.com/questions/4381618/exit-a-script-on-error
    # Redirect to stderror (file descriptor 2)
    echo "Error: Invalid perturbation index: $KIX < 0. Exiting." 1>&2
    # Exit status 1 indicates error
    exit 1
fi

# Make sure proper version of python is loaded
module load compilers/python-2.7.13-gcc

# Make sure R is loaded
module load apps/R-3.5.1

# Output information about compute node
python "$SCRIPT_DIR/cpu_info.py"

# Determine path to target local script
PY_SCRIPT_PATH="$SCRIPT_DIR/../tad_perturb.py"

# Timestamp script execution
SCRIPT_ID="$(date +%Y%m%d%H%M%S).$RANDOM"


# Default TAD identifier resolution:
#   0|None - From existing TAD directories
#   1 - From scale table data.frame
#   <path> - From custom table data.frame
FROM_TABLE="1"

# Run script
cmd="python $PY_SCRIPT_PATH --model_index $MIX --knock_in_index $KIX"
cmd="$cmd --max_proc $MAX_PROC --from_table $FROM_TABLE"
echo "$SCRIPT_ID : $(timestamp) : Running $cmd"
$cmd
echo "$SCRIPT_ID : $(timestamp) : Finished job"
